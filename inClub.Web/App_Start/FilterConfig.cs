﻿using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.ActionFilters;

namespace ServiceCenter
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            var privateSiteAccess = new PrivateSiteAccessActionFilterAttribute();
            filters.Add(privateSiteAccess);
        }
    }
}