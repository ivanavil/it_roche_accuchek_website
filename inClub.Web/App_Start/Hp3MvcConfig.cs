﻿using System.Linq;
using System.Web.Mvc;

namespace ServiceCenter.App_Start
{
    public static class Hp3MvcConfig
    {
        public static void Register()
        {
            #region View Engine

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new WebFormViewEngine());

            var engine = new Healthware.HP3.MVC.SiteViewEngine();

            var locs = engine.PartialViewLocationFormats.ToList();
            locs.Add("~/{2}/Views/Templates/{0}.cshtml");
            engine.PartialViewLocationFormats = locs.ToArray();

            locs = engine.ViewLocationFormats.ToList();
            locs.Add("~/{2}/Views/Templates/{0}.cshtml");
            engine.ViewLocationFormats = locs.ToArray();

            ViewEngines.Engines.Add(engine);

            #endregion View Engine
        }
    }
}
