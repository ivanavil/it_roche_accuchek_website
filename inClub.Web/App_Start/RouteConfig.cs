﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Healthware.HP3.MVC;
using Inclub.Web.Portal;

namespace Inclub.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {


            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            var exts = new string[] { "aspx", "htm", "html", "css", "js", "ico" };
            foreach (var ext in exts)
            {
                var regEx = string.Format(@".*\.{0}(/.*)?", ext);
                routes.IgnoreRoute("{*all}", new { all = regEx });
            }

            routes.RegisterRouteConfigurations(RouteConfigurations.Configurations);
        }
    }
}