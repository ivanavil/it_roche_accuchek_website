﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using Healthware.HP3.Common.Extensions;

namespace ServiceCenter.App_Start
{
    public class BundleConfig : Bundle
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
          
            #region Script

            var scriptBundle = new ScriptBundle("~/Scripts/js");
           // scriptBundle.Include("~/Portal/Content/_js/jquery.js");
           // scriptBundle.Include("~/Portal/Content/_js/jquery-ui.js");
           // scriptBundle.Include("~/Portal/Content/_js/jquery.ui.datepicker-it.js");
            scriptBundle.Include("~/Portal/Content/js/jquery-3.2.1.min.js");
            scriptBundle.Include("~/Portal/Content/js/swiper.min.js");
            scriptBundle.Include("~/Portal/Content/js/main.js");
             scriptBundle.Include("~/Portal/Content/js/ColorBox/jquery.colorbox.js");
      
            bundles.Add(scriptBundle);

            //var mainBundle = new ScriptBundle("~/Scripts/Mainjs");
            //mainBundle.Include("~/Portal/Content/js/main.js");
            //bundles.Add(mainBundle);

            bundles.Add(new ScriptBundle("~/bundles/validation").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));
            #endregion Script

            
            #region Style           

            var styleBundle = new StyleBundle("~/Style/css");
            //styleBundle.Include("~/Portal/Content/_css/jquery-ui.css");           
            //styleBundle.Include("~/Portal/Content/_css/responsive.css");
            styleBundle.Include("~/Portal/Content/_css/swiper.min.css");
            styleBundle.Include("~/Portal/Content/_css/font-awesome.min.css");
            styleBundle.Include("~/Portal/Content/js/ColorBox/colorbox.css");
            styleBundle.Include("~/Portal/Content/_css/style.css");

            bundles.Add(styleBundle);
          

            #endregion Style


            BundleTable.EnableOptimizations = System.Configuration.ConfigurationManager.AppSettings.GetBool("enableBundles");
        }
    }
}
