﻿using System.Web.Mvc;
using System.Web.Routing;
using ServiceCenter.App_Start;
using System.Web.Optimization;
using System.Web;
using System.Web.Http;
using System.Linq;
using System;
using System.Collections.Generic;
using ServiceCenter;
using Inclub.Web;


namespace Inclub.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Hp3MvcConfig.Register();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        /// <summary>
        /// Redirects with a 301 header to pass along any incoming
        /// PageRank/link value.
        /// </summary>
        /// <param name="url">The URL to redirect to</param>
        private void PermanentRedirect(string url)
        {
            Response.Clear();
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", url);
            Response.End();
        }
        protected void Application_BeginRequest(Object source, EventArgs e)
        {


            if (!HttpContext.Current.Request.Url.Authority.Contains("admin.accu-chekinclub.it") && !HttpContext.Current.Request.Url.Authority.Contains("healthwareinternational") && !HttpContext.Current.Request.Url.Authority.Contains("localhost"))
            {
                if (!HttpContext.Current.Request.Url.Authority.Contains("www"))
                {
                    dynamic urlHttps = "https://www." + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.RawUrl;
                    HttpContext.Current.Response.Redirect(urlHttps);
                }
            }

            System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string> { ".jpg", ".png", ".jpeg", ".gif", ".bmp", ".pdf", ".htm", ".html", ".xml", ".doc", ".docx", ".css", ".thmx" };
            // Get the requested URL so we can do some validation on it.
            // We exclude the query string, and add that later, so it's not included
            // in the validation
            string url = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath);


            ////DC gestione HOME (IT/EN) SITO MULTILINGUA
            //if (HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/it") || HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/it/") || (HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/it/home-it"))) 
            //{
            //    string redirectUrl = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/");
            //    PermanentRedirect(redirectUrl);
            //}
            //if (HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/en") ||HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/en/") )
            //{
            //    string redirectUrl = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/en/home");
            //    PermanentRedirect(redirectUrl);
            //}




            if (Healthware.HP3.Core.Web.MasterPageManager.UseDriver("ShortUrlDriver"))
            {
                Healthware.HP3.Core.Web.MasterPageManager man = new Healthware.HP3.Core.Web.MasterPageManager();






                if (!HttpContext.Current.Request.Url.Authority.Contains("admin.accu-chekinclub.it") && HttpContext.Current.Request.Url.Scheme != "https" && !HttpContext.Current.Request.Url.Authority.Contains("healthwareinternational") && !HttpContext.Current.Request.Url.Authority.Contains("localhost"))
                {
                    dynamic urlHttps = "https://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.RawUrl;
                    HttpContext.Current.Response.Redirect(urlHttps);
                }

            }




            // If we're not a request for the root, and end with a slash, strip it off
            if (HttpContext.Current.Request.Url.AbsolutePath != "/" && HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/"))
                PermanentRedirect(url.Substring(0, url.Length - 1) + HttpContext.Current.Request.Url.Query);

            // If we end with /1 we're a page 1, and don't need (shouldn't have) the page number
            //else if (HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/1"))
            //    PermanentRedirect(url.Substring(0, url.Length - 2) + HttpContext.Current.Request.Url.Query);

            // If we have double-slashes, strip them out
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("//"))
                PermanentRedirect(url.Replace("//", "/") + HttpContext.Current.Request.Url.Query);

            //// If we've got uppercase characters, fix
            //else if (System.Text.RegularExpressions.Regex.IsMatch(url, @"[A-Z]"))
            //    PermanentRedirect(url.ToLower() + HttpContext.Current.Request.Url.Query);


            var httpContext = HttpContext.Current;

            string cultureName = "it-IT";
            //Set Culture
            System.Globalization.CultureInfo objCultureInfo = new System.Globalization.CultureInfo(cultureName);
            System.Threading.Thread.CurrentThread.CurrentCulture = objCultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = objCultureInfo;

            FirstRequestInitialisation.Initialise(httpContext);
        }

        class FirstRequestInitialisation
        {
            public static void Initialise(HttpContext context)
            {
                Uri uri = HttpContext.Current.Request.Url;
                if (uri.ToString().IndexOf("trials") > 0 && uri.ToString().IndexOf("www") < 0 && uri.ToString().IndexOf("admin.trials") < 0 && uri.ToString().IndexOf("admin.trials") < 0)
                {
                    //TODO Remove comment
                    //context.Response.Redirect(String.Format("{0}{1}{2}{3}", uri.Scheme, Uri.SchemeDelimiter, "www.", uri.Host));
                }
                //else { context.Response.Redirect(String.Format("{0}{1}{2}{3}", uri.Scheme, Uri.SchemeDelimiter, "", uri.Host)); }

            }
        }
    }
}