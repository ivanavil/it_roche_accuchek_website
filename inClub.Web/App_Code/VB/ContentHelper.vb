﻿Imports Microsoft.VisualBasic
Imports Healthware.HP3.Core.Content
Imports Healthware.HP3.Core.Content.ObjectValues
Imports Healthware.HP3.Core.Base.ObjectValues

Namespace HP3Office.CMS.Utility
    Public Class ContentHelper
        Public Shared Function GetContentIdentificator(ByVal pId As Integer, ByVal pIdLanguage As Integer) As ContentIdentificator
            If pId = 0 Then Return Nothing
            Dim _oContentManager As New ContentManager()

            Dim so As New ContentSearcher

            so.key.Id = pId
            so.key.IdLanguage = pIdLanguage

            so.Properties.Add("Key.PrimaryKey")
            so.Properties.Add("Key.Id")
            so.Properties.Add("Key.IdLanguage")

            ' so.Delete = SelectOperation.All
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All

            _oContentManager.Cache = False
            Dim oContentCollection As ContentCollection = _oContentManager.Read(so)
            If Not oContentCollection Is Nothing AndAlso oContentCollection.Any Then
                Return oContentCollection(0).Key
            End If
            Return Nothing

        End Function
        'M1149
        Public Shared Function GetContentIdentificator(ByVal primaryKey As Integer) As ContentIdentificator
            If primaryKey = 0 Then Return Nothing
            Dim _oContentManager As New ContentManager()

            Dim so As New ContentSearcher

            so.key.PrimaryKey = primaryKey

            so.Properties.Add("Key.PrimaryKey")
            so.Properties.Add("Key.Id")
            so.Properties.Add("Key.IdLanguage")

            ' so.Delete = SelectOperation.All
            so.Active = SelectOperation.All
            so.Display = SelectOperation.All

            _oContentManager.Cache = False
            Dim oContentCollection As ContentCollection = _oContentManager.Read(so)
            If Not oContentCollection Is Nothing AndAlso oContentCollection.Any Then
                Return oContentCollection(0).Key
            End If
            Return Nothing

        End Function
    End Class
End Namespace

