﻿using Healthware.HP3.Core.Web.ObjectValues;
using Inclub.Web.Portal.ActionFilterAttributes;
using Inclub.Web.Portal.AuthorizedAttributes;
using Inclub.Web.Portal.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Controllers
{
    public class PrivateAreaController : Controller
    {
        // GET: PrivateArea
        [LoggedOrAuthorizedAttribute(View = "Index", Master="PrivateArea")]
        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //trace
           // TraceHelper.TraceEvent(pageInfo.Key); 

            return View();
        }
    }

       
}