﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.Utility;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Healthware.HP3.Core.Site.ObjectValues;
using inClub.Web.Portal.Helper;
using Inclub.Web.Portal.Keys;

namespace inClub.Web.Portal.Controllers
{
    public class DocumentiController : Controller
    {
        //
        // GET: /Documenti/

        public ActionResult Index(int groupId, short langId)
        {
            #region Page Info
            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;
            this.ViewBag.groupId = groupId;
            this.ViewBag.langId = langId;
            this.ViewBag.groupIsOpen = DocumentiHelper.groupIsOpen(groupId, langId);

            string role = "member";
            var roleMng = new ProfilingManager();
            bool isAdmin = false;

            //controllo se l'utente loggato è sysadmin
            var objContentManager = new ContentManager();
            UserBaseValue user = new UserBaseValue();
            user = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
            isAdmin = roleMng.HasRole(user.Key, Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

            //se non lo è controllo se può accedere al gruppo, e leggo il ruolo che ha sul gruppo della discussione
            if (!isAdmin)
            {
                ContentUserCollection cuc = new ContentUserCollection();
                ContentUserSearcher oContUsSo = new ContentUserSearcher();
                oContUsSo.KeyUser.Id = user.Key.Id;
                oContUsSo.KeyContent.Id = groupId;
                oContUsSo.KeyContent.IdLanguage = langId;

                cuc = objContentManager.ReadContentUser(oContUsSo);

                if (cuc != null && cuc.Any())
                {
                    if (cuc[0].KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin)
                    {
                        role = "admin";
                    }

                }
                else
                {
                    //se non è associato al gruppo reindirizzo alla pagina dell'elenco dei gruppi
                    return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
                }
            }
            else
            {
                role = "admin";
            }

            this.ViewBag.role = role;


            #endregion Page Info

            return PartialView("Index"); 
        }

        public ActionResult Archive(int groupId, short langId, int folderId, int onlyResources, int pagenum, int pagesize, int commentid = 0, string viewName = "", string docid = "0")
        {
            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            int selDoc = 0;

            this.ViewBag.groupId = groupId;
            this.ViewBag.folderId = folderId;

            if ((docid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(docid), out selDoc))){
                this.ViewBag.selDocID = selDoc;
            }                
            else
            {
                this.ViewBag.selDocID = 0;
            }
                

            if (groupId <= 0)
            {
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

            var objContentManager = new ContentManager();
            

            if (viewName == "")
                viewName = "_ItemDocumenti";
            
            ContentSearcher so = new ContentSearcher();
                
                so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                so.Properties.Add("Title");
                so.Properties.Add("Copyright");
                so.Properties.Add("Code");
                so.Properties.Add("Launch");
                so.Properties.Add("DatePublish");
                so.Properties.Add("Owner.Key.Id");
                so.Properties.Add("Owner.Name");
                so.Properties.Add("Owner.Surname");
                so.Properties.Add("Owner.Title");
                so.Properties.Add("SiteArea.Key.Id");
                so.Properties.Add("ContentType.Key.Id");
                so.Properties.Add("Qty");
                so.Properties.Add("IdContentSubCategory");

                so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id;

                if (selDoc != 0)
                    so.key = new ContentIdentificator(selDoc, langId);
                else
                    so.Code = folderId.ToString();


                so.KeysContentType = Inclub.Web.Portal.Keys.ContentTypeKeys.Documenti.Id + (onlyResources == 0 ? "," + Inclub.Web.Portal.Keys.ContentTypeKeys.DocFolder.Id : "");

                so.PageNumber = pagenum;
                so.PageSize = pagesize;
                
                so.SortingCriteria.Add(new ContentSort(1, ContentGenericComparer.SortType.ByPriorityRel));
                so.SortingCriteria.Add(new ContentSort(2, ContentGenericComparer.SortType.ByData, ContentGenericComparer.SortOrder.DESC));

                if (folderId > 0)
                {
                    so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(folderId, langId);
                    so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources; 
                }else{
                    so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
                    so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources; 
                }
                              


                so.LoadOwnerDetails = true;
                so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
                so.Delete = SelectOperation.Enabled();

                objContentManager.Cache = false;
                ContentCollection materials = objContentManager.Read(so);

                List<inClub.Web.Portal.Models.Documenti> docObj = new List<inClub.Web.Portal.Models.Documenti>();
                inClub.Web.Portal.Models.Documenti material = null;
                var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Documenti>();


                //System.Web.HttpContext.Current.Response.Write("selDoc:" + selDoc);

                if (materials != null && materials.Any())
                {
                    modelColl.CurrentPage = pagenum;
                    modelColl.TotalPagesCount = materials.PagerTotalNumber;
                    modelColl.TotalItemsCount = materials.PagerTotalCount;

                    foreach (ContentValue m in materials){
                        material = Inclub.Web.Portal.Helper.DocumentiHelper.GetResource(m, m.Owner, groupId, commentid);

                        if (selDoc != 0 && material.FolderID > 0)
                        {
                            material.Folder = objContentManager.Read(new ContentIdentificator(material.FolderID, langId)).Title;
                        }

                        docObj.Add(material);
                    }

                    modelColl.Items = docObj;

                    return PartialView(viewName, modelColl);
                }


                return PartialView(viewName, null);
        }

        public PartialViewResult ViewVideo()
        {
            inClub.Web.Portal.Models.ShowVideo model = new inClub.Web.Portal.Models.ShowVideo();
            ContentManager objContentManager = new ContentManager();
            AccessService objAccess = new AccessService();

            int dwnlID = 0;
            short langID = 0;
            string codeFile = "";
            codeFile = Request["intPk"].ToString();
            model.codeFile = codeFile;
            int intResourcePk = 0;
            int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(codeFile), out intResourcePk);
            
            if ((intResourcePk != 0) && (objAccess.IsAuthenticated()))
            {
                DownloadManager oDownloadManager = new DownloadManager();
                DownloadSearcher oDSrc = new DownloadSearcher();
                oDSrc.key = new ContentIdentificator(intResourcePk);
                DownloadCollection oDownloadColl = oDownloadManager.Read(oDSrc);

                if ((oDownloadColl != null) && (oDownloadColl.Count > 0))
                {
                    DownloadValue oDownloadVal = new DownloadValue();
                    oDownloadVal = oDownloadColl[0];

                    dwnlID = oDownloadVal.Key.Id;
                    langID = oDownloadVal.Key.IdLanguage;

                    model.Name = oDownloadVal.Title;
                    model.Description = oDownloadVal.Launch;
                    if (oDownloadVal.DownloadTypeCollection != null && oDownloadVal.DownloadTypeCollection.Count > 0)
                    {
                        DownloadTypeValue dwnType = oDownloadVal.DownloadTypeCollection[0];
                        model.Ext = dwnType.DownloadType.Label;
                        model.fileName = oDownloadVal.Key.PrimaryKey + "." + model.Ext;
                    }

                    objContentManager.Cache = false;

                    //Check sull'accoppiamente risorsa-gruppo
                    ContentRelationSearcher _so = new ContentRelationSearcher();
                   
                    _so.KeyContentRelated.PrimaryKey = intResourcePk;
                    _so.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources;
                    _so.ActiveContentRelated = SelectOperation.All();
                    _so.ActiveContent = SelectOperation.All();

                    ContentRelationCollection _oColl = null;
                    _oColl = objContentManager.ReadContentRelation(_so);

                    int _intGroupId = 0;
                    if ((_oColl != null) && (_oColl.Count > 0))
                    {
                        _intGroupId = _oColl[0].KeyContent.Id;
                    }
                    else
                    {
                        return PartialView("_ViewVideo", null);
                    }

                    _oColl = null;
                    _so = null;


                    UserBaseValue userLogged = new UserBaseValue();
                    userLogged = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());

                    var roleMng = new ProfilingManager();

                    //Check sull'accoppiamente utente-gruppo
                    if (roleMng.HasRole(userLogged.Key, Inclub.Web.Portal.Keys.RoleKeys.SysAdmin))
                    {
                        //Trace-------------------------------------------------
                        TraceObject objTrace = new TraceObject();
                        var _with6 = objTrace;
                        _with6.Content = null;
                        _with6.ContentId = dwnlID;
                        _with6.Description = "Visualizzazione video";
                        _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.ViewVideo.Id;
                        _with6.LanguageId = langID;
                        _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                        _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                        _with6.UserId = userLogged.Key.Id;
                        TraceHelper.TraceEvent(objTrace);
                        //------------------------------------------------------
                        return PartialView("_ViewVideo", model);
                    }
                        

                    ContentUserSearcher _usSo = new ContentUserSearcher();
                    var _with4 = _usSo;
                    _with4.KeyContent.Id = _intGroupId;
                    _with4.KeyUser.Id = userLogged.Key.Id;
                    _with4.Order = 1;
                    objContentManager.Cache = false;
                    ContentUserCollection oColl = null;
                    oColl = objContentManager.ReadContentUser(_usSo);
                    if ((oColl != null) && (oColl.Count > 0))
                    {
                        //Trace-------------------------------------------------
                        TraceObject objTrace = new TraceObject();
                        var _with6 = objTrace;
                        _with6.Content = null;
                        _with6.ContentId = dwnlID;
                        _with6.Description = "Visualizzazione video";
                        _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.ViewVideo.Id;
                        _with6.LanguageId = langID;
                        _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                        _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                        _with6.UserId = userLogged.Key.Id;
                        TraceHelper.TraceEvent(objTrace);
                        //------------------------------------------------------
                        return PartialView("_ViewVideo", model);
                    }
                    else
                    {
                        return PartialView("_ViewVideo", null); 
                    }

                }
                else
                {
                    return PartialView("_ViewVideo", null);
                }

                //return PartialView("_ViewVideo", model);
            }
            else
            {
                return PartialView("_ViewVideo", null);
            }

            
        }

        public PartialViewResult AggiungiDocumento()
        {
            inClub.Web.Portal.Models.AddDocumento model = new inClub.Web.Portal.Models.AddDocumento();

            
            int groupID = 0;
            int.TryParse(Request["gId"].ToString(), out groupID);
            int langID = 0;
            int.TryParse(Request["lId"].ToString(), out langID);            
            string role = "";
            role = Request["r"].ToString();
            int groupIsOpen = 0;            
            int.TryParse(Request["groupIsOpen"].ToString(), out groupIsOpen); 

            model.groupID = groupID;
            model.langID = (short)langID;            
            model.role = role;
            model.groupIsOpen = groupIsOpen;
            InitViewData(ref model);
            return PartialView("_AggiungiDocumento", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiungiDocumento(inClub.Web.Portal.Models.AddDocumento model)
        {
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            bool checkCreate = false;

            

            if (model.Title == null)
            {
                return Content("Inserire il Nome del documento!", "text/html");
            }
            else if (model.Description == null)
            {
                return Content("Inserire la descrizione!", "text/html");
            }
            else
            {

                if (ModelState.IsValid)
                {
                    checkCreate = CreaDocumento(model);

                    if (checkCreate)
                    {
                        if ((model.role.ToLower() == "member") && (model.groupIsOpen == 0))
                        {
                            return Content("Il Documento è stato caricato correttamente, sarà visibile all'interno del gruppo non appena approvato.", "text/html"); 
                        }
                        else
                        {
                            return Content("Documento caricato correttamente", "text/html");
                        }
                    }
                    else
                    {
                        return Content("Si è verificato un'errore nella creazione del documento", "text/html");
                    }

                }
                else
                {
                    return Content("Si è verificato un'errore nella creazione del documento", "text/html");
                }
            }
        }

        private void InitViewData(ref inClub.Web.Portal.Models.AddDocumento model)
        {

            List<SelectListItem> discItems = GetDiscussions(model.groupID, model.langID, model.role, model.groupIsOpen);
            if (discItems != null)
            { 
            model.Discussion = new SelectList(discItems, "Value", "Text");

            List<SelectListItem> typeItems = new List<SelectListItem>();
            SelectListItem firstItem = new SelectListItem();
            firstItem.Value = "1";
            firstItem.Text = string.Format("{0}", "Documento");

            SelectListItem secondItem = new SelectListItem();
            secondItem.Value = "2";
            secondItem.Text = string.Format("{0}", "Video");

            typeItems.Insert(0, firstItem);
            typeItems.Insert(1, secondItem);

            model.TypeDoc = new SelectList(typeItems, "Value", "Text");

            }
            
        }

        public static List<SelectListItem> GetDiscussions(int groupId, int langId, string role, int groupIsOpen)
        {
            List<SelectListItem> temp = null;

           
         
         

            ContentSearcher so = new ContentSearcher();
            ContentManager objContentManager = new ContentManager();

            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Title");
            so.Properties.Add("Key");
            so.Properties.Add("Code");
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;
            so.PageNumber = 0;
            so.PageSize = 0;
            so.SortingCriteria.Add(new ContentSort(1, ContentGenericComparer.SortType.ByTitle));
            so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
            so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;            
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
            so.Delete = SelectOperation.Enabled();

            ContentCollection coll = objContentManager.Read(so);
                        
            if (coll != null && coll.Any())
            {
                temp = coll.OrderBy(item => item.Title)
                        .Select(item =>
                            new SelectListItem
                            {
                                Text = item.Title,
                                Value = item.Code
                            }).ToList();


                SelectListItem firstItem = new SelectListItem();
                firstItem.Value = "0";
                firstItem.Text = string.Format("- {0} -", "Seleziona una discussione");
                temp.Insert(0, firstItem);
                SelectListItem newItem = new SelectListItem();
                newItem.Value = "";
                newItem.Text = string.Format("{0}", "--- Crea una nuova Discussione ---");

                if ((role.ToLower() != "member") || (groupIsOpen == 1))
                    temp.Insert(1, newItem);
              
                
               
            }

            else
            {

                temp = new List<SelectListItem>();

                SelectListItem firstItem = new SelectListItem();
                firstItem.Value = "0";
                firstItem.Text = string.Format("- {0} -", "Seleziona una discussione");

                temp.Insert(0, new SelectListItem { Value = firstItem.Value, Text = firstItem.Text });

               

                SelectListItem newItem = new SelectListItem();
                newItem.Value = "";
                newItem.Text = string.Format("{0}", "--- Crea una nuova Discussione ---");

                if ((role.ToLower() != "member") || (groupIsOpen == 1))
                    temp.Insert(1, new SelectListItem { Value = newItem.Value, Text = newItem.Text });

         
            }

            

            //clear
            objContentManager = null;
            coll = null;

            return temp;
        }



        private bool CreaDocumento(inClub.Web.Portal.Models.AddDocumento doc)
        {
            ContentValue newDisc = null;
            if ((doc.SelectedDiscId == null) || (doc.SelectedDiscId == ""))
            {
                Models.AddDiscussione discussion = new Models.AddDiscussione();

                discussion.Title = doc.TitleNewDiscussion;
                discussion.Description = doc.DescriptionNewDiscussion;
                discussion.groupID = doc.groupID;
                discussion.langID = doc.langID;

                newDisc = Controllers.DiscussioniController.CreaDiscussione(discussion);

                doc.SelectedDiscId = newDisc.Code;
            }

            Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
            ContentManager objcntMan = new ContentManager();
            ContentValue groupVal = new ContentValue();

            DownloadManager dwnlMng = new DownloadManager();
            System.IO.FileInfo oFileInfo = null;
            oFileInfo = new System.IO.FileInfo(Path.Combine(Server.MapPath(Inclub.Web.Portal.Keys.LabelKeys.TEMP_DOWNLOAD_PATH), doc.tempFile));
            string extension = string.Empty;
            string FolderName = string.Empty;
            string targetFileName = string.Empty;

            ProfilingManager roleMng = new ProfilingManager();
            string _notifyResName = string.Empty;
            int _notifyResId = 0;
            UserBaseValue owner = new UserBaseValue();
            owner = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
          

            int approval = (doc.role.ToLower() == "member" ? 0 : 1);
            approval = (doc.groupIsOpen == 1 ? 1 : approval);

            DownloadValue dwnVal = new DownloadValue();
            //oFileInfo = new System.IO.FileInfo(Inclub.Web.Portal.Keys.LabelKeys.TEMP_DOWNLOAD_PATH + doc.tempFile);

            dwnVal.Title = doc.Title.Trim();
            dwnVal.Launch = doc.Description.Trim();
            dwnVal.Active =  true;
            dwnVal.ApprovalStatus = (approval == 1 ? ContentBaseValue.ApprovalWFStatus.Approved : ContentBaseValue.ApprovalWFStatus.ToBeApproved);
            dwnVal.Display = true;
            dwnVal.DateCreate = System.DateTime.Now;
            dwnVal.DateInsert = System.DateTime.Now;
            dwnVal.DatePublish = System.DateTime.Now;
            dwnVal.DateUpdate = System.DateTime.Now;
            dwnVal.Code = doc.SelectedDiscId;
            //dwnVal.Copyright = ddlCartella.SelectedItem.Text;
            dwnVal.Key.IdLanguage = doc.langID;
            dwnVal.ContentType.Key.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Documenti.Id;
            dwnVal.IdContentSubCategory = doc.SelectedTypeDoc; // 1  = documento  2 = video
            dwnVal.Indexable = true;
            dwnVal.Owner.Key.Id = owner.Key.Id;

            DownloadValue newDownVal = new DownloadValue();
            newDownVal = dwnlMng.Create(dwnVal);
            if ((newDownVal != null) && newDownVal.Key.Id > 0)
            {
               
                _notifyResId = newDownVal.Key.Id;
                _notifyResName = newDownVal.Title;

                SaveDownloadType(newDownVal.Key.PrimaryKey, oFileInfo.Extension.Replace(".", ""), oFileInfo.Length);

                //Accoppiamento sitearea Documenti------------------------
                ContentSiteAreaValue oCoSaVal = new ContentSiteAreaValue();
                var _with3 = oCoSaVal;
                _with3.KeyContent = newDownVal.Key;
                _with3.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id);
                objcntMan.CreateContentSiteArea(oCoSaVal);
                //-------------------------------------------------------

                //Accoppiamento sito-----------------------------------
                oCoSaVal.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                objcntMan.CreateContentSiteArea(oCoSaVal);
                oCoSaVal = null;

                int folderId = 0;
                int.TryParse(dwnVal.Code, out folderId);

                if (folderId > 0)
                {
                    FolderName = (objcntMan.Read(new ContentIdentificator(folderId, doc.langID))).Title;
                }
                
                groupVal = objcntMan.Read(doc.groupID, doc.langID);

                //Accoppiamento col gruppo
                ContentRelationValue oContRelValue = new ContentRelationValue();
                var _with4 = oContRelValue;
                _with4.KeyContent.PrimaryKey = groupVal.Key.PrimaryKey;                
                _with4.KeyContentRelation = newDownVal.Key;
                _with4.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources;
                objcntMan.CreateContentRelation(oContRelValue);

                

                int selectDF = 0;
                int.TryParse(doc.SelectedDiscId, out selectDF);
                if ((selectDF != 0))
                {
                    int selectDF_PK =  objcntMan.ReadPrimaryKey(selectDF, doc.langID);

                    //Accoppiamento con la folder
                    oContRelValue = null;
                    oContRelValue = new ContentRelationValue();
                    var _with5 = oContRelValue;
                    _with5.KeyContent.PrimaryKey = selectDF_PK;
                    _with5.KeyContentRelation = newDownVal.Key;
                    _with5.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.ResourceFolder;
                    objcntMan.CreateContentRelation(oContRelValue);

                    if (newDownVal.ApprovalStatus == ContentBaseValue.ApprovalWFStatus.Approved)
                    {
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino + 1) where contents_key = " + selectDF_PK, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;
                    }
                }

                targetFileName = newDownVal.Key.PrimaryKey.ToString();
                
                //Trace-------------------------------------------------
                TraceObject objTrace = new TraceObject();
                var _with6 = objTrace;
                _with6.Content = null;
                _with6.ContentId = newDownVal.Key.Id;
                _with6.Description = "Nuovo documento";
                _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.AddDocumento.Id;
                _with6.LanguageId = newDownVal.Key.IdLanguage;
                _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                _with6.UserId = owner.Key.Id;
                TraceHelper.TraceEvent(objTrace);
                //------------------------------------------------------
                               
            

            FileHelper.Move(Inclub.Web.Portal.Keys.LabelKeys.TEMP_DOWNLOAD_PATH, Inclub.Web.Portal.Keys.LabelKeys.DOWNLOAD_PATH, doc.tempFile, targetFileName);

            dwnlMng.Cache = false;
            //TODO: invio notifica
            
            NotificationHelper oNotificationHelper = new NotificationHelper();
            NotificationData oNotData = new NotificationData();
                        
                oNotData.Notification = notificationAction.NewAddedResource; 
                oNotData.NotifiedId = newDownVal.Key.Id;
                oNotData.NotifiedName = newDownVal.Title;
                oNotData.NotifyingUserEmail = String.Empty; //Email utente che ha com piuto l'azione (non indispensabile)
                oNotData.NotifyingUserFullName = owner.Name.ToUpper() + " " + owner.Surname.ToUpper(); 
                oNotData.NotifyingUserId = owner.Key.Id;
                oNotData.FolderName = ((FolderName != "") ? FolderName : "Cartella principale");
                oNotData.GroupId = doc.groupID;
                oNotData.GroupName = groupVal.Title; 
                oNotData.NotifyToMembers = true;
                oNotData.NotifyToAdmin = true;
                oNotData.NotifiedIdLanguage = doc.langID;
                //Url della consolle per gestire l'approvazione
                oNotData.GroupUrl = string.Format("{0}://{1}{2}", System.Web.HttpContext.Current.Request.Url.Scheme.ToString(), System.Web.HttpContext.Current.Request.Url.Host.ToString(), NotificationHelper.ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(oNotData.GroupId, oNotData.NotifiedIdLanguage), new LanguageIdentificator(1), oNotData.GroupName, "detail", SimpleHash.EncryptRijndaelAdvanced(oNotData.NotifiedId.ToString()), "documenti"));

                if (newDownVal.ApprovalStatus == ContentBaseValue.ApprovalWFStatus.Approved)
                {
                    NotificationHelper.DeliveryNotification(oNotData);
                }
                else
                {

                    oNotData.GroupUrl = string.Format("{0}://{1}{2}", System.Web.HttpContext.Current.Request.Url.Scheme.ToString(), System.Web.HttpContext.Current.Request.Url.Host.ToString(), NotificationHelper.ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(oNotData.GroupId, oNotData.NotifiedIdLanguage), new LanguageIdentificator(1), oNotData.GroupName, "detail", SimpleHash.EncryptRijndaelAdvanced(oNotData.NotifiedId.ToString()), "approvadoc"));

                    SendNotificaBoard(oNotData, false);
                    //SendMailMessage(oNotData, false);

                }

                return true;    
            }

            return false;
        }


        private void SendNotificaBoard(NotificationData oNotData, bool throwOn)
        {
           
                string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);
                Dictionary<string, string> replacing = new Dictionary<string, string>();
                
                replacing.Add("[ID]", oNotData.NotifiedId.ToString()); 
                replacing.Add("[TITLE]", oNotData.NotifiedName);
                replacing.Add("[GROUPID]", oNotData.GroupId.ToString());
                replacing.Add("[GROUPNAME]", oNotData.GroupName);
                replacing.Add("[USERID]", oNotData.NotifyingUserId.ToString());
                replacing.Add("[USERNAME]", oNotData.NotifyingUserFullName);
                replacing.Add("[URL]", oNotData.GroupUrl);


                int langId = (int)LabelKeys.LANGUAGE_PORTAL;
                MailHelper.SendMail(MailTemplateKeys.NotificaBoardNewDoc, langId, htmlTemplate, replacing, false, "");
            
        }


        private static void SendMailMessage(NotificationData oNotData, bool throwOn, string strSenderName = "")
        {
            try
            {
                Healthware.HP3.Core.Utility.ObjectValues.MailValue oHp3MailValue = new Healthware.HP3.Core.Utility.ObjectValues.MailValue();
                oHp3MailValue.MailTo.Add("giulia.vento@healthtouch.eu");
                oHp3MailValue.MailCC.Add("annadina.bove@healthwareinternational.com");
                oHp3MailValue.MailBcc.Add("davide.muccioli@healthware.it");
                oHp3MailValue.MailFrom = new MailAddress("noreply@mail.healthware.it", "Accu-Chek");

                System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage();

                if ((oHp3MailValue.MailTo != null) && oHp3MailValue.MailTo.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailTo)
                    {
                        oMail.To.Add(objTo);
                    }
                }
                if ((oHp3MailValue.MailCC != null) && oHp3MailValue.MailCC.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailCC)
                    {
                        oMail.CC.Add(objTo);
                    }
                }

                if ((oHp3MailValue.MailBcc != null) && oHp3MailValue.MailBcc.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailBcc)
                    {
                        //  System.Web.HttpContext.Current.Response.Write(objTo.Address)
                        oMail.Bcc.Add(objTo);
                    }
                }

                try
                {
                    
                        oMail.From = new System.Net.Mail.MailAddress(oHp3MailValue.MailFrom.Address, oHp3MailValue.MailFrom.DisplayName);
                    
                }
                catch (Exception ex)
                {
                    //ErrorNotification.Helper.ErrorNotifier.NotifyByMail(ex, string.Empty);
                }

                oMail.Subject = "Accu-Chek - Una nuova risorsa  è stata caricata, gestire approvazione"; //oHp3MailValue.MailSubject;
                oMail.Body = string.Format("ID:{0}<br>Titolo:{1}<br>GroupID:{2}<br>GroupName:{3}<br>IDUser:{4}<br>FullName:{5}", oNotData.NotifiedId, oNotData.NotifiedName, oNotData.GroupId, oNotData.GroupName, oNotData.NotifyingUserId.ToString(), oNotData.NotifyingUserFullName); //oHp3MailValue.MailBody;
                oMail.IsBodyHtml = true;

                System.Net.Mail.SmtpClient mySmtpMail = new System.Net.Mail.SmtpClient();
                mySmtpMail.Host = System.Configuration.ConfigurationManager.AppSettings["smtp"];
                mySmtpMail.Send(oMail);
            }
            catch (Exception ex)
            {
                if (throwOn)
                {
                    throw new ApplicationException(ex.Message, ex);
                }
                else
                {
                    //ErrorNotification.Helper.ErrorNotifier.NotifyByMail(ex, string.Empty);
                }
            }
        }


        public void SaveDownloadType(int oDownPk, string ext, long oFileLength)
        {
            DownloadManager dwnMng = new DownloadManager();
            DownloadType oTypeInfo = dwnMng.Read(ext);
            DownloadTypeValue oDownTypeValue = new DownloadTypeValue();
            var _with1 = oDownTypeValue;
            _with1.KeyContent.PrimaryKey = oDownPk;
            _with1.Weight = (int)oFileLength;
            _with1.isDefault = true;
            _with1.Visibility = DownloadManager.DownloadBehavior.isPublic;
            _with1.DownloadType = oTypeInfo;
            dwnMng.Create(oDownTypeValue);
        }


        [HttpPost]
        public JsonResult UploadFile()
        {
            HttpPostedFileBase fileContent = null;
            HashSet<string> extHs = new HashSet<string>();
            extHs.Add(".jpg");
            extHs.Add(".gif");
            extHs.Add(".png");
            extHs.Add(".doc");
            extHs.Add(".docx");
            extHs.Add(".xls");
            extHs.Add(".xlsx");
            extHs.Add(".ppt");
            extHs.Add(".pptx");
            extHs.Add(".pdf");
            extHs.Add(".zip");
            extHs.Add(".rar");
            extHs.Add(".rtf");
            extHs.Add(".mp3");
            extHs.Add(".wave");
            extHs.Add(".wma");
            extHs.Add(".mpeg");
            extHs.Add(".mp4");
            
            if (Request.Files.Count > 0)
            {
                try
                {

                    fileContent = Request.Files[0];

                    string strExtension = Path.GetExtension(fileContent.FileName);
                    if (extHs.Contains(strExtension.ToLower()))
                    {
                        string directory = Server.MapPath("/HP3Download/tmpDownloadPath/"); //Server.MapPath(Inclub.Web.Portal.Keys.LabelKeys.FolderFILE_TEMP);

                        bool exists = System.IO.Directory.Exists(directory);
                        //var dir = new DirectoryInfo(directory);

                        //if (exists)
                        //    dir.Delete(true);

                        //exists = System.IO.Directory.Exists(directory);
                        if (!exists)
                            System.IO.Directory.CreateDirectory(directory);

                        string fileToken = Guid.NewGuid().ToString();
                        string tmpFileName  = String.Format("{0}{1}", fileToken, strExtension);
                        //var fileServerPath = Path.Combine(directory, tmpFileName);

                        FileHelper.SaveFile(Inclub.Web.Portal.Keys.LabelKeys.TEMP_DOWNLOAD_PATH, tmpFileName, fileContent);
                        return Json(tmpFileName);
                    }
                    else
                    {
                        return Json("resko: tipo di file non consentito");
                    }


                    
                }
                catch (Exception ex)
                {
                    // Helper.ErrorLogHelper.NotifyDBLog(ex, string.Empty);
                    return Json("resko:" + ex.Message);
                }
            }

            return Json("resko: errore");
        }

    }
}
