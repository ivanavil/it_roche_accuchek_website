﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.MVC;


namespace Inclub.Web.Portal.Controllers
{
    public class ConfermaRegistrazioneController : Controller
    {
        //
        // GET: /ConfermaRegistrazione/
        public ActionResult Index(string id)
        {

            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            int idUser = 0;

            if (string.IsNullOrWhiteSpace(id) || !int.TryParse(CryptHelper.urlDecrypt(id), out idUser))
                return new RedirectResult("/"); // go to home prelogin

            Inclub.Web.Portal.Models.OperationResult operationResult = new Inclub.Web.Portal.Models.OperationResult();
            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;
            EnumeratorKeys.UserStatus userStatus = UserHelper.GetUserStatus(idUser);

            switch (userStatus)
            {
                case EnumeratorKeys.UserStatus.Active:
                case EnumeratorKeys.UserStatus.NoActive:
                case EnumeratorKeys.UserStatus.NotFound:
                case EnumeratorKeys.UserStatus.Disable:
                case EnumeratorKeys.UserStatus.Deleted:
                    return new RedirectResult("/", false);
                case EnumeratorKeys.UserStatus.Pending:
                    if (SetStatusConfirmEmail(idUser))
                    {

                        this.Trace(pageInfo.Key, TraceEventKeys.SubmitRegForm);
                        //trace
                        TraceHelper.TraceEvent(pageInfo.Key, TraceEventKeys.SubmitRegForm);
                        operationResult.Message = DictionaryHelper.GetLabel(DictionaryKeys.MsgConfirmRegistration);
                        operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;

                        this.ViewBag.Message = DictionaryHelper.GetLabel(DictionaryKeys.MsgConfirmRegistration);
                        UserSendMail(idUser);
                    }
                    else
                    {
                        return new RedirectResult("/", false);
                    }
                    break;
            }

            //trace            
            TraceHelper.TraceEvent(pageInfo.Key);

            return View();
        }

        private static void UserSendMail(int idUser)
        {
            string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);

            UserValue hcpVal = new UserValue();
            hcpVal = UserHelper.GetUserBaseValueByUserId(idUser);

            Dictionary<string, string> replacing = new Dictionary<string, string>();
            replacing.Add(LabelKeys.em_IDUSER, idUser.ToString());


            replacing.Add(LabelKeys.em_NAME, hcpVal.Name);
            replacing.Add(LabelKeys.em_SURNAME, hcpVal.Surname);
            replacing.Add(LabelKeys.em_EMAIL, hcpVal.Email.ToString());


            int langId = LanguageHelper.GetLanguageFromUrl().Id;
            MailHelper.SendMail(MailTemplateKeys.RegistrationConfirm, langId, htmlTemplate, replacing, false, hcpVal.Email.ToString());
        }

        private bool SetStatusConfirmEmail(int idUser)
        {
            UserManager manager = new UserManager();
            if (manager.Update(new UserIdentificator(idUser), "Status", (int)EnumeratorKeys.UserStatus.NoActive))
            {
                //distruzione cache
                CacheHelper.DeleteGroupCache(CacheKeys.GroupCacheUser);

                return true;

            }
            return false;
        }

    }
}
