﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.BL;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace inClub.Web.Portal.Controllers
{
    public class ContributiController : Controller
    {
        //
        // GET: /Contributi/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Archive(int discussionId, short langId, int groupId, int pagenum, int pagesize, string viewName, string postid = "0", string commid = "0", string type = "1")
        {

            int selPost = 0;
            int selComm = 0;

            if ((postid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(postid), out selPost)))
            {
                this.ViewBag.selPostID = selPost;
            }
            else
            {
                this.ViewBag.selPostID = 0;
            }

            if ((commid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(commid), out selComm)))
            {
                this.ViewBag.selCommID = selComm;
            }
            else
            {
                this.ViewBag.selCommID = 0;
            }



            if ((!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated()) || (discussionId <= 0))
            {                
                return View();
            }

            ViewBag.discussionId = discussionId;
            ViewBag.langId = langId;
            ViewBag.groupId = groupId;
            ViewBag.type = type;

            if (viewName == "")
                viewName = "_ItemContributi";

            var so = new CommentSearcher();
            var objCommentManager = new CommentManager();

            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.KeyContent = new ContentIdentificator(discussionId, langId);            
            so.PageNumber = pagenum;
            so.PageSize = pagesize;
            so.LoadHasSons = true;

            so.Status = CommentValue.EnumStatus.Approved;
            
            so.SortOrder = CommentGenericComparer.SortOrder.DESC;
            so.SortType = CommentGenericComparer.SortType.ById;

            if (selPost != 0)
                so.key.Id = selPost;

            var comments = new CommentCollection();
            String Cachekey = String.Empty;

            //Cachekey = "DiscPosts|" + discussionId;
                
            ////comments = (CommentCollection)CacheManager.Read(Cachekey, CacheManager.FirstLevelCache);


            //if (comments == null) {                    
                objCommentManager.RemoveCache();
                comments = objCommentManager.Read(so);
            //    CacheManager.Insert(Cachekey, comments, CacheManager.FirstLevelCache);
            //}

            List<inClub.Web.Portal.Models.Contributo> commentsObj = null;
            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Contributo>();


            if (comments != null && comments.Any())
            {
                modelColl.CurrentPage = pagenum;
                modelColl.TotalPagesCount = comments.PagerTotalNumber;
                modelColl.TotalItemsCount = comments.PagerTotalCount;

                commentsObj = comments.Select(toContributo).ToList();

                modelColl.Items = commentsObj;



                return PartialView(viewName, modelColl);
            }

            return PartialView(viewName, null);
        }

        private static inClub.Web.Portal.Models.Contributo toContributo(CommentValue x)
        {
             var ret = new inClub.Web.Portal.Models.Contributo();
             int numCommenti = 0;
             int.TryParse(x.Author.ExternalID, out numCommenti);
        

              ret.PostKey = x.Key;
              ret.Description = x.Text;

              ret.DiscussionKey = x.KeyContent;
              ret.CreationDate = x.DateCreate.Value.ToShortDateString();              
              ret.UserCreate = x.Author.Key;
              ret.UserCreateName =  x.Author.Name + ' ' + x.Author.Surname;
              ret.UserCover = Inclub.Web.Portal.Helper.CoverHelper.getUserImage(x.Author.Key.Id);
              ret.NumCommenti = numCommenti;  //legge da una campo denormalizzato


              //creo la relazione tra il contributo e il file
              Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
              SqlDataReader reader; 
              SqlCommand command = new SqlCommand();
              command = new SqlCommand("select embeddata, id_content, type from commentEmbedRelation where id_comment = " + x.Key.Id + " ", manager.GetConnection());
              command.CommandType = CommandType.Text;
              manager.OpenConnection();
              reader = command.ExecuteReader();

              List<ContentBaseValue> Resources = new List<ContentBaseValue>();
              if (reader.HasRows)
              {
                  while(reader.Read())
                  {
                      if (reader["type"].ToString() == "download") {
                       ContentBaseValue cBV = new ContentBaseValue();
                        cBV.Code = (Path.GetExtension(reader["embeddata"].ToString())).Replace(".", "");
                        cBV.LinkUrl = "/Portal/_async/DownloadFile.ashx?intPk=" + SimpleHash.UrlEncryptRijndaelAdvanced(reader["id_content"].ToString());
                        cBV.Key.PrimaryKey = (int)reader["id_content"];
                        cBV.Title = reader["embeddata"].ToString();
                        Resources.Add(cBV);
                      }
                  }
              }
              
              command.Dispose();
              command = null;

              ret.ResourceList = Resources;

            //if (x.HasSons) {
            
            //    List<inClub.Web.Portal.Models.Contributo> sons = new List<inClub.Web.Portal.Models.Contributo>();
            //    sons = ReadSons(x.KeyContent, x.Key.Id, 1, 0);

            //    if (sons !=null && sons.Any()){
            //        ret.CommentList = sons;
            //        ret.NumCommenti = sons.Count;
            //    }
            //}


              return ret;
          }

        public ActionResult ReadSons(int discussionId, int fatherId, string commid = "0")
        {

            this.ViewBag.selCommID = commid;
            

            if ((!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated()) || (discussionId <= 0))
            {
                return View("Index", "Community");
            }

            List<inClub.Web.Portal.Models.Contributo> results = null;
            if (discussionId > 0)
            {
                CommentSearcher so = new CommentSearcher();
                so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                if ((fatherId > 0))
                {
                    so.FatherId.Id = fatherId;
                }

                so.Status = CommentValue.EnumStatus.Approved;
                so.KeyContent.Id = discussionId;
                so.PageNumber = 0;
                so.PageSize = 0;                

                so.SortOrder = CommentGenericComparer.SortOrder.DESC;
                so.SortType = CommentGenericComparer.SortType.ById;

                CommentManager objCommentManager = new CommentManager();
                objCommentManager.Cache = false;
                CommentCollection sons = objCommentManager.Read(so);

                if ((!(sons == null) && sons.Any()))
                {
                    results = (sons.Select(toContributo).ToList()).OrderBy(s => s.PostKey.Id).ToList();

                    return PartialView("_ItemSons", results);
                }
                else
                {

                    return PartialView("_ItemSons", null);
                }

            }
            else
            {

                return PartialView("_ItemSons", null);
            }
            
        }



        private static List<inClub.Web.Portal.Models.Contributo> ReadSons(ContentIdentificator key, int fatherId, int pagenum = 1, int pagesize = 0)
        {

            List<inClub.Web.Portal.Models.Contributo> results = null;
            if ((!(key == null) && (key.Id > 0)))
            {
                CommentSearcher so = new CommentSearcher();
                so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                if ((fatherId > 0))
                {
                    so.FatherId.Id = fatherId;
                }
                
                so.Status = CommentValue.EnumStatus.Approved;                
                so.KeyContent = key;
                so.PageNumber = pagenum;
                so.PageSize = pagesize;

                so.SortOrder = CommentGenericComparer.SortOrder.DESC;
                so.SortType = CommentGenericComparer.SortType.ById;

                CommentManager objCommentManager = new CommentManager();
                objCommentManager.Cache = false;
                CommentCollection sons = objCommentManager.Read(so);                
                
                if ((!(sons == null) && sons.Any()))
                {                  
                    results = (sons.Select(toContributo).ToList()).OrderBy(s => s.PostKey.Id).ToList();
                }

            }
            return results;
        }


        public PartialViewResult AggiungiContributo()
        {
            inClub.Web.Portal.Models.AddContributo model = new inClub.Web.Portal.Models.AddContributo();
            
            int discussionID = 0;
            int.TryParse(Request["dId"].ToString(),out discussionID);
            int groupID = 0;
            int.TryParse(Request["gId"].ToString(), out groupID);
            int langID = 0;
            int.TryParse(Request["lId"].ToString(), out langID);
            int fatherID = 0;
            int.TryParse(Request["fId"].ToString(), out fatherID);
            int discFolderKey = 0;
            int.TryParse(Request["fDisc"].ToString(), out discFolderKey);
            string role = "";
            role = Request["r"].ToString();
            string type = "";
            type = (!string.IsNullOrEmpty(Request["type"]) ? Request["type"].ToString() : "1");


            model.discussionID = discussionID;
            model.groupID = groupID;
            model.langID = (short)langID;
            model.fatherID = fatherID;
            model.discFolderKey = discFolderKey;
            model.role = role;
            model.type = type;
            return PartialView("_AggiungiContributo", model);
        }


        public PartialViewResult AggiungiCommento()
        {
            inClub.Web.Portal.Models.AddContributo model = new inClub.Web.Portal.Models.AddContributo();

            int discussionID = 0;
            int.TryParse(Request["dId"].ToString(), out discussionID);
            int groupID = 0;
            int.TryParse(Request["gId"].ToString(), out groupID);
            int langID = 0;
            int.TryParse(Request["lId"].ToString(), out langID);
            int fatherID = 0;
            int.TryParse(Request["fId"].ToString(), out fatherID);
            string type = "";
            type = (!string.IsNullOrEmpty(Request["type"]) ? Request["type"].ToString() : "1");

            model.discussionID = discussionID;
            model.groupID = groupID;
            model.langID = (short)langID;
            model.fatherID = fatherID;
            model.discFolderKey = 0;
            model.type = type;

            return PartialView("_AggiungiCommento", model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiungiContributo(inClub.Web.Portal.Models.AddContributo model)
        {
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info
            
            bool checkCreate = false;

            if (model.Description == null)
            {
                return Content("Inserire la descrizione!", "text/html");
            }
            else { 

            if (ModelState.IsValid)
            {
                checkCreate = CreaContributo(model);

                if (checkCreate)
                {
                    return Content("Commento creato correttamente", "text/html");
                }
                else
                {
                    return Content("Si è verificato un'errore nella creazione del commento", "text/html");
                }
                
            }
            else
            {
                return Content("Si è verificato un'errore nella creazione del contributo", "text/html");
            }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiungiCommento(inClub.Web.Portal.Models.AddContributo model)
        {
            bool checkCreate = false;
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            if (model.Description == null)
            {
                return Content("Inserire la descrizione!", "text/html");
            }
            else
            {

                if (ModelState.IsValid)
                {
                   checkCreate = CreaContributo(model);
                    
                    if (checkCreate) {
                        return Content("Commento creato correttamente", "text/html");
                    }else{
                        return Content("Si è verificato un'errore nella creazione del commento", "text/html");
                    }
                    
                }
                else
                {
                    return Content("Si è verificato un'errore nella creazione del commento", "text/html");
                }
            }
        }

        private bool CreaContributo(inClub.Web.Portal.Models.AddContributo post)
        {
            ContentManager objContentManager = new ContentManager();
            CommentValue vo = new CommentValue();
            CommentManager objCommentManager = new CommentManager();
            
            int DiscussionKey = 0;
            int GroupKey = 0;

            try
            {
                objContentManager.Cache = false;
                ContentValue groupVal = new ContentValue();
                ContentValue discVal = new ContentValue();

                discVal = objContentManager.Read(post.discussionID, post.langID);
                DiscussionKey = discVal.Key.PrimaryKey;

                groupVal = objContentManager.Read(post.groupID, post.langID);
                GroupKey = groupVal.Key.PrimaryKey;

                vo.KeyContent = new ContentIdentificator(post.discussionID, post.langID);
                vo.KeyContent.PrimaryKey = DiscussionKey;
                vo.Visible = CommentValue.EnumVisibile.IsPublic;
                vo.DateCreate = System.DateTime.Now;
                vo.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                vo.Text = post.Description;
                vo.Title = "";
                vo.FatherId.Id = post.fatherID;

                vo.Status = CommentValue.EnumStatus.Approved;
                //IIf(approval = 1, CommentValue.EnumStatus.Approved, CommentValue.EnumStatus.Pending) 
                vo.HasSons = false;

                UserBaseValue owner = new UserBaseValue();
                owner = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
                
                vo.Author.Key.Id = owner.Key.Id;
                vo.Author.Name = owner.Name;
                vo.Author.Surname = owner.Surname;

                FileInfo attch = FileHelper.getFileInFolder(Inclub.Web.Portal.Keys.LabelKeys.FolderFILE_TEMP);                

                vo = objCommentManager.Create(vo);

                if ((vo.Key != null) && vo.Key.Id > 0)
                {

                    post.ID = vo.Key.Id;

                    if (null != attch)
                    {                        
                        int idFileAtt = 0;
                        idFileAtt = createAttachedDoc(attch, post, owner);

                        //creo la relazione tra il contributo e il file
                        Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                        
                        SqlCommand cmdInsDwnl = new SqlCommand();
                        cmdInsDwnl = new SqlCommand("Insert into commentEmbedRelation (id_content, id_comment, embeddata, type) values ('" + idFileAtt + "', '" + post.ID + "', '" + attch.Name + "','download') ", manager.GetConnection());
                        cmdInsDwnl.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        cmdInsDwnl.ExecuteNonQuery();
                        cmdInsDwnl.Dispose();
                        cmdInsDwnl = null;
                    }

                    objContentManager = new ContentManager();
                    
                    //check ruolo utente
                    ProfilingManager roleMng = new ProfilingManager();
                    int _role = 0;

                    if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id)))
                    {
                        _role = Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id;

                    }
                    else if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id)))
                    {
                        _role = Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id;

                        //Controllo per verificare che un utente group admin sia group admin anche per il gruppo corrente
                        objContentManager.Cache = false;
                        ContentUserSearcher oContUsSo = new ContentUserSearcher();
                        var _with1 = oContUsSo;
                        _with1.KeyUser.Id = owner.Key.Id;
                        _with1.KeyContent.Id = post.groupID;
                        _with1.KeyContent.IdLanguage = post.langID;
                        _with1.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin;
                        if (objContentManager.CountContentUser(oContUsSo) == 0)
                            _role = Inclub.Web.Portal.Keys.RoleKeys.Member.Id;

                    }
                    else if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.Member.Id)))
                    {
                        objContentManager.Cache = false;
                        ContentUserSearcher oContUsSo = new ContentUserSearcher();
                        var _with1 = oContUsSo;
                        _with1.KeyUser.Id = owner.Key.Id;
                        _with1.KeyContent.Id = post.groupID;
                        _with1.KeyContent.IdLanguage = post.langID;
                        _with1.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.GrpMember;
                        if (objContentManager.CountContentUser(oContUsSo) == 0)
                            _role = Inclub.Web.Portal.Keys.RoleKeys.Member.Id;
                        
                    }

                    //invio notifica                    
                    inClub.Web.Portal.Helper.NotificationHelper.DeliveryNotification(vo.Key.Id, post.groupID, post.discussionID, post.langID, groupVal.Title, post.fatherID, discVal.Title, owner, _role, post.type);


                    //aggiorno i contatori x commenti e contributi a bordo del contributo, della discussione, dell'utente e del gruppo                                        
                    if (post.fatherID != 0) {

                          //aggiorno il dato contatore commenti a bordo del gruppo                         
                          Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                          dynamic command = new SqlCommand("Update pp_contents set contents_link_label = ((cast(isNull(nullif(contents_link_label,''),'0') as int)) + 1) where contents_key = " + GroupKey, manager.GetConnection());
                          command.CommandType = CommandType.Text;
                          manager.OpenConnection();
                          command.ExecuteNonQuery();
                          command.Dispose();
                          command = null;

                          //aggiorno il dato contatore commenti a bordo del contributo       
                          dynamic command0 = new SqlCommand("Update HP3_ContentComment set comment_externalUserId = (comment_externalUserId + 1) where comment_id = " + post.fatherID, manager.GetConnection());
                          command0.CommandType = CommandType.Text;
                          manager.OpenConnection();
                          command0.ExecuteNonQuery();
                          command0.Dispose();
                          command0 = null;                             

                          //aggiorno il dato contatore commenti a bordo della discussione                          
                          dynamic command1 = new SqlCommand("Update pp_contents set contents_importants = (contents_importants + 1) where contents_key = " + DiscussionKey, manager.GetConnection());
                          command1.CommandType = CommandType.Text;
                          manager.OpenConnection();
                          command1.ExecuteNonQuery();
                          command1.Dispose();
                          command1 = null;                          

                          //aggiorno il dato contatore commenti a bordo dell'utente che ha creato il commento                          
                          dynamic command2 = new SqlCommand("Update pp_user set user_fax = ((cast(isNull(nullif(user_fax,''),'0') as int)) + 1) where user_id = " + owner.Key.Id.ToString(), manager.GetConnection());
                          command2.CommandType = CommandType.Text;
                          manager.OpenConnection();
                          command2.ExecuteNonQuery();
                          command2.Dispose();
                          command2 = null;

                    }
                    else
                    {
                        //aggiorno il dato contatore contributi a bordo del gruppo
                        Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                        dynamic command = new SqlCommand("Update pp_contents set contents_codice_magazzino = ((cast(isNull(nullif(contents_codice_magazzino,''),'0') as int)) + 1) where contents_key = " + GroupKey, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;

                        //aggiorno il dato contatore contributi a bordo della discussione
                        dynamic command1 = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino + 1) where contents_key = " + DiscussionKey, manager.GetConnection());
                        command1.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command1.ExecuteNonQuery();
                        command1.Dispose();
                        command1 = null;
                       

                        //aggiorno il dato contatore contributi a bordo dell'utente che ha creato il cotributo                        
                        dynamic command2 = new SqlCommand("Update pp_user set user_cel = ((cast(isNull(nullif(user_cel,''),'0') as int)) + 1) where user_id = " + owner.Key.Id.ToString(), manager.GetConnection());
                        command2.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command2.ExecuteNonQuery();
                        command2.Dispose();
                        command2 = null;
                    }

                    

                    //TODO: mapComment ????
                    Inclub.Web.Portal.Helper.ContentHelper.mapComment(post.ID, post.discussionID, post.groupID, discVal.Title, 1);

                   
                    //TODO: testare tracciamento
                    Inclub.Web.Portal.Helper.TraceHelper.TraceComment(post.discussionID, post.langID, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni.Id, owner.Key.Id, (post.fatherID == 0 ? Inclub.Web.Portal.Keys.TraceEventKeys.AddContributo.Id : Inclub.Web.Portal.Keys.TraceEventKeys.AddCommento.Id));

                    //objCommentManager.Cache = False
                    return true;
                }else{

                    return false;
                }
               

            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write(ex.ToString());
                return false;
            }

            

        }

        public void SaveDownloadType(int oDownPk, string ext, long oFileLength)
        {
            DownloadManager dwnMng = new DownloadManager();
            DownloadType oTypeInfo = dwnMng.Read(ext);
            DownloadTypeValue oDownTypeValue = new DownloadTypeValue();
            var _with1 = oDownTypeValue;
            _with1.KeyContent.PrimaryKey = oDownPk;
            _with1.Weight = (int)oFileLength;
            _with1.isDefault = true;
            _with1.Visibility = DownloadManager.DownloadBehavior.isPublic;
            _with1.DownloadType = oTypeInfo;
            dwnMng.Create(oDownTypeValue);
        }


        private int createAttachedDoc(FileInfo attach, Models.AddContributo post, UserBaseValue owner)
        {

            string filePath = string.Empty;

            if (null != attach)
            {
                filePath = attach.FullName;
            }

            ContentManager cMan = new ContentManager();
            DownloadValue dV = new DownloadValue();
            
            dV.Title = attach.Name;
            dV.Launch = "Risorsa relativa al contributo:" + post.Description;
            dV.Active = true;
            dV.ApprovalStatus = ContentBaseValue.ApprovalWFStatus.Approved;

            dV.DateCreate = System.DateTime.Now;
            dV.DateInsert = System.DateTime.Now;
            dV.DatePublish = System.DateTime.Now;
            dV.DateUpdate = System.DateTime.Now;
            dV.Code = post.discFolderKey.ToString(); //TODO: id cartella discussione  //ddlCartella.SelectedItem.Value;
            dV.Copyright = "";   //TODO: nome  cartella discussione   // ddlCartella.SelectedItem.Text;

            dV.Display = true;

            dV.Key.IdLanguage = post.langID;
            dV.ContentType.Key.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Documenti.Id;
            dV.IdContentSubCategory = 1;
            dV.Indexable = true;
            dV.Owner.Key.Id = owner.Key.Id;

            int intResourceId = 0;

            ContentManager cntMng = new ContentManager();
            DownloadManager dwnMng = new DownloadManager();
            DownloadValue newDownVal = dwnMng.Create(dV);
            if ((newDownVal != null) && newDownVal.Key.Id > 0)
            {
                intResourceId = newDownVal.Key.Id;


                SaveDownloadType(newDownVal.Key.PrimaryKey, attach.Extension.Replace(".",""), attach.Length);

                //Accoppiamento sitearea Documenti------------------------
                ContentSiteAreaValue oCoSaVal = new ContentSiteAreaValue();
                var _with3 = oCoSaVal;
                _with3.KeyContent = newDownVal.Key;
                _with3.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id);
                cMan.CreateContentSiteArea(oCoSaVal);
                //-------------------------------------------------------

                //Accoppiamento sito-----------------------------------
                oCoSaVal.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                cMan.CreateContentSiteArea(oCoSaVal);
                oCoSaVal = null;

                //Accoppiamento col gruppo
                ContentRelationValue oContRelValue = new ContentRelationValue();                
                oContRelValue.KeyContent.PrimaryKey = cntMng.ReadPrimaryKey(post.groupID, post.langID);
                oContRelValue.KeyContentRelation = newDownVal.Key;
                oContRelValue.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources;  // JLink.RelationTypeConstants.IDs.Resource;
                cMan.CreateContentRelation(oContRelValue);

                if (post.discFolderKey != 0)
                {

                    int discFold_PKey = cntMng.ReadPrimaryKey(post.discFolderKey, post.langID);

                    //Accoppiamento con la folder
                    oContRelValue = null;
                    oContRelValue = new ContentRelationValue();
                    oContRelValue.KeyContent.PrimaryKey = discFold_PKey;
                    oContRelValue.KeyContentRelation = newDownVal.Key;
                    oContRelValue.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.ResourceFolder;
                    cntMng.CreateContentRelation(oContRelValue);


                    Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                    dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino + 1) where contents_key = " + discFold_PKey, manager.GetConnection());
                    command.CommandType = CommandType.Text;
                    manager.OpenConnection();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    command = null;
                }

                string targetFileName = newDownVal.Key.PrimaryKey.ToString();
                

                //Trace-------------------------------------------------
                TraceObject objTrace = new TraceObject();
                var _with6 = objTrace;
                _with6.Content = null;
                _with6.ContentId = newDownVal.Key.Id;
                _with6.Description = "Risorsa associata al contributo:" + post.ID;
                _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.AddDocumento.Id;
                _with6.LanguageId = newDownVal.Key.IdLanguage;
                _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                _with6.ThemeId = 0;
                _with6.UserId = owner.Key.Id;
                TraceHelper.TraceEvent(objTrace);
                //------------------------------------------------------


                FileHelper.Move(Inclub.Web.Portal.Keys.LabelKeys.FolderFILE_TEMP, Inclub.Web.Portal.Keys.LabelKeys.DOWNLOAD_PATH, attach.Name, targetFileName);
                return newDownVal.Key.PrimaryKey;
            }
            else
            {

                return 0;
            }
            
            
        }


        [HttpPost]
        public JsonResult UploadFile()
        {
            HttpPostedFileBase fileContent = null;

            if (Request.Files.Count > 0)
            {
                try
                {

                    fileContent = Request.Files[0];
                
                    string directory = Server.MapPath(Inclub.Web.Portal.Keys.LabelKeys.FolderFILE_TEMP);

                    bool exists = System.IO.Directory.Exists(directory);
                    var dir = new DirectoryInfo(directory);

                    if (exists)
                        dir.Delete(true);

                    exists = System.IO.Directory.Exists(directory);
                    if (!exists)
                        System.IO.Directory.CreateDirectory(directory);

                    FileHelper.SaveFile(Inclub.Web.Portal.Keys.LabelKeys.FolderFILE_TEMP, fileContent.FileName.ToString(), fileContent);
                    return Json("ok");
                }
                catch (Exception ex)
                {
                    // Helper.ErrorLogHelper.NotifyDBLog(ex, string.Empty);
                    return Json("ko" + ex.Message);
                }
            }

            return Json("ko");
        }

    }
}
