﻿using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Utility;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.BL;
using Inclub.Web.Portal.Models;
using Healthware.HP3.MVC;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.User;

namespace Inclub.Web.Portal.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info


            //trace
            // TraceHelper.TraceEvent(pageInfo.Key); 
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info
            
            Helper.TraceHelper.TraceEvent(pageInfo.Key, Keys.TraceEventKeys.LoginOut);            

            if (Request.Cookies["UserReminder"] != null)
            {
                HttpCookie myCookie = new HttpCookie("UserReminder");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            LoginHelper.LogOut();
            
            return Redirect("/");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(UserLogIn user)
        {
            //if (user == null)
            //{
            //    HttpContext.Response.Write("utente null");
            //}
            //else
            //{
            //    HttpContext.Response.Write("utente ok");
            //}

            //return null;

            string _returnUrl = string.Empty;
            TicketValue userTicket = null;
            if (ModelState.IsValid)
            {
                
                userTicket = LoginHelper.LogIn(user.Email.Trim(), user.Password.Trim());

                if (userTicket != null && userTicket.ErrorOccurred.Number < 0)
                {
                    int ErrorCode = userTicket.ErrorOccurred.Number * -1;
                    //login KO
                    string ErrorLoginMsg = Helper.DictionaryHelper.GetLabel(string.Format(Keys.DictionaryKeys.ErrorLoginCode, ErrorCode), 1);

                    TempData["LoginError"] = ErrorLoginMsg;

                    //trace login KO
                    //Helper.TraceHelper.TraceEvent(pageInfo.Key, Keys.TraceEventKeys.LoginFailed);

                }
                else if (userTicket != null && userTicket.ErrorOccurred.Number >= 0)
                {
                    //trace login ok
               
                    this.Trace(Keys.TraceEventKeys.LogIn);

                    //HttpContext.Response.Write(Session["returnUrl"]);

                    //gestione iscrizione automatica webinar
                    var sessRetUrl = System.Web.HttpContext.Current.Session["backURL"];
                    var roleMng = new ProfilingManager();
                    //DateTime webinarDay = DateTime.Parse("24/10/2017 15:30");
                    //DateTime orario = System.DateTime.Now;
                    //string webinar = Request["webinar"];

                    if (user.webinar == true && (!roleMng.HasRole(userTicket.User.Key, Inclub.Web.Portal.Keys.RoleKeys.accWebinar2)))
                    {
                        roleMng.CreateUserRole(userTicket.User.Key, Inclub.Web.Portal.Keys.RoleKeys.accWebinar2);

                        this.Trace(Keys.TraceEventKeys.IscrizioneWebinar);

                        return Redirect("/home#webinar");
                    }


                    if  ((sessRetUrl != null) && (sessRetUrl.ToString() != "/"))
                        return Redirect(sessRetUrl.ToString());

                    return Redirect("/home");
                }

                   
            }

            return Redirect("/home");

      }
    }
}