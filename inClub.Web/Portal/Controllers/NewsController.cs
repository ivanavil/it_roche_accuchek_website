﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.MVC;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.User;


namespace Inclub.Web.Portal.Controllers
{
    public class NewsController : Controller
    {
        
        [Inclub.Web.Portal.Attribute.AuthoriseSiteAccessAttribute]
        public ActionResult Detail(int id)
        {
         
           if(Helper.UserHelper.HaveAccess())
           { 

            #region Page Info

            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, ThemeHelper.getCurrentThemeIdentificator()); 

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info


            //Populate Searcher
            var so = BL.SearcherBuilders.InitContentExtraSearcher(this.Request.RequestContext,BL.SearcherBuilders.DataLoadingType.DetailsById, id);
            var manager = new Inclub.Web.Portal.BL.GenericContent();
            var objModel = manager.GetDetail(so);

            if (objModel != null && objModel.Key != null && objModel.Key.Id > 0)
            {
                //For MetaKeyword e Meta Description in all details
                var metaModel = objModel as MetaData;
                if (null != metaModel && null != pageInfo)
                    pageInfo.Populate(metaModel);

                this.Trace(pageInfo.Key,Keys.TraceEventKeys.Index, objModel.Key);
            }

            return View("Detail", objModel);
           }
           else
           {
               return RedirectToAction("Index", "Home");

           }


        }
          [Inclub.Web.Portal.Attribute.AuthoriseSiteAccessAttribute]
          public ActionResult Archives(int? nPage) 
        {
              if(Helper.UserHelper.HaveAccess())
           { 
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Populate Searcher
            var soModel = new Models.GenericSearcherContentModel();
            soModel.PageSize = 5; // numbers of item visible in the page
            soModel.PageNumber = nPage.GetValueOrDefault(1); //Start Page as const
            soModel.LanguageKey = Helper.LanguageHelper.GetLanguageFromUrl();
            soModel.LanguageId = Helper.LanguageHelper.GetLanguageFromUrl().Id;
            soModel.SiteAreaId =   Helper.SiteAreaHelper.GetSiteAreaIdentificator().Id;
            soModel.ContenTypeId = Helper.ContentTypeHelper.GetContentTypeIdentificator().Id;
            soModel.ThemeId = Helper.ThemeHelper.getCurrentThemeIdentificator().Id;


            this.ViewBag.Searcher = soModel;

            var so = BL.SearcherBuilders.InitContentJsonSearcher(this.Request.RequestContext, BL.SearcherBuilders.DataLoadingType.List, soModel);
            var manager = new BL.GenericContent();
            var objModel = manager.GetArchive(so, Helper.LanguageHelper.GetLanguageFromUrl().Id, Helper.ThemeHelper.getCurrentThemeIdentificator().Id);

            if (null != objModel && objModel.Any())
            {
            
                this.ViewBag.PagerTotalNumber = objModel.TotalCount;
            }
            else
            {
                this.ViewBag.PagerTotalNumber = 0;
            }


            this.Trace();


            return View("Archives", objModel);
           }
                  else
           {
               return RedirectToAction("Index", "Home");

           }
        }

        [HttpPost]
        [Healthware.HP3.MVC.Attributes.AjaxOnly]
        public JsonResult JsonArchives(GenericSearcherContentModel soView)
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Populate Searcher
            var soModel = new Models.GenericSearcherContentModel();
            soModel.PageSize = soView.PageSize;
            soModel.PageNumber = soView.PageNumber;
            soModel.LanguageId = soView.LanguageId;
            soModel.ContenTypeId = soView.ContenTypeId;
            soModel.SiteAreaId = soView.SiteAreaId;
              
            
          

            var so = BL.SearcherBuilders.InitContentJsonSearcher(this.Request.RequestContext, BL.SearcherBuilders.DataLoadingType.List, soModel);
            var manager = new BL.GenericContent();
            var objModel = manager.GetArchive(so, soView.LanguageId, soView.ThemeId);
            int PageTotalNumber = 0;
            if (objModel != null && objModel.Any())
            {
                PageTotalNumber = objModel.TotalCount;
            }

            string render = this.ControllerContext.RenderPartialToString("_jsonArchive", objModel);
            return Json(new { data = render, PageTotalNumber = PageTotalNumber }, JsonRequestBehavior.DenyGet);
        }

    }
}
