﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.BL;
using System.Data.SqlClient;
using System.Data;

namespace inClub.Web.Portal.Controllers
{
    public class DiscussioniController : Controller
    {
        //
        // GET: /Discussioni/

        public ActionResult Index(int groupId, int langId)
        {
            #region Page Info

            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;
            this.ViewBag.groupId = groupId;
            this.ViewBag.langId = langId;
            #endregion Page Info

            return PartialView("Index"); 
        }


        public ActionResult Detail(int discussionId, int groupId, short langId)
        {

            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}
            var objContentManager = new ContentManager();
            string role = "member";

            if (discussionId <= 0)
            {
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }
            else
            {
                
                var roleMng = new ProfilingManager();
                bool isAdmin = false;

                //controllo se l'utente loggato è sysadmin
                UserBaseValue user = new UserBaseValue();
                user = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
                isAdmin = roleMng.HasRole(user.Key, Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

                //se non lo è controllo se può accedere al gruppo, e leggo il ruolo che ha sul gruppo della discussione
                if (!isAdmin)
                {
                        ContentUserCollection cuc = new ContentUserCollection();
                        ContentUserSearcher oContUsSo = new ContentUserSearcher();
                        oContUsSo.KeyUser.Id = user.Key.Id;
                        oContUsSo.KeyContent.Id = groupId;
                        oContUsSo.KeyContent.IdLanguage = langId;
                
                        cuc = objContentManager.ReadContentUser(oContUsSo);

                        if (cuc != null && cuc.Any())
                        {
                            if (cuc[0].KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin)
                            {
                                role = "admin";
                            }
                            
                        }
                        else
                        {
                            //se non è associato al gruppo reindirizzo alla pagina dell'elenco dei gruppi
                            return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
                        }
                }
                else
                {
                    role = "admin";
                }


            }


            var manager = new AccessService();
            var ticket = manager.GetTicket();
            
           
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.key = new ContentIdentificator(discussionId, langId);
            so.Properties.Add("Title");
            so.Properties.Add("Launch");
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");
            so.Properties.Add("Importants"); //num commenti
            so.Properties.Add("Qty"); //num contributi
            so.Properties.Add("Code"); //primarykey della Folder creata automaticamente per la discussione

            so.LoadOwnerDetails = true;
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;

            so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
            so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;
           

            objContentManager.Cache = false;

            ContentCollection discussions = objContentManager.Read(so);
            List<inClub.Web.Portal.Models.Discussioni> discObj = new List<inClub.Web.Portal.Models.Discussioni>();

            if (discussions != null && discussions.Any())
            {

                discObj = discussions.Select(toDiscussione).ToList();
                discObj.ToList().ForEach(c => { (c.GroupKey = new ContentIdentificator()).Id = groupId; c.Role = role; });

                return PartialView("Detail", discObj[0]);
            }
          
            return PartialView("Detail", null);
        }



        public ActionResult Archive(int groupId, short langId, int pagenum, int pagesize, string viewName = "", string discid = "0")
        {

            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            int selDisc = 0;

            if ((discid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(discid), out selDisc)))
            {
                this.ViewBag.selDiscID = selDisc;
            }
            else
            {
                this.ViewBag.selDiscID = 0;
            }

            if (groupId <= 0)
            {                
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

            if (viewName == "")
                viewName = "_LastDiscussions";

            var manager = new AccessService();
            var ticket = manager.GetTicket();
            var objContentManager = new ContentManager();

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

            so.Properties.Add("Title");
            so.Properties.Add("Launch");
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");
            so.Properties.Add("Importants"); //num commenti
            so.Properties.Add("Qty"); //num contributi
            so.Properties.Add("Code"); //primarykey della Folder creata automaticamente per la discussione

            if (selDisc != 0)
                so.key = new ContentIdentificator(selDisc, langId);
                

            so.LoadOwnerDetails = true;
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;

            so.PageNumber = pagenum;
            so.PageSize = pagesize;

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;

            so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
            so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;
            

            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);


            if (!isAdmin)
            {
                String keysToExclude = ReadPrivateDiscussions(userID, groupId);

                 if (!String.IsNullOrEmpty(keysToExclude))
                     so.KeysToExclude = keysToExclude;

            }

           
            objContentManager.Cache = false;
            
            ContentCollection discussions = objContentManager.Read(so);
            List<inClub.Web.Portal.Models.Discussioni> discObj = new List<inClub.Web.Portal.Models.Discussioni>();
            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Discussioni>();

            if (discussions != null && discussions.Any())
            {
               modelColl.CurrentPage = pagenum;
               modelColl.TotalPagesCount = discussions.PagerTotalNumber;
               modelColl.TotalItemsCount = discussions.PagerTotalCount;

               discObj = discussions.Select(toDiscussione).ToList();
               discObj.ToList().ForEach(c => (c.GroupKey = new ContentIdentificator()).Id = groupId);

               modelColl.Items = discObj;

               return PartialView(viewName, modelColl);
            }

            return PartialView(viewName, null);
        }


        string ReadPrivateDiscussions(int userId, int groupId) {

             ContentUserSearcher so = new ContentUserSearcher();
             so.Order = groupId;
             so.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.RelDisUser;
             ContentManager ContentManager = new ContentManager();
             ContentManager.Cache = false;
             ContentUserCollection coll = ContentManager.ReadContentUser(so);
             string keyPrivateDisc = String.Empty;
             if ((!(coll == null) && (coll.Count > 0))) {
            
                 foreach (ContentUserValue disc in coll) {
                     if (((disc.KeyUser.Id != userId) && (disc.KeyRelationOwner.Id != userId))) {
                         keyPrivateDisc = (keyPrivateDisc + (disc.KeyContent.PrimaryKey + ","));
                     }
                 
                 }
             
                 if (!string.IsNullOrEmpty(keyPrivateDisc)) {
                     keyPrivateDisc = keyPrivateDisc.Remove((keyPrivateDisc.Length - 1));
                 }
             
                 return keyPrivateDisc;
             }
         
                return "";
         }

        private Func<ContentValue, inClub.Web.Portal.Models.Discussioni> toDiscussione =
           x => new inClub.Web.Portal.Models.Discussioni
           {
               DiscussionKey = x.Key,
               Title = x.Title,
               Description = x.Launch,
               UserCreate = x.Owner.Key,
               UserCreateName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname,
               CreationDate = x.DateCreate.Value.ToShortDateString(),
               LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
               NumContributi = x.Qty,
               NumCommenti = Convert.ToInt32(x.Importants),
               FolderKey = x.Code
           };




        public PartialViewResult AggiungiDiscussione()
        {
            inClub.Web.Portal.Models.AddDiscussione model = new inClub.Web.Portal.Models.AddDiscussione();
            
            int groupID = 0;
            int.TryParse(Request["gId"].ToString(), out groupID);
            int langID = 0;
            int.TryParse(Request["lId"].ToString(), out langID);
            
            model.groupID = groupID;
            model.langID = (short)langID;
            
            return PartialView("_AggiungiDiscussione", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiungiDiscussione(inClub.Web.Portal.Models.AddDiscussione model)
        {
            bool checkCreate = false;
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            if (model.Title == null)
            {
                return Content("Inserire il titolo!", "text/html");
            }
            else if (model.Description == null)
            {
                return Content("Inserire la descrizione!", "text/html");
            }
            else
            {

                if (ModelState.IsValid)
                {
                    checkCreate = (CreaDiscussione(model) != null);

                    if (checkCreate)
                    {
                        return Content("Discussione creata correttamente", "text/html");
                    }
                    else
                    {
                        return Content("Si è verificato un'errore nella creazione della discussione", "text/html");
                    }

                }
                else
                {
                    return Content("Si è verificato un'errore nella creazione della discussione", "text/html");
                }
            }
        }


        public static ContentValue CreaDiscussione(inClub.Web.Portal.Models.AddDiscussione discussion)
        {
            try
            {                
                ContentValue  DiscussionValue = new ContentValue();
			    ContentManager objContentManager = new ContentManager();

			    DiscussionValue.Title = discussion.Title;
			    DiscussionValue.Launch = discussion.Description;
			    DiscussionValue.Active = true;
			    DiscussionValue.ApprovalStatus = ContentBaseValue.ApprovalWFStatus.Approved;
			    DiscussionValue.ActualStatus = 1;			
			    DiscussionValue.DateCreate = DateTime.Now;
			    DiscussionValue.DateExpiry = DateTime.Now;
			    DiscussionValue.DateInsert = DateTime.Now;
			    DiscussionValue.DateUpdate = DateTime.Now;
                DiscussionValue.Display = true;			
			    DiscussionValue.Key.IdLanguage = discussion.langID;
			    DiscussionValue.ContentType.Key.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;  
			    DiscussionValue.IdContentSubType = ContentBaseValue.Subtypes.Content;
			    DiscussionValue.Indexable = true;			
			    DiscussionValue.DatePublish = DateTime.Now;
			    DiscussionValue.Rank = 0;

                UserBaseValue owner = new UserBaseValue();
                owner = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());

                ContentValue groupVal = new ContentValue();
                groupVal = objContentManager.Read(discussion.groupID, discussion.langID);
                int groupKey = groupVal.Key.PrimaryKey;
                 
                
                DiscussionValue.Owner.Key.Id = owner.Key.Id;
                DiscussionValue.Code = CreateFolder(DiscussionValue, groupKey);

			    ContentValue newDiscussion = objContentManager.Create(DiscussionValue);

			    if ((newDiscussion != null) && newDiscussion.Key.Id > 0) {
				    UserBaseValue Owner = new UserBaseValue();
				    Owner.Key.Id = owner.Key.Id;
				    Owner.Name = owner.Name;
				    Owner.Surname = owner.Surname;
				
				    //Accoppiamento sitearea gruppi------------------------
				    ContentSiteAreaValue oCoSaVal = new ContentSiteAreaValue();				    
                    oCoSaVal.KeyContent = newDiscussion.Key;
                    oCoSaVal.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id);
				    objContentManager.CreateContentSiteArea(oCoSaVal);
				    //-------------------------------------------------------
				
				    //Accoppiamento sito-----------------------------------
				    oCoSaVal.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
				    objContentManager.CreateContentSiteArea(oCoSaVal);
				    oCoSaVal = null;                    

				    //Accoppiamento discussione gruppo
				    ContentRelationValue oContRelValue = new ContentRelationValue();				    
                    oContRelValue.KeyContent.PrimaryKey = groupKey;
                    oContRelValue.KeyContentRelation = newDiscussion.Key;
                    oContRelValue.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;
				    objContentManager.CreateContentRelation(oContRelValue);

				    // HttpContext.Current.Response.Write("create6 ")

				    //Trace-------------------------------------------------
				    //Forzo la sitearea sul cotenuto
				    newDiscussion.SiteArea.Key.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;

				    TraceObject objTrace = new TraceObject();
				
				    objTrace.Content = newDiscussion;
				    objTrace.ContentId = newDiscussion.Key.Id;
				    objTrace.Description = "Creazione discussione per gruppo " + discussion.groupID + ":" + discussion.Title;
				    objTrace.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.AddDiscussione.Id;
				    objTrace.LanguageId = newDiscussion.Key.IdLanguage;
				    objTrace.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
				    objTrace.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Discussioni.Id;
				    objTrace.UserId = owner.Key.Id;
				    TraceHelper.TraceEvent(objTrace);
				    //-------------------------------------------------------								

				    //---------INVIO NOTIFICHE PUSH----------
				    //check ruolo utente
				    ProfilingManager roleMng = new ProfilingManager();
				    int _role = 0;

				    if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id))) {
					    _role = Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id;

				    } else if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id))) {
					    _role = Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id;

					    //Controllo per verificare che un utente group admin sia group admin anche per il gruppo corrente
					    objContentManager.Cache = false;
					    ContentUserSearcher oContUsSo = new ContentUserSearcher();					
					    oContUsSo.KeyUser.Id = owner.Key.Id;
					    oContUsSo.KeyContent.Id = discussion.groupID;
					    oContUsSo.KeyContent.IdLanguage = discussion.langID;
					    oContUsSo.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin;
					    if (objContentManager.CountContentUser(oContUsSo) == 0)
						    _role = Inclub.Web.Portal.Keys.RoleKeys.Member.Id;

				    } else if (roleMng.HasRole(new UserIdentificator(owner.Key.Id), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.Member.Id))) {
                        ContentUserSearcher oContUsSo = new ContentUserSearcher();
                        var _with1 = oContUsSo;
                        _with1.KeyUser.Id = owner.Key.Id;
                        _with1.KeyContent.Id = discussion.groupID;
                        _with1.KeyContent.IdLanguage = discussion.langID;
                        _with1.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.GrpMember;
                        if (objContentManager.CountContentUser(oContUsSo) == 0)
                            _role = Inclub.Web.Portal.Keys.RoleKeys.Member.Id;
				    }

				     //invio notifica                       
                    inClub.Web.Portal.Helper.NotificationHelper.DeliveryNotification(discussion.groupID, groupVal.Title, discussion.langID, DiscussionValue.Key.Id, discussion.Title, owner, _role);


                    //incremento il contatore delle discussioni a bordo del gruppo                    
                    Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                    dynamic command = new SqlCommand("Update pp_contents set contents_importants = ((cast(isNull(nullif(contents_importants,''),'0') as int)) + 1) where contents_key = " + groupKey, manager.GetConnection());
                    command.CommandType = CommandType.Text;
                    manager.OpenConnection();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    command = null;

                    return newDiscussion;

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }



        public static string CreateFolder(ContentValue discVal, int groupKey)
        {
            ContentManager objContentManager = new ContentManager();
            ContentValue folder = new ContentValue();
            folder.Display = true;
            folder.Active = true;
            folder.ApprovalStatus = ContentBaseValue.ApprovalWFStatus.Approved;           
            folder.DateCreate = System.DateTime.Now;
            folder.DateInsert = System.DateTime.Now;
            folder.DatePublish = System.DateTime.Now;
            folder.DateUpdate = System.DateTime.Now;

            folder.Owner.Key.Id = discVal.Owner.Key.Id;

            folder.Title = discVal.Title;
            folder.Launch = discVal.Launch;

            folder.Key.IdLanguage = discVal.Key.IdLanguage;
            folder.ContentType.Key.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.DocFolder.Id;
            folder.Code = "0";

            //it create the folder as content
            folder = objContentManager.Create(folder);

            //it associates the material with sitearea and site 
            if ((folder != null) && folder.Key.Id > 0)
            {
                ContentSiteAreaValue cSV = new ContentSiteAreaValue();
                cSV.KeyContent = folder.Key;
                cSV.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id;
                cSV.Priority = 1;

                objContentManager.CreateContentSiteArea(cSV);
                objContentManager.CreateContentSiteArea(folder.Key.Id, Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);

                cSV = null;

                //Accoppiamento cartella gruppo
                ContentRelationValue oContRelValue = new ContentRelationValue();                
                oContRelValue.KeyContent.PrimaryKey = groupKey;
                oContRelValue.KeyContentRelation = folder.Key;
                oContRelValue.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources;
                objContentManager.CreateContentRelation(oContRelValue);

              
                return folder.Key.Id.ToString();
            }
            else
            {
                return "0";
            }

        }



    }
}