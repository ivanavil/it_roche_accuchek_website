﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.MVC;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Utility;


namespace Inclub.Web.Portal.Controllers
{

    public class GuideController : Controller
    {

        public ActionResult Index()
        {
           
            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;            
            

            return View();
        }


        [Inclub.Web.Portal.Attribute.AuthoriseSiteAccessAttribute]
        public ActionResult Archive(short langId, int pagenum, int pagesize) 
        {
           
            var objContentManager = new ContentManager();
            var viewName = "_ItemGuide";

            ContentSearcher so = new ContentSearcher();

            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Title");
            so.Properties.Add("Copyright");
            so.Properties.Add("Code");
            so.Properties.Add("Launch");
            so.Properties.Add("DatePublish");            
            so.Properties.Add("SiteArea.Key.Id");
            so.Properties.Add("ContentType.Key.Id");
            so.Properties.Add("Qty");
            so.Properties.Add("IdContentSubCategory");

            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.GuidePol.Id;

            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Guide.Id;

            so.PageNumber = pagenum;
            so.PageSize = pagesize;

            so.SortingCriteria.Add(new ContentSort(1, ContentGenericComparer.SortType.ByPriorityRel));
            so.SortingCriteria.Add(new ContentSort(2, ContentGenericComparer.SortType.ByData, ContentGenericComparer.SortOrder.DESC));
           
            so.LoadOwnerDetails = true;
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
            so.Delete = SelectOperation.Enabled();

            objContentManager.Cache = false;
            ContentCollection materials = objContentManager.Read(so);

            List<inClub.Web.Portal.Models.Documenti> docObj = new List<inClub.Web.Portal.Models.Documenti>();
            inClub.Web.Portal.Models.Documenti material = null;
            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Documenti>();


            //System.Web.HttpContext.Current.Response.Write("selDoc:" + selDoc);

            if (materials != null && materials.Any())
            {
                modelColl.CurrentPage = pagenum;
                modelColl.TotalPagesCount = materials.PagerTotalNumber;
                modelColl.TotalItemsCount = materials.PagerTotalCount;

                foreach (ContentValue m in materials)
                {
                    material = GetDoc(m);
                  

                    docObj.Add(material);
                }

                modelColl.Items = docObj;

                return PartialView(viewName, modelColl);
            }


            return PartialView(viewName, null);
        }

          public static inClub.Web.Portal.Models.Documenti GetDoc(ContentValue vo)
          {
              DownloadManager objDownloadManager = new DownloadManager();
              DownloadSearcher sodw = new DownloadSearcher();
              sodw.Properties.Add("Title");
              sodw.Properties.Add("Launch");
              sodw.Properties.Add("DatePublish");
              sodw.Properties.Add("Copyright");
              sodw.Properties.Add("Code");
              sodw.Properties.Add("Qty");
              sodw.Properties.Add("IdContentSubCategory");

              sodw.key = vo.Key;
              sodw.key.IdLanguage = vo.Key.IdLanguage;
              sodw.KeyContentType = vo.ContentType.Key;
              sodw.KeySiteArea = vo.SiteArea.Key;

              sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

              objDownloadManager.Cache = false;
              DownloadCollection downColl = objDownloadManager.Read(sodw);

              if (downColl != null && downColl.Count > 0)
              {
                  DownloadValue downValue = new DownloadValue();
                  downValue = downColl[0];
                  
                      DownloadTypeValue dwnType = downValue.DownloadTypeCollection[0];
                      return ResourceAdapter(downValue, dwnType.DownloadType.Label, dwnType.Weight);
                 
              }              

              return null;

          }

          public static inClub.Web.Portal.Models.Documenti ResourceAdapter(DownloadValue dwnl, string estension, int weight)
          {

              if (dwnl != null)
              {

                  int folderId;
                  bool test = int.TryParse(dwnl.Code, out folderId);

                  inClub.Web.Portal.Models.Documenti resource = new inClub.Web.Portal.Models.Documenti();

                  resource.ResourceKey = dwnl.Key;
                  resource.CodeGetFile = SimpleHash.UrlEncryptRijndaelAdvanced(dwnl.Key.PrimaryKey.ToString());
                  resource.Name = dwnl.Title;
                  resource.Description = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(dwnl.Launch);
                  resource.Type = "file";
                  resource.Ext = estension;
                  
                  if (dwnl.DatePublish.HasValue)
                  {
                      resource.CreationDate = dwnl.DatePublish.Value.ToShortDateString();
                  }
                  else
                  {
                      resource.CreationDate = System.DateTime.Now.ToString();
                  }
                 
                  resource.NumDownload = dwnl.Qty;
                  resource.SizeKb = DocumentiHelper.roundObjectSize(weight.ToString());                 
                  resource.linkFile = "/Portal/_async/DownloadFile.ashx?intPk=" + resource.CodeGetFile;
                  return resource;
              }

              return null;
          }
    

    }
}
