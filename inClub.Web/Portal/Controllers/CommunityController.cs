﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.BL;
using Inclub.Web.Portal.Keys;
using Healthware.HP3.Core.Site.ObjectValues;
using System.Data.SqlClient;
using System.Data;
using Inclub.Web.Portal.Extensions;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.MVC;
using Inclub.Web.Portal.Helper;
using System.Configuration;
using Inclub.Web.Portal.Models;

namespace inClub.Web.Portal.Controllers
{
    public class CommunityController : Controller
    {
        //
        // GET: /Community/
        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info
          

            //Ricavo i Contenuti dei Box

            var valBox1 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("5");
            this.ViewBag.TitleBox1 = valBox1.Title;
            this.ViewBag.TestoBox1 = valBox1.FullText;

            var valBox2 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("6");
            this.ViewBag.TitleBox2 = valBox2.Title;
            this.ViewBag.TestoBox2 = valBox2.FullText;

            var valBox3 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("7");
            this.ViewBag.TitleBox3 = valBox3.Title;
            this.ViewBag.TestoBox3 = valBox3.FullText;

            var valBox4 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("4");
            this.ViewBag.TitleBox4 = valBox4.Title;
            this.ViewBag.TestoBox4 = valBox4.FullText;

            var manager = new AccessService();
            var ticket = manager.GetTicket();



            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Key.Id");
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            ViewBag.AccessWebinar = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.accWebinar2);

            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

            if (!isAdmin)
            {
                so.RelatedTo.ContentUser.LoadRelationDetails = true;
                so.RelatedTo.ContentUser.KeyUser.Id = userID;

            }

            ContentManager objContentManager = new ContentManager();
            objContentManager.Cache = false;

            ContentCollection groups = objContentManager.Read(so);

            // trace
            TraceHelper.TraceEvent(pageInfo.Key);
            //return View();

            if (groups != null && groups.Any())
            {
                ViewBag.listaGruppi = String.Join(",", groups.Select(cx => cx.Key.Id.ToString()).ToArray());
                return View();
            }
            else
            {
                //se non è associato al gruppo reindirizzo alla pagina dell'elenco dei gruppi
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(1)) + "?nogroup=true");
            }



           
        }


        public ActionResult lastFromGroup()
        {
            var manager = new AccessService();
            var ticket = manager.GetTicket();
            Boolean isAdmin = false;
            var HPLastUpdatesList = new List<inClub.Web.Portal.Models.HPLastUpdates>();
            inClub.Web.Portal.Models.HPLastUpdates modelColl;         
            var objContentManager = new ContentManager();
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;
            
            so.Properties.Add("Key.Id");
            so.Properties.Add("Key.PrimaryKey");
            so.Properties.Add("Key.IdLanguage");
            so.Properties.Add("Title");
            
            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByTitle;
            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);


            if (!isAdmin)
            {
                so.RelatedTo.ContentUser.LoadRelationDetails = true;
                so.RelatedTo.ContentUser.KeyUser.Id = userID;

            }

            ContentCollection groups = objContentManager.Read(so);

            if (groups != null && groups.Any())
            {
                    foreach (ContentValue grp in groups)
                    {
                        int grpKey = grp.Key.PrimaryKey;
                
                    modelColl = new inClub.Web.Portal.Models.HPLastUpdates();
                    modelColl.GroupId = grp.Key.Id;
                    modelColl.GroupName =grp.Title;

                    so = null;
                    so = new ContentSearcher();
                    so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

                    so.Properties.Add("Title");
                    so.Properties.Add("DateCreate");
                    so.Properties.Add("DatePublish");
                    so.Properties.Add("Owner.Key.Id");
                    so.Properties.Add("Owner.Name");
                    so.Properties.Add("Owner.Surname");
                    so.Properties.Add("Owner.Title");

                    so.LoadOwnerDetails = true;
                    so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;
                    so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;

                    so.PageNumber = 1;
                    so.PageSize = 3;

                    so.SortOrder = ContentGenericComparer.SortOrder.DESC;
                    so.SortType = ContentGenericComparer.SortType.ByData;

                    so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(grp.Key.Id, grp.Key.IdLanguage);
                    so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;


                    objContentManager.Cache = false;

                    ContentCollection discussions = objContentManager.Read(so);
                    List<inClub.Web.Portal.Models.Discussioni> discObj = new List<inClub.Web.Portal.Models.Discussioni>();
           
                    if (discussions != null && discussions.Any())
                    {

                        discObj = discussions.Select(toDiscussione).ToList();
                        //discObj.ToList().ForEach(c => { (c.GroupKey = new ContentIdentificator()).Id = grp.Key.Id; (c.GroupKey = new ContentIdentificator()).IdLanguage = grp.Key.IdLanguage; });
                        
                        discObj.ToList().ForEach(c => (c.GroupKey = new ContentIdentificator()).IdLanguage = grp.Key.IdLanguage);
                        discObj.ToList().ForEach(c => (c.GroupKey = new ContentIdentificator()).Id = grp.Key.Id);

                        modelColl.LastDiscussionsList = discObj;

                        inClub.Web.Portal.Models.Contributo comm;
                        List<inClub.Web.Portal.Models.Contributo> commList = new List<inClub.Web.Portal.Models.Contributo>();

                        string connStr = ConfigurationManager.AppSettings["conn-string"];
                        string spRead = "proc_ReadLastComment";
                        SqlDataAdapter adapter = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        using (SqlConnection sqlConn = new SqlConnection(connStr))
                        {
                            using (SqlCommand sqlCmd = new SqlCommand(spRead, sqlConn))
                            {
                                sqlConn.Open();
                                sqlCmd.Connection = sqlConn;
                                sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;

                                List<SqlParameter> sqlParams = new List<SqlParameter>();
                                sqlParams.Clear();
                                sqlParams.Add(new SqlParameter("@group_id", grpKey));

                                sqlCmd.Parameters.AddRange(sqlParams.ToArray());

                                adapter.SelectCommand = sqlCmd;
                                adapter.Fill(ds);

                                if ((ds != null) && (ds.Tables != null) && (ds.Tables.Count > 0))
                                {
                                    DataTable dt = ds.Tables[0];
                                    if ((dt.Rows != null) && (dt.Rows.Count > 0))
                                    {
                                        foreach (DataRow dr in dt.Rows)
                                        {

                                            comm = new inClub.Web.Portal.Models.Contributo();
                                            comm.PostKey = new CommentIdentificator();
                                            comm.PostKey.Id = (int)dr["comment_id"];
                                            comm.UserCover = Inclub.Web.Portal.Helper.CoverHelper.getUserImage((int)dr["comment_user_id"]);
                                            comm.Description = dr["contributo"].ToString();
                                            comm.GroupKey = new ContentIdentificator();
                                            comm.GroupKey.Id = (int)dr["group_id"];
                                            comm.GroupKey.IdLanguage = grp.Key.IdLanguage;
                                            comm.UserCreateName = dr["owner_name"].ToString();
                                            comm.UserCreate = new UserIdentificator();
                                            comm.UserCreate.Id = (int)dr["comment_user_id"];
                                            comm.DiscussionKey = new ContentIdentificator();
                                            comm.DiscussionKey.Id = (int)dr["id_discussione"];
                                            comm.DiscussionTitle = dr["discussione"].ToString();
                                            comm.FatherID = (int)dr["comment_father_id"];

                                            if ((int)dr["comment_father_id"] == 0)
                                            {
                                                comm.FatherID = (int)dr["comment_id"];
                                            }

                                            commList.Add(comm);                                        
                                        }

                                        modelColl.LastPostsList = commList;

                                    }
                                }

                            }
                        }

                    }
                    else
                    {
                        modelColl.LastDiscussionsList = null;
                        modelColl.LastPostsList = null;
                    }
                    HPLastUpdatesList.Add(modelColl);
                }
            }

            return PartialView(HPLastUpdatesList);
        }



        private Func<ContentValue, inClub.Web.Portal.Models.Discussioni> toDiscussione =
            x => new inClub.Web.Portal.Models.Discussioni
            {
                DiscussionKey = x.Key,
                Title = x.Title,
                Description = x.Launch,
                UserCreate = x.Owner.Key,
                UserCreateName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname,
                CreationDate = x.DateCreate.Value.ToShortDateString(),
                LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
                NumContributi = x.Qty,
                NumCommenti = Convert.ToInt32(x.Importants),
                FolderKey = x.Code
            };


        public ActionResult IndexOLD()
        {

            var manager = new AccessService();           
            var ticket = manager.GetTicket();

            

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Key.Id");
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            var roleMng = new ProfilingManager();
            int userID  = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);
            
            if (!isAdmin){
                    so.RelatedTo.ContentUser.LoadRelationDetails = true;
                    so.RelatedTo.ContentUser.KeyUser.Id = userID;
               
            }

            ContentManager objContentManager = new ContentManager();
            objContentManager.Cache = false;
        
            ContentCollection groups  = objContentManager.Read(so);
               
            if (groups != null && groups.Any())
            {
                ViewBag.AccessWebinar = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.accWebinar2);
                return View();                
            }
            else 
            {
                //se non è associato al gruppo reindirizzo alla pagina dell'elenco dei gruppi
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(1)) + "?nogroup=true");
            }
        }

        [HttpPost]
        public ActionResult RegistraWebinar()
        {
            try
            {
                var manager = new AccessService();
                var ticket = manager.GetTicket();
                var roleMng = new ProfilingManager();
                int userID = ticket.User.Key.Id;
                if (userID > 0)
                {
                    roleMng.CreateUserRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.accWebinar2);
                    this.Trace(Inclub.Web.Portal.Keys.TraceEventKeys.IscrizioneWebinar);
                    return Content("ok", "text/html");
                }
                else { return Content("Utente non esistente", "text/html"); }
                
            }
            catch (Exception ex)
            {
                return Content("Si è verificato un errore, riprovare." + ex.ToString(), "text/html");
            }
        }


        public ActionResult LastNews(string groupsList, int langId, string altText)
        {
            //Populate Searcher
            var soModel = new Inclub.Web.Portal.Models.GenericSearcherContentModel();
            soModel.PageSize = 2; // numbers of item visible in the page
            soModel.PageNumber = 1; //Start Page as const
            soModel.LanguageKey.Id = langId;
            soModel.LanguageId = langId;
            soModel.SiteAreaId = Inclub.Web.Portal.Keys.SiteAreaKeys.News.Id;
            soModel.ContenTypeId = Inclub.Web.Portal.Keys.ContentTypeKeys.News.Id;
            soModel.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.DigitalhealthNews.Id;
            soModel.SortType = ContentGenericComparer.SortType.ById;
            soModel.SortOrder = ContentGenericComparer.SortOrder.DESC;

            this.ViewBag.Searcher = soModel;

            var so = Inclub.Web.Portal.BL.SearcherBuilders.InitContentJsonSearcher(this.Request.RequestContext, Inclub.Web.Portal.BL.SearcherBuilders.DataLoadingType.List, soModel);
            var manager = new Inclub.Web.Portal.BL.GenericContent();
            var objModel = manager.GetArchive(so, langId, Inclub.Web.Portal.Keys.ThemeKeys.DigitalhealthNews.Id);

            if (null != objModel && objModel.Any())
            {

                return PartialView(objModel);
            }
            else
            {
                return Content(altText);
            }
        }

        public ActionResult LastVideos(string groupsList, int langId, string altText)
        {
            if (groupsList != "")  //deve essere !="", lo lascio così finchè non finisco in modo da far uscire il testo fisso che sta nel box1 ora
            {
                //prendere l'ultimo video per ognuno dei gruppi, fare una view apposita
                var objContentManager = new ContentManager();
                List<inClub.Web.Portal.Models.Interviste> intrvObj = new List<inClub.Web.Portal.Models.Interviste>();
                Array groups = groupsList.Split(',');

                    foreach (string groupStr in groups)
                    {

                        int groupId = 0;
                        int.TryParse(groupStr, out groupId);

                        ContentCollection contentsRel = objContentManager.ReadContentRelation(new ContentIdentificator(objContentManager.ReadPrimaryKey(groupId, langId)), new ContentRelationTypeIdentificator(Inclub.Web.Portal.Keys.RelationTypeKeys.Interviste));

                        if (contentsRel != null && contentsRel.Any()) { 

                            int idVideo = 0;
                            idVideo = (contentsRel.OrderByDescending(item => item.Key.Id).First()).Key.PrimaryKey;

                            if (idVideo != 0)
                            {

                                DownloadManager objDownloadManager = new DownloadManager();
                                DownloadSearcher sodw = new DownloadSearcher();
                                sodw.Properties.Add("Title");
                                sodw.Properties.Add("Key");                            

                                sodw.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Interviste.Id;
                                sodw.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Interviste.Id;
                                sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                                sodw.SortOrder = DownloadGenericComparer.SortOrder.DESC;
                                sodw.SortType = DownloadGenericComparer.SortType.ByData;
                                sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
                                sodw.Keys = idVideo.ToString();
                    
                                DownloadCollection interviste = objDownloadManager.Read(sodw);
                            
                                var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Interviste>();
                                inClub.Web.Portal.Models.Interviste intervista = null;

                                if (interviste != null && interviste.Any())
                                {
                       
                                    foreach (DownloadValue m in interviste)
                                    {
                                        intervista = GetResource(m, groupId);

                                        if (intervista != null)
                                            intrvObj.Add(intervista);

                                    }
                        
                                }
                            }
                        }
                    }

                return PartialView(intrvObj);

            }
            else
            {
                return Content(altText);
            }
            
        }

        public static inClub.Web.Portal.Models.Interviste GetResource(DownloadValue vo, int groupId)
        {
            inClub.Web.Portal.Models.Interviste resource = new inClub.Web.Portal.Models.Interviste();
            int widthVideo = 0;
            int.TryParse(vo.Copyright, out widthVideo);

            resource.IntervistaKey = vo.Key;
            resource.Title = vo.Title;           
            resource.GroupKey = new ContentIdentificator();
            resource.GroupKey.Id = groupId;

            return resource;
        }


        public ActionResult LastFromGroups()
        {

            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home" ; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            var manager = new AccessService();           
            var ticket = manager.GetTicket();

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

            so.Properties.Add("Key.Id");
            so.Properties.Add("Title");
            so.Properties.Add("Launch");
            so.Properties.Add("Copyright");
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("MetaKeywords");
            so.Properties.Add("MetaDescription");
            so.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
            so.Properties.Add("Qty"); //num di iscritti
            so.Properties.Add("Importants");  //num di discussioni
            so.Properties.Add("Code"); //num contributi
            so.Properties.Add("LinkLabel"); //num commenti
            so.Properties.Add("Copyright"); //blocco colori
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");

            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            so.PageNumber = 1;
            so.PageSize = 3;

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;
            so.LoadOwnerDetails = true;

            var roleMng = new ProfilingManager();
            int userID  = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);
            

            if (!isAdmin){
                    so.RelatedTo.ContentUser.LoadRelationDetails = true;
                    so.RelatedTo.ContentUser.KeyUser.Id = userID;
               
            }

            ContentManager objContentManager = new ContentManager();
            objContentManager.Cache = false;
        
            ContentCollection groups  = objContentManager.Read(so);
               
            List<inClub.Web.Portal.Models.Gruppo> groupsObj = new List<inClub.Web.Portal.Models.Gruppo>();

            if (groups != null && groups.Any())
            { 

                groupsObj = groups.Select(toGruppo).ToList();

                //se isAdmin tutti i Role diventano "admin"
                if (isAdmin)
                {
                    groupsObj.ToList().ForEach(c => c.Role = "admin");
                }

            return PartialView("_LastGroups", groupsObj);
            }

            
            return PartialView("_LastGroups", null);
        }


        public ActionResult OthersGroups()
        {      

            var manager = new AccessService();
            var ticket = manager.GetTicket();

            Boolean isAdmin = false;
            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
            isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

             if (!isAdmin)
            {
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;      
            so.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
         

            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            so.CalculatePagerTotalCount = false;           

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;
            so.LoadOwnerDetails = true;
            so.RelatedTo.ContentUser.LoadRelationDetails = true;
            so.RelatedTo.ContentUser.KeyUser.Id = userID;
          
            ContentManager objContentManager = new ContentManager();
            objContentManager.Cache = false;
            ContentCollection groups = objContentManager.Read(so);
            //List<inClub.Web.Portal.Models.Gruppo> groupsObj = new List<inClub.Web.Portal.Models.Gruppo>();
            string contentToEsclude = string.Empty;
                if (groups != null && groups.Any())
                {
                     
             
                 char[] charsToTrim = { ',' };
               
                     foreach (ContentValue cv in groups)              
                    {
                        contentToEsclude = string.Format("{0}{1},",contentToEsclude, cv.Key.PrimaryKey);
                    }

                     contentToEsclude = contentToEsclude.Trim(charsToTrim);
                }

                     var soT = new ContentSearcher();
                     soT.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                     soT.Properties.Add("Title");
                     soT.Properties.Add("Launch");
                     soT.Properties.Add("Copyright");
                     soT.Properties.Add("DateCreate");
                     soT.Properties.Add("DatePublish");
                     soT.Properties.Add("MetaKeywords");
                     soT.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id"); 
                     soT.Properties.Add("MetaDescription");                   
                     soT.Properties.Add("Qty"); //num di iscritti
                     soT.Properties.Add("Importants");  //num di discussioni
                     so.Properties.Add("Code"); //num contributi
                     so.Properties.Add("LinkLabel"); //num commenti
                     soT.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
                     soT.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;
                     soT.SetMaxRow = 2;
                     soT.CalculatePagerTotalCount = false;

                     soT.SortOrder = ContentGenericComparer.SortOrder.DESC;
                     soT.SortType = ContentGenericComparer.SortType.ByData;
                     soT.LoadOwnerDetails = true;
                     soT.RelatedTo.ContentUser.LoadRelationDetails = false;
                     soT.KeysToExclude = contentToEsclude;
                     ContentManager objContentManagerT = new ContentManager();
                     objContentManagerT.Cache = false;
                     ContentCollection groupsT = objContentManagerT.Read(soT);
                     List<inClub.Web.Portal.Models.Gruppo> groupsObjT = new List<inClub.Web.Portal.Models.Gruppo>();

                     if (groupsT != null && groupsT.Any())
                     {
                         groupsObjT = groupsT.Select(toOtherGroups).ToList();
                         return PartialView("_OthersGroups", groupsObjT);
                     }
                     return PartialView("_OthersGroups", null);
                }
                return PartialView("_OthersGroups", null);
          
        }
        

       private Func<ContentValue, inClub.Web.Portal.Models.Gruppo> toGruppo = 
           x => new inClub.Web.Portal.Models.Gruppo
            {
                GroupKey = x.Key,
                Name = x.Title,
                BlockColor = x.Copyright,
                Description = x.Launch,
                Admin = x.Owner.Key,
                AdminName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname, //x.MetaDescription,
                CreationDate = x.DateCreate.Value.ToShortDateString(),
                LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
                Role = ((x.RelatedTo.ContentUser.Relation.KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin) ? "admin" : ""),
                NumIscritti = x.Qty,
                NumDiscussioni = Convert.ToInt32(x.Importants),
                NumContributi = Convert.ToInt32((x.Code == "" ? "0" : x.Code)),
                NumCommenti = Convert.ToInt32((x.LinkLabel == "" ? "0" : x.LinkLabel))
            };


       private Func<ContentValue, inClub.Web.Portal.Models.Gruppo> toOtherGroups =
         x => new inClub.Web.Portal.Models.Gruppo
         {
             GroupKey = x.Key,
             Name = x.Title,
             BlockColor = x.Copyright,
             Description = x.Launch,
             Admin = x.Owner.Key,
             AdminName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname, //x.MetaDescription,
             CreationDate = x.DateCreate.Value.ToShortDateString(),
             LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
             //Role = ((x.RelatedTo.ContentUser.Relation.KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin) ? "admin" : ""),
             NumIscritti = x.Qty,
             NumDiscussioni = Convert.ToInt32(x.Importants),
             NumContributi = Convert.ToInt32((x.Code == "" ? "0" : x.Code)),
             NumCommenti = Convert.ToInt32((x.LinkLabel == "" ? "0" : x.LinkLabel))
         };





       public PartialViewResult ProponiGruppo()
       {
           ProponiGruppo model = new ProponiGruppo();

           return PartialView("_ProponiGruppo", model);
       }



       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult ProponiGruppo(ProponiGruppo model)
       {
           #region Page Info
           var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, ThemeKeys.Community);
           this.ViewBag.PageInfo = pageInfo;
           #endregion Page Info

           if (ModelState.IsValid)
           {                                              

               SendEmailNuovoGruppo(model, MailTemplateKeys.PropostaGruppo, "");
               return Content(DictionaryHelper.GetLabel(DictionaryKeys.sendNewGroupOk.ToString(), LabelKeys.LANGUAGE_PORTAL), "text/html");
           }
           else
           {
               return Content(DictionaryHelper.GetLabel(DictionaryKeys.sendNewGroupKo.ToString(), LabelKeys.LANGUAGE_PORTAL), "text/html");
           }



       }


       private void SendEmailNuovoGruppo(ProponiGruppo ModelP, MailTemplateIdentificator TemplateIDent, string Emaildestinatario)
       {
           UserLogged user = new UserLogged();
           user = UserHelper.readUserLogged();

           if (user.Key.Id > 0)
           {
               string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);
               Dictionary<string, string> replacing = new Dictionary<string, string>();
               var _description = "";
               if (ModelP.Description != null)
               {
                   _description = ModelP.Description.Replace(Environment.NewLine, "<br />").ToString();
               }

               replacing.Add(LabelKeys.em_NAME, user.Name);
               replacing.Add(LabelKeys.em_SURNAME, user.Surname);
               replacing.Add(LabelKeys.em_EMAIL, user.Email.ToString());
               replacing.Add(LabelKeys.em_CONTENT_TITLE, ModelP.Title.ToString());
               replacing.Add(LabelKeys.em_CONTENT_DESCRIPTION, _description);


               int langId = (int)LabelKeys.LANGUAGE_PORTAL;
               MailHelper.SendMail(TemplateIDent, langId, htmlTemplate, replacing, false, Emaildestinatario);
           }
       }






       [HttpPost]
       [Healthware.HP3.MVC.Attributes.AjaxOnly]
       public JsonResult SetContentUser(string ContentCrypt)
       {
           bool result=false;

        int contentID =0;
           int.TryParse(CryptHelper.urlDecrypt(ContentCrypt.ToString()),out contentID);

              var manager = new AccessService();
            var ticket = manager.GetTicket();

         
            int userID = ticket.User.Key.Id;
     

           if (contentID>0 && userID > 0 )
           {
               ContentManager objContentManager = new ContentManager();
               int groupKey = objContentManager.ReadPrimaryKey(contentID, 1);
                
               result = Inclub.Web.Portal.BL.UsersManage.CreateUserContent(userID, contentID,RelationTypeKeys.GrpMember);

               ProfilingManager roleMng = new ProfilingManager();
               if (!roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.Member))
                    roleMng.CreateUserRole(new UserIdentificator(userID), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.Member.Id));


               //incremento il contatore degli iscritti a bordo del gruppo                    
               Healthware.HP3.Core.Base.Persistent.SqlConnectionManager sqlManager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
               dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = ((cast(isNull(nullif(contents_qta_magazzino,''),'0') as int)) + 1) where contents_key = " + groupKey, sqlManager.GetConnection());
               command.CommandType = CommandType.Text;
               sqlManager.OpenConnection();
               command.ExecuteNonQuery();
               command.Dispose();
               command = null;

           }

          

       

           return Json(new { data = result}, JsonRequestBehavior.DenyGet);
       }


    
  

}

}
