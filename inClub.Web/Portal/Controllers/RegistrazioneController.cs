﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;
using System.Text.RegularExpressions;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.Utility;
using System.IO;

namespace Inclub.Web.Portal.Controllers
{
    public class RegistrazioneController : Controller
    {
        //
        // GET: /Registrazione/

        public ActionResult Index()
        {
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            //check sul codice, lo decripto, se è ok faccio compilare il form sennò lo reindirizzo di nuovo su accesscode
            string accessCode = "";
            accessCode = ((Request["ac"]!= null) ? SimpleHash.UrlDecryptRijndaelAdvanced(Request["ac"].ToString()) : "");

            //codice commentato per non esseree reindirizzati alla pagina del codice quando si vuole accedere al form di registrazione, decommentare se non si vuole far registrare qualsiasi utente
            if ((accessCode == "") || (!GenericHelper.checkAccessCode(accessCode)))
                return new RedirectResult(LinkHelper.ThemeLink(this.ControllerContext, ThemeKeys.AccessCode) + "?nac=true");


            if (UserHelper.getUserLoggedId() > 0)
            {
                return new RedirectResult(LinkHelper.ThemeLink(this.ControllerContext, ThemeKeys.ModificaDati));
            }

            //trace            
            TraceHelper.TraceEvent(pageInfo.Key);

            UserRegistration model = new UserRegistration();
            model.AcceptNotify = true;
            model.TermsAndConditions = true;
            model.AccessCode = accessCode;
            InitViewData(ref model);

            
            //se no o reindirizzo a accesscode o stampo un messaggio con link ad accesscode
            //se sì lo metto in un campo nascosto che poi passo al metodo per salvare l'utente, a buon esisto della registrazione flaggo il codice come utilizzato

            return View(model);
        }

        private void InitViewData(ref UserRegistration model)
        {
            var so = new ContextSearcher();
            so = new ContextSearcher();
            so.KeyContextGroup = ContextGroupKeys.SpecializzazioniRegistrationForm;
            List<SelectListItem> specItems = ContextHelper.GetContexts(so, "");
            model.Specializzazioni = new SelectList(specItems, "Value", "Text");
            so = null;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserRegistration model, HttpPostedFileBase file)
        {
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            Inclub.Web.Portal.Models.OperationResult operationResult = new Inclub.Web.Portal.Models.OperationResult();
            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;

            InitViewData(ref model);

            if (ModelState.ContainsKey("FirstName"))
                ModelState["FirstName"].Errors.Clear();
            if (ModelState.ContainsKey("LastName"))
                ModelState["LastName"].Errors.Clear();
            if (ModelState.ContainsKey("AcceptInfo"))
                ModelState["AcceptInfo"].Errors.Clear();


            if (ModelState.IsValid)
            {

                var captchaValid = false;
                try
                {
                    captchaValid = CaptchaGeneratorHelper.Validate(this.HttpContext, CaptchaKeys.RegistrationForm, model.Captcha);
                    if (!captchaValid)
                    {
                        ModelState.AddModelError("Captcha", DictionaryHelper.GetLabel(DictionaryKeys.CaptchaError));

                    }
                }
                catch (Exception ex)
                {

                    ModelState.AddModelError("Captcha", DictionaryHelper.GetLabel(DictionaryKeys.CaptchaError));
                }


                if (captchaValid)
                {


                    this.ViewBag.ShowMessage = true;
                    //registrazione

                    try
                    {
                        EnumeratorKeys.UserStatus userStatType;


                        //controllo che non esista un utente registrato con la stessa userid/email
                        userStatType = UserHelper.GetUserTypeByEmail(model.Email);
                        bool continueRegistration = true;
                        switch (userStatType)
                        {
                            case EnumeratorKeys.UserStatus.Active:
                                operationResult.Message = DictionaryHelper.GetLabel(string.Format(DictionaryKeys.regFormMsg, (int)EnumeratorKeys.UserStatus.Active));
                                operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;
                                continueRegistration = false;
                                break;
                            case EnumeratorKeys.UserStatus.Pending:
                                operationResult.Message = DictionaryHelper.GetLabel(string.Format(DictionaryKeys.regFormMsg, (int)EnumeratorKeys.UserStatus.Pending));
                                operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;
                                continueRegistration = false;
                                break;
                            case EnumeratorKeys.UserStatus.NoActive:
                                operationResult.Message = DictionaryHelper.GetLabel(string.Format(DictionaryKeys.regFormMsg, (int)EnumeratorKeys.UserStatus.NoActive));
                                operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;
                                //Email di approvazione
                                //rimando la email per attivazione utente
                                //RemindActivationEmail(model);
                                continueRegistration = false;
                                break;
                            case EnumeratorKeys.UserStatus.Disable:
                            case EnumeratorKeys.UserStatus.NotFound:


                                if (continueRegistration)
                                {

                                    //creazione utente

                                    UsersManage objUserManage = new UsersManage();
                                    var objCreatedHCP = objUserManage.CreateNewUser(model);

                                    //Salvo L'immagine del Profilo
                                    if (null != objCreatedHCP && objCreatedHCP.Key.Id > 0)
                                    {

                                        if (file != null)
                                        {
                                            var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "gif" };
                                            var fileName = Path.GetFileName(file.FileName);

                                            var ext = Path.GetExtension(file.FileName);
                                            if (allowedExtensions.Contains(ext)) //check what type of extension  
                                            {
                                                string myfile = string.Format("{0}{1}", CryptHelper.urlEncrypt(objCreatedHCP.Key.Id.ToString()), ext);
                                                string myfile_small = string.Format("{0}_100{1}", CryptHelper.urlEncrypt(objCreatedHCP.Key.Id.ToString()), ext);
                                                var path = Path.Combine(Server.MapPath("~//HP3Image/User/cover/"), myfile);
                                                var path_small = Path.Combine(Server.MapPath("~//HP3Image/User/cover/"), myfile);



                                                //Se esiste già Elimino la cover originale
                                                if (System.IO.File.Exists(path))
                                                {
                                                    System.IO.File.Delete(path);
                                                }
                                                //Se esiste già Elimino la cover originale
                                                if (System.IO.File.Exists(path_small))
                                                {
                                                    System.IO.File.Delete(path_small);
                                                }

                                                Image myfilename = new Bitmap(file.InputStream);
                                                Image img = Image.FromStream(file.InputStream);
                                                int actualWidth_small = img.Width;
                                                int actualHeight_small = img.Height;
                                                int MaxWidth_small = LabelKeys.UserCoverWidth;
                                                if (img.Width > MaxWidth_small)
                                                {
                                                    actualWidth_small = MaxWidth_small;

                                                    int originalHeight = myfilename.Height;
                                                    int originalWidth = myfilename.Width;
                                                    decimal proporzione = (decimal)actualWidth_small / originalHeight;
                                                    actualHeight_small = Convert.ToInt32(proporzione * originalWidth);
                                                }
                                                Bitmap Thumbnail2 = new Bitmap(myfilename, actualWidth_small, actualHeight_small);
                                                Thumbnail2.Save(path_small, ImageFormat.Jpeg);

                                                file.SaveAs(path);
                                            }
                                        }

                                        operationResult.Successful = true;
                                    }




                                    else
                                    {
                                        operationResult.Successful = false;
                                    }

                                    if (operationResult.Successful)
                                    {
                                        if (model.AccessCode != "")
                                        {
                                            ActivationSendEmail(objCreatedHCP, model, MailTemplateKeys.RegistrationDone, model.Email);
                                            //trace
                                            this.Trace(pageInfo.Key, TraceEventKeys.SubmitRegForm);
                                            //trace
                                            TraceHelper.TraceEvent(pageInfo.Key, TraceEventKeys.SubmitRegForm, objCreatedHCP.Key.Id);
                                            operationResult.Message = DictionaryHelper.GetLabel(DictionaryKeys.MsgRegistrationDone);
                                            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;
                                            
                                        }
                                        else {
 
                                            ActivationSendEmail(objCreatedHCP, model, MailTemplateKeys.RegistrationDoubleOptin, model.Email);
                                            //trace
                                            this.Trace(pageInfo.Key, TraceEventKeys.SubmitRegForm);
                                            //trace
                                            TraceHelper.TraceEvent(pageInfo.Key, TraceEventKeys.SubmitRegForm, objCreatedHCP.Key.Id);
                                            operationResult.Message = DictionaryHelper.GetLabel(DictionaryKeys.MsgDoubleOptinRegistration);
                                            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;
                                            //TODO: invio Email al responsabile per attivazione utente

                                        }

                                    }
                                    else
                                    {
                                        this.ViewBag.ShowMessage = true;
                                        operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Error;
                                        operationResult.Message = "Attenzione si è verificato un errore!<br> Riprovare più tardi.";
                                        ErrorLogHelper.NotifyDBLog(new Exception("Errore Creazione Utente"), "");
                                    }

                                }
                                break;
                        }

                        this.ViewBag.OperationResult = operationResult;
                    }
                    catch (Exception ex)
                    {
                        this.ViewBag.ShowMessage = true;
                        operationResult.Message = "Attenzione Si è verificato un errore!<br> Riprovare più tardi.";
                        operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Error;
                        this.ViewBag.OperationResult = operationResult;
                        ErrorLogHelper.NotifyDBLog(ex, "");
                    }

                } //end if Captcha Valid
            }
            else
            {
                this.ViewBag.ShowMessage = false;
                operationResult.Message = "Attenzione Si è verificato un errore!<br> Riprovare più tardi.";
                operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Error;
                this.ViewBag.OperationResult = operationResult;
                ErrorLogHelper.NotifyDBLog(new Exception("Errore Form di registrazione lato ModelState"), "");
            }
            model.Email = string.Empty;
            return View(model);
        }


        private void RemindActivationEmail(UserRegistration model)
        {
            UserBaseValue ubTemp = UserHelper.GetUserBaseValueByEmail(model.Email);
            UserHCPValue ubHcpTemp = new UserHCPValue();
            ubHcpTemp.Title = ubTemp.Title;
            ubHcpTemp.Name = ubTemp.Name;
            ubHcpTemp.Surname = ubTemp.Surname;
            ubHcpTemp.Email.Add(model.Email);
            ubHcpTemp.Key = ubTemp.Key;
            ReActivationSendEmail(ubHcpTemp);
            //clear  
            ubTemp = null;
            ubHcpTemp = null;
        }




        private void ActivationSendEmail(Healthware.HP3.Core.User.ObjectValues.UserHCPValue objCreatedHCP, UserRegistration ModelReg, MailTemplateIdentificator TemplateIDent, string Emaildestinatario)
        {
            if (null != objCreatedHCP)
            {
                string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);
                Dictionary<string, string> replacing = new Dictionary<string, string>();

                replacing.Add(LabelKeys.em_NAME, objCreatedHCP.Name);
                replacing.Add(LabelKeys.em_SURNAME, objCreatedHCP.Surname);
                replacing.Add(LabelKeys.em_EMAIL, objCreatedHCP.Email.ToString());


                replacing.Add(LabelKeys.em_LINK, getLinkConfirm(objCreatedHCP.Key.Id));
                int langId = (int)LabelKeys.LANGUAGE_PORTAL;
                MailHelper.SendMail(TemplateIDent, langId, htmlTemplate, replacing, false, Emaildestinatario);
            }
        }


        private void ReActivationSendEmail(Healthware.HP3.Core.User.ObjectValues.UserHCPValue objCreatedHCP)
        {
            if (null != objCreatedHCP)
            {
                string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);
                Dictionary<string, string> replacing = new Dictionary<string, string>();
                replacing.Add(LabelKeys.em_TITLE, objCreatedHCP.Title);
                replacing.Add(LabelKeys.em_NAME, objCreatedHCP.Name);
                replacing.Add(LabelKeys.em_SURNAME, objCreatedHCP.Surname);
                replacing.Add(LabelKeys.em_TELEPHONE, objCreatedHCP.Telephone.GetString);
                replacing.Add(LabelKeys.em_EMAIL, objCreatedHCP.Email.GetString);

                replacing.Add(LabelKeys.em_LINK, getLinkConfirm(objCreatedHCP.Key.Id));
                int langId = (int)LabelKeys.LANGUAGE_PORTAL;
                MailHelper.SendMail(MailTemplateKeys.RegistrationConfirm, langId, htmlTemplate, replacing, false, objCreatedHCP.Email.ToString());
            }
        }

        private string getLinkConfirm(int idUSer)
        {

            var url = string.Format(LabelKeys.LINK_PROTOCOL, GenericHelper.SiteDomain()) + LinkHelper.ThemeLink(ControllerContext, ThemeKeys.ConfermaRegistrazione);
            url = string.Format("{0}/?id={1}", url, CryptHelper.urlEncrypt(idUSer.ToString()));

            return url;

        }



        public PartialViewResult ResetPassword()
        {
            UserResetPassword model = new UserResetPassword();
            return PartialView("_ResetPassword", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(UserResetPassword model)
        {


            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, ThemeKeys.ModificaDati);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info
            EnumeratorKeys.UserStatus usrType = EnumeratorKeys.UserStatus.NotFound;
            UserValue uVal = null;
             var captchaValid = false;
            try
            {
                captchaValid = CaptchaGeneratorHelper.Validate(this.HttpContext, CaptchaKeys.RegistrationForm, model.Captcha);
                if (!captchaValid)
                {

                    return Content(DictionaryHelper.GetLabel(DictionaryKeys.CaptchaError), "text/html");
                  
                }
            }
            catch (Exception ex)
            {

                return Content(DictionaryHelper.GetLabel(DictionaryKeys.CaptchaError), "text/html");
            }


            if (captchaValid && ModelState.IsValid)
            {


            if (model.Email == null)
            {
                return Content("Inserire l'indirizzo email", "text/html");
            }
            else
            {             

                    if (ModelState.IsValid)
                    {
                        usrType = UserHelper.GetUserValueAndTypeByEmail(model.Email, out uVal);
                        TraceHelper.TraceEvent(ThemeKeys.Home, TraceEventKeys.ResetPassword, model.Email.ToLower());

                        switch (usrType)
                        {
                            case EnumeratorKeys.UserStatus.NotFound:
                                return Content(DictionaryHelper.GetLabel(string.Format(DictionaryKeys.recoveryPasswordMsg, ((int)EnumeratorKeys.UserStatus.NotFound).ToString()), LabelKeys.LANGUAGE_PORTAL), "text/html");
                            case EnumeratorKeys.UserStatus.Active:
                                UpdateForceChangePassword(uVal.Key.Id, true);
                                SendChangePasswordMail(uVal);
                                return PartialView("_ResetPasswordOK", null);
                            case EnumeratorKeys.UserStatus.Pending:
                                return Content(DictionaryHelper.GetLabel(string.Format(DictionaryKeys.recoveryPasswordMsg, ((int)EnumeratorKeys.UserStatus.Pending).ToString()), LabelKeys.LANGUAGE_PORTAL), "text/html");
                            case EnumeratorKeys.UserStatus.NoActive:
                                return Content(DictionaryHelper.GetLabel(string.Format(DictionaryKeys.recoveryPasswordMsg, ((int)EnumeratorKeys.UserStatus.NoActive).ToString()), LabelKeys.LANGUAGE_PORTAL), "text/html");
                            case EnumeratorKeys.UserStatus.Disable:
                                return Content(DictionaryHelper.GetLabel(string.Format(DictionaryKeys.recoveryPasswordMsg, ((int)EnumeratorKeys.UserStatus.Deleted).ToString()), LabelKeys.LANGUAGE_PORTAL), "text/html");
                            default:
                                break;
                        }

                        return PartialView("_ResetPasswordOK", null);
                        
                       

                    }
                    else
                    {
                        //Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                        ErrorLogHelper.NotifyDBLog(new Exception("Errore ModelState in modifica Reset Password!"), "");
                        return Content("Inserire un indirizzo email valido", "text/html");
                    }


                }
            }
            else
                return Content(DictionaryHelper.GetLabel(DictionaryKeys.CaptchaError, LabelKeys.LANGUAGE_PORTAL), "text/html");
        }

        private void UpdateForceChangePassword(int userId, bool force)
        {
            UserManager man = new UserManager();
            man.Update(new UserIdentificator(userId), "PasswordForceChange", force);
            CacheHelper.DeleteGroupCache(CacheKeys.GroupCacheUser);
        }

        private string getLinkForgotPassword(int idUser)
        {
            var url = string.Format(LabelKeys.LINK_PROTOCOL, GenericHelper.SiteDomain()) + LinkHelper.ThemeLink(ControllerContext, ThemeKeys.RecuperaPassword);
            url = string.Format("{0}/?{1}={2}", url, LabelKeys.UserIdSL_Generic, CryptHelper.urlEncrypt(idUser.ToString()));
            return url;
        }

        private void SendChangePasswordMail(UserValue user)
        {
            if (user != null)
            {
                int langId = LabelKeys.LANGUAGE_PORTAL;

                string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, (short)langId);
                Dictionary<string, string> replacing = new Dictionary<string, string>();
                replacing.Add(LabelKeys.em_LINK, getLinkForgotPassword(user.Key.Id));

                try
                {
                    MailHelper.SendMail(MailTemplateKeys.ForgotPasswordMail, langId, htmlTemplate, replacing, true, user.Email[0]);
                }
                catch (Exception ex)
                {
                    ErrorLogHelper.NotifyDBLog(ex, string.Empty);
                }
            }
        }


        }


    
}
