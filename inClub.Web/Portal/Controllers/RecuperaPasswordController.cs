﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC.Attributes;

using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;

namespace Inclub.Web.Portal.Controllers
{
    public class RecuperaPasswordController : Controller
    {
        //
        // GET: /RecuperaPassword/

        public ActionResult Index(int? ok)
        {
            UserResetPasswordForm model = new UserResetPasswordForm();
              model.Id = 0;
            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info
            
            //in modo da tracciare solo quando arrivo alla pagina e non quando ho effettuato il cambio password
            if ((!ok.HasValue) || (ok.Value != 1))
            {
                //trace
                Helper.TraceHelper.TraceEvent(pageInfo.Key);
                int userID = 0;

                if (Request.QueryString[LabelKeys.UserIdSL_Generic] != null)
                { 

                int.TryParse( CryptHelper.urlDecrypt(Request.QueryString[LabelKeys.UserIdSL_Generic]), out userID);
                if (userID > 0)
                {
                    model.Id = userID;
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return View("_IndexOK");
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(UserResetPasswordForm model)
        {
            Inclub.Web.Portal.Models.OperationResult operationResult = new Inclub.Web.Portal.Models.OperationResult();
            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;

            if (ModelState.IsValid)
            {
                if (model.Id>0)
                {
                    UsersManage manager = new UsersManage();
                    manager.UpdateHCPResetPassword(model.Id, model.Password);
                    Helper.TraceHelper.TraceEvent(Keys.ThemeKeys.RecuperaPassword, Keys.TraceEventKeys.ChangePassword);
                    operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;
                    operationResult = null;
                    return new RedirectResult(string.Format("{0}?ok={1}", Helper.LinkHelper.ThemeLink(this.ControllerContext, Keys.ThemeKeys.RecuperaPassword), 1));
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            else
            {
           
                Helper.ErrorLogHelper.NotifyDBLog(new Exception("Errore ModelState in Reset Password!"), "");
                return View("_IndexKO");
            }

        }

        private void UpdateForceChangePassword(int userId, bool force)
        {
            UserManager man = new UserManager();
            man.Update(new UserIdentificator(userId), "PasswordForceChange", force);

            Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);
        }

        


    }
}
