﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Controllers
{
    public class MenuController : Controller
    {
        //Get Header Menu
        public ActionResult Header()
        {

            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Set the root theme based on Authentication
            //If the user is authenticated and the request is Public Area /HomePage
            //Or user is not authenticated
            ThemeIdentificator _rootTheme = new ThemeIdentificator();
            _rootTheme = Keys.ThemeKeys.Home;

            //If the user is authenticated and the request is PrivateArea/HomePage
            //if (Helper.LoginHelper.IsAuthenticated() && Helper.ThemeHelper.getCurrentThemeIdentificator().Id.Equals(Keys.ThemeKeys.AreaRiservata.Id))
            //{
            //    _rootTheme = Keys.ThemeKeys.AreaRiservata;
            //}

            var tcoll = Enumerable.Empty<Healthware.HP3.Core.Site.ObjectValues.ThemeValue>();
            tcoll = Helper.ThemeHelper.getThemeMenuCollectionByFather(_rootTheme);

            List<ThemeModel.ThemeBaseModel> tCollTemp = null;

            //Nota: temi di primo livello
            if (tcoll != null && tcoll.Count() > 0)
            {
                tCollTemp = new List<ThemeModel.ThemeBaseModel>();

                foreach (ThemeValue th in tcoll)
                {
                    var objThemeBase = Adapters.ThemeAdapter.Convert(th);

                    tCollTemp.Add(objThemeBase);
                }
            }

            return PartialView("_HeaderMenu", tCollTemp); 
        }

        //Get Footer Menu
        public PartialViewResult Footer() 
        {

            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info


            var tcoll = Enumerable.Empty<Healthware.HP3.Core.Site.ObjectValues.ThemeValue>();

            tcoll = Helper.ThemeHelper.getThemeMenuCollectionByFather(Keys.ThemeKeys.Footer);

            List<ThemeModel.ThemeBaseModel> tCollTemp = null;

            //Nota: temi di primo livello
            if (tcoll != null && tcoll.Count() > 0)
            {
                tCollTemp = new List<ThemeModel.ThemeBaseModel>();

                foreach (ThemeValue th in tcoll)
                {
                    var objThemeBase = Adapters.ThemeAdapter.Convert(th);

                    tCollTemp.Add(objThemeBase);
                }
            }
            return PartialView("_FooterMenu", tCollTemp);
        }


        //Get Footer Menu
        public PartialViewResult Language()
        {

            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            var langscoll = Enumerable.Empty<Healthware.HP3.Core.Localization.ObjectValues.LanguageValue>();
            langscoll = Helper.LanguageHelper.GetSiteLanguages();

            if (null != langscoll && langscoll.Any())
            {
                return PartialView("_LanguagesMenu", langscoll);
            }

            return PartialView("_LanguagesMenu", null);
        }


        //Get Footer Menu
        public PartialViewResult Area()
        {

            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Set the root theme based on Authentication
            //If the user is authenticated and the request is Public Area /HomePage
            //Or user is not authenticated
            ThemeIdentificator themeKey = new ThemeIdentificator();
            themeKey = Keys.ThemeKeys.Home;

            //If the user is authenticated and the request is PrivateArea/HomePage
            //if (!Helper.LoginHelper.IsAuthenticated() || (Helper.LoginHelper.IsAuthenticated() && (null != pageInfo.Roles && pageInfo.Roles.HasRole(Keys.RoleKeys.PublicArea.Id))))
            //{
            //    themeKey = Keys.ThemeKeys.AreaRiservata;
            //}

            ThemeValue theme = Inclub.Web.Portal.BL.Shared.ReadTheme(this.Request.RequestContext, themeKey, Helper.LanguageHelper.GetLanguageFromUrl().Id);

            return PartialView("_AreaMenu", theme);
        }
    }
}