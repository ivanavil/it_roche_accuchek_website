﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Box.ObjectValues;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.Content;

//#1941
namespace Inclub.Web.Portal.Controllers
{
    public class BoxAndRelatedController : Controller
    {      

        
        public ContentSearcher getSearcher(SiteAreaIdentificator keySA, ContentTypeIdentificator keyCT)
        {
            var searcher = new ContentSearcher()
               {
                   Display = SelectOperation.Enabled(),
                   Active = SelectOperation.Enabled(),
                   Delete = SelectOperation.Enabled(),
                   KeySiteArea = keySA,
                   KeyContentType = keyCT,
                   KeySite = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite,
                   SortType = ContentGenericComparer.SortType.ByData,
                   SortOrder = ContentGenericComparer.SortOrder.DESC
               };

            var urlLang = LanguageHelper.GetLanguageFromUrl();
            if (null != urlLang)
                searcher.key.IdLanguage = (short)urlLang.Id;

            //LacyLoad
            searcher.CalculatePagerTotalCount = false;

            searcher.Properties.Add("Title");
            searcher.Properties.Add("Launch");

            return searcher;

        }

        // GET: /BoxResources/
        [HttpPost]
        [Healthware.HP3.MVC.Attributes.AjaxOnly]
        public JsonResult RelatedContents(int idContent, short LanguageId, int saKeyId, int ctKeyId)
        {

            ContentTypeIdentificator ctKey = ContentTypeHelper.GetContentTypeIdentificator();

            string cacheKey = string.Format( Inclub.Web.Portal.Keys.CacheKeys.BoxRelatedContent, idContent, LanguageId, saKeyId, ctKeyId);

            var objOperationResult = new OperationResult();
            string render = string.Empty;
            Boolean success = false;


            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                render = (string)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
                if (!string.IsNullOrEmpty(render))
                    success = true;

                objOperationResult.Message = "cache";
            }
            else
            {
                //Populate Searcher
                var so = BL.SearcherBuilders.InitRelatedContentSearcher(this.Request.RequestContext, LanguageId, idContent, new SiteAreaIdentificator(saKeyId), new ContentTypeIdentificator(ctKeyId));
                objOperationResult.Message = "NO cache";
                IEnumerable<GenericContentBox> coll = null;

                if (idContent > 0 && LanguageId > 0)
                {
                    var manager = new BL.BoxAndRelatedManage();
                    coll = manager.ReadRelatedContentBox(so, Inclub.Web.Portal.Keys.EnumeratorKeys.ContentRelationType.DefaultRelation);

                    if (coll != null && coll.Any())
                    {
                        success = true;
                    }
                }

                render = this.ControllerContext.RenderPartialToString("_jsonBoxRelatedContent", coll);

                CacheManager.Insert(cacheKey, render, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);

            }



            objOperationResult.Successful = success;
            objOperationResult.Result = render;

            return Json(new { data = objOperationResult }, JsonRequestBehavior.DenyGet);
        }


        // GET: /IEnumerable BoxResources/
        [HttpGet, ChildActionOnly]
        public PartialViewResult GetRelatedContents(int idContent, short LanguageId, int saKeyId, int ctKeyId)
        {
            string cacheKey = string.Format(Inclub.Web.Portal.Keys.CacheKeys.BoxRelatedContent, idContent, LanguageId, saKeyId, ctKeyId);
            IEnumerable<Models.GenericContentBox> objGenericContentsRelated = null;
            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                objGenericContentsRelated = (IEnumerable<Models.GenericContentBox>)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
            }
            else
            {
                var so = BL.SearcherBuilders.InitRelatedContentSearcher(this.Request.RequestContext, LanguageId, idContent);
                if (idContent > 0 && LanguageId > 0)
                {
                    var manager = new BL.BoxAndRelatedManage();
                    objGenericContentsRelated = manager.ReadRelatedContentBox(so, Keys.EnumeratorKeys.ContentRelationType.DefaultRelation);
                }
                CacheManager.Insert(cacheKey, objGenericContentsRelated, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);
            }
            return PartialView("_BoxRelatedContents", objGenericContentsRelated);
        }


        [HttpGet, ChildActionOnly]
        public PartialViewResult GetLastNews(int MaxRow)
        {
            string cacheKey = string.Format(Inclub.Web.Portal.Keys.CacheKeys.BoxLastNews, MaxRow, Keys.SiteAreaKeys.News, Keys.ContentTypeKeys.News);
            IEnumerable<Models.GenericContentBox> objGenericContents = null;
            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                objGenericContents = (IEnumerable<Models.GenericContentBox>)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
            }
            else
            {
                var so = BL.SearcherBuilders.InitLastNewsSearcher(this.Request.RequestContext, MaxRow);
               
                    var manager = new BL.GenericContentManage();
                    objGenericContents = manager.LastNews(so);
                
                CacheManager.Insert(cacheKey, objGenericContents, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);
            }
            return PartialView("_BoxLastNews", objGenericContents);
        }


       



       

    }
}
