﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC.Attributes;
using Healthware.HP3.Core.Utility;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;

namespace Inclub.Web.Portal.Controllers
{
    public class AccessCodeController : Controller
    {

        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            if (Request["nac"] == "true")
            {
                this.ViewBag.Message = "E' richiesto un codice di accesso valido per poter procedere alla registrazione.";
            }


            if ((Request["cd"] != null) && !(string.IsNullOrEmpty(Request["cd"])))
            {
                UserAccessCode model = new UserAccessCode();
                model.AccessCode = Request["cd"];

                return  GetUser(model);
            }
            else
            {
                // trace
                TraceHelper.TraceEvent(pageInfo.Key);
                UserAccessCode model = new UserAccessCode();
                model.AccessCode = "";
                return View(model);
            }

            
        }



        [HttpPost]
        public ActionResult GetUser(UserAccessCode model)
        {

            
            Inclub.Web.Portal.Models.OperationResult operationResult = new Inclub.Web.Portal.Models.OperationResult();
            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;

            if (ModelState.IsValid)
            {
                if (model.AccessCode != null)
                {

                    TicketValue userTicket = null;
                    UsersManage manager = new UsersManage();
                    UserHCPSearcher so = new UserHCPSearcher();
                    so.HCPRegistration = model.AccessCode;
                    UserAccessCode usB = new UserAccessCode();
                    usB = manager.GetUserByCode(so);
                    if (usB != null)
                    {
                        if (usB.Id > 0)
                        {

                            userTicket = Helper.LoginHelper.LogIn(usB.Username, usB.Password);
                            Helper.TraceHelper.TraceEvent(Keys.ThemeKeys.AccessCode, Keys.TraceEventKeys.AccessByCode);
                            if (Helper.LoginHelper.IsAuthenticated())
                            {
                                if (usB.MaritalStatus == 1)
                                {
                                    UserRegistration usR = new UserRegistration();
                                    usR.Id = usB.Id;
                                    //Se l'utente è stato importato e accede per la prima volta lo mando a modifica Dati
                                    return new RedirectResult(Helper.LinkHelper.ThemeLink(this.ControllerContext, Keys.ThemeKeys.ModificaDati));
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Home");
                                }


                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }

                        }

                    }
                    else
                    {
                        //controllo se esiste il codice nella tabella
                        //se esiste faccio il redirect al form di registrazione col codice criptato
                        if (GenericHelper.checkAccessCode(model.AccessCode))
                        {
                            return new RedirectResult((LinkHelper.ThemeLink(this.ControllerContext, ThemeKeys.Registrazione)) + "?ac=" + SimpleHash.UrlEncryptRijndaelAdvanced(model.AccessCode));
                        }
                        else
                        {                            
                            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;
                            operationResult = null;
                            return View("_IndexKO");
                        }
                        
                    }

                    operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Succeded;
                    operationResult = null;
                    return View("_IndexKO");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            else
            {

                Helper.ErrorLogHelper.NotifyDBLog(new Exception("Errore ModelState in AccessCode!"), "");
                return View("_IndexKO");
            }

        }

    }
}