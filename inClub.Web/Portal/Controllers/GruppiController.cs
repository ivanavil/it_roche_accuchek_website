﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;

namespace inClub.Web.Portal.Controllers
{
    public class GruppiController : Controller
    {
        //
        // GET: /Gruppi/

        public ActionResult IndexOLD(int pagenum = 1, int pagesize = 5, string viewName = "")
        {            
            var manager = new AccessService();
            var ticket = manager.GetTicket();

            ViewBag.Nominativo = ticket.User.Name + " " + ticket.User.Surname;

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Key.Id");
            so.Properties.Add("Title");
            so.Properties.Add("Launch");            
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("MetaKeywords");
            so.Properties.Add("MetaDescription");
            so.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
            so.Properties.Add("Qty"); //num di iscritti
            so.Properties.Add("Importants");  //num di discussioni
            so.Properties.Add("Code"); //num contributi
            so.Properties.Add("LinkLabel"); //num commenti
            so.Properties.Add("Copyright"); //blocco colore
            so.Properties.Add("IdContentSubCategory"); //gruppo aperto
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");

            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            so.PageNumber = pagenum;
            so.PageSize = pagesize;

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;
            so.LoadOwnerDetails = true;

            

            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);


            //if (!isAdmin)
            //{
            //    so.RelatedTo.ContentUser.LoadRelationDetails = true;
            //    so.RelatedTo.ContentUser.KeyUser.Id = userID;
            //}

            ContentManager objContentManager = new ContentManager();

            ContentCollection groups = objContentManager.Read(so);
            List<inClub.Web.Portal.Models.Gruppo> groupsObj = new List<inClub.Web.Portal.Models.Gruppo>();
            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Gruppo>();

            if (groups != null && groups.Any())
            {
                modelColl.CurrentPage = pagenum;
                modelColl.TotalPagesCount = groups.PagerTotalNumber;
                modelColl.TotalItemsCount = groups.PagerTotalCount;

                groupsObj = groups.Select(toGruppo).ToList();

                //se isAdmin tutti i Role diventano "admin"
                if (isAdmin)
                {
                    groupsObj.ToList().ForEach(c => c.Role = "admin");
                }

                modelColl.Items = groupsObj;

                if (viewName == "")
                {
                    return View(modelColl);
                }
                else
                {
                    return PartialView(viewName, modelColl);
                }
                
            }

            return View();
        }

        public ActionResult Index()
        {           
            var manager = new AccessService();
            var ticket = manager.GetTicket();
            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;

            var valBox2 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("6");
            this.ViewBag.TitleBox2 = valBox2.Title;
            this.ViewBag.TestoBox2 = valBox2.FullText;

            var valBox4 = Inclub.Web.Portal.Helper.ContentHelper.GetBoxHomeContent("4");
            this.ViewBag.TitleBox4 = valBox4.Title;
            this.ViewBag.TestoBox4 = valBox4.FullText;

            //Boolean isAdmin = false;
            //if (userID > 0)
            //    isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

            
                ViewBag.AccessWebinar = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.accWebinar2);
                return View();
                        
        }


        public ActionResult LastFromGroups()
        {

            var manager = new AccessService();
            var ticket = manager.GetTicket();

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

            so.Properties.Add("Key.Id");
            so.Properties.Add("Title");
            so.Properties.Add("Launch");            
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("MetaKeywords");
            so.Properties.Add("MetaDescription");
            so.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
            so.Properties.Add("Qty"); //num di iscritti
            so.Properties.Add("Importants");  //num di discussioni
            so.Properties.Add("Code"); //num contributi
            so.Properties.Add("LinkLabel"); //num commenti
            so.Properties.Add("Copyright"); //blocco colori
            so.Properties.Add("IdContentSubCategory"); //gruppo aperto
            so.Properties.Add("LinkUrl"); //gruppo del board
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");

            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            so.PageNumber = 1;
            so.PageSize = 0;

            so.SortOrder = ContentGenericComparer.SortOrder.DESC;
            so.SortType = ContentGenericComparer.SortType.ByData;
            so.LoadOwnerDetails = true;

            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);


            if (!isAdmin)
            {
                so.RelatedTo.ContentUser.LoadRelationDetails = true;
                so.RelatedTo.ContentUser.KeyUser.Id = userID;

            }

            ContentManager objContentManager = new ContentManager();
            objContentManager.Cache = false;

            ContentCollection groups = objContentManager.Read(so);

            List<inClub.Web.Portal.Models.Gruppo> groupsObj = new List<inClub.Web.Portal.Models.Gruppo>();

            if (groups != null && groups.Any())
            {
                string contentToEsclude = "";
                char[] charsToTrim = { ',' };

                foreach (ContentValue cv in groups)
                {
                    contentToEsclude = string.Format("{0}{1},", contentToEsclude, cv.Key.PrimaryKey);
                }

                contentToEsclude = contentToEsclude.Trim(charsToTrim);


                groupsObj = groups.Select(toGruppo).ToList();

                //se isAdmin tutti i Role diventano "admin"
                if (isAdmin)
                {
                    groupsObj.ToList().ForEach(c => c.Role = "admin");
                }
                else
                {


                    var soT = new ContentSearcher();
                    soT.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                    soT.Properties.Add("Title");
                    soT.Properties.Add("Launch");
                    soT.Properties.Add("Copyright");
                    soT.Properties.Add("DateCreate");
                    soT.Properties.Add("DatePublish");
                    soT.Properties.Add("MetaKeywords");
                    soT.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
                    soT.Properties.Add("MetaDescription");
                    soT.Properties.Add("IdContentSubCategory"); //gruppo aperto
                    soT.Properties.Add("Qty"); //num di iscritti
                    soT.Properties.Add("Importants");  //num di discussioni
                    soT.Properties.Add("Code"); //num contributi
                    soT.Properties.Add("LinkLabel"); //num commenti
                    soT.Properties.Add("LinkUrl"); //gruppo del board
                    soT.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
                    soT.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;                    
                    soT.CalculatePagerTotalCount = false;

                    soT.SortOrder = ContentGenericComparer.SortOrder.DESC;
                    soT.SortType = ContentGenericComparer.SortType.ByData;
                    soT.LoadOwnerDetails = true;
                    soT.RelatedTo.ContentUser.LoadRelationDetails = false;
                    soT.KeysToExclude = contentToEsclude;
                    ContentManager objContentManagerT = new ContentManager();
                    objContentManagerT.Cache = false;
                    ContentCollection groupsT = objContentManagerT.Read(soT);
                    List<inClub.Web.Portal.Models.Gruppo> groupsObjT = new List<inClub.Web.Portal.Models.Gruppo>();


                    if (groupsT != null && groupsT.Any())
                    {
                        groupsObjT = groupsT.Select(toGruppo).ToList();
                        groupsObjT.ToList().ForEach(c => c.Role = "none");

                        groupsObj.AddRange(groupsObjT);
                    }

                }

                return PartialView("_LastGroups", groupsObj);
            }
            else
            {
                var soT = new ContentSearcher();
                    soT.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                    soT.Properties.Add("Title");
                    soT.Properties.Add("Launch");
                    soT.Properties.Add("Copyright");
                    soT.Properties.Add("DateCreate");
                    soT.Properties.Add("DatePublish");
                    soT.Properties.Add("MetaKeywords");
                    soT.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
                    soT.Properties.Add("MetaDescription");
                    soT.Properties.Add("Qty"); //num di iscritti
                    soT.Properties.Add("Importants");  //num di discussioni
                    soT.Properties.Add("Code"); //num contributi
                    soT.Properties.Add("LinkLabel"); //num commenti
                    soT.Properties.Add("IdContentSubCategory"); //gruppo aperto
                    soT.Properties.Add("LinkUrl"); //gruppo del board
                    soT.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
                    soT.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;                    
                    soT.CalculatePagerTotalCount = false;

                    soT.SortOrder = ContentGenericComparer.SortOrder.DESC;
                    soT.SortType = ContentGenericComparer.SortType.ByData;
                    soT.LoadOwnerDetails = true;
                    soT.RelatedTo.ContentUser.LoadRelationDetails = false;                    
                    ContentManager objContentManagerT = new ContentManager();
                    objContentManagerT.Cache = false;
                    ContentCollection groupsT = objContentManagerT.Read(soT);
                    List<inClub.Web.Portal.Models.Gruppo> groupsObjT = new List<inClub.Web.Portal.Models.Gruppo>();


                    if (groupsT != null && groupsT.Any())
                    {
                        groupsObjT = groupsT.Select(toGruppo).ToList();
                        groupsObjT.ToList().ForEach(c => c.Role = "none");

                        groupsObj.AddRange(groupsObjT);
                    }

                
                return PartialView("_LastGroups", groupsObj);
            }


            return PartialView("_LastGroups", null);
        }

       

        private Func<ContentValue, inClub.Web.Portal.Models.Gruppo> toGruppo =
            x => new inClub.Web.Portal.Models.Gruppo
            {
                GroupKey = x.Key,
                Name = x.Title,
                Description = x.Launch,
                BlockColor = x.Copyright,
                GrpAperto = x.IdContentSubCategory,
                Admin = x.Owner.Key,
                AdminName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname, //x.MetaDescription,
                isBoard = (x.LinkUrl.ToLower() == "board"),
                CreationDate = x.DateCreate.Value.ToShortDateString(),
                LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
                Role = ((x.RelatedTo.ContentUser.Relation != null) ? "" : ((x.RelatedTo.ContentUser.Relation.KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin) ? "admin" : "member")),
                NumIscritti = x.Qty,
                NumDiscussioni = Convert.ToInt32(x.Importants),
                NumContributi = Convert.ToInt32((x.Code == "" ? "0" : x.Code)),
                NumCommenti = Convert.ToInt32((x.LinkLabel == "" ? "0" : x.LinkLabel))
            };


        public ActionResult LastVideos(int langId, int type, string altText)
        {
            
                            List<inClub.Web.Portal.Models.Interviste> intrvObj = new List<inClub.Web.Portal.Models.Interviste>();

                            DownloadManager objDownloadManager = new DownloadManager();
                            DownloadSearcher sodw = new DownloadSearcher();
                            sodw.Properties.Add("Title");
                            sodw.Properties.Add("Key");
                            //sodw.Properties.Add("IdContentSubCategory");

                            sodw.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Interviste.Id;
                            sodw.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Interviste.Id;
                            sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                            sodw.SortOrder = DownloadGenericComparer.SortOrder.DESC;
                            sodw.SortType = DownloadGenericComparer.SortType.ByData;
                            sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
                            //sodw.IdContentSubCategory = 1; //1 webinar
                            sodw.KeyContext.Id = type;
                            sodw.PageSize = 3;
                            sodw.PageNumber = 1;

                            //sodw.Keys = idVideo.ToString();

                            DownloadCollection interviste = objDownloadManager.Read(sodw);

                            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Interviste>();
                            inClub.Web.Portal.Models.Interviste intervista = null;

                            if (interviste != null && interviste.Any())
                            {

                                foreach (DownloadValue m in interviste)
                                {
                                    intervista = GetResource(m, 0);

                                    if (intervista != null) {
                                        intervista.Type = type;
                                        intrvObj.Add(intervista);
                                    }

                                }

                            }
                            else
                            {
                                return Content(altText);
                            }     

                return PartialView(intrvObj);
            

        }



        public static inClub.Web.Portal.Models.Interviste GetResource(DownloadValue vo, int groupId)
        {
            inClub.Web.Portal.Models.Interviste resource = new inClub.Web.Portal.Models.Interviste();
            int widthVideo = 0;
            int.TryParse(vo.Copyright, out widthVideo);

            resource.IntervistaKey = vo.Key;
            resource.Title = vo.Title;
            resource.GroupKey = new ContentIdentificator();
            resource.GroupKey.Id = groupId;

            return resource;
        }


    }
}
