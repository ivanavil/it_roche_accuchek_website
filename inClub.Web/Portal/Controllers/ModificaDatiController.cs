﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.BL;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;


namespace Inclub.Web.Portal.Controllers
{
    public class ModificaDatiController : Controller
    {
        //
        // GET: /ModificaDati/

        public ActionResult Index()
        {
            if (Helper.UserHelper.HaveAccess())
            {
                #region Page Info
                var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());
                this.ViewBag.PageInfo = pageInfo;
                #endregion Page Info

                //trace            
                Helper.TraceHelper.TraceEvent(pageInfo.Key);

                int userId = Helper.UserHelper.getUserLoggedId();
                UserRegistration model = new UserRegistration();

                if (userId > 0)
                {
                    UserHCPSearcher so = new UserHCPSearcher();
                    so.Key.Id = userId;
                    so.Status = (int)EnumeratorKeys.UserStatus.Active;
                    BL.UsersManage Manager = new BL.UsersManage();

                    model = Manager.ReadHcpEditUser(so);
                    InitViewData(ref model);
                }

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");

            }
        }

        private void InitViewData(ref UserRegistration model)
        {
            var so = new ContextSearcher();
            so = new ContextSearcher();
            so.KeyContextGroup = ContextGroupKeys.SpecializzazioniRegistrationForm;
            List<SelectListItem> specItems = ContextHelper.GetContexts(so, "");
            model.Specializzazioni = new SelectList(specItems, "Value", "Text");
            so = null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserRegistration model, HttpPostedFileBase file)
        {

            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, ThemeKeys.ModificaDati);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info




            Inclub.Web.Portal.Models.OperationResult operationResult = new Inclub.Web.Portal.Models.OperationResult();
            operationResult.StatusCode = (int)EnumeratorKeys.StatusCode.Failed;
            operationResult.Successful = false;


            if (Helper.LoginHelper.IsAuthenticated())
            {
                this.ViewBag.ShowMessage = true;


                if (ModelState.ContainsKey("Captcha"))
                    ModelState["Captcha"].Errors.Clear();


                if (model.MaritalStatus == 1)
                {
                    //Se è un utente importato
                }

                else
                { 

                    if (ModelState.ContainsKey("Password"))
                        ModelState["Password"].Errors.Clear();              

                    if (ModelState.ContainsKey("ConfirmPassword"))
                        ModelState["ConfirmPassword"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsResponsabilita"))
                        ModelState["TermsResponsabilita"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsMedical"))
                        ModelState["TermsMedical"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsMedical1"))
                        ModelState["TermsMedical1"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsMedical2"))
                        ModelState["TermsMedical2"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsMedical3"))
                        ModelState["TermsMedical3"].Errors.Clear();

                    if (ModelState.ContainsKey("TermsMedical4"))
                        ModelState["TermsMedical4"].Errors.Clear();
                }
                    
                    

                 if (ModelState.ContainsKey("TermsAndConditions"))
                    ModelState["TermsAndConditions"].Errors.Clear();
                 
                

                if (ModelState.ContainsKey("ConfirmPassword"))
                    ModelState["ConfirmPassword"].Errors.Clear();
                if (ModelState.ContainsKey("FirstName"))
                    ModelState["FirstName"].Errors.Clear();
                if (ModelState.ContainsKey("LastName"))
                    ModelState["LastName"].Errors.Clear();
                if (ModelState.ContainsKey("AcceptInfo"))
                    ModelState["AcceptInfo"].Errors.Clear();

                if (ModelState.IsValid)
                {
                    model.Id = Helper.UserHelper.getUserLoggedId();
                    int retVal = UsersManage.UpdateUser(model);

                    if (retVal == (int)Keys.EnumeratorKeys.StatusCode.Succeded)
                    {

                        //Salvo L'immagine del Profilo


                        if (file != null)
                        {
                            var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "gif" };
                            var fileName = Path.GetFileName(file.FileName);

                            var ext = Path.GetExtension(file.FileName);
                            if (allowedExtensions.Contains(ext)) //check what type of extension  
                            {
                                string myfile = string.Format("{0}{1}", CryptHelper.urlEncrypt(model.Id.ToString()), ext);
                                string myfile_small = string.Format("{0}_100{1}", CryptHelper.urlEncrypt(model.Id.ToString()), ext);
                                var path = Path.Combine(Server.MapPath("~//HP3Image/User/cover/"), myfile);
                                var path_small = Path.Combine(Server.MapPath("~//HP3Image/User/cover/"), myfile_small);
                                //string path_small  = string.Format("\HP3Image\User\cover\{0}",myfile_small);

                                //Se esiste già Elimino la cover originale
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                                //Se esiste già Elimino la cover originale
                                if (System.IO.File.Exists(path_small))
                                {
                                    System.IO.File.Delete(path_small);
                                }

                                Image myfilename = new Bitmap(file.InputStream);
                                Image img = Image.FromStream(file.InputStream);
                                int actualWidth_small = img.Width;
                                int actualHeight_small = img.Height;
                                int MaxWidth_small = LabelKeys.UserCoverWidth;
                                if (img.Width > MaxWidth_small)
                                {
                                    actualWidth_small = MaxWidth_small;

                                    int originalHeight = myfilename.Height;
                                    int originalWidth = myfilename.Width;
                                    decimal proporzione = (decimal)actualWidth_small / originalWidth;
                                    actualHeight_small = Convert.ToInt32(proporzione * originalHeight);
                                }
                                Bitmap Thumbnail2 = new Bitmap(myfilename, actualWidth_small, actualHeight_small);
                                Thumbnail2.Save(path_small, ImageFormat.Jpeg);

                                file.SaveAs(path);
                            }
                        }


                        Helper.TraceHelper.TraceEvent(pageInfo.Key, Keys.TraceEventKeys.UpdateRegForm);

                        operationResult.Successful = true;
                        operationResult.StatusCode = (int)Keys.EnumeratorKeys.StatusCode.Succeded;
                        operationResult.Message = Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.UpdateOk, Keys.LabelKeys.LANGUAGE_PORTAL);
                    }
                    else
                    {
                        this.ViewBag.ShowMessage = false;
                        operationResult.Message = Helper.DictionaryHelper.GetLabel("L'email inserita appartiene ad un utente già registrato");
                        operationResult.StatusCode = (int)Keys.EnumeratorKeys.StatusCode.Error;
                    }
                }
                else
                {
                    this.ViewBag.ShowMessage = false;
                    operationResult.Message = Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.GenericError);
                    operationResult.StatusCode = (int)Keys.EnumeratorKeys.StatusCode.Error;
                    Helper.ErrorLogHelper.NotifyDBLog(new Exception("Si è Verificato un Errore"), "");
                }
            }
            this.ViewBag.OperationResult = operationResult;
            InitViewData(ref model);

            return View(model);
        }


        public PartialViewResult ChangeAvatar()
        {
            UserAvatar model = new UserAvatar();
            model.Id = Helper.UserHelper.getUserLoggedId();
            model.Success = false;
            return PartialView("_ChangeAvatar", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeAvatar(UserAvatar model)
        {


            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, ThemeKeys.ModificaDati);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            string file = model.Avatar;

             if (model.Id >0)
            {

                 string myfile_small = string.Format("{0}_100.jpg", CryptHelper.urlEncrypt(model.Id.ToString()));
                 var path = Path.Combine(Server.MapPath("~//HP3Image/User/cover/"), myfile_small);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
              
            if (file != null && file.Trim() !=string.Empty)
            {
                
             
               string base64 = file;
                Char delimiter = ',';
                string substrings = base64.Split(delimiter)[1];
                byte[] bytes = Convert.FromBase64String(substrings);
                using (FileStream stream = new FileStream(path, FileMode.Create)) {
	                stream.Write(bytes, 0, bytes.Length);
	                stream.Flush();
                }
                   
                }
            }
            
                    
                        //Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                        //Helper.ErrorLogHelper.NotifyDBLog(new Exception("Errore ModelState in modifica password!"), "");
                        //return Content("Inserire almeno 6 caratteri nel campo password!", "text/html");
                        model.Success = true;
                        return PartialView("_ChangeAvatar",model);
            
            
        }



        public PartialViewResult ChangePassword()
        {
            UserChangePassword model = new UserChangePassword();
        
            return PartialView("_ChangePassword", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserChangePassword model)
        {


            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, ThemeKeys.ModificaDati);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info


            if (model.Password == null || model.ConfirmPassword == null || model.OldPassword == null)
            {
                return Content("Compilare tutti i campi!", "text/html");
            }
            else
            {
                if (!model.Password.ToLower().Equals(model.ConfirmPassword.ToLower()))
                {
                    return Content("Le password non coincidono!", "text/html");
                }
                else
                {

                    if (ModelState.IsValid)
                    {

                        if (Helper.LoginHelper.IsAuthenticated())
                        {

                            var idUserLogged = Helper.UserHelper.getUserLoggedId();
                            UserSearcher so = UsersManage.InitChangePasswordUserSearcher();
                            so.Key.Id = idUserLogged;

                            var user = UsersManage.ReadChangePasswordUser(so);

                            if (null == user)
                            {
                                //Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                                Helper.ErrorLogHelper.NotifyDBLog(new Exception("Utente Non trovato in modifica password!"), "");
                                return Content("Siamo spiacenti, si e\' verificato un errore riprovare piu\' tardi!", "text/html");
                            }


                            if (!model.OldPassword.ToLower().Equals(user.Password.ToLower()))
                            {
                                return Content("La vecchia password non è corretta!", "text/html");
                            }
                            else
                            {

                                UsersManage manager = new UsersManage();
                                manager.UpdateHCPResetPassword(idUserLogged, model.Password);


                                //trace
                                Helper.TraceHelper.TraceEvent(pageInfo.Key, Keys.TraceEventKeys.ChangePassword);
                                return PartialView("_ChangePasswordOK", null);
                            }

                        }
                        else
                        {
                            //Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                            Helper.ErrorLogHelper.NotifyDBLog(new Exception("Sessione scaduta in modifica password!"), "");
                            return Content("Sessione Scaduta!", "text/html");
                        }

                    }
                    else
                    {
                        //Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                        Helper.ErrorLogHelper.NotifyDBLog(new Exception("Errore ModelState in modifica password!"), "");
                        return Content("Inserire almeno 6 caratteri nel campo password!", "text/html");
                    }


                }
            }
        }


    }
}