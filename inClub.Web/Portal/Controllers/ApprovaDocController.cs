﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.Utility;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.BL;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Healthware.HP3.Core.Site.ObjectValues;
using inClub.Web.Portal.Helper;
using Inclub.Web.Portal.Keys;

namespace inClub.Web.Portal.Controllers
{
    public class ApprovaDocController : Controller
    {
        //
        // GET: /Documenti/

        public ActionResult Index(int groupId, short langId)
        {
            #region Page Info
            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;
            this.ViewBag.groupId = groupId;
            this.ViewBag.langId = langId;

            string role = "member";
            var roleMng = new ProfilingManager();
            bool isAdmin = false;

            //controllo se l'utente loggato è sysadmin
            var objContentManager = new ContentManager();
            UserBaseValue user = new UserBaseValue();
            user = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
            isAdmin = roleMng.HasRole(user.Key, Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

            //se non lo è controllo se può accedere al gruppo, e leggo il ruolo che ha sul gruppo della discussione
            if (!isAdmin)
            {
                ContentUserCollection cuc = new ContentUserCollection();
                ContentUserSearcher oContUsSo = new ContentUserSearcher();
                oContUsSo.KeyUser.Id = user.Key.Id;
                oContUsSo.KeyContent.Id = groupId;
                oContUsSo.KeyContent.IdLanguage = langId;

                cuc = objContentManager.ReadContentUser(oContUsSo);

                if (cuc != null && cuc.Any())
                {
                    if (cuc[0].KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin)
                    {
                        role = "admin";
                    }

                }
                else
                {
                    //se non è associato al gruppo reindirizzo alla pagina dell'elenco dei gruppi
                    return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
                }
            }
            else
            {
                role = "admin";
            }

            this.ViewBag.role = role;


            #endregion Page Info

            return PartialView("Index"); 
        }

        public ActionResult Archive(int groupId, short langId, int folderId, int onlyResources, int pagenum, int pagesize, int commentid = 0, string viewName = "", string docid = "0")
        {
            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            

            int selDoc = 0;

            this.ViewBag.groupId = groupId;
            //this.ViewBag.folderId = folderId;

            if ((docid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(docid), out selDoc))){
                this.ViewBag.selDocID = selDoc;
            }                
            else
            {
                this.ViewBag.selDocID = 0;
            }
                

            if (groupId <= 0)
            {
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

            var objContentManager = new ContentManager();
            

            if (viewName == "")
                viewName = "_ItemApprovaDoc";
            
            ContentSearcher so = new ContentSearcher();
                
                so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                so.Properties.Add("Title");
                so.Properties.Add("Copyright");
                so.Properties.Add("Code");
                so.Properties.Add("Launch");
                so.Properties.Add("DatePublish");
                so.Properties.Add("Owner.Key.Id");
                so.Properties.Add("Owner.Name");
                so.Properties.Add("Owner.Surname");
                so.Properties.Add("Owner.Title");
                so.Properties.Add("SiteArea.Key.Id");
                so.Properties.Add("ContentType.Key.Id");
                so.Properties.Add("Qty");
                so.Properties.Add("IdContentSubCategory");

                so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id;

                if (selDoc != 0)
                    so.key = new ContentIdentificator(selDoc, langId);
                
                //so.Code = folderId.ToString();
                so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.ToBeApproved;
                //so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;

                so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Documenti.Id; //+ (onlyResources == 0 ? "," + Inclub.Web.Portal.Keys.ContentTypeKeys.DocFolder.Id : "");

                so.PageNumber = pagenum;
                so.PageSize = pagesize;
                
                so.SortingCriteria.Add(new ContentSort(1, ContentGenericComparer.SortType.ByPriorityRel));
                so.SortingCriteria.Add(new ContentSort(2, ContentGenericComparer.SortType.ByData, ContentGenericComparer.SortOrder.DESC));

                //if (folderId > 0)
                //{
                //    so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(folderId, langId);
                //    so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources; 
                //}else{
                so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
                so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources; 
                //}
                              


                so.LoadOwnerDetails = true;
                
                so.Delete = SelectOperation.Enabled();


                objContentManager.Cache = false;
                ContentCollection materials = objContentManager.Read(so);

                List<inClub.Web.Portal.Models.Documenti> docObj = new List<inClub.Web.Portal.Models.Documenti>();
                inClub.Web.Portal.Models.Documenti material = null;
                var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Documenti>();

                if (materials != null && materials.Any())
                {
                    modelColl.CurrentPage = pagenum;
                    modelColl.TotalPagesCount = materials.PagerTotalNumber;
                    modelColl.TotalItemsCount = materials.PagerTotalCount;

                    foreach (ContentValue m in materials){
                        material = Inclub.Web.Portal.Helper.DocumentiHelper.GetResource(m, m.Owner, groupId, commentid);

                        if (material.FolderID > 0) {
                            material.Folder = objContentManager.Read(new ContentIdentificator(material.FolderID, langId)).Title;
                        }

                        docObj.Add(material);
                    }

                    modelColl.Items = docObj;


                    return PartialView(viewName, modelColl);
                }


                return PartialView(viewName, null);
        }

     
        public PartialViewResult ModificaDocumento()
        {
            inClub.Web.Portal.Models.ModificaDocumento model = new inClub.Web.Portal.Models.ModificaDocumento();

            int groupID = 0;
            int.TryParse(Request["gId"].ToString(), out groupID);

            int docID = 0;
            int.TryParse(Request["dId"].ToString(), out docID);

            int langID = 0;
            int.TryParse(Request["lId"].ToString(), out langID);


            model.ID = docID;
            model.langID = (short)langID;
            model.groupID = groupID;

            InitViewData(ref model);
            return PartialView("_ModificaDocumento", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificaDocumento(inClub.Web.Portal.Models.ModificaDocumento model)
        {
            #region Page Info
            var pageInfo = Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Keys.ThemeKeys.Discussioni);
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            bool checkCreate = false;

            

            if (model.Title == null)
            {
                return Content("Inserire il Nome del documento!", "text/html");
            }
            else if (model.Description == null)
            {
                return Content("Inserire la descrizione!", "text/html");
            }
            else
            {

                if (ModelState.IsValid)
                {
                    checkCreate = ModDocumento(model);

                    if (checkCreate)
                    {
                        
                            return Content("Dati del documento modificato correttamente", "text/html");
                        
                    }
                    else
                    {
                        return Content("Si è verificato un'errore nella creazione del documento", "text/html");
                    }

                }
                else
                {
                    return Content("Si è verificato un'errore nella creazione del documento", "text/html");
                }
            }
        }

        private void InitViewData(ref inClub.Web.Portal.Models.ModificaDocumento model)
        {

            List<SelectListItem> discItems = GetDiscussions(model.groupID, model.langID);
            if (discItems != null)
            { 
                model.Discussion = new SelectList(discItems, "Value", "Text");
                    
                var objContentManager = new ContentManager();
                objContentManager.Cache = false;
                ContentSearcher so = new ContentSearcher();
                so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                so.Properties.Add("Title");
                so.Properties.Add("Code");
                so.Properties.Add("Launch");
                so.Properties.Add("DatePublish");
                so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Documenti.Id;
                so.key = new ContentIdentificator(model.ID, model.langID);
                so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Documenti.Id;
                so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.All;

                ContentCollection materials = objContentManager.Read(so);
                if (materials != null && materials.Any())
                {
                    model.SelectedDiscId = materials[0].Code;
                    model.Title = materials[0].Title;
                    model.Description = materials[0].Launch;
                    model.folderID = materials[0].Code;
                    model.ID = materials[0].Key.PrimaryKey;
                }
            
            }

            
        }

        public static List<SelectListItem> GetDiscussions(int groupId, int langId)
        {
            List<SelectListItem> temp = null;
            
            ContentSearcher so = new ContentSearcher();
            ContentManager objContentManager = new ContentManager();

            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.Properties.Add("Title");
            so.Properties.Add("Key");
            so.Properties.Add("Code");
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Discussioni.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Discussioni.Id;
            so.PageNumber = 0;
            so.PageSize = 0;
            so.SortingCriteria.Add(new ContentSort(1, ContentGenericComparer.SortType.ByTitle));
            so.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
            so.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Discussioni;            
            so.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
            so.Delete = SelectOperation.Enabled();

            ContentCollection coll = objContentManager.Read(so);
                        
            if (coll != null && coll.Any())
            {
                temp = coll.OrderBy(item => item.Title)
                        .Select(item =>
                            new SelectListItem
                            {
                                Text = item.Title,
                                Value = item.Code
                            }).ToList();


                SelectListItem firstItem = new SelectListItem();
                firstItem.Value = "0";
                firstItem.Text = string.Format("- {0} -", "Seleziona una discussione");
                temp.Insert(0, firstItem);
                SelectListItem newItem = new SelectListItem();
                newItem.Value = "";
                newItem.Text = string.Format("{0}", "--- Crea una nuova Discussione ---");                
                temp.Insert(1, newItem);
              
            }

            else
            {

                temp = new List<SelectListItem>();

                SelectListItem firstItem = new SelectListItem();
                firstItem.Value = "0";
                firstItem.Text = string.Format("- {0} -", "Seleziona una discussione");
                temp.Insert(0, new SelectListItem { Value = firstItem.Value, Text = firstItem.Text });

                SelectListItem newItem = new SelectListItem();
                newItem.Value = "";
                newItem.Text = string.Format("{0}", "--- Crea una nuova Discussione ---");
                temp.Insert(1, new SelectListItem { Value = newItem.Value, Text = newItem.Text });
        
            }

            //clear
            objContentManager = null;
            coll = null;

            return temp;
        }

        [HttpPost]
        [Healthware.HP3.MVC.Attributes.AjaxOnly]
        public JsonResult AbilitaDoc(int groupId, short langId, int docId)
        {
            bool result = false;
            try
            {
                ContentManager objcntMan = new ContentManager(); 
                ContentSearcher so = new ContentSearcher();
                ContentValue dwnVal = new ContentValue();

                int docKey = objcntMan.ReadPrimaryKey(docId, langId);

                so.Properties.Add("Title");                
                so.Properties.Add("Code");                
                so.Properties.Add("DatePublish");
                so.Properties.Add("Owner.Key.Id");
                so.Properties.Add("Owner.Name");
                so.Properties.Add("Owner.Surname");                

                so.key.PrimaryKey = docKey;
                so.LoadOwnerDetails = true;
                ContentCollection materials = objcntMan.Read(so);

                if (materials != null && materials.Any())
                {
                    dwnVal = materials[0];


                    objcntMan.Update(dwnVal.Key, "ApprovalStatus", 1);

                    //Trace-------------------------------------------------
                    TraceObject objTrace = new TraceObject();
                    var _with6 = objTrace;
                    _with6.Content = null;
                    _with6.ContentId = dwnVal.Key.Id;
                    _with6.Description = "Approvazione Documento";
                    _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.AprvDocumento.Id;
                    _with6.LanguageId = dwnVal.Key.IdLanguage;
                    _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                    _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                    _with6.UserId = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId()).Key.Id;
                    TraceHelper.TraceEvent(objTrace);
                    //------------------------------------------------------

                    string FolderName = string.Empty;

                    int selectDF = 0;
                    int.TryParse(dwnVal.Code, out selectDF);

                    if (selectDF > 0) { 
                        int selectDF_PK = objcntMan.ReadPrimaryKey(selectDF, langId);

                        Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino + 1) where contents_key = " + selectDF_PK, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;

                        FolderName = (objcntMan.Read(new ContentIdentificator(selectDF_PK))).Title;
                    }
                   


                    ContentValue groupVal = objcntMan.Read(groupId, langId);

                    //invia notifica
                    NotificationHelper oNotificationHelper = new NotificationHelper();
                    NotificationData oNotData = new NotificationData();

                    oNotData.Notification = notificationAction.NewAddedResource;
                    oNotData.NotifiedId = dwnVal.Key.Id;
                    oNotData.NotifiedName = dwnVal.Title;
                    oNotData.NotifyingUserEmail = String.Empty; //Email utente che ha com piuto l'azione (non indispensabile)
                    oNotData.NotifyingUserFullName = dwnVal.Owner.Name.ToUpper() + " " + dwnVal.Owner.Surname.ToUpper();
                    oNotData.NotifyingUserId = dwnVal.Owner.Key.Id;
                    oNotData.FolderName = ((FolderName != "") ? FolderName : "Cartella principale");
                    oNotData.GroupId = groupId;
                    oNotData.GroupName = groupVal.Title;
                    oNotData.NotifyToMembers = true;
                    oNotData.NotifyToAdmin = true;
                    oNotData.NotifiedIdLanguage = langId;
                    //Url della consolle per gestire l'approvazione
                    oNotData.GroupUrl = string.Format("{0}://{1}{2}", System.Web.HttpContext.Current.Request.Url.Scheme.ToString(), System.Web.HttpContext.Current.Request.Url.Host.ToString(), NotificationHelper.ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(oNotData.GroupId, oNotData.NotifiedIdLanguage), new LanguageIdentificator(1), oNotData.GroupName, "detail", SimpleHash.EncryptRijndaelAdvanced(oNotData.NotifiedId.ToString()), "documenti"));

                    NotificationHelper.DeliveryNotification(oNotData);
            
                    //todo: inviare una mail di notifica approvazione doc al membro che ha postato il documento
                    string mailOwner = "";
                    UserManager usMng = new UserManager();
                    mailOwner = usMng.Read(dwnVal.Owner.Key).Email.ToString();

                    if (!string.IsNullOrEmpty(mailOwner))
                        SendNotificaMembro(oNotData, false, mailOwner);


                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                result = false;
            }

            return Json(result , JsonRequestBehavior.DenyGet);
        }


        private void SendNotificaMembro(NotificationData oNotData, bool throwOn, string mailTo)
        {

            string htmlTemplate = DictionaryHelper.GetLabel(DictionaryKeys.htmlMailTemplate, LabelKeys.LANGUAGE_PORTAL);
            Dictionary<string, string> replacing = new Dictionary<string, string>();

            replacing.Add("[ID]", oNotData.NotifiedId.ToString());
            replacing.Add("[TITLE]", oNotData.NotifiedName);
            replacing.Add("[GROUPID]", oNotData.GroupId.ToString());
            replacing.Add("[GROUPNAME]", oNotData.GroupName);
            replacing.Add("[USERID]", oNotData.NotifyingUserId.ToString());
            replacing.Add("[USERNAME]", oNotData.NotifyingUserFullName);


            int langId = (int)LabelKeys.LANGUAGE_PORTAL;
            MailHelper.SendMail(MailTemplateKeys.NotificaDocApproved, langId, htmlTemplate, replacing, false, mailTo);

        }

        private bool ModDocumento(inClub.Web.Portal.Models.ModificaDocumento doc)
        {
            ContentValue newDisc = null;
            if ((doc.SelectedDiscId == null) || (doc.SelectedDiscId == ""))
            {
                Models.AddDiscussione discussion = new Models.AddDiscussione();

                discussion.ID = doc.ID;
                discussion.Title = doc.TitleNewDiscussion;
                discussion.Description = doc.DescriptionNewDiscussion;
                discussion.groupID = doc.groupID;
                discussion.langID = doc.langID;

                newDisc = Controllers.DiscussioniController.CreaDiscussione(discussion);

                doc.SelectedDiscId = newDisc.Code;
            }

            Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
            ContentManager objcntMan = new ContentManager();
            ContentValue groupVal = new ContentValue();

            DownloadManager dwnlMng = new DownloadManager();           
            DownloadValue dwnVal = new DownloadValue();
            
            dwnVal = dwnlMng.Read(new ContentIdentificator(doc.ID));

            dwnVal.Title = doc.Title.Trim();
            dwnVal.Launch = doc.Description.Trim();            
            dwnVal.Code = doc.SelectedDiscId;            
            dwnVal.Key.PrimaryKey = doc.ID;

            DownloadValue newDownVal = new DownloadValue();
            newDownVal = dwnlMng.Update(dwnVal);
            if ((newDownVal != null) && newDownVal.Key.Id > 0)
            {               

                int selectDF = 0;
                int.TryParse(doc.SelectedDiscId, out selectDF);
                if ((selectDF != 0) && (doc.SelectedDiscId != doc.folderID))
                {
                    int selectDF_PK =  objcntMan.ReadPrimaryKey(selectDF, doc.langID);

                    //Accoppiamento con la folder
                    ContentRelationValue oContRelValue = new ContentRelationValue();
                    var _with5 = oContRelValue;
                    _with5.KeyContent.PrimaryKey = selectDF_PK;
                    _with5.KeyContentRelation = newDownVal.Key;
                    _with5.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.ResourceFolder;
                    objcntMan.CreateContentRelation(oContRelValue);

                    if (newDownVal.ApprovalStatus == ContentBaseValue.ApprovalWFStatus.Approved)
                    {
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino + 1) where contents_key = " + selectDF_PK, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;
                    }
                }
                
                //se doc.folderID>0 e selectDF= 0 allora va cancellata l'associzione con la vecchia folder
                if ((selectDF == 0) && (doc.folderID != "0"))
                {
                    int.TryParse(doc.folderID, out selectDF);
                    int selectDF_PK = objcntMan.ReadPrimaryKey(selectDF, doc.langID);

                    ContentRelationValue oContRelValue = new ContentRelationValue();
                    var _with5 = oContRelValue;
                    _with5.KeyContent.PrimaryKey = selectDF_PK;
                    _with5.KeyContentRelation = newDownVal.Key;
                    _with5.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.ResourceFolder;
                    objcntMan.RemoveContentRelation(oContRelValue);

                    if (newDownVal.ApprovalStatus == ContentBaseValue.ApprovalWFStatus.Approved)
                    {
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (contents_qta_magazzino - 1) where contents_key = " + selectDF_PK, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;
                    }
                }

                //Trace-------------------------------------------------
                TraceObject objTrace = new TraceObject();
                var _with6 = objTrace;
                _with6.Content = null;
                _with6.ContentId = newDownVal.Key.Id;
                _with6.Description = "Modifica documento";
                _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.ModDocumento.Id;
                _with6.LanguageId = newDownVal.Key.IdLanguage;
                _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                _with6.UserId = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId()).Key.Id; 
                TraceHelper.TraceEvent(objTrace);
                //------------------------------------------------------
                               
            

                //FileHelper.Move(Inclub.Web.Portal.Keys.LabelKeys.TEMP_DOWNLOAD_PATH, Inclub.Web.Portal.Keys.LabelKeys.DOWNLOAD_PATH, doc.tempFile, targetFileName);

                dwnlMng.Cache = false;
            

                return true;    
            }

            return false;
        }





    }
}
