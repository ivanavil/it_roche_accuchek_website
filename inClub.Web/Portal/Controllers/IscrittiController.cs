﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;

namespace inClub.Web.Portal.Controllers
{
    public class IscrittiController : Controller
    {
       public static int idGroup = 0;
       public static short idLang = 0;

        //
        // GET: /Iscritti/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int userId)
        {
             UserHCPSearcher so = new UserHCPSearcher();
            
            so.Status = 1;
            so.SiteStatus = 1;         
            so.Properties.Add("Name");
            so.Properties.Add("Surname");
            so.Properties.Add("Email.GetString");            
            so.Properties.Add("HCPAddress.GetString");
            so.Properties.Add("HCPStructure");
            so.Properties.Add("HCPWard");
            so.Properties.Add("HCPTelephone.GetString");
            so.Properties.Add("HCPCellular.GetString");
            so.Properties.Add("HCPFax.GetString");
            so.Properties.Add("HCPMoreInfo.GetString");
            so.Properties.Add("HCPOtherInfo.GetString");
            so.Properties.Add("Title");
            so.Properties.Add("Cellular.GetString");
            so.Properties.Add("Fax.GetString");
            so.Properties.Add("HCPProfessionalCodeGeo.Id");

            so.Key.Id = userId;                      
           
            UserHCPManager objHCPManager = new UserHCPManager();
            
            UserHCPCollection coll = objHCPManager.Read(so);
            inClub.Web.Portal.Models.Iscritti usVal = new inClub.Web.Portal.Models.Iscritti();

            if (coll != null && coll.Any())
            {
                usVal = (coll.Select(toUser).ToList())[0];

                return PartialView("_popupUser", usVal);
            }
            
                return PartialView("_popupUser", null);
            
        }




        public ActionResult Archive(int groupId, short langId, int pagenum, int pagesize)
        {

            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            if (groupId <= 0)
            {                
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

            idGroup = groupId;
            idLang = langId;

            UserHCPSearcher so = new UserHCPSearcher();
            so.InclusiveOf.LoadContexts = true;
            so.Status = 1;
            so.SiteStatus = 1;         
            so.Properties.Add("Name");
            so.Properties.Add("Surname");
            so.Properties.Add("Email.GetString");            
            so.Properties.Add("HCPAddress.GetString");
            so.Properties.Add("HCPStructure");
            so.Properties.Add("HCPWard");
            so.Properties.Add("HCPTelephone.GetString");
            so.Properties.Add("HCPCellular.GetString");
            so.Properties.Add("HCPFax.GetString");
            so.Properties.Add("HCPMoreInfo.GetString");
            so.Properties.Add("HCPOtherInfo.GetString");
            so.Properties.Add("Title");
            so.Properties.Add("Cellular.GetString");
            so.Properties.Add("Fax.GetString");
            so.Properties.Add("HCPProfessionalCodeGeo.Id");


            so.RelatedTo.Content.Key = new ContentIdentificator(groupId, langId);
            so.RelatedTo.Content.RelationSearch = RelationTypeSearch.AllTypes();
            so.RelatedTo.Content.Active = SelectOperation.Enabled();
            so.RelatedTo.Content.Delete = SelectOperation.Enabled();
            //so.RelatedTo.Content.LoadRelationDetails = true;

            so.PageNumber = pagenum;
            so.PageSize = pagesize;
            so.SortType = UserGenericComparer.SortType.BySurName;
            so.SortOrder = UserGenericComparer.SortOrder.ASC;

           
            UserHCPManager objHCPManager = new UserHCPManager();
            
            UserHCPCollection coll = objHCPManager.Read(so);
            List<inClub.Web.Portal.Models.Iscritti> usObj = new List<inClub.Web.Portal.Models.Iscritti>();
            var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Iscritti>();

            
            if (coll != null && coll.Any()){

               modelColl.CurrentPage = pagenum;
               modelColl.TotalPagesCount = coll.PagerTotalNumber;
               modelColl.TotalItemsCount = coll.PagerTotalCount;

               usObj = coll.Select(toUser).ToList();
               //usObj.ToList().ForEach(c => (c.GroupKey = new ContentIdentificator()).Id = groupId);

               modelColl.Items = usObj;

               return PartialView("_ItemIscritti", modelColl);

            }
            else
            {
                return PartialView("_ItemIscritti", modelColl);
            }

        }

        //todo: ruolo su gruppo
        private Func<UserHCPValue, inClub.Web.Portal.Models.Iscritti> toUser =
            x => new inClub.Web.Portal.Models.Iscritti
            {
                UserID = x.Key,
                Name = x.Name,
                Surname = x.Surname,
                Photo = Inclub.Web.Portal.Helper.CoverHelper.getUserImage(x.Key.Id),
                Email = x.Email.ToString(),
                Specialty = getSpecialty(x.HCPProfessionalCodeGeo.Id),
                Hospital = x.HCPStructure,                
                City = x.HCPCity,
                NumContributi = (x.Cellular.ToString() == "" ? "0" : x.Cellular.ToString()),
                NumCommenti = (x.Fax.ToString() == "" ? "0" : x.Fax.ToString()),
                LinkedinLink = x.HCPMoreInfo.ToString(),
                SkypeLink = x.HCPTelephone.ToString(),
                TwitterLink = x.HCPOtherInfo.ToString(),
                Role = getUserRole(x.Key.Id)
            };

        static string getUserRole(int idUser)
        {
            string role = "Member";
            var roleMng = new ProfilingManager();
            bool isAdmin = false;

            //controllo se l'utente loggato è sysadmin
            var objContentManager = new ContentManager();
            isAdmin = roleMng.HasRole(new UserIdentificator(idUser), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

            //se non lo è controllo se può accedere al gruppo, e leggo il ruolo che ha sul gruppo della discussione
            if (!isAdmin)
            {
                ContentUserCollection cuc = new ContentUserCollection();
                ContentUserSearcher oContUsSo = new ContentUserSearcher();
                oContUsSo.KeyUser.Id = idUser;
                oContUsSo.KeyContent.Id = idGroup;
                oContUsSo.KeyContent.IdLanguage = idLang;

                cuc = objContentManager.ReadContentUser(oContUsSo);

                if (cuc != null && cuc.Any())
                {
                    if (cuc[0].KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin)
                    {
                        role = "Admin";
                    }

                }                
            }
            else
            {
                role = "SysAdmin";
            }

            return role;
        }

        static string getSpecialty(int idSpec)
        {
            if (idSpec > 0) { 
                ContextManager manager = new ContextManager();
                ContextValue ctxVal = manager.Read(new ContextIdentificator(idSpec));
                if (ctxVal != null)
                {
                    return ctxVal.Description;
                }
            }
            return "";
        }


    }
}
