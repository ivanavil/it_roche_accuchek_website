﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.BL;
using System.Data.SqlClient;
using System.Data;
using inClub.Web.Portal.Helper;
using Inclub.Web.Portal.Keys;

namespace inClub.Web.Portal.Controllers
{
    public class IntervisteController : Controller
    {
        //
        // GET: /Interviste/

        public ActionResult Index()
        {
            #region Page Info
            int type = 0;
            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;
            this.ViewBag.groupId = 0;
            this.ViewBag.langId = 1;
            #endregion Page Info


            if (Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator().Id == ThemeKeys.Video.Id)
            {
                this.ViewBag.title = "Video interviste";
                type = ContextGroupKeys.CtxInterviste.Id;
            }
            else
            {
                this.ViewBag.title = "Webinar";
                type = ContextGroupKeys.CtxWebinar.Id;
            }

            this.ViewBag.typeId = type;

            return View(); 
        }


        public ActionResult Detail(int id)
        {
            int type = 0;
            if (Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator().Id == ThemeKeys.Video.Id)
            {
                this.ViewBag.title = "Video interviste";
                type = ContextGroupKeys.CtxInterviste.Id;
            }
            else
            {
                this.ViewBag.title = "Webinar";
                type = ContextGroupKeys.CtxWebinar.Id;
            }
            this.ViewBag.typeId = type;


            int intervistaId = id;
            short langId = 1;
            var objContentManager = new ContentManager();
            string role = "member";

            if (intervistaId <= 0)
            {
                return Redirect("/");
            }
            

            var manager = new AccessService();
            var ticket = manager.GetTicket();

            DownloadManager objDownloadManager = new DownloadManager();
            DownloadSearcher sodw = new DownloadSearcher();
            sodw.Properties.Add("Title");
            sodw.Properties.Add("Launch");
            sodw.Properties.Add("DatePublish");
            sodw.Properties.Add("Copyright"); //width del video
            sodw.Properties.Add("Importants"); //num commenti
            sodw.Properties.Add("Qty"); //num contributi
            sodw.Properties.Add("Code"); //primarykey della Folder creata automaticamente per la discussione

            sodw.key = new ContentIdentificator(intervistaId, langId);
            sodw.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Interviste.Id;
            sodw.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Interviste.Id;
            sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;                        
            //sodw.RelatedTo.Content.Key.PrimaryKey = objContentManager.ReadPrimaryKey(groupId, langId);
            //sodw.RelatedTo.Content.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Interviste;

            var roleMng = new ProfilingManager();
            UserBaseValue user = new UserBaseValue();
            user = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId());
            if (roleMng.HasRole(user.Key, Inclub.Web.Portal.Keys.RoleKeys.SysAdmin)){
                role = "sysadmin";
            }

            if (role =="sysadmin")
            {
                sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.All;
            }
            else
            {
                sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
            }

            objDownloadManager.Cache = false;
            DownloadCollection interviste = objDownloadManager.Read(sodw);
            List<inClub.Web.Portal.Models.Interviste> discObj = new List<inClub.Web.Portal.Models.Interviste>();

            if (interviste != null && interviste.Any())
            {
                DownloadValue m = interviste[0];
                inClub.Web.Portal.Models.Interviste intervista = GetResource(m, m.Owner, 0);

                return View("Detail", intervista);
            }
          
            return View("Detail", null);
        }



        public ActionResult Archive(int groupId, short langId, int type, int pagenum, int pagesize, string viewName = "", string discid = "0", int excludeId = 0)
        {

            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

                int selDisc = 0;

                if ((discid != "0") && (int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(discid), out selDisc)))
                {
                    this.ViewBag.selDocID = selDisc;
                }
                else
                {
                    this.ViewBag.selDocID = 0;
                }

                //if (groupId <= 0)
                //{                
                //    return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
                //}

                if (viewName == "")
                    viewName = "_Lastinterviste";

                var manager = new AccessService();
                var ticket = manager.GetTicket();
                var objContentManager = new ContentManager();

                Boolean isSysAdmin = false;
                var roleMng = new ProfilingManager();
                int userID = ticket.User.Key.Id;
                if (userID > 0)
                    isSysAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);

                this.ViewBag.isSysAdmin = isSysAdmin;
          

                DownloadManager objDownloadManager = new DownloadManager();
                DownloadSearcher sodw = new DownloadSearcher();
                sodw.Properties.Add("Title");
                sodw.Properties.Add("Launch");
                sodw.Properties.Add("DatePublish");
                sodw.Properties.Add("Copyright");  //width del video                     
                sodw.Properties.Add("Importants"); //num commenti
                sodw.Properties.Add("Qty"); //num contributi
                sodw.Properties.Add("Code"); //primarykey della Folder creata automaticamente per la discussione
                sodw.Properties.Add("ApprovalStatus");

                    //if (selDisc != 0)
                    //    sodw.key = new ContentIdentificator(selDisc, langId);

                if (excludeId != 0)
                    sodw.KeysToExclude = excludeId.ToString();
                    

                    sodw.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Interviste.Id;
                    sodw.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Interviste.Id;
                    sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                    sodw.PageNumber = pagenum;
                    sodw.PageSize = pagesize;
                    sodw.SortOrder = DownloadGenericComparer.SortOrder.DESC;
                    sodw.SortType = DownloadGenericComparer.SortType.ByData;
                    sodw.KeyContext.Id = type;

                    if (isSysAdmin)
                    {
                        sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.All;
                    }
                    else
                    {
                        sodw.ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved;
                    }

                    //sodw.Keys = cntList;
           
                   
                    objDownloadManager.Cache = false;
                    DownloadCollection interviste = objDownloadManager.Read(sodw);
                    

                    List<inClub.Web.Portal.Models.Interviste> intrvObj = new List<inClub.Web.Portal.Models.Interviste>();
                    var modelColl = new inClub.Web.Portal.Models.Archive<inClub.Web.Portal.Models.Interviste>();
                    inClub.Web.Portal.Models.Interviste intervista = null;

                    if (interviste != null && interviste.Any())
                    {
                       modelColl.CurrentPage = pagenum;
                       modelColl.TotalPagesCount = interviste.PagerTotalNumber;
                       modelColl.TotalItemsCount = interviste.PagerTotalCount;

                       foreach (DownloadValue m in interviste)
                       {
                           intervista = GetResource(m, m.Owner, 0);
                           intervista.Type = type;
                           intrvObj.Add(intervista);
                       }

                       modelColl.Items = intrvObj;

                       return PartialView(viewName, modelColl);
                    }
            
            

            return PartialView(viewName, null);
        }


        public static inClub.Web.Portal.Models.Interviste GetResource(DownloadValue vo, UserBaseValue Owner, int groupId)
        {
            inClub.Web.Portal.Models.Interviste resource = new inClub.Web.Portal.Models.Interviste();
            int widthVideo = 0;
            int.TryParse(vo.Copyright, out widthVideo);

            resource.IntervistaKey = vo.Key;            
            resource.Title = vo.Title;
            resource.Description = System.Web.HttpContext.Current.Server.HtmlDecode(vo.Launch);
            resource.CreationDate = vo.DatePublish.Value.ToShortDateString();
            resource.GroupKey = new ContentIdentificator();
            resource.GroupKey.Id = groupId;
            resource.Poster = "/HP3Image/cover/" + CoverHelper.getCover(vo.Key, widthVideo, 0, 0);
            resource.Width = widthVideo;
            resource.NumCommenti = (int)vo.Importants;
            resource.NumContributi = vo.Qty;
            resource.Status = (int)vo.ApprovalStatus;

            if (vo.DownloadTypeCollection != null && vo.DownloadTypeCollection.Count > 0)
                {
                    DownloadTypeValue dwnType = vo.DownloadTypeCollection[0];
               
                    resource.Ext = dwnType.DownloadType.Label;
                    resource.SizeKb = DocumentiHelper.roundObjectSize(dwnType.Weight.ToString());
                    resource.FileName = vo.Key.PrimaryKey + "." + dwnType.DownloadType.Label;
                    return resource;
                }

            return null;
        }


        [HttpPost]
        [Healthware.HP3.MVC.Attributes.AjaxOnly]
        public JsonResult Pubblica(int groupId, short langId, int videoId)
        {
            bool result = false;
            try
            {
                ContentManager objcntMan = new ContentManager();
                ContentSearcher so = new ContentSearcher();
                ContentValue videoVal = new ContentValue();

                int docKey = objcntMan.ReadPrimaryKey(videoId, langId);

                so.Properties.Add("Title");                
                so.Properties.Add("DatePublish");                
                so.key.PrimaryKey = docKey;
                
                ContentCollection videos = objcntMan.Read(so);

                if (videos != null && videos.Any())
                {
                    videoVal = videos[0];

                    objcntMan.Update(videoVal.Key, "ApprovalStatus", 1);

                    //Trace-------------------------------------------------
                    TraceObject objTrace = new TraceObject();
                    var _with6 = objTrace;
                    _with6.Content = null;
                    _with6.ContentId = videoVal.Key.Id;
                    _with6.Description = "Pubblicazione video intervista e invio notifica";
                    _with6.EventId = Inclub.Web.Portal.Keys.TraceEventKeys.PubbVideo.Id;
                    _with6.LanguageId = videoVal.Key.IdLanguage;
                    _with6.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                    _with6.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                    _with6.UserId = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueById(Inclub.Web.Portal.Helper.UserHelper.getUserLoggedId()).Key.Id;
                    TraceHelper.TraceEvent(objTrace);
                    //------------------------------------------------------                   


                    ContentValue groupVal = objcntMan.Read(groupId, langId);

                    //invia notifica
                    NotificationHelper oNotificationHelper = new NotificationHelper();
                    NotificationData oNotData = new NotificationData();

                    oNotData.Notification = notificationAction.NewAddedVideo;
                    oNotData.NotifiedId = videoVal.Key.Id;
                    oNotData.NotifiedName = videoVal.Title;
                    oNotData.NotifyingUserEmail = String.Empty; //Email utente che ha com piuto l'azione (non indispensabile)
                    oNotData.NotifyingUserFullName = "";
                    oNotData.NotifyingUserId = 0;
                    oNotData.FolderName = "";
                    oNotData.GroupId = groupId;
                    oNotData.GroupName = groupVal.Title;
                    oNotData.NotifyToMembers = true;
                    oNotData.NotifyToAdmin = true;
                    oNotData.NotifiedIdLanguage = langId;
                    //Url del video
                    oNotData.GroupUrl = string.Format("{0}://{1}{2}", System.Web.HttpContext.Current.Request.Url.Scheme.ToString(), System.Web.HttpContext.Current.Request.Url.Host.ToString(), NotificationHelper.ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(oNotData.GroupId, oNotData.NotifiedIdLanguage), new LanguageIdentificator(1), oNotData.GroupName, "detail", SimpleHash.EncryptRijndaelAdvanced(oNotData.NotifiedId.ToString()), "interviste"));

                    NotificationHelper.DeliveryNotification(oNotData);
                    
                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                result = false;
            }

            return Json(result, JsonRequestBehavior.DenyGet);
        }


        


    }
}