﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.Models;


namespace inClub.Web.Portal.Controllers
{
    public class HPGruppoController : Controller
    {
        //
        // GET: /HPGruppo/

        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = Inclub.Web.Portal.BL.Shared.ReadPageInfo(this.Request.RequestContext, Inclub.Web.Portal.Helper.ThemeHelper.getCurrentThemeIdentificator(), Inclub.Web.Portal.Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            return View();
        }


        public ActionResult Detail(int id)
        {
            //if (!Inclub.Web.Portal.Helper.LoginHelper.IsAuthenticated())
            //{
            //    string _redirectHome = "/home"; //Inclub.Web.Portal.Helper.ThemeHelper.getHomeLinkByLanguage(true);
            //    return Redirect(_redirectHome);
            //}

            short langId = 1;

            if (id <= 0)
            {
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

            var manager = new AccessService();
            var ticket = manager.GetTicket();

            Boolean isAdmin = false;
            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.key = new ContentIdentificator(id, langId);
            so.Properties.Add("Title");
            so.Properties.Add("Launch");
            so.Properties.Add("Copyright");
            so.Properties.Add("DateCreate");
            so.Properties.Add("DatePublish");
            so.Properties.Add("MetaKeywords");
            so.Properties.Add("MetaDescription");
            so.Properties.Add("RelatedTo.ContentUser.Relation.KeyRelationType.Id");
            so.Properties.Add("Qty"); //num di iscritti
            so.Properties.Add("Importants");  //num di discussioni
            so.Properties.Add("IdContentSubCategory"); //gruppo aperto
            so.Properties.Add("Owner.Key.Id");
            so.Properties.Add("Owner.Name");
            so.Properties.Add("Owner.Surname");
            so.Properties.Add("Owner.Title");
            so.Properties.Add("Code");
            so.Properties.Add("LinkLabel");    


            so.LoadOwnerDetails = true;
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;

            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                isAdmin = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin);


            if (!isAdmin)
            {
                so.RelatedTo.ContentUser.LoadRelationDetails = true;
                so.RelatedTo.ContentUser.KeyUser.Id = userID;
            }

            ContentManager objContentManager = new ContentManager();

            ContentCollection groups = objContentManager.Read(so);
            List<inClub.Web.Portal.Models.Gruppo> groupsObj = new List<inClub.Web.Portal.Models.Gruppo>();

            if (groups != null && groups.Any())
            {

                groupsObj = groups.Select(toGruppo).ToList();

                //se isAdmin tutti i Role diventano "admin"
                if (isAdmin)
                {
                    groupsObj.ToList().ForEach(c => c.Role = "sysadmin");
                }

                return View(groupsObj[0]);
            }
            else
            {
                //se non vede il gruppo viene reindirizzato alla pagina dei gruppi
                return Redirect(this.Url.ThemeLink(Inclub.Web.Portal.Keys.ThemeKeys.Gruppi, new LanguageIdentificator(langId)));
            }

        }


        private Func<ContentValue, inClub.Web.Portal.Models.Gruppo> toGruppo =
            x => new inClub.Web.Portal.Models.Gruppo
            {
                GroupKey = x.Key,
                Name = x.Title,
                Description = x.Launch,

                BlockColor= x.Copyright,
                GrpAperto = x.IdContentSubCategory,
                Admin = x.Owner.Key,
                AdminName = x.Owner.Title + ' ' + x.Owner.Name + ' ' + x.Owner.Surname,

                CreationDate = x.DateCreate.Value.ToShortDateString(),
                LastUpdateDate = x.DatePublish.Value.ToShortDateString(),
                Role = ((x.RelatedTo.ContentUser.Relation.KeyRelationType.Id == Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin) ? "admin" : ""),
                NumIscritti = x.Qty,
                NumDiscussioni = Convert.ToInt32(x.Importants),
                NumContributi = Convert.ToInt32(x.Code),
                NumCommenti = Convert.ToInt32(x.LinkLabel)
            };

    }
}
