﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;


namespace Inclub.Web.Portal.Controllers
{
    public class CaptchaController : Controller
    {

        public FileResult Get(string name)
        {
            var image = CaptchaGeneratorHelper.GetCurrentCaptchaImage(base.HttpContext, name);
            if (null == image)
                image = CaptchaGeneratorHelper.Init(this.HttpContext, name);

            if (null != image)
            {
                base.HttpContext.Response.AddHeader("Pragma", "no-cache");
                base.HttpContext.Response.AddHeader("cache-control", "no-store");
                base.HttpContext.Response.Expires = -1;
                base.HttpContext.Response.ContentType = "image/jpeg";

                image.Save(base.HttpContext.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            return null;
        }

        public JsonResult Regenerate(string name)
        {
            try
            {
                CaptchaGeneratorHelper.Init(this.HttpContext, name);
                return Json(true);
            }
            catch { }

            return Json(false);
        }

    }
}