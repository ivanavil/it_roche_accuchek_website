﻿using Healthware.HP3.Core.Web.ObjectValues;
using Inclub.Web.Portal.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Keys;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using System.Data.SqlClient;
using System.Data;

namespace Inclub.Web.Portal.Controllers
{ 
    public class HomeController : Controller 
    {
        // GET: Home
        public ActionResult Index()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info     

            DateTime webinarDay = DateTime.Parse("07/11/2017 15:20");
            DateTime orario = System.DateTime.Now;

            if (!(string.IsNullOrEmpty(Request["webinar0711"])) && (Request["webinar0711"] == "si"))
            {
                if (orario < webinarDay)
                {
                    this.ViewBag.regWebinar = true;
                    this.ViewBag.noWebinar = false;
                }
                else
                {
                    this.ViewBag.noWebinar = true;
                    this.ViewBag.regWebinar = false;
                }
                
            }
            else
            {
                this.ViewBag.noWebinar = false;
                this.ViewBag.regWebinar = false;
            }

            //Ricavo i Contenuti dei Box

            var valBox1 = Helper.ContentHelper.GetBoxHomeContent("1");
            this.ViewBag.TitleBox1 = valBox1.Title;
            this.ViewBag.TestoBox1 = valBox1.FullText;

            var valBox2 = Helper.ContentHelper.GetBoxHomeContent("2");
            this.ViewBag.TitleBox2 = valBox2.Title;
            this.ViewBag.TestoBox2 = valBox2.FullText;

            var valBox3 = Helper.ContentHelper.GetBoxHomeContent("3");
            this.ViewBag.TitleBox3 = valBox3.Title;
            this.ViewBag.TestoBox3 = valBox3.FullText;

            var valBox4 = Helper.ContentHelper.GetBoxHomeContent("4");
            this.ViewBag.TitleBox4 = valBox4.Title;
            this.ViewBag.TestoBox4 = valBox4.FullText;



           // trace
            TraceHelper.TraceEvent(pageInfo.Key);
            return View();
        }

        


    }

       
}