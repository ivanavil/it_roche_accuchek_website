﻿using Healthware.HP3.MVC.Attributes;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;

namespace Inclub.Web.Portal.Controllers
{
    public class ContattiController : Controller
    {
        //
        // GET: /ContactUs/

        public ActionResult Index()
        {

            ContactUs modelcontact = new ContactUs();


            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info

            var manager = new AccessService();
            var Userticket = manager.GetTicket();
            if  (Userticket.User.Key.Id>0)
            {

                //IDictionary<string, object> atts = new Dictionary<string, object>();
                //atts.Add("class", "UserField");

                //atts.Add("readonly", "readonly");
               

                UserValue user = new UserValue();
                user = Inclub.Web.Portal.Helper.UserHelper.GetUserBaseValueByUserId(Userticket.User.Key.Id);
                modelcontact.Name = user.Name;
                modelcontact.Surname = user.Surname;
                modelcontact.Email = user.Email.GetString;
               

                //HtmlAttributeDict = atts;
               


                return View(modelcontact);
            }

            //trace
            //Helper.TraceHelper.TraceEvent(pageInfo.Key);
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult SendContact(ContactUs model)
        {
            var captchaValid = false;
            string _span = "<p  class='errorTxt'>{0}</p>";
            var result = new OperationResult() { Successful = false };

            captchaValid = Helper.CaptchaGeneratorHelper.Validate(this.HttpContext, Keys.CaptchaKeys.ContactUseForm, model.Captcha);
            if (!captchaValid)
            {
                ModelState.AddModelError("Captcha", string.Format(_span, Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.CaptchaError)));
                result.Successful = false;
                result.StatusCode = 5;
                result.Message = "<p  class='errorTxt'>Codice Capcha Errato</p>"; 
                return Json(result, JsonRequestBehavior.DenyGet);
            }

            if (ModelState.IsValid && captchaValid)
            {                         
                   
                    try
                    {

                         //SendMail
                        SendMail(model);
                        result.Successful = true;
                        result.Message = String.Format("<p>{0}</p>",Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.SendContactUsSuccessfully,Keys.LabelKeys.LANGUAGE_PORTAL));
                        Helper.TraceHelper.TraceEvent(ThemeKeys.Contatti, TraceEventKeys.SendContactUs);
                    }
                    catch (Exception ex)
                    {
                        result.Message = String.Format("<p>{0}</p>", Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.GenericError, Keys.LabelKeys.LANGUAGE_PORTAL));
                        result.StatusCode = (int)Keys.EnumeratorKeys.StatusCode.Error;
                        Helper.ErrorLogHelper.NotifyDBLog(ex, "");
                    }
                
            }
            else
            {

                //SendMail
              
                result.Successful = false;
                result.StatusCode = 5;
                result.Message = "<p  class='errorTxt'>Tutti i campi sono obbligatori</p>"; 

            }
               // return Json(string.Format(_span, Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.CaptchaError, LabelKeys.LANGUAGE_PORTAL)));

            return Json(result, JsonRequestBehavior.DenyGet);
        }


        #region Private

        private void SendMail(ContactUs model)
        {
            string htmlTemplate = Helper.DictionaryHelper.GetLabel(Keys.DictionaryKeys.htmlMailTemplate, Keys.LabelKeys.LANGUAGE_PORTAL);
            Dictionary<string, string> replacing = new Dictionary<string, string>();
            replacing.Add(Keys.LabelKeys.em_NAME, model.Name);
            replacing.Add(Keys.LabelKeys.em_SURNAME, model.Surname);    
            replacing.Add(Keys.LabelKeys.em_SUBJECT, model.Subject);
            replacing.Add(Keys.LabelKeys.em_EMAIL, model.Email);
            replacing.Add(Keys.LabelKeys.em_MESSAGE, model.MessageBody);
            int langId = Keys.LabelKeys.LANGUAGE_PORTAL;
            Helper.MailHelper.SendMail(MailTemplateKeys.ContactUs, langId, htmlTemplate, replacing, false, "");

        }

      
        #endregion
    }
}
