﻿using Healthware.HP3.MVC.Attributes;
using Healthware.HP3.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using Healthware.HP3.Common.Extensions;
using Inclub.Web.Portal.Extensions;
using Healthware.HP3.Core.Base;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using System.Data.SqlClient;


namespace Inclub.Web.Portal.Controllers{
    public class popupVideoController : Controller
    {

        public ActionResult Index(string pathVideo) 
        {
            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());
            this.ViewBag.PageInfo = pageInfo;
            #endregion Page Info
            this.Trace();

            if (!string.IsNullOrEmpty(pathVideo))
            {
                this.ViewBag.pathVideo = pathVideo;
            }

            return View();
        }


    }
}