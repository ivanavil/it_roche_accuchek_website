﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Helper;
using Healthware.HP3.MVC;
using Inclub.Web.Portal.Attribute;
using Healthware.HP3.Core.Content.ObjectValues;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Extensions;

using System.Web.Mvc;

namespace Inclub.Web.Portal.Controllers
{
    public class GenContentController : Controller
    {
        // GET: 
        public ActionResult Index()
        {
            #region Page Info
            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator());
            #endregion Page Info

            var tcoll = Enumerable.Empty<Healthware.HP3.Core.Site.ObjectValues.ThemeValue>();

            tcoll = Helper.ThemeHelper.getThemeMenuCollectionByFather(pageInfo.KeyFather);

            this.ViewBag.Themes = tcoll;
            this.ViewBag.PageI = pageInfo;
            this.ViewBag.PageInfo = pageInfo;

            GenericSearcherContentModel SearchModel = new GenericSearcherContentModel();
            SearchModel.CalculatePagerTotalCount = false;
            var so = BL.SearcherBuilders.InitContentSearcher(Keys.EnumeratorKeys.DataLoadingType.Lite, SearchModel);

            var manager = new BL.GenericContentManage();

            ContentCollection coll = manager.GetArchive(so);

            if (!(null != coll && coll.Any()))
                throw new HttpException(404, "Page not found");

            var soExtra = BL.SearcherBuilders.InitContentExtraSearcher(coll.First().Key.Id);

            var objModel = manager.GetDetailGenCont(soExtra, true);

            if (objModel == null)
                throw new HttpException(404, "Page not found");

            if (objModel != null && objModel.Key != null && objModel.Key.Id > 0)
            {
                //For MetaKeyword e Meta Description in all details
                //var metaModel = objModel as MetaData;
                //if (null != metaModel && null != pageInfo)
                //    pageInfo.Populate(metaModel);


                //TRACE
                Helper.TraceHelper.TraceEvent(pageInfo.Key, Inclub.Web.Portal.Keys.TraceEventKeys.Detail, objModel.Key);
            }


            return View("Index", objModel);
        }

    }
}