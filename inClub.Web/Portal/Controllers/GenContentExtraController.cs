﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Controllers
{
    public class GenContentExtraController : Controller
    {
        // GET: 
        public ActionResult Index(int id)
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(),Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info
            return View();
        }

        public ActionResult Detail(int id)
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info
            return View();
        }


        // GET: Detail ContentType Inclub
        public ActionResult InclubDetail()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Trace Event
            // TraceHelper.TraceEvent(pageInfo.Key); 

            //theme with associated default content
            if (null != pageInfo && null != pageInfo.KeyContent && pageInfo.KeyContent.Id > 0)
            {
                var so = BL.SearcherBuilders.InitContentExtraSearcher(this.Request.RequestContext, Inclub.Web.Portal.BL.SearcherBuilders.DataLoadingType.Details, pageInfo.KeyContent.Id);
                so.key.Id = pageInfo.KeyContent.Id;
                var objModel = Helper.ContentHelper.GetDetailExtraCont(so);

                if (null != objModel) return View("InclubDetail", objModel);
            }

            return View("InclubDetail", null);
        }

        // GET: Detail ContentType Footer
        public ActionResult FooterDetail()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Trace Event
            // TraceHelper.TraceEvent(pageInfo.Key); 

            //theme with associated default content
            if (null != pageInfo && null != pageInfo.KeyContent && pageInfo.KeyContent.Id > 0)
            {

                var so = BL.SearcherBuilders.InitContentExtraSearcher(this.Request.RequestContext, Inclub.Web.Portal.BL.SearcherBuilders.DataLoadingType.Details, pageInfo.KeyContent.Id);
                so.key.Id = pageInfo.KeyContent.Id;
                var objModel = Helper.ContentHelper.GetDetailExtraCont(so);

                if (null != objModel) return View("_FooterDetail", objModel);
            }

            return View("_FooterDetail", null);
        }


        // GET: Detail ContentType Inclub
        public ActionResult TherapeuticArea()
        {
            #region Page Info

            var pageInfo = BL.Shared.ReadPageInfo(this.Request.RequestContext, Helper.ThemeHelper.getCurrentThemeIdentificator(), Helper.LanguageHelper.GetLanguageFromUrl());

            this.ViewBag.PageInfo = pageInfo;

            #endregion Page Info

            //Trace Event
            // TraceHelper.TraceEvent(pageInfo.Key); 

            //theme with associated default content
            if (null != pageInfo && null != pageInfo.KeyContent && pageInfo.KeyContent.Id > 0)
            {
                var so = BL.SearcherBuilders.InitContentExtraSearcher(this.Request.RequestContext, Inclub.Web.Portal.BL.SearcherBuilders.DataLoadingType.Details, pageInfo.KeyContent.Id);
                so.key.Id = pageInfo.KeyContent.Id;
                var objModel = Helper.ContentHelper.GetDetailExtraCont(so);

                if (null != objModel) return View("TherapeuticArea", objModel);
            }

            return View("TherapeuticArea", null);
        }
    }
}