﻿using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class ContentTypeHelper
    {
        public static ContentTypeIdentificator GetContentTypeIdentificator()
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();


            var _return = objMasterPageManager.GetContenType();

            objMasterPageManager = null;

            return _return;
        }
    }
}
