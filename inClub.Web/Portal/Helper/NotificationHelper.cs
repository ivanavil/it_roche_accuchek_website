﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using Microsoft.VisualBasic;
using Healthware.HP3.Core.Localization;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.Utility.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Inclub.Web.Portal.BL;

namespace inClub.Web.Portal.Helper
{



    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    public enum notificationAction
    {
        NothingNew = 0,
        NewAddedPost = 1,
        NewAddedComment = 2,
        NewAddedResource = 3,
        NewAddedDiscussion = 4,
        NewAddedDiscComment = 5,
        MessageSend = 6,
        NewAddedPostVI = 7,
        NewAddedCommentVI = 8,
        NewAddedVideo = 9
    }

    public class NotificationData
    {
        private notificationAction _notification = notificationAction.NothingNew;
        private string _notifyingUserFullName = string.Empty;
        private string _notifyingUserEmail = string.Empty;
        private int _notifyingUserId = 0;
        private int _notifiedId = 0;
        private short _notifiedIdLanguage = 0;
        private string _notifiedName = string.Empty;
        private bool _notifyToAdmin = true;
        private bool _notifyToMembers = false;
        private string _folderName = string.Empty;
        private int _groupId = 0;
        private string _groupName = string.Empty;
        private string _groupUrl = string.Empty;
        
        //String = String.Empty
        private bool _notifyToSingleMember = false;

        private string _notifySingleMemberEmail = string.Empty;
        public notificationAction Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        public string NotifyingUserFullName
        {
            get { return _notifyingUserFullName; }
            set { _notifyingUserFullName = value; }
        }

        public string NotifyingUserEmail
        {
            get { return _notifyingUserEmail; }
            set { _notifyingUserEmail = value; }
        }

        public int NotifyingUserId
        {
            get { return _notifyingUserId; }
            set { _notifyingUserId = value; }
        }

        public int NotifiedId
        {
            get { return _notifiedId; }
            set { _notifiedId = value; }
        }

        public short NotifiedIdLanguage
        {
            get { return _notifiedIdLanguage; }
            set { _notifiedIdLanguage = value; }
        }

        public string NotifiedName
        {
            get { return _notifiedName; }
            set { _notifiedName = value; }
        }

        public bool NotifyToAdmin
        {
            get { return _notifyToAdmin; }
            set { _notifyToAdmin = value; }
        }

        public bool NotifyToSingleMember
        {
            get { return _notifyToSingleMember; }
            set { _notifyToSingleMember = value; }
        }

        public string NotifySingleMemberEmail
        {
            get { return _notifySingleMemberEmail; }
            set { _notifySingleMemberEmail = value; }
        }

        public bool NotifyToMembers
        {
            get { return _notifyToMembers; }
            set { _notifyToMembers = value; }
        }
        
        public string FolderName
        {
            get { return _folderName; }
            set { _folderName = value; }
        }

        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }
        
        public string GroupUrl
        {
            get { return _groupUrl; }
            set { _groupUrl = value; }
        }

       

    }



    public class NotificationMsgData
    {
        private string _notifyingUserFullName = string.Empty;
        private string _notifyingUserEmail = string.Empty;
        private int _notifyingUserId = 0;
        private string _object = string.Empty;
        private string _body = string.Empty;
        //String = String.Empty
        private bool _notifyToGroupMembers = false;
        private List<int> _groupIds = null;

        private bool _notifyToGroupAdmin = false;


        public string NotifyingUserFullName
        {
            get { return _notifyingUserFullName; }
            set { _notifyingUserFullName = value; }
        }

        public string NotifyingUserEmail
        {
            get { return _notifyingUserEmail; }
            set { _notifyingUserEmail = value; }
        }

        public int NotifyingUserId
        {
            get { return _notifyingUserId; }
            set { _notifyingUserId = value; }
        }

        public string ObjectTxt
        {
            get { return _object; }
            set { _object = value; }
        }

        public string BodyTxt
        {
            get { return _body; }
            set { _body = value; }
        }

        public bool NotifyToGroupAdmin
        {
            get { return _notifyToGroupAdmin; }
            set { _notifyToGroupAdmin = value; }
        }

        public List<int> GroupIds
        {
            get { return _groupIds; }
            set { _groupIds = value; }
        }

        public bool NotifyToGroupMembers
        {
            get { return _notifyToGroupMembers; }
            set { _notifyToGroupMembers = value; }
        }


    }



    public class NotificationHelper
    {

        public static string ThemeLinkDetail(ThemeIdentificator key, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, LanguageIdentificator KeyLang, string title, string action, string param = "", string folder = "")
        {
            var searcher = new ThemeSearcher();
            searcher.Display = SelectOperation.Enabled();
            searcher.Active = SelectOperation.Enabled();
            searcher.Delete = SelectOperation.Enabled();              
            searcher.Key = key;
            searcher.KeyLanguage = KeyLang;
            searcher.Display = SelectOperation.All();
            searcher.LoadRoles = true;
            var manager = new ThemeManager();
            manager.Cache = true;
            var themes = manager.Read(searcher);

            if (null == themes) return string.Empty;

            var theme = themes[0];
            
            if (null == theme) return string.Empty;

            var rc = HttpContext.Current.Request.RequestContext;
            System.Web.Mvc.UrlHelper urlH = new System.Web.Mvc.UrlHelper(rc);
            
            string x = urlH.RouteUrl(Inclub.Web.Portal.RouteConfigurations.ContentDetails.RouteName,
                 new
                 {
                     controller = theme.SeoName.FormatLinkTitle(),
                     action = action,
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id,
                     folderName = folder,
                     parameter = param
                 });

            return x;
        }

        public static void DeliveryNotification(int grpId, string grpName, short IdLanguage, int discussionId, string discussionName, UserBaseValue user, int role)
        {
            try
            {
                NotificationHelper oNotificationHelper = new NotificationHelper();
                NotificationData oNotData = new NotificationData();

                
                oNotData.Notification = notificationAction.NewAddedDiscussion;
                
                //Enumerato che identifica il tipo di notifica
                oNotData.NotifiedId = discussionId;
                //ID della risorsa appena creata
                oNotData.NotifiedName = discussionName;
                //Nome della risorsa appena creata
                oNotData.NotifyingUserEmail = string.Empty;
                //Email utente che ha com piuto l'azione (non indispensabile)
                oNotData.NotifyingUserFullName = user.Name.ToUpper() + " " + user.Surname.ToUpper();
                //Nome e cognome di chi commpie l'azione
                oNotData.NotifyingUserId = user.Key.Id;
                //Id di chi commpie l'azione
                oNotData.GroupId = grpId;
                //Id del gruppo
                oNotData.GroupName = grpName;
                //Nome del gruppo
                oNotData.NotifyToMembers = true;
                oNotData.FolderName = "";

                if (role == Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id || role == Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id)
                    oNotData.NotifyToAdmin = false;
                
                //Url alla pagina di dettaglio della discussione
                
                oNotData.GroupUrl = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme.ToString(), HttpContext.Current.Request.Url.Host.ToString(), ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(grpId, IdLanguage), new LanguageIdentificator(IdLanguage), discussionName, "detail", SimpleHash.UrlEncryptRijndaelAdvanced(discussionId.ToString()), "discussioni")); 
                                  //string.Format("{0}://{1}{2}?discussionid={3}&groupId={4}&postId={5}&languageId={6}", context.Request.Url.Scheme.ToString, context.Request.Url.Host.ToString, JLink.DashboardNavigation.Pages.ManageGroupDiscussionPostPage, SimpleHash.UrlEncryptRijndaelAdvanced(discussionId), SimpleHash.UrlEncryptRijndaelAdvanced(grpId), SimpleHash.UrlEncryptRijndaelAdvanced(commId), languageId);
               
                DeliveryNotification(oNotData);
            }
            catch (Exception ex)
            {

            }
        }

        public static void DeliveryNotification(int commId, int grpId, int discussionId, short IdLanguage, string grpName, int fatherId, string discussionName, UserBaseValue user, int role, string type = "1")
        {
            try
            {
                NotificationHelper oNotificationHelper = new NotificationHelper();
                NotificationData oNotData = new NotificationData();
               
                if (fatherId > 0)
                    oNotData.Notification = ((type == "1") ? notificationAction.NewAddedComment : notificationAction.NewAddedCommentVI);
                else
                    oNotData.Notification = ((type == "1") ? notificationAction.NewAddedPost : notificationAction.NewAddedPostVI);

                //Enumerato che identifica il tipo di notifica
                oNotData.NotifiedId = commId;
                //ID della risorsa appena creata
                oNotData.NotifiedName = discussionName;
                //Nome della risorsa appena creata
                oNotData.NotifyingUserEmail = string.Empty;
                //Email utente che ha com piuto l'azione (non indispensabile)
                oNotData.NotifyingUserFullName = user.Name.ToUpper() + " " + user.Surname.ToUpper();
                //Nome e cognome di chi commpie l'azione
                oNotData.NotifyingUserId = user.Key.Id;
                //Id di chi commpie l'azione
                oNotData.GroupId = grpId;
                //Id del gruppo
                oNotData.GroupName = grpName;
                //Nome del gruppo
                oNotData.NotifyToMembers = true;
                oNotData.FolderName =  "";                

                if (role == Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id || role == Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id) 
                    oNotData.NotifyToAdmin = false;

                              
                if (type == "2")
                {
                    //Url della pagina della video intervista a cui è stato aggiunto un contributo  
                    oNotData.GroupUrl = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme.ToString(), HttpContext.Current.Request.Url.Host.ToString(), ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(grpId, IdLanguage), new LanguageIdentificator(IdLanguage), discussionName, "detail", discussionId.ToString(), "interviste") + "&p=" + SimpleHash.UrlEncryptRijndaelAdvanced(fatherId.ToString()) + "&c=" + SimpleHash.UrlEncryptRijndaelAdvanced(commId.ToString()));
                }
                else {
                    //Url della pagina della discussione a cui è stato aggiunto un contributo  
                    oNotData.GroupUrl = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme.ToString(), HttpContext.Current.Request.Url.Host.ToString(), ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(grpId, IdLanguage), new LanguageIdentificator(IdLanguage), discussionName, "detail", discussionId.ToString(), "discussioni") + "&p=" + SimpleHash.UrlEncryptRijndaelAdvanced(fatherId.ToString()) + "&c=" + SimpleHash.UrlEncryptRijndaelAdvanced(commId.ToString()));
                }

                DeliveryNotification(oNotData);
            }
            catch (Exception ex)
            {

            }
        }



        public static void DeliveryNotification(NotificationData oNotificationData)
        {
            bool _hasError = false;
            try
            {
                int typeNotification = 0;
                typeNotification = (int)((object)oNotificationData.Notification);

                string notificationMailTemplate = GetMailTemplateName(typeNotification);
                if (!string.IsNullOrWhiteSpace(notificationMailTemplate))
                {
                    //Dim oDictionaryManager As New DictionaryManager
                    //oDictionaryManager.Read("jLinkNotificationMailTemplate", 1)
                    string htmlTemplate = "htmlMailTemplate";

                    Dictionary<string, string> replacing = new Dictionary<string, string>();
                    replacing.Add("[userCompleteName]", oNotificationData.NotifyingUserFullName);
                    replacing.Add("[userEmailAddress]", oNotificationData.NotifyingUserEmail);
                    replacing.Add("[groupUrl]", oNotificationData.GroupUrl);
                    replacing.Add("[groupName]", oNotificationData.GroupName);
                    replacing.Add("[NotifiedName]", oNotificationData.NotifiedName);
                    replacing.Add("[Folder]", oNotificationData.FolderName);

                    //string GroupUrl = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme.ToString(), HttpContext.Current.Request.Url.Host.ToString(), ThemeLinkDetail(Inclub.Web.Portal.Keys.ThemeKeys.HPGruppo, new ContentIdentificator(oNotificationData.GroupId, oNotificationData.NotifiedIdLanguage), new LanguageIdentificator(1), oNotificationData.GroupName, "detail", SimpleHash.EncryptRijndaelAdvanced(oNotificationData.NotifiedId.ToString()), "documenti"));
                    //replacing.Add("[groupUrl]", GroupUrl);


                    string strMailToAddress = string.Empty;
                    inClub.Web.Portal.Models.DashboardGroupManager oDashboardGroupManager = new inClub.Web.Portal.Models.DashboardGroupManager();
                    if (oNotificationData.NotifyToAdmin)
                    {
                        inClub.Web.Portal.Models.DashboardGroupMembers oGrpAdmin = oDashboardGroupManager.getGroupUserCollection(oNotificationData.GroupId, oNotificationData.NotifiedIdLanguage, Inclub.Web.Portal.Keys.RelationTypeKeys.GrpAdmin, string.Empty);
                        if ((oGrpAdmin != null) && (oGrpAdmin.GroupMemberColl != null) && oGrpAdmin.GroupMemberColl.Any())
                        {
                            foreach (inClub.Web.Portal.Models.DashboardGroupMember Item in oGrpAdmin.GroupMemberColl)
                            {
                                if ((!string.IsNullOrEmpty(Item.AdminEmail)))
                                    strMailToAddress += ";" + Item.AdminEmail;
                            }
                            if (!string.IsNullOrWhiteSpace(strMailToAddress))
                                strMailToAddress = strMailToAddress.Substring(1);
                        }

                        oGrpAdmin = null;
                        oGrpAdmin = oDashboardGroupManager.getGroupUserCollection(oNotificationData.GroupId, oNotificationData.NotifiedIdLanguage, Inclub.Web.Portal.Keys.RelationTypeKeys.SysAdmin, string.Empty);
                        if ((oGrpAdmin != null) && (oGrpAdmin.GroupMemberColl != null) && oGrpAdmin.GroupMemberColl.Any())
                        {
                            foreach (inClub.Web.Portal.Models.DashboardGroupMember Item in oGrpAdmin.GroupMemberColl)
                            {
                                if ((!string.IsNullOrEmpty(Item.AdminEmail)))
                                    strMailToAddress += ";" + Item.AdminEmail;
                            }
                            //If Not String.IsNullOrWhiteSpace(strMailToAddress) Then strMailToAddress = strMailToAddress.Substring(1)
                        }
                    }

                    string strMailToMembersAddress = string.Empty;
                    if (oNotificationData.NotifyToMembers)
                    {
                        inClub.Web.Portal.Models.DashboardGroupMembers oGrpMembers = oDashboardGroupManager.getGroupUserCollection(oNotificationData.GroupId, oNotificationData.NotifiedIdLanguage, Inclub.Web.Portal.Keys.RelationTypeKeys.GrpMember, string.Empty);
                        if ((oGrpMembers != null) && (oGrpMembers.GroupMemberColl != null) && oGrpMembers.GroupMemberColl.Any())
                        {
                            foreach (inClub.Web.Portal.Models.DashboardGroupMember Item in oGrpMembers.GroupMemberColl)
                            {
                                if ((Item.AdminRecNotif & (!string.IsNullOrEmpty(Item.AdminEmail))))
                                    strMailToMembersAddress += ";" + Item.AdminEmail;
                            }
                            if (!string.IsNullOrWhiteSpace(strMailToMembersAddress))
                            {
                                if (!string.IsNullOrWhiteSpace(strMailToAddress))
                                {
                                    strMailToAddress += strMailToMembersAddress;
                                }
                                else
                                {
                                    strMailToAddress = strMailToMembersAddress.Substring(1);
                                }
                            }

                        }
                    }


                    if (oNotificationData.NotifyToSingleMember)
                    {

                        if (!string.IsNullOrWhiteSpace(oNotificationData.NotifySingleMemberEmail))
                        {
                            strMailToAddress = oNotificationData.NotifySingleMemberEmail;
                        }
                    }

                   SendMessage(new MailTemplateIdentificator(notificationMailTemplate), 1, htmlTemplate, ref replacing, false, strMailToAddress);
                }
                else
                {
                    throw new Exception("Il template richiesto non esiste");
                }
            }
            catch (Exception ex)
            {
            }
        }


        public static void SendMessage(MailTemplateIdentificator mailTemplateKey, short langId, string htmlTemplate, ref Dictionary<string, string> replaceDictionary, bool throwException, string toAddress = "")
        {
            //, Optional currentBrand As Integer = 0)

            try
            {
                MailTemplateManager manager = new MailTemplateManager();
                dynamic template = manager.Read(mailTemplateKey);

                MailValue message = new MailValue();
                message.MailFrom = new Healthware.HP3.Core.Utility.MailAddress(template.SenderAddress, template.SenderName);
                if (string.IsNullOrEmpty(toAddress))
                {
                    message.MailBcc = GetMailAddressCollection(template.Recipients, ',');
                }
                else
                {
                    message.MailBcc.Clear();

                    Array ar = toAddress.Split(';');
                    foreach (string s in ar)
                    {
                        message.MailBcc.Add(new Healthware.HP3.Core.Utility.MailAddress(s));
                    }
                }

                message.MailCC = GetMailAddressCollection(template.CcRecipients, ',');
                message.MailTo = GetMailAddressCollection(template.Recipients, ',');
                message.MailSubject = template.Subject;

                if (!string.IsNullOrWhiteSpace(htmlTemplate))
                {
                    string strHtmlMailTemplate = GetHtmlMailTemplate(langId, htmlTemplate);
                    message.MailBody = strHtmlMailTemplate.Replace("[TEMPLATEBODY]", template.Body);
                }
                else
                {
                    message.MailBody = template.Body;
                }
                //--------------------------------------------------------------------
                if ((replaceDictionary != null))
                    message.MailBody = FieldReplace(message.MailBody, replaceDictionary);

                if (template.FormatType == 1)
                {
                    message.MailIsBodyHtml = true;
                }
                else
                {
                    message.MailIsBodyHtml = false;
                }

                message.MailPriority = MailValue.Priority.Normal;
                SendMailMessage(message, throwException);

            }
            catch (Exception ex)
            {
                
            }
        }


        public static string GetHtmlMailTemplate(int langId, string htmlMailTemplate)
        {
            string _out = string.Empty;
            DictionaryManager oDictMan = new DictionaryManager();
            return oDictMan.Read(htmlMailTemplate, langId);
        }



        public static string FieldReplace(string str, Dictionary<string, string> fields)
        {
            foreach (KeyValuePair<string, string> item in fields)
            {
                str = str.Replace(item.Key, item.Value);
            }
            return str;
        }


        public static Healthware.HP3.Core.Utility.MailAddressCollection GetMailAddressCollection(string addressStr, char separator = ',')
        {
            Healthware.HP3.Core.Utility.MailAddressCollection retCol = new Healthware.HP3.Core.Utility.MailAddressCollection();
            string[] addresses = addressStr.Split(separator);
            foreach (string address in addresses)
            {
                try
                {
                    Healthware.HP3.Core.Utility.MailAddress mailAddress = new Healthware.HP3.Core.Utility.MailAddress(address);
                    retCol.Add(mailAddress);
                }
                catch
                {
                    continue;
                }
            }
            return retCol;
        }


        private static void SendMailMessage(object message, bool throwOn, string strSenderName = "")
        {
            try
            {
                MailValue oHp3MailValue = (MailValue)message;
                System.Net.Mail.MailMessage oMail = new System.Net.Mail.MailMessage();

                if ((oHp3MailValue.MailTo != null) && oHp3MailValue.MailTo.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailTo)
                    {
                        oMail.To.Add(objTo);
                    }
                }
                if ((oHp3MailValue.MailCC != null) && oHp3MailValue.MailCC.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailCC)
                    {
                        oMail.CC.Add(objTo);
                    }
                }

                if ((oHp3MailValue.MailBcc != null) && oHp3MailValue.MailBcc.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailBcc)
                    {
                        //  System.Web.HttpContext.Current.Response.Write(objTo.Address)
                        oMail.Bcc.Add(objTo);
                    }
                }

                try
                {
                    if (oHp3MailValue.MailFrom.Address.ToString().IndexOf("janssen") > -1)
                    {
                        oMail.ReplyToList.Add(string.Format("{0}<{1}>", "", "noreply@jboard.it"));
                        oMail.Headers.Add("Return-Path", "noreply@jboard.it");

                        if (!string.IsNullOrEmpty(strSenderName))
                        {
                            oMail.From = new System.Net.Mail.MailAddress("noreply@jboard.it", strSenderName);
                        }
                        else
                        {
                            oMail.From = new System.Net.Mail.MailAddress("noreply@jboard.it");
                        }
                    }
                    else
                    {
                        oMail.From = new System.Net.Mail.MailAddress(oHp3MailValue.MailFrom.Address, oHp3MailValue.MailFrom.DisplayName);
                    }
                }
                catch (Exception ex)
                {
                    //ErrorNotification.Helper.ErrorNotifier.NotifyByMail(ex, string.Empty);
                }

                oMail.Subject = oHp3MailValue.MailSubject;
                oMail.Body = oHp3MailValue.MailBody;
                oMail.IsBodyHtml = true;
                
                SmtpClient mySmtpMail = new SmtpClient();
                mySmtpMail.Host = System.Configuration.ConfigurationManager.AppSettings["smtp"];
                mySmtpMail.Send(oMail);
            }
            catch (Exception ex)
            {
                if (throwOn)
                {
                    throw new ApplicationException(ex.Message, ex);
                }
                else
                {
                    //ErrorNotification.Helper.ErrorNotifier.NotifyByMail(ex, string.Empty);
                }
            }
        }



        public static string GetMailTemplateName(int intNotificationType)
        {
            switch (intNotificationType)
            {
                case 0:

                    return string.Empty;
                case 1:

                    return "NewAddedPost";
                case 2:

                    return "NewAddedComment";
                case 3:

                    return "NewAddedResource";
                case 4:

                    return "NewAddedDiscussion";
                case 5:

                    return "NewAddedDiscComment";
                case 7:

                    return "NewAddedPostVI";
                case 8:

                    return "NewAddedCommentVI";
                case 9:

                    return "NewAddedVideo";
                default:
                    return string.Empty;
            }
           
        }

        

    }
}