﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Web;
using Healthware.HP3.Core.Web.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using Inclub.Web.Portal.Extensions;
using Healthware.HP3.Core.Content.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class LinkHelper
    {



        public static string ThemeLink(ControllerContext context, ThemeIdentificator key)
        {
            var theme = BL.Shared.ReadTheme(context.RequestContext, key);
            if (null == theme) return string.Empty;

            UrlHelper urlHelper = new UrlHelper(context.RequestContext);
            return urlHelper.RouteUrl(RouteConfigurations.Default.RouteName, new { controller = theme.SeoName.FormatLinkTitle(), action = string.Empty });
        }

       

        public static string ThemeLinkDetail(ControllerContext context, ThemeIdentificator key, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, string title)
        {
            var theme = BL.Shared.ReadTheme(context.RequestContext, key);
            if (null == theme) return string.Empty;

            UrlHelper urlHelper = new UrlHelper(context.RequestContext);
            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetailsImplicit.RouteName,
                 new
                 {
                     controller = theme.SeoName.FormatLinkTitle(),
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id
                 });

            return x;
        }

        public static int UrlId(System.Web.Routing.RouteData rd )
        {
            int idContent =0;

             if(null != rd.Values["id"] && !string.IsNullOrWhiteSpace(rd.Values["id"].ToString()))
                        {int.TryParse(rd.Values["id"].ToString(), out idContent);}

             return idContent;
        }

    }
}