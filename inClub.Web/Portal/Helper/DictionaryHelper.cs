﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Localization;

namespace Inclub.Web.Portal.Helper
{
    public class DictionaryHelper
    {
        public static string GetLabel(string dictKey)
        {
            string label = string.Empty;

            var manager = new DictionaryManager();
            var langKey = LanguageHelper.GetLanguageFromUrl();
            if (null != langKey)
            {
                label = manager.Read(dictKey, langKey.Id);
            }

            return label;

        }

        public static string GetLabel(string dictKey, int langId)
        {
            string label = string.Empty;

            var manager = new DictionaryManager();

            if (langId > 0)
            {
                label = manager.Read(dictKey, langId);
            }

            return label;

        }
    }
}
