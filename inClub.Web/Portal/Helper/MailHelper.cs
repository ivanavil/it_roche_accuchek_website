﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using Healthware.HP3.Core.Localization;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.Utility.ObjectValues;
using Healthware.HP3.Core.Web;

namespace Inclub.Web.Portal.Helper
{
    public class MailHelper
    {
        static MasterPageManager manager = null;

        public MailHelper()
        { }

        public static MailTemplateValue GetMailTemplate(String templateName)
        {
            var langKey = manager.GetLanguageFromUrl();
            if (null != langKey)
                return GetMailTemplate(templateName, langKey.Id);

            return null;
        }

        public static MailTemplateValue GetMailTemplate(String templateName, int langId)
        {
            MailTemplateSearcher so = new MailTemplateSearcher();

            MailTemplateManager templateManager = new MailTemplateManager();
            manager = new MasterPageManager();

            so.Key.KeyLanguage = new LanguageIdentificator(langId);
            so.KeySite = manager.GetSite();

            so.Key.MailTemplateName = templateName;

            var coll = templateManager.Read(so);

            if (coll != null && coll.Any())
            {
                return coll.FirstOrDefault();
            }

            return null;
        }

        public static void SendMail(MailTemplateIdentificator templateName,
                                        int langId,
                                        string htmlTemplate,
                                        Dictionary<String, String> replaceDictionary,
                                        bool throwException,
                                        string toAddress)
        {
            try
            {
                var template = GetMailTemplate(templateName.MailTemplateName, langId);
                if (null != template)
                {
                    MailValue message = new MailValue();
                    message.MailFrom = new Healthware.HP3.Core.Utility.MailAddress(template.SenderAddress, template.SenderName);

                    if (String.IsNullOrEmpty(toAddress))
                        message.MailTo = GetMailAddressCollection(template.Recipients, ',');
                    else
                    {
                        message.MailTo.Clear();
                        message.MailTo.Add(new Healthware.HP3.Core.Utility.MailAddress(toAddress));
                    }

                    message.MailCC = GetMailAddressCollection(template.CcRecipients, ',');
                    message.MailBcc = GetMailAddressCollection(template.BccRecipients, ',');
                    message.MailSubject = FieldReplace(template.Subject, replaceDictionary);

                    if (!string.IsNullOrWhiteSpace(htmlTemplate))
                    {
                        message.MailBody = htmlTemplate.Replace(Keys.LabelKeys.TEMPLATE_BODY_PLACEHOLDER, template.Body);
                    }
                    else
                    {
                        message.MailBody = template.Body;
                    }

                    if (replaceDictionary != null)
                    {
                        message.MailBody = FieldReplace(message.MailBody, replaceDictionary);
                        message.MailSubject = FieldReplace(message.MailSubject, replaceDictionary);

                    }

                    if (template.FormatType == 1)
                        message.MailIsBodyHtml = true;
                    else
                        message.MailIsBodyHtml = false;

                    message.MailPriority = MailValue.Priority.Normal;

                    //Save Mesasge in database
                    //var dictmanager = new DictionaryManager();
                    //var dictVal = new DictionaryValue();
                    //dictVal.Active = false;
                    //dictVal.Key.Name="";
                    //dictVal.Description = "On:<br>"+ DateTime.Now.ToString() + "<br>To:<br>" + message.MailTo.ToString() + "<br>Subject:<br>" + message.MailSubject + "<br>Body:<br>" + message.MailBody;
                    //dictVal.KeyLanguage.Id = 1;
                    //dictVal.Label = "ContactUs_Form_Message_Saved";
                    //dictVal=dictmanager.Create(dictVal);
                    //saved

                    SendMailMessage(message, throwException, string.Empty);

                    //if (dictVal != null && dictVal.Key.Id > 0)
                    //{
                    //    //per capire i messaggi che sono stati inviati 
                    //    dictVal.Key.Name = "sent";
                    //    dictVal = dictmanager.Update(dictVal);
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected static void SendMailMessage(object message, bool throwOn, string strSenderName)
        {
            try
            {
                MailValue oHp3MailValue = (MailValue)message;
                MailMessage oMail = new MailMessage();
                if (oHp3MailValue != null && oHp3MailValue.MailCC.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailCC)
                    {
                        oMail.CC.Add(objTo);
                    }

                }
                if (oHp3MailValue != null && oHp3MailValue.MailTo.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailTo)
                    {
                        oMail.To.Add(objTo);
                    }
                }
                if (oHp3MailValue != null && oHp3MailValue.MailBcc.Any())
                {
                    foreach (System.Net.Mail.MailAddress objTo in oHp3MailValue.MailBcc)
                    {
                        oMail.Bcc.Add(objTo);
                    }
                }



                oMail.ReplyToList.Add(new System.Net.Mail.MailAddress(String.Format("{0}<{1}>", "", "noreply@mail.healthware.it")));
                oMail.Headers.Add("Return-Path", "noreply@mail.healthware.it");
                oMail.From = new System.Net.Mail.MailAddress(oHp3MailValue.MailFrom.Address, oHp3MailValue.MailFrom.DisplayName);
               

                oMail.Subject = oHp3MailValue.MailSubject;
                oMail.IsBodyHtml = true;
                oMail.Priority = MailPriority.Normal;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(oHp3MailValue.MailBody, System.Text.Encoding.UTF8, "text/html");
                oMail.AlternateViews.Add(htmlView);

                if (oHp3MailValue != null && oHp3MailValue.MailAttachments.Count > 0)
                {
                    foreach (MailAttachment attachment in oHp3MailValue.MailAttachments)
                    {
                        oMail.Attachments.Add(attachment);
                    }
                }

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = ConfigurationManager.AppSettings["smtp"];
                smtpClient.Send(oMail);

            }
            catch (Exception ex)
            {
                if (throwOn)
                {
                    throw new ApplicationException(ex.Message, ex);
                }
                else
                {
                    //ErrorNotifier.NotifyByMail(ex);
                }
            }

        }

        protected static Healthware.HP3.Core.Utility.MailAddressCollection GetMailAddressCollection(string addressStr, char separator)
        {
            Healthware.HP3.Core.Utility.MailAddressCollection retCol = new Healthware.HP3.Core.Utility.MailAddressCollection();
            var addresses = addressStr.Split((char)separator);
            foreach (string address in addresses)
            {
                try
                {
                    Healthware.HP3.Core.Utility.MailAddress mailAddress = new Healthware.HP3.Core.Utility.MailAddress(address);
                    retCol.Add(mailAddress);
                }
                catch
                {
                    continue;
                }
            }
            return retCol;
        }

        protected static string FieldReplace(string str, Dictionary<string, string> fields)
        {
            foreach (KeyValuePair<string, string> item in fields)
            {
                str = str.Replace(item.Key, item.Value);
            }
            return str;
        }
    }
}
