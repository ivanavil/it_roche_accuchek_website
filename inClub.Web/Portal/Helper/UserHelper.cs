﻿using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.User;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Content;
using System.Linq;
using Inclub.Web.Portal.Keys;
using System;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using System.Collections.Generic;
using Healthware.HP3.Core.Site;

namespace Inclub.Web.Portal.Helper
{
    public class UserHelper
    {

      

        public static int getUserLoggedId()
        {

            var manager = new AccessService();
            int _idUser = 0;

            var ticket = manager.GetTicket();

            if (ticket != null && ticket.User != null && ticket.User.Key != null && ticket.User.Key.Id > 0)
            {
                _idUser = ticket.User.Key.Id;
            }

            manager = null;

            return _idUser;
        }


        public static UserBaseValue GetUserBaseValueById(int id)
        {

            if (id == 0)
                return null;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;
            UserBaseValue uVal = null;

            uSearch.Key.Id = id;
            uSearch.Properties.Add("Title");
            uSearch.Properties.Add("Name");
            uSearch.Properties.Add("Surname");

            uVal = uMan.Read(new UserIdentificator(id));

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));

            if (uColl != null && uColl.Count > 0)
            {
                uVal =  uColl.FirstOrDefault();                
            }

            return uVal;
        }


        public static RoleCollection getUserRoles()
        {
            var manager = new AccessService();
            RoleCollection _roles = null;

            var ticket = manager.GetTicket();

            if (ticket != null && ticket.User != null && ticket.User.Key != null && ticket.User.Key.Id > 0)
            {
                _roles = ticket.Roles;
            }

            manager = null;
            return _roles;
        }        


        public static UserLogged readUserLogged()
        {
            int IdUserLogged = getUserLoggedId();


            UserHCPManager userHCPManager = new UserHCPManager();
            UserLogged _return = null;
            UserHCPSearcher uso = null;
            if (IdUserLogged > 0)
            {

              
                    uso = new UserHCPSearcher();
                    uso.Key.Id = IdUserLogged;
                    uso.Status = 1;
                    uso.SetMaxRow = 1;

                    uso.Properties.Add("Key.Id");
                    uso.Properties.Add("Name");
                    uso.Properties.Add("Surname");
                    uso.Properties.Add("Email.GetString");
                    uso.Properties.Add("PasswordForceChange");
                    uso.Properties.Add("Title");
                    uso.Properties.Add("HCPStructure"); //has Spec
                    uso.Properties.Add("HCPOtherInfo.GetString"); //Specializzazionie Id
                    uso.Properties.Add("SiteAcquaintedThrougt"); //siteThrougt Id


                    Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uso));
                    //userManager.Cache = false;
                    var userColl = userHCPManager.Read(uso);

                    if (userColl != null && userColl.Any())
                    {
                        var u = userColl.First();                    

                        var retVal = Adapters.UserAdapter.AdaptLoggedUser(u);
                        _return = retVal;                        
                        
                    }

                    uso = null;
                    userColl = null;
                }            
            //clear
            userHCPManager = null;

            return _return;
        }


        public static bool UserExists(UserBase user)
        {
            var so = new UserSearcher();
            so.Username = user.Email;
            so.Properties.Add("Email.GetString");

            UserManager objctUserManager = new UserManager();
            Helper.CacheHelper.InsertKeyInGroupCache(CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(so));


            UserCollection coll = objctUserManager.Read(so);

            if (coll != null && coll.Any())
            {
                foreach (var item in coll)
                {
                    if (item.Email.ToString().ToLower().Equals(user.Email.ToLower()) && user.Id != item.Key.Id)
                        return true;
                }

            }

            return false;
        }          


        //E' basic quindi controllare  i parametri del lacy Load
        public static UserValue GetUserBaseValueByUserId(int userId)
        {

            if (userId == 0)
                return null;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;
            UserValue uVal = null;

            uSearch.Key.Id = userId;
            uSearch.Properties.Add("Title");
            uSearch.Properties.Add("Name");
            uSearch.Properties.Add("Surname");
            uSearch.Properties.Add("Email.GetString");


            uColl = uMan.Read(uSearch);

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));



            if (uColl != null && uColl.Count > 0)
            {

                uVal = uColl.FirstOrDefault();

            }

            return uVal;
        }


        //leggo l'utente by username
        public static UserValue GetUserBaseValueByUserName(string username, out UserValue uVal)
        {
            uVal = null;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;

            uSearch.Username = username;
            uSearch.Status = (int)Keys.EnumeratorKeys.UserStatus.Active; //solo utenti attivi

            uSearch.Properties.Add("Status");
            uSearch.Properties.Add("Title");
            uSearch.Properties.Add("Name");
            uSearch.Properties.Add("Surname");
            uSearch.Properties.Add("Username");
            uSearch.Properties.Add("Password");
            uSearch.Properties.Add("Email.GetString");
            uSearch.Properties.Add("SiteAcquaintedThrougt");


            uColl = uMan.Read(uSearch);

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));

            if (uColl != null && uColl.Count > 0)
            {
                uVal = uColl.FirstOrDefault();
            }

            return uVal;
        }

        public static EnumeratorKeys.UserStatus GetUserTypeByEmail(string email)
        {
            EnumeratorKeys.UserStatus type = EnumeratorKeys.UserStatus.NotFound;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;
            UserValue uVal = null;

            email = email.Trim();

            uSearch.Username = email;
            uSearch.Properties.Add("Status");
            uSearch.Properties.Add("Email.GetString");

            uColl = uMan.Read(uSearch);

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));

            if (uColl != null && uColl.Count > 0)
            {
                if (uColl.Count > 1)
                {
                    foreach (UserValue u in uColl)
                    {

                        if (u.Status != (int)Keys.EnumeratorKeys.UserStatus.Disable)
                        {
                            type = GetType(email, u);
                            return type;
                        }
                    }
                }
                else
                {
                    uVal = uColl.FirstOrDefault();
                }
                type = GetType(email, uVal);
            }

            return type;
        }


        public static EnumeratorKeys.UserStatus GetUserValueAndTypeByEmail(string email, out UserValue uVal)
        {
            EnumeratorKeys.UserStatus type = EnumeratorKeys.UserStatus.NotFound;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;
            uVal = null;

            email = email.Trim();

            uSearch.Username = email;
            uSearch.Properties.Add("Title");
            uSearch.Properties.Add("Name");
            uSearch.Properties.Add("Surname");
            uSearch.Properties.Add("Status");
            uSearch.Properties.Add("Email.GetString");
            uSearch.Properties.Add("Password");

            uColl = uMan.Read(uSearch);

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));


            //Può capitare che ci siano email duplicate nel portale perchè un utente potrebbe avere status Reject=4 , in quel caso è possibile registrare un utente.
            //per evitare problemi in fase di lettura dati data l'email, nel caso di email doppie escludo il type reject.
            //es. entro con email test@test.it, per questa email potrei avere un utente con status 1 o 2 o 3, ed un' altro  con status 4(Reject), fa fede quello con status diverso da 4
            if (uColl != null && uColl.Count > 0)
            {

                if (uColl.Count > 1)
                {
                    foreach (UserValue u in uColl)
                    {
                        //sarà valido il primo utente trovato diverso da status reject
                        if (u.Status != (int)Keys.EnumeratorKeys.UserStatus.Disable)
                        {
                            uVal = u;
                            type = GetType(email, uVal);
                            return type;
                        }
                    }
                }
                else
                {
                    uVal = uColl.FirstOrDefault();
                }
                type = GetType(email, uVal);


            }

            return type;
        }

        public static UserBaseValue GetUserBaseValueByEmail(string email)
        {

            if (string.IsNullOrWhiteSpace(email))
                return null;

            UserManager uMan = new UserManager();
            UserSearcher uSearch = new UserSearcher();
            UserCollection uColl = null;
            UserBaseValue uVal = null;

            email = email.Trim();

            uSearch.Username = email;
            uSearch.Properties.Add("Title");
            uSearch.Properties.Add("Name");
            uSearch.Properties.Add("Surname");


            uColl = uMan.Read(uSearch);

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(uSearch));


            //Può capitare che ci siano email duplicate nel portale perchè un utente potrebbe avere status Reject=4 , in quel caso è possibile registrare un utente.
            //per evitare problemi in fase di lettura dati data l'email, nel caso di email doppie escludo il type reject.
            //es. entro con email test@test.it, per questa email potrei avere un utente con status 1 o 2 o 3, ed un' altro  con status 4(Reject), fa fede quello con status diverso da 4
            if (uColl != null && uColl.Count > 0)
            {
                if (uColl.Count > 1)
                {
                    foreach (UserValue u in uColl)
                    {
                        //sarà valido il primo utente trovato diverso da status reject
                        if (u.Status != (int)Keys.EnumeratorKeys.UserStatus.Disable)
                        {
                            return u;
                        }
                    }
                }
                else
                {
                    uVal = uColl.FirstOrDefault();
                }
            }

            return uVal;
        }

      


        public static void SubscribeNewsletter(int userId, string userEmail, int newsdetterId)
        {
            bool subscribed = IsSubcribedToNewsletter(userId, userEmail, newsdetterId);
            if (!subscribed)
            {
                NewsletterSubscriptionValue subscription = new NewsletterSubscriptionValue();
                subscription.DateSubscription = DateTime.Now;
                subscription.Email = userEmail.ToLower();
                subscription.KeyNewsletterList = newsdetterId;
                subscription.KeyUser.Id = userId;
                NewsletterManager manager = new NewsletterManager();
                manager.Cache = false;
                manager.Subscribe(subscription);

            }
        }

        public static void UnsubscribeNewsletter(int userId, string userEmail, int newsdetterId)
        {
            NewsletterSubscriptionValue subscription = new NewsletterSubscriptionValue();
            subscription.Email = userEmail;
            subscription.KeyNewsletterList = newsdetterId;
            subscription.KeyUser.Id = userId;
            subscription.DateUnSubscription = DateTime.Now;
            NewsletterManager manager = new NewsletterManager();
            manager.UnSubscribe(subscription);

        }

        public static bool IsSubcribedToNewsletter(int userId, string userEmail, int newsdetterId)
        {
            NewsletterSubscriptionSearcher so = new NewsletterSubscriptionSearcher();
            so.Email = userEmail;
            so.KeyUser.Id = userId;
            so.KeyNewsletterList = newsdetterId;
            NewsletterManager man = new NewsletterManager();

            man.Cache = false;
            var coll = man.Read(so);
            if (null != coll && coll.Any())
            {
                foreach (NewsletterSubscriptionValue item in coll)
                {
                    if (!item.DateUnSubscription.HasValue)
                        return true;
                }
            }
            return false;
        }


        public static UserLogIn ReadLogInUser(int UserId, string _returnUrl)
        {

            UserSearcher so = new UserSearcher();

            so.Key.Id = UserId;

            so.Properties.Add("Password");
            so.Properties.Add("Email.GetString");

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserManager man = new UserManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptLogInUser(user, _returnUrl);
                }
            }

            return null;
        }

        public static bool HaveAccess()
        {

            var manager = new AccessService();
            var ticket = manager.GetTicket();
            Boolean _HaveAccess = false;
            var roleMng = new ProfilingManager();
            int userID = ticket.User.Key.Id;
            if (userID > 0)
                _HaveAccess = roleMng.HasRole(new UserIdentificator(userID), Inclub.Web.Portal.Keys.RoleKeys.userAccess);

            return _HaveAccess;
        }

        public static EnumeratorKeys.UserStatus GetUserStatus(int idUser)
        {
            var so = new UserSearcher();
            so.Key.Id = idUser;
            so.Properties.Add("Status");

            UserManager objctUserManager = new UserManager();
            CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, CacheHelper.getKeyBySearcher(so));

            UserCollection coll = objctUserManager.Read(so);
            EnumeratorKeys.UserStatus retVal = EnumeratorKeys.UserStatus.NotFound;

            if (coll != null && coll.Any())
            {
                UserValue user = coll.FirstOrDefault();
                if (null != user)
                {
                    retVal = (EnumeratorKeys.UserStatus)user.Status;
                }
            }

            return retVal;
        }

        private static EnumeratorKeys.UserStatus GetType(string email, UserValue uVal)
        {
            EnumeratorKeys.UserStatus type = EnumeratorKeys.UserStatus.NotFound;

            if (uVal != null)
            {
                if (uVal.Email[0] == email)
                {
                    switch (uVal.Status)
                    {
                        //Attivo
                        case 1:
                            type = EnumeratorKeys.UserStatus.Active;
                            break;
                        //Pending
                        case 2:
                            type = EnumeratorKeys.UserStatus.Pending;
                            break;                 
                        case 3:
                            type = EnumeratorKeys.UserStatus.NoActive;
                            break;                     
                        case 4:
                            type = EnumeratorKeys.UserStatus.Disable;
                            break;
                        case 5:
                            type = EnumeratorKeys.UserStatus.Deleted;
                            break;
                        default:
                            break;
                    }
                }
            }

            return type;
        }



    }
}