﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Utility;

namespace Inclub.Web.Portal.Helper
{
    public class DocumentiHelper
    {

        public static inClub.Web.Portal.Models.Documenti GetResource(ContentValue vo, UserBaseValue Owner, int groupId, int commentid)
        {
            DownloadManager objDownloadManager = new DownloadManager();
            DownloadSearcher sodw = new DownloadSearcher();
            sodw.Properties.Add("Title");
            sodw.Properties.Add("Launch");
            sodw.Properties.Add("DatePublish");
            sodw.Properties.Add("Copyright");
            sodw.Properties.Add("Code");
            sodw.Properties.Add("Qty");
            sodw.Properties.Add("IdContentSubCategory");

            sodw.key = vo.Key;
            sodw.key.IdLanguage = vo.Key.IdLanguage;
            sodw.KeyContentType = vo.ContentType.Key;
            sodw.KeySiteArea = vo.SiteArea.Key;

            sodw.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;

            objDownloadManager.Cache = false;
            DownloadCollection downColl = objDownloadManager.Read(sodw);

            if (downColl != null && downColl.Count > 0) {
                DownloadValue downValue  = new DownloadValue();
                downValue = downColl[0];
                //downValue.IdContentSubCategory = vo.IdContentSubCategory;

                if ( downValue.DownloadTypeCollection !=null && downValue.DownloadTypeCollection.Count > 0) {
                        DownloadTypeValue dwnType = downValue.DownloadTypeCollection[0];
                        return ResourceAdapter(downValue, Owner, groupId, dwnType.DownloadType.Label, dwnType.Weight, commentid);
                }

            }
            else
            {

                return FolderAdapter(vo, Owner, groupId);
            }

            return null;

        }

        public static inClub.Web.Portal.Models.Documenti ResourceAdapter(DownloadValue dwnl,  UserBaseValue Owner , int groupId, string estension, int weight , int commentid = 0)
        {
           
            if (dwnl != null) 
            {

                int folderId;
                bool test =  int.TryParse(dwnl.Code, out folderId);

                    inClub.Web.Portal.Models.Documenti resource = new inClub.Web.Portal.Models.Documenti();

                    resource.ResourceKey = dwnl.Key;        
                    resource.CodeGetFile = SimpleHash.UrlEncryptRijndaelAdvanced(dwnl.Key.PrimaryKey.ToString());
                    resource.Name = dwnl.Title;
                    resource.Description = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(HttpContext.Current.Server.HtmlDecode(dwnl.Launch));
                    resource.Type = "file";
                    resource.Ext = estension;
                    resource.TypeDoc = (dwnl.IdContentSubCategory == 1 ? "documento" : "video");
                    resource.GroupKey = new ContentIdentificator();
                    resource.GroupKey.Id = groupId;
        
                    if (dwnl.DatePublish.HasValue){
                        resource.CreationDate = dwnl.DatePublish.Value.ToShortDateString();
                    }else{
                        resource.CreationDate = System.DateTime.Now.ToString();
                    }

                    resource.OwnerID = new UserIdentificator();
                    resource.OwnerID.Id = ((Owner == null)? 0: Owner.Key.Id);
                    resource.OwnerName = ((Owner == null)? String.Empty: Owner.Title.ToLower() + " " + Owner.Name.ToLower() + " " + Owner.Surname.ToLower());
        
                    resource.NumDownload = dwnl.Qty;
                    resource.SizeKb = roundObjectSize(weight.ToString());
                    resource.Folder = (String.IsNullOrEmpty(dwnl.Copyright)? "Cartella principale": dwnl.Copyright);
                    resource.FolderID = ((String.IsNullOrEmpty(dwnl.Code)) ? 0 : folderId);
                    resource.linkFile = (resource.TypeDoc == "documento" ? "/Portal/_async/DownloadFile.ashx?intPk=" + resource.CodeGetFile : "/Documenti/ViewVideo?intPk=" + resource.CodeGetFile);
                    return resource;
             }
            
            return null;
        }


        public static inClub.Web.Portal.Models.Documenti FolderAdapter(ContentValue vo, UserBaseValue Owner , int groupId)
        {
            if (vo != null)
            {
        

                inClub.Web.Portal.Models.Documenti resource = new inClub.Web.Portal.Models.Documenti();
                resource.ResourceKey = vo.Key;
                resource.CodeGetFile = SimpleHash.UrlEncryptRijndaelAdvanced(vo.Key.PrimaryKey.ToString());                
                resource.Name = vo.Title;
                resource.Description = Healthware.HP3.Core.Utility.StringUtility.CleanHtml(HttpContext.Current.Server.HtmlDecode(vo.Launch));
                resource.Type = "folder";
                resource.GroupKey = new ContentIdentificator();
                resource.GroupKey.Id = groupId;
                resource.CreationDate = vo.DatePublish.Value.ToShortDateString();
                resource.OwnerID = new UserIdentificator();
                resource.OwnerID.Id = (Owner == null? 0: Owner.Key.Id);
                resource.OwnerName = (Owner == null ? String.Empty: Owner.Title.ToLower() + " " + Owner.Name.ToLower() + " " + Owner.Surname.ToLower());

                resource.SizeKb = vo.Qty.ToString();

                return resource;

            }

            return null;
        }


        public static  string roundObjectSize(string ObjectSize)
        {
           const int oneByte = 1;
           const int kiloByte = 1024;
           const int megaByte = 1048576;
           const int gigaByte = 1073741824;
           const long terraByte = 1099511627776L;
           
            switch (Convert.ToInt64(ObjectSize))
            {
                case 0: // TODO: to kiloByte - 1
                    if ((Convert.ToDouble(checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / oneByte))) >= 1000) == false)
                    {
                        ObjectSize = Convert.ToString(Convert.ToInt32(ObjectSize) / 1) + " Bytes";
                    }
                    else
                    {
                        ObjectSize = "1,00 KB";
                    }

                    break;
                case kiloByte: // TODO: to megaByte - 1
                    if ((Convert.ToDouble(checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / kiloByte))) >= 1000) == false)
                    {
                        ObjectSize = checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / kiloByte)) + " KB";
                    }
                    else
                    {
                        ObjectSize = "1,00 MB";
                    }

                    break;
                case megaByte: // TODO: to gigaByte - 1
                    if ((Convert.ToDouble(checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / megaByte))) >= 1000) == false)
                    {
                        ObjectSize = checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / megaByte)) + " MB";
                    }
                    else
                    {
                        ObjectSize = "1,00 GB";
                    }

                    break;
                case gigaByte: // TODO: to terraByte - 1
                    if ((Convert.ToDouble(checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / gigaByte))) >= 1000) == false)
                    {
                        ObjectSize = checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / gigaByte)) + " GB";
                    }
                    else
                    {
                        ObjectSize = "1,00 TB";
                    }

                    break;
                case terraByte: // TODO: to pettaByte - 1
                    if ((Convert.ToDouble(checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / terraByte))) >= 1000) == false)
                    {
                        ObjectSize = checkIfValueIsDecimal(Convert.ToString(Convert.ToDecimal(ObjectSize) / terraByte)) + " TB";
                    }
                    else
                    {
                        ObjectSize = "1,00 PB";
                    }
                    break;
            }
            return ObjectSize;
        }


        public static string checkIfValueIsDecimal(string value)
        {
            string result = null;
            if (value.Contains(","))
            {
                result = Convert.ToDouble(value).ToString("###.##");
            }
            else
            {
                result = Convert.ToDouble(value).ToString("###.00");
            }
            return result;
        }


        public static int groupIsOpen(int id, short lang_id)
        {
            int result = 0;

            var so = new ContentSearcher();
            so.KeySite.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
            so.key = new ContentIdentificator(id, lang_id);            
            so.Properties.Add("IdContentSubCategory"); //gruppo aperto            
            so.KeySiteArea.Id = Inclub.Web.Portal.Keys.SiteAreaKeys.Gruppi.Id;
            so.KeyContentType.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.Gruppi.Id;
            
            ContentManager objContentManager = new ContentManager();

            ContentCollection groups = objContentManager.Read(so);

            if (groups != null && groups.Any())
            {
                result = groups[0].IdContentSubCategory;
            }

            return result;
        }

    }
}
