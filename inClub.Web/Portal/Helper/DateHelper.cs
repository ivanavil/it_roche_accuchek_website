﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Globalization;

namespace Inclub.Web.Portal.Helper
{
    public static class DateHelper
    {


        public static string getDate(string dateStart)
        {
            DateTime _dateN = DateTime.Now;
            if (dateStart == null)
            { _dateN = DateTime.Parse(dateStart,new CultureInfo("it-IT", true)); }

            return getFormatDate(_dateN);  

            //return string.Format("{0:d} {1:MMM} - {2:d} {3:MMM}, {4}", dateStart.Day, dateStart, dateEnd.Day, dateEnd, dateEnd.Year);
        }


        public static string getFormatDate(DateTime dateStart)
        {
            if (dateStart == null )
                return string.Empty;

        
                return string.Format("{0:d} {1:MMM}, {2}", dateStart.Day, dateStart, dateStart.Year);

          

            //return string.Format("{0:d} {1:MMM} - {2:d} {3:MMM}, {4}", dateStart.Day, dateStart, dateEnd.Day, dateEnd, dateEnd.Year);
        }

        public static string getFormatDateEvent(DateTime dateStart, DateTime dateEnd)
        {
            if (dateStart == null || dateEnd == null)
                return string.Empty;

            if (dateStart.Equals(dateEnd))
                return string.Format("{0:d} {1:MMM}, {2}", dateStart.Day, dateStart, dateStart.Year);

            if (dateStart.Month.Equals(dateEnd.Month))
                return string.Format("{0:d} - {1:d} {2:MMM}, {3}", dateStart.Day, dateEnd.Day, dateStart, dateEnd.Year);

            return string.Format("{0:d} {1:MMM} - {2:d} {3:MMM}, {4}", dateStart.Day, dateStart, dateEnd.Day, dateEnd, dateEnd.Year);
        }

        public static string FormatDateEventShort(DateTime dateStart, DateTime dateEnd)
        {
            if (dateStart == null || dateEnd == null)
                return string.Empty;

            if (dateStart.Equals(dateEnd))
                return string.Format("{0:d} {1:MMM}", dateStart.Day, dateStart);

            if (dateStart.Month.Equals(dateEnd.Month))

                return string.Format("from {0:d} <br>to {1:d} {2:MMM}", dateStart.Day, dateEnd.Day, dateStart);

            return string.Format("from {0:d} {1:MMM} <br>to {2:d} {3:MMM}", dateStart.Day, dateStart, dateEnd.Day, dateEnd);
        }

    }
}