﻿using System.Collections.Generic;
using System.Linq;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Common.Extensions;

namespace Inclub.Web.Portal.Helper
{
    public class ThemeHelper
    {

        public ThemeHelper()
        {

        }

        public static ThemeValue GetThemeValueByThemeId(int ThemeId)
        {
            ThemeManager manager = new ThemeManager();
            ThemeValue obj = null;
            obj = manager.Read(new ThemeIdentificator(ThemeId));

            manager = null;

            return obj;
        }

        public static ThemeValue getThemeTraceValue(ThemeIdentificator themeIdentificator)
        {
            ThemeValue obj = null;
            ThemeManager objManager = null;

            string cacheKey = string.Format(Keys.CacheKeys.getThemeTraceValue, themeIdentificator.Id);

            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                obj = (ThemeValue)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
            }
            else
            {
                var so = new ThemeSearcher();

                objManager = new ThemeManager();

                //Searcher
                so.LoadHasSon = false;
                so.KeySite = Keys.SiteAreaKeys.PortalSite;
                so.Key.Id = themeIdentificator.Id;
                so.Display = SelectOperation.All();
                so.Active = SelectOperation.Enabled();
                so.Delete = SelectOperation.Enabled();

                //LacyLoad                                                                                
                so.Properties.Add(th => th.KeyContentType.Id);
                so.Properties.Add(th => th.KeySiteArea.Id);
                so.Properties.Add(th => th.KeyTraceEvent.Id);


                so.LoadRoles = false;
                var coll = objManager.Read(so);

                if (null != coll && coll.Any())
                {
                    obj = coll.First();
                }

                CacheManager.Insert(cacheKey, obj, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);

                //clear
                objManager = null;


            }

            return obj;
        }

        public static ThemeValue GetThemeBaseValue(ThemeIdentificator themeKey)
        {
            if (themeKey == null)
                return null;
            ThemeValue objTheme = null;
            var so = new ThemeSearcher();
            so.Key = themeKey;
            so.Delete = SelectOperation.Enabled();
            so.Active = SelectOperation.All();
            so.Display = SelectOperation.All();
            so.LoadHasSon = false;
            so.LoadRoles = false;

            so.Properties.Add(th => th.Description);
            so.Properties.Add(th => th.KeySiteArea.Domain);
            so.Properties.Add(th => th.KeySiteArea.Id);
            so.Properties.Add(th => th.KeySiteArea.Name);
            so.Properties.Add(th => th.KeyContentType.Domain);
            so.Properties.Add(th => th.KeyContentType.Name);
            so.Properties.Add(th => th.KeyContentType.Id);
            so.Properties.Add(th => th.SeoName);

            var objManager = new ThemeManager();
            var coll = objManager.Read(so);

            if (coll != null && coll.Any())
            {
                objTheme = coll.FirstOrDefault();
            }

            objManager = null;
            coll = null;

            return objTheme;
        }


        public static string GetThemeDescription(ThemeIdentificator themeKey)
        {
            if (themeKey == null)
                return string.Empty;

            var so = new ThemeSearcher();
            so.Key = themeKey;
            so.LoadHasSon = false;
            so.LoadRoles = false;

            so.Properties.Add(th => th.Description);

            var objManager = new ThemeManager();
            var coll = objManager.Read(so);

            if (coll != null && coll.Any())
            {
                return coll[0].Description;
            }

            objManager = null;
            return string.Empty;

        }

        public static ThemeIdentificator GetCurrentThemeIdentificator()
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            return objMasterPageManager.GetTheme();
        }

        public static ThemeCollection getThemeMenuCollectionByFather(ThemeIdentificator themeFatherIdentificator)
        {
            ThemeCollection coll = null;
            ThemeManager objManager = null;

            string cacheKey = string.Format(Keys.CacheKeys.getThemeCollectionByFather, themeFatherIdentificator.Id, LanguageHelper.GetLanguageFromUrl().Code);

            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                coll = (ThemeCollection)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
            }
            else
            {
                var so = new ThemeSearcher();

                objManager = new ThemeManager();

                //Searcher
                so.LoadHasSon = false;
                so.KeySite = Keys.SiteAreaKeys.PortalSite;
                so.KeyLanguage = LanguageHelper.GetLanguageFromUrl();
                so.KeyFather = themeFatherIdentificator;
                so.Display = SelectOperation.Enabled();
                so.Active = SelectOperation.Enabled();
                so.Delete = SelectOperation.Enabled();

                so.LoadRoles = true;
                coll = objManager.Read(so);

                CacheManager.Insert(cacheKey, coll, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);

                //clear
                objManager = null;


            }

            return coll;
        }

        public static ThemeValue getThemeValueForLink(SiteAreaIdentificator SAkey, ContentTypeIdentificator CTKey)
        {
            ThemeValue obj = null;
            var so = new ThemeSearcher();
            so.KeyContentType = CTKey;
            so.KeySiteArea = SAkey;
            so.KeySite.Id = Keys.SiteAreaKeys.PortalSite.Id;
            so.Display = SelectOperation.All(); ;
            so.LoadHasSon = false;
            so.LoadRoles = false;

            so.Properties.Add(th => th.KeyContent.Id);
            so.Properties.Add(th => th.SeoName);

            var objManager = new ThemeManager();
            var coll = objManager.Read(so);

            if (coll != null && coll.Any())
            {
                obj = coll.FirstOrDefault();
            }

            coll = null;
            objManager = null;


            return obj;

        }
        public static ThemeValue getThemeValueForLink(ThemeIdentificator keyTh)
        {
            ThemeValue obj = null;
            var so = new ThemeSearcher();
            so.Key = keyTh;
            so.KeySite.Id = Keys.SiteAreaKeys.PortalSite.Id;
            so.LoadHasSon = false;
            so.LoadRoles = false;

            so.Properties.Add(th => th.KeyContent.Id);
            so.Properties.Add(th => th.SeoName);

            var objManager = new ThemeManager();
            var coll = objManager.Read(so);

            if (coll != null && coll.Any())
            {
                obj = coll.FirstOrDefault();
            }

            coll = null;
            objManager = null;


            return obj;

        }

        public static ThemeIdentificator getCurrentThemeIdentificator()
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            return objMasterPageManager.GetTheme();
        }

        public static string SiteLink()
        {
            SiteManager siteMan = new SiteManager();
            SiteValue site = siteMan.Read(Keys.SiteAreaKeys.PortalSite);
            if (null != site)
            {
                return site.Domain;
            }
            return string.Empty;
        }
    }
}
