﻿using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Content.Persistent;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Search;
using Healthware.HP3.Core.Search.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Models;
using System.Collections.Generic;
using System.Linq;
using Inclub.Web.Portal.Keys;
using System;

namespace Inclub.Web.Portal.Helper
{
    public class ContentHelper
    {
        public static GenericContentDetails GetDetailGenCont(ContentSearcher so)
        {
            if (so == null)
                return null;

            var manager = new ContentManager();
            ContentValue objContentValue = null;

            var coll = manager.Read(so);
            GenericContentDetails model = null;

            if (coll != null && coll.Any())
            {
                objContentValue = coll.FirstOrDefault();
                model = Adapters.ContentAdapter.AdaptDetails(objContentValue);
            }

            return model;

        }


        public static void CreateUserContent(int userId, int contentId, int RelTypeID)
        {

            if (userId != 0 && contentId != 0 && RelTypeID != 0)
            {

                bool exist = ExistUserContent(userId, contentId, RelTypeID);

                if (!exist)
                {
                    ContentUserValue ctxUsr = new ContentUserValue();
                    ctxUsr.KeyContent.Id = contentId;
                    ctxUsr.KeyUser.Id = userId;
                    ctxUsr.KeyRelationType.Id = RelTypeID;
                    ctxUsr.DateCreate = DateTime.Now;
                    ctxUsr.Order = 1;
                    ctxUsr.Rank = 100;
                    ContentManager usrCtxMan = new ContentManager();
                    usrCtxMan.CreateContentUser(ctxUsr);
                    usrCtxMan = null;
                    ctxUsr = null;
                    //CacheHelper.DeleteGroupCache(CacheKeys.GroupCacheUser);
                    CacheHelper.DeleteGroupCache(CacheKeys.CACHE_GROUP_USERCONTEXT);
                }

            }
        }


        public static bool ExistUserContent(int userId, int contentId, int RelTypeID)
        {

            if (userId == 0 || contentId == 0)
                return false;

            var roleMng = new ProfilingManager();

            if (roleMng.HasRole(new UserIdentificator(userId), Inclub.Web.Portal.Keys.RoleKeys.SysAdmin))
                return true;

            ContentUserSearcher ctxUsr = new ContentUserSearcher();
            ctxUsr.KeyContent.Id = contentId;
            ctxUsr.KeyUser.Id = userId;
            if (RelTypeID > 0) { 
                ctxUsr.KeyRelationType.Id = RelTypeID;
            }

            ContentManager usrCtxMan = new ContentManager();
            ContentUserCollection cColl = new ContentUserCollection();
            cColl = usrCtxMan.ReadContentUser(ctxUsr);
            if (cColl != null && cColl.Any())
            {
                return true;


            }

            usrCtxMan = null;
            ctxUsr = null;
            return false;

        }



        public static ContentExtraValue GetBoxHomeContent(string CodeBox)
        {
            var Val = new ContentExtraValue();
            Val = null;
            var Coll = new ContentExtraCollection();
            var Man = new ContentExtraManager();
            var searcher = new ContentExtraSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                KeySiteArea = Keys.SiteAreaKeys.BoxHome,
                KeyContentType = Keys.ContentTypeKeys.GenCont,
                KeySite = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite,
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC,
                Code = CodeBox.ToString()

            };

            var urlLang = LanguageHelper.GetLanguageFromUrl();
            if (null != urlLang)
                searcher.key.IdLanguage = (short)urlLang.Id;

            //LacyLoad
            searcher.CalculatePagerTotalCount = false;


            Coll = Man.Read(searcher);
            if (Coll != null && Coll.Any())
            {
                Val = Coll[0];
            }



            return Val;

        }


        public static GenericContentDetails GetDetailExtraCont(ContentExtraSearcher so)
        {
            if (so == null)
                return null;


            var manager = new ContentExtraManager();
            ContentExtraValue objContentExtraValue = null;

            var coll = manager.Read(so);
            GenericContentDetails model = null;
            if (coll != null && coll.Any())
            {
                objContentExtraValue = coll.FirstOrDefault();
                model = Adapters.ContentAdapter.AdaptDetails(objContentExtraValue);
            }
            return model;
        }


        public static IEnumerable<ThemeModel.ThemeContentModel> GetThemeContents(IEnumerable<ThemeValue> tcoll, ContentSearcher so)
        {
            if (so == null)
                return null;

            if (tcoll == null)
                return null;

            var manager = new ContentManager();

            var modelColl = new List<ThemeModel.ThemeContentModel>();


            if (tcoll != null && tcoll.Any())
            {
                foreach (ThemeValue tv in tcoll)
                {
                    so.key = tv.KeyContent;
                    so.key.IdLanguage = (short)tv.KeyLanguage.Id;
                    so.KeyContentType = ContentTypeKeys.GenContExt;  //ContentType specifico per il Menu

                    var coll = manager.Read(so);

                    if (coll != null && coll.Any())
                    {
                        ContentValue objContentValue = coll.FirstOrDefault();

                        ThemeModel.ThemeContentModel objThemeContentModel = Adapters.ContentAdapter.AdaptThemeContent(objContentValue, tv);

                        if (objThemeContentModel != null)
                        {
                            modelColl.Add(objThemeContentModel);
                        }

                    }
                }
            }



            return modelColl;
        }



        public static inClub.Web.Portal.Models.Archive<GenericContentDetails> GetListOfExtraCont(ContentExtraSearcher so)
        {
            if (so == null)
                return null;

            var Manager = new ContentExtraManager();
            ContentExtraCollection coll = Manager.Read(so);

            var modelColl = new inClub.Web.Portal.Models.Archive<GenericContentDetails>();

            if (coll != null && coll.Any())
            {
                modelColl.Items = (from m in coll
                                   select Adapters.ContentAdapter.AdaptDetails(m)).ToList();

                modelColl.CurrentPage = so.PageNumber;
                modelColl.TotalPagesCount = coll.PagerTotalNumber;
                modelColl.TotalItemsCount = coll.PagerTotalCount;

            }

            return modelColl;
        }



        public static void mapComment(int intCommentId, int intDiscussionId, int intGroupId, string strDiscussionTitle, int intOption, bool doIndex = true)
        {
            //intOption = 1 Create Content
            //intOption = 2 Update Content
            //intOption = 3 Disattiva Content

            ContentManager oContentManager = new ContentManager();
            if (intOption == 1)
            {
                CommentManager oCommentManager = new CommentManager();
                oCommentManager.Cache = false;
                CommentValue oCommentVal = oCommentManager.Read(new CommentIdentificator(intCommentId));

                if ((oCommentVal != null) && oCommentVal.Key.Id > 0)
                {
                    if (!string.IsNullOrWhiteSpace(oCommentVal.Title))
                    {
                        int intCId = 0;
                        int.TryParse(oCommentVal.Title, out intCId);
                        if (intCId > 0)
                            oContentManager.Update(new ContentIdentificator(intCId, 1), "Delete", 0);

                    }
                    else
                    {
                        ContentValue oMappingContent = new ContentValue();
                        var _with1 = oMappingContent;
                        _with1.Title = strDiscussionTitle;
                        _with1.Launch = oCommentVal.Text;
                        _with1.Active = true;
                        _with1.ApprovalStatus = ContentBaseValue.ApprovalWFStatus.Approved;
                        _with1.DateCreate = DateTime.Now;
                        _with1.DateExpiry = DateTime.Now;
                        _with1.DateInsert = DateTime.Now;
                        _with1.DateUpdate = DateTime.Now;
                        _with1.Display = true;
                        _with1.Key.IdLanguage = 1;
                        _with1.ContentType.Key.Id = Inclub.Web.Portal.Keys.ContentTypeKeys.DiscPost.Id;
                        _with1.IdContentSubType = ContentBaseValue.Subtypes.Content;
                        _with1.Indexable = true;
                        _with1.Owner.Key.Id = oCommentVal.Author.Key.Id;
                        _with1.DatePublish = DateTime.Now;

                        _with1.Rank = oCommentVal.Key.Id;
                        _with1.Qty = intGroupId;
                        _with1.Code = intDiscussionId.ToString();

                        oMappingContent = oContentManager.Create(oMappingContent);

                        if ((oMappingContent != null) && oMappingContent.Key.Id > 0)
                        {
                            //Update commento inserendo l'id del contenuto
                            oCommentVal.Title = oMappingContent.Key.Id.ToString();
                            oCommentManager.Update(oCommentVal);
                            //-------------------------------------------

                            //Accoppiamento sitearea gruppi------------------------
                            ContentSiteAreaValue oCoSaVal = new ContentSiteAreaValue();
                            var _with2 = oCoSaVal;
                            _with2.KeyContent = oMappingContent.Key;
                            _with2.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.DiscPost.Id);
                            oContentManager.CreateContentSiteArea(oCoSaVal);
                            //-------------------------------------------------------

                            //Accoppiamento sito-----------------------------------
                            oCoSaVal.KeySiteArea = new SiteAreaIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                            oContentManager.CreateContentSiteArea(oCoSaVal);
                            oCoSaVal = null;

                            //Accoppiamento contributo mappato col gruppo
                            ContentRelationValue oContRelValue = new ContentRelationValue();
                            var _with3 = oContRelValue;
                            _with3.KeyContent.PrimaryKey = oContentManager.ReadPrimaryKey(intGroupId, 1);
                            _with3.KeyContentRelation = oMappingContent.Key;
                            _with3.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.MapDiscAtt;
                            oContentManager.CreateContentRelation(oContRelValue);

                            if (doIndex)
                            {
                                //Indicizzazione del contenuto che mappa il commento--------------
                                IndexerModule indexM = new IndexerModule();
                                PositionValue po = new PositionValue();
                                po.Language = new LanguageIdentificator(1);
                                po.Site = new SiteIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                                indexM.AddContent(new ContentIdentificator(oMappingContent.Key.Id, 1), po);
                                //---------------------------------------------------------------
                            }


                        }
                    }
                }

                //Update
            }
            else if (intOption == 2)
            {
                CommentManager oCommentManager = new CommentManager();
                oCommentManager.Cache = false;
                CommentValue oCommentVal = oCommentManager.Read(new CommentIdentificator(intCommentId));
                if ((oCommentVal != null) && oCommentVal.Key.Id > 0)
                {
                    int intCId = 0;
                    int.TryParse(oCommentVal.Title.Trim(), out intCId);

                    if (intCId > 0)
                    {
                        ContentSearcher oContentSearcher = new ContentSearcher();
                        var _with4 = oContentSearcher;
                        _with4.key.Id = intCId;
                        _with4.key.IdLanguage = 1;
                        _with4.Active = SelectOperation.All();
                        _with4.Display = SelectOperation.All();


                        ContentCollection oContColl = oContentManager.Read(oContentSearcher);
                        ContentValue oContVal = null;
                        if ((oContColl != null) && oContColl.Any())
                        {
                            oContVal = new ContentValue();
                            oContVal = oContColl[0];
                            oContVal.Launch = oCommentVal.Text;
                            oContentManager.Update(oContVal);

                            //Indicizzazione del contenuto che mappa il commento--------------
                            if (oContVal.Active == true & oContVal.Display == true)
                            {
                                try
                                {
                                    IndexerModule indexM = new IndexerModule();
                                    PositionValue po = new PositionValue();
                                    po.Language = new LanguageIdentificator(1);
                                    po.Site = new SiteIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                                    indexM.AddContent(oContVal.Key, po);
                                    //indexM.AddContent(New ContentIdentificator(Integer.Parse(oCommentVal.Title), 1), po)
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            //---------------------------------------------------------------
                        }
                    }
                }

                //Rimozione
            }
            else if (intOption == 3)
            {
                CommentManager oCommentManager = new CommentManager();
                oCommentManager.Cache = false;
                CommentValue oCommentVal = oCommentManager.Read(new CommentIdentificator(intCommentId));

                if ((oCommentVal != null) && oCommentVal.Key.Id > 0)
                {
                    int intCId = 0;
                    int.TryParse(oCommentVal.Title.Trim(), out intCId);
                    // HttpContext.Current.Response.Write(intCId)

                    //HttpContext.Current.Response.End()
                    if (intCId > 0)
                    {
                        ContentSearcher oContentSearcher = new ContentSearcher();
                        var _with5 = oContentSearcher;
                        _with5.key.Id = intCId;
                        _with5.key.IdLanguage = 1;
                        _with5.Active = SelectOperation.All();
                        _with5.Display = SelectOperation.All();

                        ContentCollection oContColl = oContentManager.Read(oContentSearcher);
                        ContentValue oContVal = null;
                        if ((oContColl != null) && oContColl.Any())
                        {
                            oContVal = new ContentValue();
                            oContVal = oContColl[0];
                            oContVal.Delete = true;
                            oContentManager.Update(oContVal);
                        }
                        //oContentManager.Update(New ContentIdentificator(intCId, 1), "Delete", 1)

                        //Indicizzazione del contenuto che mappa il commento--------------
                        try
                        {
                            IndexerModule indexM = new IndexerModule();
                            PositionValue po = new PositionValue();
                            po.Language = new LanguageIdentificator(1);
                            po.Site = new SiteIdentificator(Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id);
                            indexM.RemoveContent(oContVal.Key, po);
                        }
                        catch (Exception ex)
                        {
                        }
                        //---------------------------------------------------------------
                    }
                }
            }
        }






    }
}