﻿using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class CacheHelper
    {


        public static void InsertKeyInGroupCache(string keyGroup, string keyCache)
        {
            CacheManager.InsertKeyIntoGroup(keyCache, keyGroup);
        }

        public static string getKeyBySearcher(IObjectSearcher so)
        {
            return so.GetString();
        }

        public static void DeleteGroupCache(string groupCacheKey)
        {
            CacheManager.RemoveGroup(groupCacheKey);

        }
    }
}
