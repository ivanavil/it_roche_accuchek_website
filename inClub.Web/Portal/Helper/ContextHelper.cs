﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class ContextHelper
    {

        public static void CreateUserContext(int userId, int contextId, Keys.EnumeratorKeys.UserContextRelationType userContxRelType)
        {

            if (userId == 0 || contextId == 0 || userContxRelType == null)
                return;

            UserContextValue ctxUsr = new UserContextValue();
            ctxUsr.ContextId = contextId;
            ctxUsr.UserId = userId;
            ctxUsr.RelationType = (int)userContxRelType;
            ctxUsr.DateRelation = DateTime.Now;
            UserManager usrCtxMan = new UserManager();
            usrCtxMan.CreateUserContext(ctxUsr);
            usrCtxMan = null;
            ctxUsr = null;
        }

        public static void DeleteUserContext(int userId,  Keys.EnumeratorKeys.UserContextRelationType userContxRelType)
        {

            if (userId == 0 ||  userContxRelType == null)
                return;

            UserContextValue ctxUsr = new UserContextValue();            
            ctxUsr.UserId = userId;
            ctxUsr.RelationType = (int)userContxRelType;           
            UserManager usrCtxMan = new UserManager();
            usrCtxMan.RemoveUserContext(ctxUsr);
            usrCtxMan = null;
            ctxUsr = null;
        }


        public static List<SelectListItem> GetContexts(ContextSearcher so, string firsItem)
        {
            if (null == so)
                return null;

            ContextManager manager = new ContextManager();
            var coll = manager.Read(so);
            List<SelectListItem> temp = null;
            if (coll != null && coll.Any())
            {
                temp = coll.OrderBy(item => item.Description)
                        .Select(item =>
                            new SelectListItem
                            {
                                Text = item.Description,
                                Value = item.Key.Id.ToString()
                            }).ToList();

                SelectListItem firstItem = new SelectListItem();

                if (!string.IsNullOrWhiteSpace(firsItem))
                {
                    firstItem.Text = string.Format(" {0} ", firsItem);
                }
                else
                {
                    firstItem.Text = string.Format("- {0} -", "Seleziona");
                }


                temp.Insert(0, firstItem);
            }
            //clear
            manager = null;
            coll = null;

            return temp;
        }

        public static ContextBaseValue GetContextBase(ContextIdentificator ident)
        {
            ContextManager manager = new ContextManager();
            ContextBaseValue contextBaseValue  = manager.Read(ident);

            manager = null;

            return contextBaseValue;
        }
    }
}