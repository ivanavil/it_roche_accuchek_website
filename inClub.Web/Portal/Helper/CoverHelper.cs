﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

using System.Web.ApplicationServices;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Utility.ObjectValues;

using System.Web.Mvc;
using System.Globalization;

namespace Inclub.Web.Portal.Helper
{
    public class CoverHelper
    {

        public static bool VerifyExistCover(ContentIdentificator contentKey)
        {
            bool _return = false;

            if (File.Exists(HttpContext.Current.Server.MapPath("/HP3image/cover/" + contentKey.Id + "_gen.jpg")))
            {
                _return = true;
            }

            if (File.Exists(HttpContext.Current.Server.MapPath("/HP3image/cover/" + contentKey.Id + "_gen.png")))
            {
                _return = true;
            }

            if (File.Exists(HttpContext.Current.Server.MapPath("/HP3image/cover/" + contentKey.Id + "_gen.gif")))
            {
                _return = true;
            }

            return _return;

        }

        public static string getCover(ContentIdentificator contentKey, int width, int height, Inclub.Web.Portal.Keys.EnumeratorKeys.coverPosition position)
        {
            string _return = string.Empty;

            var quality = new ImageQualityValue();

            quality.CompositingQuality = ImageQualityValue.EnumCompositingQuality.HighQuality;
            quality.SmoothingMode = ImageQualityValue.EnumSmoothingMode.HighQuality;
            quality.ImageQuality = ImageQualityValue.EnumImageQuality.High;
            quality.JPEGImageCompression = ImageQualityValue.EnumJPEGcompression.L0;
            quality.PixelOffsetMode = ImageQualityValue.EnumPixelOffsetMode.HighQuality;

            var manager = new ContentManager();

            int temp = (int)position;

            if (contentKey != null)
                _return = manager.GetCover(contentKey, width, height, quality, temp);


            //clear
            quality = null;
            manager = null;

            return _return;

        }

        public static bool ExistUserImage(int userKey)
        {

            string myfile_small = string.Format("{0}_100.jpg", CryptHelper.urlEncrypt(userKey.ToString()));
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~//HP3Image/User/cover/"), myfile_small);
            if (System.IO.File.Exists(path))
            {
                return true;
            }


            return false;
        }

        public static string getUserImage(int userKey)
        {

            string _return = string.Empty;       

            if (userKey >0)
            {
                if (ExistUserImage(userKey))
                {
                string myfile = string.Format("{0}_100{1}?{2}", CryptHelper.urlEncrypt(userKey.ToString()), ".jpg", DateTime.Now.ToString().Replace(" ", "").Replace(":", "").Replace("/", ""));

                var path = "/HP3Image/User/cover/" + myfile;
                _return = path; 
            }
                else
                {
                    var path = "/HP3Image/User/emptyAvatar.jpg";
                    _return = path; 
                }

            }                         

            return _return;
        }


        string getContentType(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return "Image/bmp";
                case ".gif": return "Image/gif";
                case ".jpg": return "Image/jpeg";
                case ".png": return "Image/png";
                default: break;
            }
            return "";
        }

        public static ImageFormat getImageFormat(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return ImageFormat.Bmp;
                case ".gif": return ImageFormat.Gif;
                case ".jpg": return ImageFormat.Jpeg;
                case ".png": return ImageFormat.Png;
                default: break;
            }
            return ImageFormat.Jpeg;
        }

    }
}
