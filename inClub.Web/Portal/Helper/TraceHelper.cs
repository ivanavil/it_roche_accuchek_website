﻿using System;
using System.Web;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using System.Collections.Generic;
using Healthware.HP3.Core.Web;
using Healthware.HP3.Core.Web.ObjectValues;
using Healthware.HP3.Core.Site;
using System.Linq;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Common.Extensions;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;

namespace Inclub.Web.Portal.Helper
{

    
    public class TraceObject
    {
        private bool _denormalize = false;
        private string _description = string.Empty;
        private ContentValue _content = null;
        private int _eventId = 0;
        private int _contentId = 0;
        private int _productId = 0;
        private short _languageId = 0;
        private int _themeId = 0;
        private int _siteId = 0;

        private int _userId = 0;
        public bool Denormalize
        {
            get { return _denormalize; }
            set { _denormalize = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public ContentValue Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public int ContentId
        {
            get { return _contentId; }
            set { _contentId = value; }
        }

        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        public short LanguageId
        {
            get { return _languageId; }
            set { _languageId = value; }
        }

        public int ThemeId
        {
            get { return _themeId; }
            set { _themeId = value; }
        }

        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
    }

    public class TraceHelper
    {

        public static void TraceEvent(TraceObject objTraceData)
        {
            TraceValue traceValue = new TraceValue();
            if (objTraceData.Content == null & objTraceData.ContentId > 0 & objTraceData.LanguageId > 0)
            {
                ContentManager oCMan = new ContentManager();

                ContentSearcher oContS = new ContentSearcher();
                var _with1 = oContS;
                _with1.key.IdLanguage = objTraceData.LanguageId;
                _with1.key.Id = objTraceData.ContentId;
                _with1.Active = SelectOperation.All();
                _with1.Delete = SelectOperation.All();
                _with1.Display = SelectOperation.All();
                ContentCollection oContColl = oCMan.Read(oContS);
                if ((oContColl != null) && (oContColl.Any()))
                {
                    ContentValue oContVal = oContColl[0];
                    if ((oContVal != null) && oContVal.Key.Id > 0)
                    {
                        objTraceData.Content = oContVal;
                    }
                }
            }

            var _with2 = traceValue;
            _with2.Denormalize = objTraceData.Denormalize;
            _with2.KeyUser = new UserIdentificator(objTraceData.UserId);
            _with2.KeyLanguage = new LanguageIdentificator(objTraceData.LanguageId);
            _with2.KeyEvent = new TraceEventIdentificator(objTraceData.EventId);
            _with2.KeyContent.Id = objTraceData.ContentId;
            _with2.KeyContent.IdLanguage = objTraceData.LanguageId;
            if ((objTraceData.Content != null))
            {
                _with2.KeyContent.PrimaryKey = objTraceData.Content.Key.PrimaryKey;
                _with2.KeySiteArea = objTraceData.Content.SiteArea.Key;
                _with2.KeyContentType = objTraceData.Content.ContentType.Key;
            }
            _with2.KeySite.Id = objTraceData.SiteId;
            _with2.KeyTheme.Id = objTraceData.ThemeId;
            _with2.KeyUser.Id = objTraceData.UserId;
            _with2.IdProduct = objTraceData.ProductId;
            _with2.DateInsert = DateTime.Now;
            _with2.Description = objTraceData.Description;
            SaveTraceEvent(traceValue);
        }


        public static void SaveTraceEvent(TraceValue oTraceval)
        {
            try
            {
                if ((oTraceval != null))
                {
                    TraceService traceManager = new TraceService();
                    oTraceval.IdSession = traceManager.GetIdSession();
                    traceManager.Create(oTraceval);
                }
            }
            catch (Exception ex)
            {
                //Errror Notifier
            }
        }


        public static void TraceEvent(ThemeIdentificator ThemeIdent, TraceEventIdentificator eventIdent, ContentIdentificator contentIdent)
        {
            TraceExtend();

            ContentManager man = new ContentManager();
            var content = man.Read(contentIdent);

            TraceValue objTrace = CreateTraceValue(ref content);

            //inserisco comunque il tracciamento anche se la read del content  è null
            if (objTrace == null)
            {
                objTrace = new TraceValue();
            }

            string eventDescription = string.Empty;

            string customField = string.Empty;

            TraceService manager = new TraceService();

            objTrace.KeyEvent.Id = eventIdent.Id;
            objTrace.KeyTheme.Id = ThemeIdent.Id;
            objTrace.CustomField = customField;


            if (!string.IsNullOrWhiteSpace(eventDescription))
            {
                objTrace.Description = eventDescription;
            }

            manager.Create(objTrace);
        }

        private static TraceValue CreateTraceValue(ref ContentValue content)
        {
            if (null == content)
                return null;

            TraceValue obj = new TraceValue();
            var idUserLogged = Helper.UserHelper.getUserLoggedId();
            obj.DateInsert = DateTime.Now;
            obj.KeyContent.Id = content.Key.Id;
            obj.KeySite = Keys.SiteAreaKeys.PortalSite;
            obj.KeySiteArea = content.SiteArea.Key;
            obj.KeyContentType = content.ContentType.Key;
            obj.KeyLanguage.Id = content.Key.IdLanguage;
            obj.IdSession = getSessionId();
            obj.KeyUser.Id = idUserLogged;

            return obj;
        }

        public static void TraceEvent(ref Object content, int eventId, int ThemeId, string eventDescription, int idDownload)
        {
            TraceValue objTrace = CreateTraceValue(ref content);
            var idUserLogged = Helper.UserHelper.getUserLoggedId();
            TraceService manager = new TraceService();

            objTrace.KeyEvent.Id = eventId;
            objTrace.KeyDownload.Id = idDownload;
            objTrace.KeyTheme.Id = ThemeId;
            objTrace.KeyUser.Id = idUserLogged;

            if (!string.IsNullOrWhiteSpace(eventDescription))
            {
                objTrace.Description = eventDescription;
            }

            manager.Create(objTrace);
        }


        public static void TraceEvent(ThemeIdentificator ThemeIdent, TraceEventIdentificator TraceIdent, int idUser = 0)
        {
            TraceExtend();

            var objThemeValue = Helper.ThemeHelper.getThemeTraceValue(ThemeIdent);
            var idUserLogged = Helper.UserHelper.getUserLoggedId();

            string eventDescription = string.Empty;
            string customField = string.Empty;

            TraceService manager = new TraceService();
            TraceValue objTrace = null;
            if (objThemeValue != null)
            {

                objTrace = new TraceValue();
                objTrace.KeyEvent.Id = TraceIdent.Id;
                objTrace.KeyTheme.Id = objThemeValue.Key.Id;
                objTrace.KeySite = Keys.SiteAreaKeys.PortalSite;
                objTrace.KeySiteArea = objThemeValue.KeySiteArea;
                objTrace.KeyContentType = objThemeValue.KeyContentType;
                objTrace.KeyLanguage.Id = Keys.LabelKeys.LANGUAGE_PORTAL;
                objTrace.KeyUser.Id = ((idUserLogged == 0) ? idUser : idUserLogged);
                objTrace.CustomField = customField;

                objTrace.DateInsert = DateTime.Now;

                objTrace.IdSession = getSessionId();

            }
            else
            {//potrebbe essere nullo il thema ma posso  e devo comunque tracciare l'evento
                objTrace = new TraceValue();
                objTrace.KeyEvent.Id = TraceIdent.Id;
                objTrace.KeySite = Keys.SiteAreaKeys.PortalSite;
                objTrace.KeyLanguage.Id = Keys.LabelKeys.LANGUAGE_PORTAL;
                objTrace.CustomField = customField;
                objTrace.KeyUser.Id = ((idUserLogged == 0) ? idUser : idUserLogged);
                objTrace.DateInsert = DateTime.Now;

                objTrace.IdSession = getSessionId();
            }

            manager.Create(objTrace);
        }

        public static void TraceEvent(ThemeIdentificator ThemeIdent, TraceEventIdentificator TraceIdent, string eventDescription)
        {
            TraceExtend();

            var objThemeValue = Helper.ThemeHelper.getThemeTraceValue(ThemeIdent);


            string customField = string.Empty;
            var _clusterRoleId = 0;
            var UserId = 0;
            Models.UserLogged objUserLogged = Helper.UserHelper.readUserLogged();
            if (null != objUserLogged && objUserLogged.Key.Id > 0)
            {
                //eventDescription = objUserLogged.isMMG.ToString();
                customField = objUserLogged.specID.ToString();
                _clusterRoleId = objUserLogged.typeUserByRoleId;
                UserId = objUserLogged.Key.Id;
            }

            TraceService manager = new TraceService();
            TraceValue objTrace = null;
            if (objThemeValue != null)
            {



                objTrace = new TraceValue();
                objTrace.KeyEvent.Id = TraceIdent.Id;
                objTrace.KeyTheme.Id = objThemeValue.Key.Id;
                objTrace.KeySite = Keys.SiteAreaKeys.PortalSite;
                objTrace.KeySiteArea = objThemeValue.KeySiteArea;
                objTrace.KeyContentType = objThemeValue.KeyContentType;
                objTrace.KeyLanguage.Id = Keys.LabelKeys.LANGUAGE_PORTAL;
                objTrace.CustomField = customField;
                objTrace.KeyUser.Id = UserId;
                objTrace.DateInsert = DateTime.Now;
                objTrace.IdProduct = _clusterRoleId;
                objTrace.IdSession = getSessionId();


                if (!string.IsNullOrWhiteSpace(eventDescription))
                {
                    objTrace.Description = eventDescription;
                }
            }
            else
            {//potrebbe essere nullo il thema ma posso  e devo comunque tracciare l'evento
                objTrace = new TraceValue();
                objTrace.KeyEvent.Id = TraceIdent.Id;
                objTrace.KeySite = Keys.SiteAreaKeys.PortalSite;
                objTrace.KeyLanguage.Id = Keys.LabelKeys.LANGUAGE_PORTAL;
                objTrace.CustomField = customField;
                objTrace.KeyUser.Id = UserId;
                objTrace.DateInsert = DateTime.Now;
                objTrace.IdProduct = _clusterRoleId;
                objTrace.IdSession = getSessionId();
            }

            manager.Create(objTrace);
        }

        public static ThemeValue getThemeTraceValue(ThemeIdentificator themeIdentificator)
        {
            ThemeValue obj = null;
            ThemeManager objManager = null;

            string cacheKey = string.Format(Keys.CacheKeys.getThemeTraceValue, themeIdentificator.Id);

            if (CacheManager.Exists(cacheKey, CacheManager.FirstLevelCache))
            {
                obj = (ThemeValue)CacheManager.Read(cacheKey, CacheManager.FirstLevelCache);
            }
            else
            {
                var so = new ThemeSearcher();

                objManager = new ThemeManager();

                //Searcher
                so.LoadHasSon = false;
                so.KeySite = Keys.SiteAreaKeys.PortalSite;
                so.Key.Id = themeIdentificator.Id;
                so.Display = SelectOperation.All();
                so.Active = SelectOperation.Enabled();
                so.Delete = SelectOperation.Enabled();

                //LacyLoad                                                                                
                so.Properties.Add(th => th.KeyContentType.Id);
                so.Properties.Add(th => th.KeySiteArea.Id);
                so.Properties.Add(th => th.KeyTraceEvent.Id);


                so.LoadRoles = false;
                var coll = objManager.Read(so);

                if (null != coll && coll.Any())
                {
                    obj = coll.First();
                }

                CacheManager.Insert(cacheKey, obj, CacheManager.FirstLevelCache, 1440, BaseManager.CachePriorityEnum.Normal);

                //clear
                objManager = null;


            }

            return obj;
        }

        public static void TraceExtend()
        {
            try
            {
                Healthware.HP3.Core.Web.MasterPageManager objMaster = new Healthware.HP3.Core.Web.MasterPageManager();
                objMaster.TraceExtend();
            }
            catch (Exception ex)
            {
                Helper.ErrorLogHelper.NotifyDBLog(ex, "Error TraceExtend");
            }


        }

        public static void TraceEvent(int eventId, int ThemeId, string eventDescription, int _Lang)
        {
            TraceValue objTrace = CreateTraceValue(ThemeId);
            TraceService manager = null;
            if (null != objTrace)
            {

                manager = new TraceService();

                objTrace.KeyEvent.Id = eventId;
                objTrace.KeyTheme.Id = ThemeId;
                objTrace.KeyLanguage.Id = _Lang;

                if (!string.IsNullOrWhiteSpace(eventDescription))
                {
                    objTrace.Description = eventDescription;
                }
            }

            manager.Create(objTrace);
        }

        public static void TraceEvent(ThemeIdentificator ThemeIdent)
        {
            TraceExtend();
            var objThemeValue = Helper.ThemeHelper.getThemeTraceValue(ThemeIdent);

            TraceService manager = new TraceService();

            TraceValue objTrace = null;
            if (objThemeValue != null)
            {

                string eventDescription = string.Empty;
                string customField = string.Empty;

                objTrace = new TraceValue();
                objTrace.KeyEvent.Id = objThemeValue.KeyTraceEvent.Id;
                objTrace.KeyTheme.Id = objThemeValue.Key.Id;
                objTrace.KeySite = Keys.SiteAreaKeys.PortalSite;
                objTrace.KeySiteArea = objThemeValue.KeySiteArea;
                objTrace.KeyContentType = objThemeValue.KeyContentType;
                objTrace.KeyLanguage.Id = Keys.LabelKeys.LANGUAGE_PORTAL;
                objTrace.CustomField = customField;
                objTrace.DateInsert = DateTime.Now;
                objTrace.IdSession = getSessionId();


                if (!string.IsNullOrWhiteSpace(eventDescription))
                {
                    objTrace.Description = eventDescription;
                }
            }

            manager.Create(objTrace);
        }

        private static TraceValue CreateTraceValue(int _themeId)
        {

            ThemeValue objTheme = Helper.ThemeHelper.GetThemeValueByThemeId(_themeId);
            TraceValue obj = null;
            if (null != objTheme)
            {
                obj = new TraceValue();

                obj.DateInsert = DateTime.Now;

                obj.KeySite = Keys.SiteAreaKeys.PortalSite;
                obj.KeySiteArea = objTheme.KeySiteArea;
                obj.KeyContentType = objTheme.KeyContentType;
                obj.KeySite = objTheme.KeySite;

                obj.CustomField = HttpContext.Current.Request.QueryString.ToString();
                //obj.KeyUser.Id = Helper.UserHelper.getUserLoggedId();
                obj.IdSession = getSessionId();
            }

            return obj;

        }

        private static TraceValue CreateTraceValue(ref Object content)
        {

            ContentValue contentValue = (ContentValue)content;
            TraceValue obj = new TraceValue();

            obj.DateInsert = DateTime.Now;
            obj.KeyContent.Id = contentValue.Key.Id;
            obj.KeySite = Keys.SiteAreaKeys.PortalSite;
            obj.KeySiteArea = contentValue.SiteArea.Key;
            obj.KeyContentType = contentValue.ContentType.Key;
            obj.KeyLanguage.Id = contentValue.Key.IdLanguage;
            obj.Description = contentValue.Title;
            obj.CustomField = HttpContext.Current.Request.QueryString.ToString();
            //obj.KeyUser.Id = Helper.UserHelper.getUserLoggedId();
            obj.IdSession = getSessionId();


            return obj;

        }


        public static string getSessionId()
        {
            var manager = new Healthware.HP3.Core.User.AccessService();
            string _idSession = string.Empty;

            var ticket = manager.GetTicket();

            if (ticket != null && ticket.IdSession != null)
            {
                _idSession = ticket.IdSession;
            }

            manager = null;

            return _idSession;
        }


        public static void TraceComment(int discussionId, short languageId, int themeId, int userId, int eventID)
        {
            try
            {

                TraceObject objTrace = new TraceObject();

                var _with1 = objTrace;
                _with1.Content = null;
                _with1.ContentId = discussionId;
                _with1.Description = string.Empty;
                _with1.EventId = eventID;
                _with1.LanguageId = languageId;
                _with1.SiteId = Inclub.Web.Portal.Keys.SiteAreaKeys.PortalSite.Id;
                _with1.ThemeId = themeId;
                _with1.UserId = userId;

                TraceHelper.TraceEvent(objTrace);

            }
            catch (Exception ex)
            {
            }
        }
    }
}
