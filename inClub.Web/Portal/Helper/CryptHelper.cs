﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Utility;

namespace Inclub.Web.Portal.Helper
{
    public class CryptHelper
    {
        public static string urlEncrypt(string stringEncrypt)
        {
            return SimpleHash.UrlEncryptRijndaelAdvanced(stringEncrypt);
        }

        public static string urlDecrypt(string stringDecrypt)
        {
            return SimpleHash.UrlDecryptRijndaelAdvanced(stringDecrypt);
        }
    }
}
