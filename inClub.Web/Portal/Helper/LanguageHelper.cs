﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.Web;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization;

namespace Inclub.Web.Portal.Helper
{
    public class LanguageHelper
    {
        static MasterPageManager objMasterPageManager = new MasterPageManager();
        static LanguageManager objLanguageManager = new LanguageManager();

        public static LanguageValue GetLanguageById(int id)
        {
            if (id==0)
                return null;

            var objLanguageValue = objLanguageManager.Read(id);
            if (null != objLanguageValue)
            {
                return objLanguageValue;
            }

            return null;
        }

        public static string GetLanguageCode(int id)
        {
            if (id == 0)
                return string.Empty;

            var objLanguageValue = objLanguageManager.Read(id);
            if (null != objLanguageValue)
            {
                return objLanguageValue.Key.Code.ToLower();
            }

            return string.Empty;
        }

        public static LanguageIdentificator GetLanguageFromUrl()
        {
            LanguageIdentificator _return = null;
            _return = new LanguageIdentificator(1, "it"); //objMasterPageManager.GetLanguageFromUrl();
       
            return _return;
        }

        public static LanguageCollection GetSiteLanguages()
        {
            LanguageCollection _objLanguageColl = null;
            var objSiteManager = new Healthware.HP3.Core.Site.SiteAreaManager();

            var so = new LanguageSiteRelationSearcher();
            so.SiteAreaId.Id = Helper.SiteAreaHelper.GetSiteIdentificator().Id;
            var coll = objSiteManager.ReadLanguageSiteRelation(so);

            if (null != coll && coll.Any())
            {
                _objLanguageColl = new LanguageCollection();
                foreach (var rel in coll){

                    LanguageValue objLanguage = GetLanguageById(rel.LanguageId.Id);
                    if (null != objLanguage && !string.IsNullOrEmpty(objLanguage.Key.Code))
                    {
                        var _curLanguage = GetLanguageFromUrl();
                        if (null != _curLanguage && _curLanguage.Id != objLanguage.Key.Id)
                            _objLanguageColl.Add(objLanguage);
                    }
                }
           }

            return _objLanguageColl;

        }

        
    }
}
