﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;

namespace Inclub.Web.Portal.Helper
{
    public static class CaptchaGeneratorHelper
    {
        #region Members

        private static string _charSet = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz";
        private static int _length = 5;
        private static bool _unique = true;
        private static Random _random = new Random();

        private static HatchStyle _textStyle = HatchStyle.NarrowHorizontal;
        private static Color _textForeColor = Color.Blue;
        private static Color _textBackColor = Color.White;
        private static HatchStyle _backStyle = HatchStyle.DottedDiamond;
        private static Color _backForeColor = Color.LightGray;
        private static Color _backBackColor = Color.White;

        private static readonly string _imageKey = "CaptchaImageKey";
        private static readonly string _textKey = "CaptchaTextKey";

        #endregion Members

        #region Methods

        public static Bitmap Init(HttpContextBase context, string uniqueName)
        {
            if (null == context)
                throw new ArgumentNullException("context");

            var text = string.Empty;
            for (int i = 0; i < _length; i++)
            {
                char c;
                do
                    c = _charSet[_random.Next(0, _charSet.Length)];
                while (_unique && _length <= _charSet.Length && text.IndexOf(c) >= 0);
                text += c;
            }

            var image = CreateImage(text);
            SetCurrentCaptchaImage(context, uniqueName, image);

            SetCurrentCaptchaText(context, uniqueName, text);

            return image;
        }

        public static string GetCurrentCaptchaText(HttpContextBase context, string uniqueName)
        {
            return context.Session[_textKey + uniqueName] as string ?? string.Empty;
        }

        public static Bitmap GetCurrentCaptchaImage(HttpContextBase context, string uniqueName)
        {
            return context.Session[_imageKey + uniqueName] as Bitmap;
        }

        public static bool Validate(HttpContextBase context, string uniqueName, string text)
        {
            if (null == context)
                throw new ArgumentNullException("context");

            var currText = GetCurrentCaptchaText(context, uniqueName);

            return currText.Equals(text, StringComparison.OrdinalIgnoreCase);
        }

        #endregion Methods

        #region Private Methods

        private static void SetCurrentCaptchaText(HttpContextBase context, string uniqueName, string text)
        {
            context.Session[_textKey + uniqueName] = text;
        }

        private static void SetCurrentCaptchaImage(HttpContextBase context, string uniqueName, Bitmap image)
        {
            context.Session[_imageKey + uniqueName] = image;
        }

        private static Bitmap CreateImage(string text)
        {
            Bitmap bitmap = new Bitmap(Width, Height, PixelFormat.Format32bppPArgb);

            Graphics g = null;
            Font font = null;
            HatchBrush textBrush = null;
            HatchBrush backBrush = null;
            try
            {
                // Fill Background
                g = Graphics.FromImage(bitmap);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle rect = new Rectangle(0, 0, Width, Height);
                textBrush = new HatchBrush(_textStyle, _textForeColor, _textBackColor);
                backBrush = new HatchBrush(_backStyle, _backForeColor, _backBackColor);
                g.FillRectangle(backBrush, rect);

                // Write Text
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                font = new Font(FontName, FontSize, FontStyle);
                GraphicsPath path = new GraphicsPath();
                path.AddString(text, font.FontFamily, (int)font.Style, font.Size, rect, stringFormat);

                // Warp Text
                if (Warp > 0)
                {
                    Matrix matrix = new Matrix();
                    float warp = 10 - Warp * 6 / 100;
                    PointF[] points = {
	                new PointF(_random.Next(rect.Width) / warp, _random.Next(rect.Height) / warp),
	                new PointF(rect.Width - _random.Next(rect.Width) / warp, _random.Next(rect.Height) / warp),
	                new PointF(_random.Next(rect.Width) / warp, rect.Height - _random.Next(rect.Height) / warp),
	                new PointF(rect.Width - _random.Next(rect.Width) / warp, rect.Height - _random.Next(rect.Height) / warp) };
                    path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);
                }
                g.FillPath(textBrush, path);

                // Random Noise over Text
                if (Noise > 0)
                {
                    int maxDim = Math.Max(rect.Width, rect.Height);
                    int radius = (int)(maxDim * Noise / 3000);
                    int maxGran = (int)(rect.Width * rect.Height
                        / (100 - (Noise >= 90 ? 90 : Noise)));
                    for (int i = 0; i < maxGran; i++)
                        g.FillEllipse(textBrush,
                            _random.Next(rect.Width),
                            _random.Next(rect.Height),
                            _random.Next(radius),
                            _random.Next(radius));
                }

                // Draw Lines over Text
                if (Lines > 0)
                {
                    int lines = ((int)Lines / 30) + 1;
                    using (Pen pen = new Pen(textBrush, 1))
                        for (int i = 0; i < lines; i++)
                        {
                            PointF[] points
                                = new PointF[lines > 2 ? lines - 1 : 2];
                            for (int j = 0; j < points.Length; j++)
                                points[j] = new PointF(
                                    _random.Next(rect.Width),
                                    _random.Next(rect.Height));
                            g.DrawCurve(pen, points, 1.75F);
                        }
                }
            }
            finally
            {
                // Dispose GDI+ objects
                if (g != null)
                    g.Dispose();
                if (font != null)
                    font.Dispose();
                if (textBrush != null)
                    textBrush.Dispose();
                if (backBrush != null)
                    backBrush.Dispose();
            }

            return bitmap;
        }

        #endregion Private Methods

        #region Properties

        public static int Noise = 60;
        public static int Warp = 70;
        public static int Lines = 120;
        public static int Width = 150;
        public static int Height = 60;
        public static string FontName = "Tahoma";
        public static int FontSize = 32;
        public static FontStyle FontStyle = FontStyle.Bold;

        #endregion Properties
    }
}