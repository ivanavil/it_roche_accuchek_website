﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class SiteAreaHelper
    {
        public static string DomainSiteLabel()
        {
            string _return = string.Empty;

            var objSiteManager = new Healthware.HP3.Core.Site.SiteManager();
            var objSiteValue = objSiteManager.GetSiteInfo();

            if (objSiteValue != null)
                _return = objSiteValue.SiteLabel;

            return _return;
        }

        //Get Current SiteArea (typeOf Area)
        public static SiteAreaIdentificator GetSiteAreaIdentificator()
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            var _return = objMasterPageManager.GetSiteArea();

            objMasterPageManager = null;

            return _return;
        }

        //Get SiteArea (typeOf Site)
        public static SiteIdentificator GetSiteIdentificator()
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            var _return = objMasterPageManager.GetSite();
            _return.Domain = Keys.SiteAreaKeys.PortalSite.Domain;
            _return.Name = Keys.SiteAreaKeys.PortalSite.Name;

            objMasterPageManager = null;

            return _return;
        }
    }
}
