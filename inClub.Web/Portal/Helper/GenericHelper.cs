﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Inclub.Web.Portal.Extensions;
using System.Data;
using System.Data.SqlClient;

namespace Inclub.Web.Portal.Helper
{
    public static class GenericHelper
    {
        /// <summary>
        /// render PartialView and return string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="partialViewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string RenderPartialToString(this ControllerContext context, string partialViewName, object model)
        {
            return RenderPartialToStringMethod(context, partialViewName, model);
        }
        /// <summary>
        /// render PartialView and return string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="partialViewName"></param>
        /// <param name="viewData"></param>
        /// <param name="tempData"></param>
        /// <returns></returns>
        public static string RenderPartialToString(ControllerContext context, string partialViewName, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            return RenderPartialToStringMethod(context, partialViewName, viewData, tempData);
        }

        public static string RenderPartialToStringMethod(ControllerContext context, string partialViewName, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            ViewEngineResult result = ViewEngines.Engines.FindPartialView(context, partialViewName);

            if (result.View != null)
            {
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(context, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }
            return String.Empty;
        }


        public static string SiteDomain()
        {
            SiteManager siteMan = new SiteManager();
            SiteValue site = siteMan.Read(Keys.SiteAreaKeys.PortalSite);
            if (null != site)
            {
                return site.Domain;
            }
            return string.Empty;
        }

        public static string RenderPartialToStringMethod(ControllerContext context, string partialViewName, object model)
        {
            ViewDataDictionary viewData = new ViewDataDictionary(model);
            TempDataDictionary tempData = new TempDataDictionary();
            return RenderPartialToStringMethod(context, partialViewName, viewData, tempData);
        }


        public static string DelLengthString(string strTruncate, int MaxLength, string final)
        {
            string newString = strTruncate;

            if (string.IsNullOrWhiteSpace(strTruncate))
                return newString;

            if (string.IsNullOrWhiteSpace(final))
                final = "...";

            if (strTruncate.Length > MaxLength)
            {
                int index = strTruncate.IndexOf(" ", MaxLength);

                if (index >= MaxLength || index <= 0)
                {
                    string tmp = strTruncate.Substring(0, MaxLength);
                    index = tmp.LastIndexOf(" ");

                    if (index > 0)
                    {
                        newString = String.Format("{0} {1}", tmp.Substring(0, index), final);
                    }
                    else
                    {
                        newString = TruncateString(strTruncate, MaxLength, final);
                    }
                }
            }

            return newString;
        }

        public static string TruncateString(string strTruncate, int MaxLength, string final)
        {

            string newString = strTruncate;

            if (string.IsNullOrWhiteSpace(strTruncate))
                return newString;

            if (string.IsNullOrWhiteSpace(final))
                final = "...";

            if (strTruncate.Length > MaxLength)
            {
                newString = String.Format("{0} {1}", strTruncate.Substring(0, MaxLength), final);
            }

            return newString;

        }

        public static ContentCollection ReadContentBox(Healthware.HP3.Core.Box.ObjectValues.BoxIdentificator boxIdentificator)
        {
            var manager = new ContentManager();

            var coll = manager.ReadContentBox(boxIdentificator);

            manager = null;

            return coll;

        }

        public static ContentCollection ReadContentRelation(ContentSearcher so, ContentRelationTypeIdentificator ident)
        {
            var manager = new ContentManager();

            var coll = manager.ReadContentRelation(so, ident);

            manager = null;

            return coll;

        }

        public static string FieldReplace(string str, System.Collections.Generic.Dictionary<string, string> fields)
        {
            foreach (System.Collections.Generic.KeyValuePair<string, string> item in fields)
            {
                str = str.Replace(item.Key, item.Value);
            }
            return str;
        }

        public static bool checkAccessCode(string code)
        {
            bool result = false;
            int id_utente = 0;

            Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
            SqlDataReader reader;
            SqlCommand command = new SqlCommand();
            command = new SqlCommand("SELECT id_accessCode, accessCode, id_user FROM accuchek_AccessCode where accessCode = '" + code + "' ", manager.GetConnection());
            command.CommandType = CommandType.Text;
            manager.OpenConnection();
            reader = command.ExecuteReader();

            List<ContentBaseValue> Resources = new List<ContentBaseValue>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                   if (int.TryParse(reader["id_user"].ToString(), out id_utente))
                   {

                    if (id_utente > 0)
                        result = false;
                    else
                        result = true;

                   }

                }
            }            

            command.Dispose();
            command = null;

            return result;
        }

        public static string GetCaptchaCode()
        {
            string captchaCode = string.Empty;
            string code = string.Empty;
            while (string.IsNullOrWhiteSpace(code))
            {
                captchaCode = Healthware.HP3.Core.Utility.ImageUtility.GetCaptchaCode();
                code = Healthware.HP3.Core.Utility.ImageUtility.DecryptCaptchaCode(System.Web.HttpUtility.UrlDecode(captchaCode)).ToString();
            }
            return captchaCode;
        }

        public static string CleanString(string s)
        {
            s = s.Normalize(NormalizationForm.FormD);
            dynamic sb = new StringBuilder();

            foreach (char c in s)
            {
                if ((System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c) != System.Globalization.UnicodeCategory.NonSpacingMark))
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
    }
}
