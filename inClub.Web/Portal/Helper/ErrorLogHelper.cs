﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Base.Logs;

namespace Inclub.Web.Portal.Helper
{
    public class ErrorLogHelper
    {
        public static void NotifyDBLog(Exception ex, string myDescription)
        {
            ErrorLogger errLog = new ErrorLogger();

            string templateFilePath = HttpContext.Current.Server.MapPath(Keys.LabelKeys.TEMPLATE_ERROR_LOG);

            if (!string.IsNullOrWhiteSpace(templateFilePath) && File.Exists(templateFilePath))
            {
                // Reading template
                StreamReader sr = new StreamReader(templateFilePath);

                string body = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();

                Dictionary<string, string> replacing = new Dictionary<string, string>();

                replacing.Add("$$MY_MESSAGE$$", String.Format("{0}", CheckString(myDescription)));
                replacing.Add("$$MESSAGE$$", String.Format("{0}", CheckString(ex.Message)));
                replacing.Add("$$STACK_TRACE$$", CheckString(ex.StackTrace));
                replacing.Add("$$INNER_EXCEPTION$$", CheckString(ex.InnerException));
                replacing.Add("$$REFERRER_URL$$", CheckString(HttpContext.Current.Request.UrlReferrer));
                replacing.Add("$$USER_AGENT$$", CheckString(HttpContext.Current.Request.UserAgent));
                replacing.Add("$$IP_ADDRESS$$", CheckString(HttpContext.Current.Request.UserHostAddress));
                replacing.Add("$$REQUEST_URL$$", CheckString(HttpContext.Current.Request.Url));
                replacing.Add("$$CURRENT_TIME$$", String.Format("{0:G}", DateTime.Now.ToString()));
                replacing.Add("$$CUSTOM_DATA$$", CheckString(ExceptionCustomData(ex)));


                errLog.AddException(new Exception(Helper.GenericHelper.FieldReplace(body, replacing)), true, null, false);

            }
        }


        private static string CheckString(Object obj)
        {
            try
            {
                if (null != obj)
                    return obj.ToString();
            }
            catch (Exception)
            {
            }
            return Keys.LabelKeys.DEFAULT_UNAVAILABLE_TEXT;
        }

        private static string CheckString(string str)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(str))
                    return str;
            }
            catch (Exception)
            {
            }
            return Keys.LabelKeys.DEFAULT_UNAVAILABLE_TEXT;
        }

        private static string ExceptionCustomData(Exception innerException)
        {
            string retStr = string.Empty;
            if ((innerException.Data != null))
            {
                try
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (System.Collections.DictionaryEntry value in innerException.Data)
                    {
                        sb.Append(value.Key.ToString());
                        sb.Append(" : ");
                        sb.AppendLine(value.Value.ToString());
                        sb.AppendLine();
                    }
                    retStr = sb.ToString();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(ex.Message, ex);
                }
            }
            return retStr;
        }

    }
}
