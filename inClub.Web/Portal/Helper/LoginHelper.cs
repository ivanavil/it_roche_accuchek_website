﻿using System;
using System.Linq;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Helper
{
    public class LoginHelper
    {

        public static Boolean LogOut()
        {
            AccessService objAccessService = new AccessService();

            return objAccessService.Logout();
        }

        public static Boolean IsAuthenticated()
        {
            Boolean _return = false;
            AccessService objAccessService = new AccessService();
            objAccessService.Cache = false;
            _return = objAccessService.IsAuthenticated();
            objAccessService = null;
            return _return;
        }

        public static Boolean HasAccess(Healthware.HP3.Core.Site.ObjectValues.SiteAreaIdentificator saIdentificator)
        {
            Boolean _return = false;
            AccessService objAccessService = new AccessService();
            _return = objAccessService.HasAccess(saIdentificator);
            objAccessService = null;
            return _return;
        }

        public static TicketValue LogIn(string email, string password)
        {
            AccessService objAccessService = new AccessService();
            TicketValue _return = null;
            //if (Helper.GenericHelper.isEmail(email.Trim()) && !string.IsNullOrWhiteSpace(password.Trim()))
            //{

            //NB: LA USERNAME = EMAIL
            //NB: BUG HP3 -> SiteIdentificator DEVE ESSERE POPOLATO ANCHE DI DOMAIN E NAME

            _return = objAccessService.Login(email.Trim(), password.Trim());

            //}

            objAccessService = null;
            return _return;
        }

        //public static TicketValue LogIn(String email)
        //{
        //    AccessService accessService = new AccessService();
        //    UserManager man = new UserManager();
        //    UserSearcher so = new UserSearcher();
        //    so.Username = email.ToLower().Trim();
        //    so.Properties.Add("Key.Id");

        //    //Helper.CacheHelper.insertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));
        //    var coll = man.Read(so);
        //    if (null != coll && coll.Any())
        //    {
        //        var user = coll.FirstOrDefault();
        //        if (null != user)
        //        {
        //            return accessService.Login((UserBaseValue)user);
        //        }
        //    }
        //    return null;
        //}

    }
}