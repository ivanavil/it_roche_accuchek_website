﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Utility;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Keys;

namespace Inclub.Web.Portal.ActionFilters
{
    public class PrivateSiteAccessActionFilterAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {

            bool isAuth = LoginHelper.IsAuthenticated();
            var theme = ThemeHelper.getCurrentThemeIdentificator();
            var returnUrl = string.Empty;
            var Url = string.Empty;
            var request = filterContext.HttpContext.Request;
            var sessRetUrl = System.Web.HttpContext.Current.Session["requestURL"];


            if (!string.IsNullOrWhiteSpace(request.Url.PathAndQuery))
            {
                returnUrl = filterContext.HttpContext.Server.UrlEncode(request.Url.PathAndQuery);
            }
            
            //      filterContext.HttpContext.Response.Write("Tema:" + theme.Name);

            if (!isAuth && theme != null)
            {
                //filterContext.HttpContext.Session["returnUrl"] = returnUrl;

                if (!PublicThemes(theme.Id))
                {
                    filterContext.Result = new RedirectResult(("/"), false);
                    
                    if (!(returnUrl.ToLower()).Contains("login"))
                        System.Web.HttpContext.Current.Session["backURL"] = System.Web.HttpContext.Current.Server.UrlDecode(returnUrl);
                    
                    //filterContext.HttpContext.Response.Write("Non pubblico:" + theme.Name);
                }

            }
            if (isAuth && theme != null)
            {
                bool ForceProfileEdit = false;
                if (theme.Id != 0)
                {
                if (theme.Id != Keys.ThemeKeys.ModificaDati.Id)
                { 
                        int userId = Helper.UserHelper.getUserLoggedId();
                        UserSearcher so = new UserSearcher();
                        so.Key.Id = userId;
                        so.Status = (int)EnumeratorKeys.UserStatus.Active;
                        
                        UserManager Manager = new UserManager();
                        Manager.Cache = false;
                       var Cuser = Manager.Read(so);
                       if (Cuser.Any())
                       {
                           if (Cuser[0].MaritalStatus == UserValue.MaritalStatusType._married)
                            {
                                ForceProfileEdit = true;                      
                            }
                       }
                }
                if (ForceProfileEdit)
                {
                    if (!PublicThemes(theme.Id)) { 
                        filterContext.Result = new RedirectResult(("/modifica-dati"), false);
                    }
                }
                else
                { 

                    if (theme.Id == ThemeKeys.Home.Id) //Se sono autenticato la home deve portarmi alla pagina dei gruppi
                    {
                        filterContext.Result = new RedirectResult(("/home"), false);

                    }
                }
                }

            }



        }

        private bool PublicThemes(int idTheme)
        {
            List<int> Themes = new List<int>();
            Themes.Add(0);
            Themes.Add(ThemeKeys.Home.Id);
            Themes.Add(ThemeKeys.RecuperaPassword.Id);
            Themes.Add(ThemeKeys.Registrazione.Id);
            Themes.Add(ThemeKeys.AccessCode.Id);
            Themes.Add(ThemeKeys.PrivacyPolicy.Id);
            Themes.Add(ThemeKeys.Contatti.Id);
            Themes.Add(ThemeKeys.CookiePolicy.Id);
            Themes.Add(ThemeKeys.AspettiLegali.Id);
            Themes.Add(ThemeKeys.CondizGen.Id);
            Themes.Add(ThemeKeys.PopupDictionary.Id);
            Themes.Add(ThemeKeys.ConfermaRegistrazione.Id);
            Themes.Add(ThemeKeys.AvvertenzaRischio.Id);
            Themes.Add(ThemeKeys.InfoTrattamento.Id);
            
            //Themes.Add(ThemeKeys.Conditions.Id.ToString());
            //Themes.Add(ThemeKeys.Privacy.Id.ToString());

            bool res = Themes.Exists(t => t.Equals(idTheme));

            return res;
        }

    }
}