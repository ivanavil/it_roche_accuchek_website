﻿using System.Web.Routing;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Common.Extensions;
using Healthware.HP3.MVC;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Web;
using Inclub.Web.Portal.Models;

namespace Inclub.Web.Portal.BL
{
    public static class SearcherBuilders
    {
        public enum DataLoadingType
        {
            Full,
            Light,
            List,
            Details,
            DetailsById
        }

        public static ThemeSearcher InitThemeSearcher(RequestContext requestContext, DataLoadingType loadingType)
        {
            var searcher = new ThemeSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                KeyLanguage = LocalizationProvider.Current.GetCurrentUILanguage(requestContext),
                LoadHasSon = false,
                LoadRoles = false
            };

            searcher.Properties.Add(th => th.Key.Id);
            searcher.Properties.Add(th => th.Key.Domain);
            searcher.Properties.Add(th => th.Key.Name);

            searcher.Properties.Add(th => th.KeySiteArea.Id);
            searcher.Properties.Add(th => th.KeySiteArea.Domain);
            searcher.Properties.Add(th => th.KeySiteArea.Name);

            searcher.Properties.Add(th => th.KeyContentType.Id);
            searcher.Properties.Add(th => th.KeyContentType.Domain);
            searcher.Properties.Add(th => th.KeyContentType.Name);

            searcher.Properties.Add(th => th.KeyContent.Id);
            searcher.Properties.Add(th => th.KeyContent.PrimaryKey);
            searcher.Properties.Add(th => th.KeyContent.IdLanguage);

            searcher.Properties.Add(th => th.Description);
            searcher.Properties.Add(th => th.SeoName);

            if (DataLoadingType.Full == loadingType)
            {
                searcher.Properties.Add(th => th.Launch);
                searcher.Properties.Add(th => th.MetaDescription);
                searcher.Properties.Add(th => th.MetaKeywords);
                searcher.Properties.Add(th => th.KeyFather.Id);
            }

            return searcher;
        }

        public static ContentSearcher InitContentSearcher(Keys.EnumeratorKeys.DataLoadingType loadingType, GenericSearcherContentModel searchModel)
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySiteArea = objMasterPageManager.GetSiteArea(),
                KeyContentType = objMasterPageManager.GetContentType(),
                KeySite = Keys.SiteAreaKeys.PortalSite,
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC
            };

            if (searchModel.SiteAreaId > 0)
                searcher.KeySiteArea = new SiteAreaIdentificator(searchModel.SiteAreaId);

            if (searchModel.ContenTypeId > 0)
                searcher.KeyContentType = new ContentTypeIdentificator(searchModel.ContenTypeId);


            if (searchModel.LanguageId > 0)
                searcher.key.IdLanguage = (short)searchModel.LanguageId;

            //LacyLoad
            if (loadingType == Keys.EnumeratorKeys.DataLoadingType.Lite)
            {
                searcher.CalculatePagerTotalCount = false;
                searcher.SetMaxRow = 1;
            }
            else if (loadingType == Keys.EnumeratorKeys.DataLoadingType.List)
            {
                searcher.CalculatePagerTotalCount = searchModel.CalculatePagerTotalCount;

                if (searchModel.PageNumber.HasValue)
                {
                    searcher.PageNumber = searchModel.PageNumber.Value;
                }
                searcher.PageSize = searchModel.PageSize;

                if (!string.IsNullOrWhiteSpace(searchModel.KeysToExclude))
                    searcher.KeysToExclude = searchModel.KeysToExclude;

                searcher.Properties.Add(so => so.Launch);
                searcher.Properties.Add(so => so.Code);
                searcher.Properties.Add(so => so.Copyright);
                searcher.Properties.Add(so => so.LinkLabel);
                searcher.Properties.Add(so => so.DatePublish);
            }
            else if (loadingType == Keys.EnumeratorKeys.DataLoadingType.Full)
            {
                searcher.CalculatePagerTotalCount = searchModel.CalculatePagerTotalCount;

                if (searchModel.PageNumber.HasValue)
                {
                    searcher.PageNumber = searchModel.PageNumber.Value;
                }
                searcher.PageSize = searchModel.PageSize;

                if (!string.IsNullOrWhiteSpace(searchModel.KeysToExclude))
                    searcher.KeysToExclude = searchModel.KeysToExclude;

                searcher.Properties.Add(so => so.Launch);
                searcher.Properties.Add(so => so.Code);
                searcher.Properties.Add(so => so.Copyright);
                searcher.Properties.Add(so => so.LinkLabel);
                searcher.Properties.Add(so => so.DatePublish);
                searcher.Properties.Add(so => so.DateCreate);
                searcher.Properties.Add(so => so.Key.Id);
                searcher.Properties.Add(so => so.Copyright);
            }
            else if (loadingType == Keys.EnumeratorKeys.DataLoadingType.SecondLevelTheme)
            {
                //searcher.SortOrder = ContentGenericComparer.SortOrder.ASC;
                searcher.CalculatePagerTotalCount = false;
                searcher.Properties.Add(so => so.PageTitle);
                searcher.Properties.Add(so => so.Launch);
            }

            searcher.Properties.Add(so => so.MetaDescription);
            searcher.Properties.Add(so => so.MetaKeywords);
            searcher.Properties.Add(so => so.Title);

            return searcher;

        }

        public static ContentExtraSearcher InitContentExtraSearcher(int idContent)
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            var searcher = new ContentExtraSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                KeySiteArea = objMasterPageManager.GetSiteArea(),
                KeyContentType = objMasterPageManager.GetContentType(),
                KeySite = objMasterPageManager.GetSite(),
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC,
                CalculatePagerTotalCount = false
            };

            var urlLang = objMasterPageManager.GetLanguageFromUrl();
            if (null != urlLang)
                searcher.key.IdLanguage = (short)urlLang.Id;

            if (idContent > 0)
                searcher.key.Id = idContent;

            //LazyLoad            
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.PageTitle);
            searcher.Properties.Add(so => so.Code);
            searcher.Properties.Add(so => so.FullText);
            searcher.Properties.Add(so => so.FullTextOriginal);
            searcher.Properties.Add(so => so.Abstract);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.TitleContentExtra);
            searcher.Properties.Add(so => so.FullTextComment);
            searcher.Properties.Add(so => so.Rank);
            searcher.Properties.Add(so => so.Qty);
            searcher.Properties.Add(so => so.MetaDescription);
            searcher.Properties.Add(so => so.MetaKeywords);
            searcher.Properties.Add(so => so.SubTitle);

            return searcher;
        }

        public static ContentSearcher InitContentSearcher(RequestContext requestContext, DataLoadingType loadingType, BaseSearcher soModel)
        {
            MasterPageManager objMasterPageManager = new MasterPageManager();

            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySiteArea = objMasterPageManager.GetSiteArea(),
                KeyContentType = objMasterPageManager.GetContentType(),
                KeySite = objMasterPageManager.GetSite(),
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC
            };

            var langKey = objMasterPageManager.GetLanguageFromUrl();
            if (null != langKey)
            {
                searcher.key.IdLanguage = (short)langKey.Id;
            }

            //LazyLoad
            if (loadingType == DataLoadingType.List)
            {
                searcher.CalculatePagerTotalCount = true;
                if (null != soModel)
                {
                    if (soModel.PageNumber.HasValue)
                    {
                        searcher.PageNumber = soModel.PageNumber.Value;
                    }
                    searcher.PageSize = soModel.PageSize;
                }
            }
            else if (loadingType == DataLoadingType.Details)
            {
                searcher.CalculatePagerTotalCount = false;
                searcher.SetMaxRow = 1;
            }

            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.Copyright);
            searcher.Properties.Add(so => so.LinkUrl);
            searcher.Properties.Add(so => so.LinkLabel);

            return searcher;

        }

        public static ContentExtraSearcher InitContentExtraSearcher(RequestContext requestContext, DataLoadingType loadingType, int idContent)
        {
            Healthware.HP3.Core.Web.MasterPageManager objMasterPageManager = new Healthware.HP3.Core.Web.MasterPageManager();

            var searcher = new ContentExtraSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                KeySiteArea = objMasterPageManager.GetSiteArea(),
                KeyContentType = objMasterPageManager.GetContentType(),
                KeySite = objMasterPageManager.GetSite(),
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC
            };

            var urlLang = objMasterPageManager.GetLanguageFromUrl();
            if (null != urlLang)
                searcher.key.IdLanguage = (short)urlLang.Id;

            //LacyLoad

            if (loadingType == DataLoadingType.DetailsById)
            {
                searcher.key = new ContentIdentificator(idContent, searcher.key.IdLanguage);
                searcher.CalculatePagerTotalCount = false;
            }
            else if (loadingType == DataLoadingType.Details)
            {
                searcher.CalculatePagerTotalCount = false;
            }

            searcher.Properties.Add(so => so.MetaKeywords);
            searcher.Properties.Add(so => so.MetaDescription);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.TitleContentExtra);
            searcher.Properties.Add(so => so.FullText);
            searcher.Properties.Add(so => so.FullTextOriginal);
            searcher.Properties.Add(so => so.Abstract);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.Code);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.SiteArea.Key.Id);
            searcher.Properties.Add(so => so.ContentType.Key.Id);

            return searcher;

        }


        public static ContentSearcher InitLastNewsSearcher(RequestContext requestContext,int maxRow)
        {
            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySite = Keys.SiteAreaKeys.PortalSite,
                KeySiteArea = Keys.SiteAreaKeys.News,
                KeyContentType = Keys.ContentTypeKeys.News,
                
               SetMaxRow=maxRow
               
            };

            searcher.CalculatePagerTotalCount = false;
          
            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.SiteArea.Key.Id);
            searcher.Properties.Add(so => so.ContentType.Key.Id);

            return searcher;

        }

        public static ContentSearcher InitRelatedContentSearcher(RequestContext requestContext, short LanguageId, int idContent, SiteAreaIdentificator saKey, ContentTypeIdentificator ctKey)
        {
            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySite = Keys.SiteAreaKeys.PortalSite,
                KeySiteArea = Helper.SiteAreaHelper.GetSiteAreaIdentificator(),
                KeyContentType = Helper.ContentTypeHelper.GetContentTypeIdentificator()
            };

            searcher.CalculatePagerTotalCount = false;
            searcher.key = new ContentIdentificator(idContent, LanguageId);
            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.SiteArea.Key.Id);
            searcher.Properties.Add(so => so.ContentType.Key.Id);

            return searcher;

        }

        public static ContentSearcher InitSearchEngineJsonSearcher(RequestContext requestContext, Models.GenericSearcherEngineContentModel soModel)
        {
            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySite = Keys.SiteAreaKeys.PortalSite,
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC
            };

            searcher.SearchString = soModel.SearchString;
            searcher.key.IdLanguage = (short)soModel.LanguageId;

            IdentificatorCollection identSiteAreaColl = new IdentificatorCollection();
            //identSiteAreaColl.Add(Keys.SiteAreaKeys.RepositoryDownalod);
            searcher.KeysSiteAreaToExclude = identSiteAreaColl;

            searcher.CalculatePagerTotalCount = true;
            searcher.Properties.Add(so => so.Launch);

            if (soModel.PageNumber.HasValue)
            {
                searcher.PageNumber = soModel.PageNumber.Value;
            }
            searcher.PageSize = soModel.PageSize;

            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.SiteArea.Key.Id);
            searcher.Properties.Add(so => so.ContentType.Key.Id);
            searcher.Properties.Add(so => so.LinkUrl);
            searcher.Properties.Add(so => so.LinkLabel);

            return searcher;
        }

        public static ContentSearcher InitRelatedContentSearcher(RequestContext requestContext, short LanguageId, int idContent)
        {
            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySite = Keys.SiteAreaKeys.PortalSite,
                CalculatePagerTotalCount = false
            };

            searcher.key = new ContentIdentificator(idContent, LanguageId);
            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.Title);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.SiteArea.Key.Id);
            searcher.Properties.Add(so => so.ContentType.Key.Id);

            return searcher;

        }


        public static ContentSearcher InitContentJsonSearcher(RequestContext requestContext, DataLoadingType loadingType, Models.GenericSearcherContentModel soModel)
        {


            var searcher = new ContentSearcher()
            {
                Display = SelectOperation.Enabled(),
                Active = SelectOperation.Enabled(),
                Delete = SelectOperation.Enabled(),
                ApprovalStatus = ContentAbstractSearcher.ApprovalWFStatus.Approved,
                KeySite = Keys.SiteAreaKeys.PortalSite,
                SortType = ContentGenericComparer.SortType.ByData,
                SortOrder = ContentGenericComparer.SortOrder.DESC
            };


            searcher.key.IdLanguage = (short)soModel.LanguageId;
            searcher.KeySiteArea.Id = soModel.SiteAreaId;
            searcher.KeyContentType.Id = soModel.ContenTypeId;

            if (soModel.ContextId > 0)
                searcher.KeyContext.Id = soModel.ContextId;          


            

            searcher.CalculatePagerTotalCount = true;

            if (searcher.PageNumber > 1)
            {
                searcher.CalculatePagerTotalCount = false;
            }

            searcher.Properties.Add(so => so.Launch);
            if (soModel.PageNumber.HasValue)
            {
                searcher.PageNumber = soModel.PageNumber.Value;
            }
            searcher.PageSize = soModel.PageSize;

            searcher.Properties.Add(so => so.Key.PrimaryKey);
            searcher.Properties.Add(so => so.DatePublish);
            searcher.Properties.Add(so => so.Launch);
            searcher.Properties.Add(so => so.Title);

            return searcher;

        }



        
    }
}
