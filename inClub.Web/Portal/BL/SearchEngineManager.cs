﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Content.ObjectValues;
using Inclub.Web.Portal.Models;

namespace Inclub.Web.Portal.BL
{
    public class SearchEngineManage
    {
        public GenericContentCollection GetArchive(ContentSearcher so, int LanguageId, Healthware.HP3.Core.Search.SearchEngine.TypeResearch type)
        {
            if (so == null || string.IsNullOrWhiteSpace(so.SearchString))
                return null;

            var manager = new Healthware.HP3.Core.Search.SearchEngine();
            Healthware.HP3.Core.Search.ObjectValues.PositionValue pos = new Healthware.HP3.Core.Search.ObjectValues.PositionValue();

            pos.Language.Id = LanguageId;
            pos.Site.Id = Keys.SiteAreaKeys.PortalSite.Id;

            var coll = manager.Search(so, pos, type);
            //GenericContentArchives modelValue = null;
            var modelColl = new GenericContentCollection();
            if (coll != null && coll.Any())
            {

                modelColl.Page = so.PageNumber;
                modelColl.PageSize = so.PageSize;
                modelColl.PagesCount = coll.PagerTotalCount;
                modelColl.TotalCount = coll.PagerTotalNumber;

                modelColl.Items = coll.Select(cv =>
                {
                    var model = Adapters.ContentAdapter.AdaptArchivesSearchEngine(cv);
                    if (model != null)
                    {
                        model.LanguageId = LanguageId;
                    }
                    return model;
                }).ToArray();

                //foreach (ContentValue cv in coll)
                //{
                //    modelValue = Adapters.GenericContentAdapters.AdaptArchivesSearchEngine(cv);

                //    if (modelValue != null)
                //    {
                //        modelValue.PagerTotalNumber = coll.PagerTotalNumber;
                //        modelValue.LanguageId = LanguageId;
                //        modelColl.Add(modelValue);
                //    }
                //    modelValue = null;
                //}

            }

            return modelColl;

        }

        public static Keys.EnumeratorKeys.ActionSearchEngineType getTypeSection(int siteAreaId)
        {
            Dictionary<int, Keys.EnumeratorKeys.ActionSearchEngineType> dictionary = DifferentialLinkSearchEngine();

            Keys.EnumeratorKeys.ActionSearchEngineType type = Keys.EnumeratorKeys.ActionSearchEngineType.Generic;

            if (dictionary != null && dictionary.ContainsKey(siteAreaId))
            {
                type = dictionary[siteAreaId];
            }

            return type;

        }

        private static Dictionary<int, Keys.EnumeratorKeys.ActionSearchEngineType> DifferentialLinkSearchEngine()
        {

            Dictionary<int, Keys.EnumeratorKeys.ActionSearchEngineType> dictionary;
            string key = "DifferentialLinkSearchEngine";


            if (CacheManager.Exists(key, 1))
            {
                dictionary = (Dictionary<int, Keys.EnumeratorKeys.ActionSearchEngineType>)CacheManager.Read(key, 1);
            }
            else
            {

                dictionary = new Dictionary<int, Keys.EnumeratorKeys.ActionSearchEngineType>();

                //CacheManager.Insert() 
                //Key SiteAra

                dictionary.Add(61, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(62, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(63, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(64, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(66, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(68, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(50, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(51, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(52, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(73, Keys.EnumeratorKeys.ActionSearchEngineType.Link);
                dictionary.Add(100, Keys.EnumeratorKeys.ActionSearchEngineType.Download);
                //HC
                dictionary.Add(90, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(91, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(92, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(93, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);
                dictionary.Add(94, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);

                dictionary.Add(96, Keys.EnumeratorKeys.ActionSearchEngineType.OnlyDetails);




                CacheManager.Insert(key, dictionary, 1, 1440);

            }

            return dictionary;

        }
    }
}
