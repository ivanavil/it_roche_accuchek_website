﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Base.Persistent;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Content.ObjectValues;
using System.Data.SqlClient;
using Healthware.HP3.Core.Base.ObjectValues;
using System.Data;
using System.Configuration;
using Healthware.HP3.Core.Site.ObjectValues;
using Inclub.Web.Portal.Helper;

namespace Inclub.Web.Portal.BL
{
    public class UsersManage
    {

        public UserAccessCode GetUserByCode(UserHCPSearcher so)
        {
            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserHCPManager man = new UserHCPManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptUserAccessCode(user);
                }
            }

            return null;
        }
        
        
        public UserRegistration ReadHcpEditUser(UserHCPSearcher so)
        {
            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserHCPManager man = new UserHCPManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptUserEdit(user);
                }
            }

            return null;
        }

        public static int GetUserStatus(string email)
        {
            var so = new UserSearcher();
            so.Username = email;

            UserManager objctUserManager = new UserManager();
            objctUserManager.Cache = false;

            UserCollection coll = objctUserManager.Read(so);
            int retVal = (int)EnumeratorKeys.UserStatus.NotFound;

            if (coll != null && coll.Any())
            {
                UserValue user = coll.FirstOrDefault();
                if (null != user)
                {
                    retVal = user.Status;
                }
            }

            return retVal;
        }

        public static bool ActivateUser(string email)
        {

            bool retVal = false;
            UserSearcher so = new UserSearcher();
            so.Username = email;

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));
            UserManager manager = new UserManager();

            UserCollection coll = manager.Read(so);
            if (coll != null && coll.Any())
            {
                UserValue user = coll.FirstOrDefault();
                if (null != user)
                {
                    retVal = manager.Update(user.Key, "Status", (int)EnumeratorKeys.UserStatus.Active);
                    //distruzione cache
                    Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);
                }
            }
            return retVal;
        }





        public static int UpdateUser(UserRegistration model)
        {
            if (null == model)
                return (int)Keys.EnumeratorKeys.StatusCode.Error;

            if (Helper.UserHelper.UserExists(model))
                return (int)Keys.EnumeratorKeys.StatusCode.ExistsConflict;

          
          
            UserHCPManager man = new UserHCPManager();
            UserHCPSearcher so = new UserHCPSearcher();
            so.Key.Id = model.Id;          

            so.Status = (int)Keys.EnumeratorKeys.UserStatus.Active;
            var incSo = new UserInclusiveSearcher();
            incSo.LoadContexts = true;
            so.InclusiveOf = incSo;

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            var coll = man.Read(so);
            if (null == coll || !coll.Any())
                return (int)Keys.EnumeratorKeys.StatusCode.Error;

            var user = coll.FirstOrDefault();
            if (null == user)
                return (int)Keys.EnumeratorKeys.StatusCode.Error;

            string oldEmail = user.Email.GetString;

            //todo: check se la nuova email è già in uso da un utente diverso dal richiedente
            UserSearcher userSo = new UserSearcher();
            userSo.Username = model.Email.Trim();
            userSo.Properties.Add("Username");
            userSo.Properties.Add("Key.Id");
            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(userSo));

            UserManager userMan = new UserManager();
            var userColl = userMan.Read(userSo);
            if (null != userColl && userColl.Any())
            {
                var userVal = userColl.FirstOrDefault();
                if (userVal.Key.Id != user.Key.Id)
                    return (int)Keys.EnumeratorKeys.StatusCode.ExistsConflict;
            }
            
            user.Name = model.Name;
            user.Surname = model.Surname;
            user.HCPStructure = model.Structure;
            user.HCPCity = model.City;
         
            user.HCPFax.Clear();
            user.HCPFax.Add(model.ProvinciaAlbo.Trim());

            if (!(string.IsNullOrEmpty(model.TermsMedical2))){
                user.HCPAddress.Clear();
                user.HCPAddress.Add(model.TermsMedical2);
                }

            if (!(string.IsNullOrEmpty(model.TermsMedical3)))
            user.HCPCap = model.TermsMedical3;

            if (!(string.IsNullOrEmpty(model.TermsMedical4)))
            user.HCPProvince = model.TermsMedical4;

            user.HCPProfessionalCodes.Clear();
            user.HCPProfessionalCodes.Add(model.NumeroIscrizioneAlbo.Trim());
            user.CanReceiveNewsletter = model.AcceptNotify;
            user.HCPTelephone.Clear();

            if (model.SkypeLink != null)
            {
                user.HCPTelephone.Add(model.SkypeLink.Trim());
            }
            else
            {
                user.HCPTelephone.Add(string.Empty);
            }

            

            user.HCPOtherInfo.Clear();
            if (model.TwitterLink != null)
            {
                user.HCPOtherInfo.Add(model.TwitterLink.Trim());
            }
            else
            {
                user.HCPOtherInfo.Add(string.Empty);
            }

            user.HCPMoreInfo.Clear();
            if (model.LinkedinLink != null)
            {
                user.HCPMoreInfo.Add(model.LinkedinLink.Trim());
            }
            else
            {
                user.HCPMoreInfo.Add(string.Empty);
            }

            user.CF = model.CodiceFiscale;


            if (model.MaritalStatus == 1)
            {
                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    user.Password = Regex.Replace(model.Password, LabelKeys.TrimAllWhiteSpaces, string.Empty);
                }
                user.MaritalStatus = UserValue.MaritalStatusType._single;
            }

            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                user.Email.Clear();
                user.Email.Add(model.Email);

                user.Username = user.Email.ToString();
            }


            if (model.SelectedSpecId > 0)
            {
                user.HCPProfessionalCodeGeo.Id = model.SelectedSpecId;
            }


            if (model.SelectedSpecId > 0)
            {
                Helper.ContextHelper.DeleteUserContext(user.Key.Id, EnumeratorKeys.UserContextRelationType.Specialization);
                Helper.ContextHelper.CreateUserContext(user.Key.Id, model.SelectedSpecId, EnumeratorKeys.UserContextRelationType.Specialization);
            }
    
            var retVal = man.Update(user);


            //distruzione cache
            Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);

            return (int)Keys.EnumeratorKeys.StatusCode.Succeded;
        }



        public static bool CreateUserContent(int userID, int ContentID, int RelationTypeUD)

        {
     
            Helper.ContentHelper.CreateUserContent(userID, ContentID, RelationTypeUD);
            return true;
        }

        public UserHCPValue CreateNewUser(UserRegistration user)
        {
            UserHCPValue _retVal = null;

            var vo = new UserHCPValue();

            vo.Name = user.Name;
            vo.HCPStructure = user.Structure;
            vo.HCPCity = user.City;
            vo.HCPFax.Clear();
            vo.HCPFax.Add(user.ProvinciaAlbo.ToString());
            vo.HCPProfessionalCodes.Add(user.NumeroIscrizioneAlbo.ToString());

            vo.Surname = user.Surname;
            vo.Username = user.Email.Trim();
            vo.CF = user.CodiceFiscale;
            
            
            vo.HCPAddress.Add(user.TermsMedical2);
            vo.HCPCap = user.TermsMedical3;
            vo.HCPProvince = user.TermsMedical4;


            vo.CanReceiveNewsletter = user.AcceptNotify; //Notifiche dai gruppi attivo di default
            vo.MaritalStatus = UserValue.MaritalStatusType._single;
            if (!string.IsNullOrWhiteSpace(user.Password))
            {
                vo.Password = Regex.Replace(user.Password, LabelKeys.TrimAllWhiteSpaces, string.Empty);
            }
            vo.Email.Add(user.Email.Trim());

            if (user.SkypeLink != null)
                vo.HCPTelephone.Add(user.SkypeLink.Trim());

            if (user.TwitterLink != null)
                vo.HCPOtherInfo.Add(user.TwitterLink.Trim());

            if (user.LinkedinLink != null)
                vo.HCPMoreInfo.Add(user.LinkedinLink.Trim());

            vo.RegistrationSite.Id = SiteAreaKeys.PortalSite.Id;

            if (user.AccessCode != "")
            {
                vo.Status = (int)EnumeratorKeys.UserStatus.Active;
            }
            else
            {
                vo.Status = (int)EnumeratorKeys.UserStatus.Pending;
            }

            vo.UserType = UserValue.UsersType.HCP;

            if (user.SelectedSpecId > 0)
            {
                vo.HCPProfessionalCodeGeo.Id = user.SelectedSpecId;
            }

            var man = new UserHCPManager();
            UserHCPValue userHCP = man.Create(vo);
            CacheHelper.DeleteGroupCache(CacheKeys.GroupCacheUser);


            if (userHCP != null && userHCP.Key.Id > 0)
            {
                if (user.AccessCode != "") { 
                //salvo l'associazione dell'utente con l'accesscode in modo da bruciarlo per successivi usi
                    Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                    dynamic command = new SqlCommand("Update accuchek_AccessCode set id_user = " + userHCP.Key.Id.ToString() + " where accessCode = '" + user.AccessCode + "'", manager.GetConnection());
                    command.CommandType = CommandType.Text;
                    manager.OpenConnection();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    command = null;
                }


                //serve l'id criptato nel caso volessimo usare EnewsletterPro
                man.Update(userHCP.Key, "SiteAcquaintedThrougt", Helper.CryptHelper.urlEncrypt(userHCP.Key.Id.ToString()));

                //Specializzazione
                if (user.SelectedSpecId > 0)
                {
                    Helper.ContextHelper.CreateUserContext(userHCP.Key.Id, user.SelectedSpecId, EnumeratorKeys.UserContextRelationType.Specialization);
                }
                //Aggiungo i ruoli necessari
                UsersManage.AddRole(userHCP.Key.Id, Keys.RoleKeys.userAccess);


                //associo l'utente ai gruppi aperti col ruolo di membro 
                //leggo i gruppi aperti, contrassegnati dall'associazione al context 96 grpDefault                
                var cSrc =new ContentContextSearcher();
                ContentManager objCntManager = new ContentManager();
                cSrc.KeyContext = Inclub.Web.Portal.Keys.ContextGroupKeys.CtxDefaultGroup;
                ContentContextCollection groups = objCntManager.ReadContentContext(cSrc);
                
                if (groups != null && groups.Any())
                { 
                    foreach (ContentContextValue cv in groups)
                    {
                        //associo ognuno dei gruppi all'utente
                        int groupKey = objCntManager.ReadPrimaryKey(cv.KeyContent.Id, 1);
                        Inclub.Web.Portal.BL.UsersManage.CreateUserContent(userHCP.Key.Id, cv.KeyContent.Id, RelationTypeKeys.GrpMember);

                        //incremento il contatore degli iscritti a bordo del gruppo    

                        Healthware.HP3.Core.Base.Persistent.SqlConnectionManager sqlManager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = ((cast(isNull(nullif(contents_qta_magazzino,''),'0') as int)) + 1) where contents_key = " + groupKey, sqlManager.GetConnection());
                        command.CommandType = CommandType.Text;
                        sqlManager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;
                    }
                }

                _retVal = userHCP;
            }

            return _retVal;

        }


        public static UserRegistration CreateUser(UserRegistration user)
        {
            var vo = new UserHCPValue();

            vo.Name = user.FirstName;
            vo.Surname = user.LastName;
            vo.Title = user.Title;

            vo.Username = user.Email.Trim();
            if (!string.IsNullOrWhiteSpace(user.Password))
            {
                vo.Password = Regex.Replace(user.Password, Keys.LabelKeys.TrimAllWhiteSpaces, string.Empty);
            }
            vo.Email.Add(user.Email.Trim());

            if (user.Telephone != null)
            {
                vo.Telephone.Add(user.Telephone);
            }
            if (user.CellPhone != null)
            {
                vo.Cellular.Add(user.CellPhone);
            }

            vo.RegistrationSite.Id = Keys.SiteAreaKeys.PortalSite.Id;

            vo.Status = (int)EnumeratorKeys.UserStatus.Pending;


            var man = new UserHCPManager();
            UserHCPValue userHCP = man.Create(vo);

            Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);

            return null;
        }

        public static void UpdateForceChangePassword(int userId, bool force)
        {
            UserManager man = new UserManager();
            man.Update(new UserIdentificator(userId), "PasswordForceChange", force);

            Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);
        }

        public static UserChangePassword ReadChangePasswordUser(UserSearcher so)
        {

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserManager man = new UserManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptChangePasswordUser(user);
                }
            }

            return null;
        }

        public static UserBase ReadUserBase(UserSearcher so)
        {

            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserManager man = new UserManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptUser(user);
                }
            }

            return null;
        }

        protected static UserValue ReadUserValue(UserSearcher so)
        {

            UserManager man = new UserManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                return users.FirstOrDefault();
            }
            return null;
        }

        public static UserSearcher InitBaseUserSearcher()
        {
            UserSearcher so = new UserSearcher();
            so.Properties.Add("Name");
            so.Properties.Add("Surname");
            so.Properties.Add("Email.GetString");
            so.Properties.Add("Username");
            so.Properties.Add("Key.Id");
            return so;
        }

        public static UserSearcher InitChangePasswordUserSearcher()
        {
            UserSearcher so = new UserSearcher();
            so.Properties.Add("Password");
            so.Properties.Add("Key.Id");

            return so;
        }

        public static UserSearcher InitForgotPasswordUserSearcher()
        {
            UserSearcher so = new UserSearcher();
            so.Properties.Add("Status");
            so.Properties.Add("Username");
            so.Properties.Add("Password");
            so.Properties.Add("Name");
            so.Properties.Add("Surname");
            so.Properties.Add("Email.GetString");
            return so;
        }




        public bool UpdateHCPResetPassword(int userId, string password)
        {

            if (userId > 0)
            {

                var so = new UserSearcher();
                so.Key = new UserIdentificator(userId);
                var user = ReadUserValue(so);
                if (null != user)
                {
                    user.Password = password;
                    UserManager man = new UserManager();
                    man.ChangePasswordProcess(user);
                    Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);

                    return true;
                }

            }
            return false;
        }

        public static bool UpdateResetPassword(int userId, string password)
        {

            if (userId > 0)
            {

                var so = new UserSearcher();
                so.Key = new UserIdentificator(userId);
                var user = ReadUserValue(so);
                if (null != user)
                {
                    user.Password = password;
                    UserManager man = new UserManager();
                    man.ChangePasswordProcess(user);
                    Helper.CacheHelper.DeleteGroupCache(Keys.CacheKeys.GroupCacheUser);

                    return true;
                }

            }
            return false;
        }

        public static UserEdit ReadEditUser(UserHCPSearcher so)
        {
            Helper.CacheHelper.InsertKeyInGroupCache(Keys.CacheKeys.GroupCacheUser, Helper.CacheHelper.getKeyBySearcher(so));

            UserHCPManager man = new UserHCPManager();
            var users = man.Read(so);
            if (null != users && users.Any())
            {
                var user = users.FirstOrDefault();
                if (null != user)
                {
                    return Adapters.UserAdapter.AdaptUserEdit(user);
                }
            }

            return null;
        }

        public static bool AddRole(UserRegistration user, RoleIdentificator roleKey)
        {

            if (null == user || null == roleKey || !(user.Id > 0))
                return false;

            ProfilingManager man = new ProfilingManager();
            return man.CreateUserRole(new UserIdentificator(user.Id), roleKey);
        }

        public static bool AddRole(int userID, RoleIdentificator roleKey)
        {

            if (null == roleKey || !(userID > 0))
                return false;

            ProfilingManager man = new ProfilingManager();
            return man.CreateUserRole(new UserIdentificator(userID), roleKey);
        }



        public static bool RemoveRole(int userID, RoleIdentificator roleKey)
        {

            if (null == roleKey || !(userID > 0))
                return false;

            ProfilingManager man = new ProfilingManager();
            return man.RemoveUserRole(new UserIdentificator(userID), roleKey);
        }

        public static int UpdateNewsletterEmail(int userID, string oldEmailAddress, string newEmailAddress)
        {

            int retVal = 0;
            try
            {
                string connStr = ConfigurationManager.AppSettings["conn-string"];
                string spRead = "proc_HP3_UpdateEmailForNewsletterSubscription";

                using (SqlConnection sqlConn = new SqlConnection(connStr))
                {
                    using (SqlCommand sqlCmd = new SqlCommand(spRead, sqlConn))
                    {
                        sqlConn.Open();
                        sqlCmd.Connection = sqlConn;
                        sqlCmd.CommandType = CommandType.StoredProcedure;

                        List<SqlParameter> sqlParams = new List<SqlParameter>();
                        sqlParams.Clear();

                        sqlParams.Add(new SqlParameter("@userID", userID));
                        sqlParams.Add(new SqlParameter("@oldEmailAddress", oldEmailAddress));
                        sqlParams.Add(new SqlParameter("@newEmailAddress", newEmailAddress));

                        sqlCmd.Parameters.AddRange(sqlParams.ToArray());

                        retVal = sqlCmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return retVal;
        }
    }
}