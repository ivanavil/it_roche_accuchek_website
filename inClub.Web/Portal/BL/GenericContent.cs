﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;

namespace Inclub.Web.Portal.BL
{
    public class GenericContent
    {
        public GenericContentDetails GetDetail(ContentExtraSearcher so)
        {
            if (so == null)
                return null;


            var manager = new ContentExtraManager();
            ContentExtraValue objContentExtraValue = null;

            var coll = manager.Read(so);
            GenericContentDetails model = null;
            if (coll != null && coll.Any())
            {
                objContentExtraValue = coll.FirstOrDefault();
                model = Adapters.ContentAdapter.AdaptDetails(objContentExtraValue);
            }
            return model;
        }


        public GenericContentDetails GetDetail(ContentIdentificator keyContent)
        {
            if (keyContent == null || keyContent.Id == 0)
                return null;

            var manager = new ContentManager();
            var obj = manager.Read(keyContent);
            GenericContentDetails model = null;
            if (obj != null && obj.Key != null && obj.Key.Id > 0)
            {
                model = Adapters.ContentAdapter.AdaptDetails(obj);
            }
            return model;
        }


        public GenericContentDetails GetDetail(ContentSearcher so)
        {
            if (so == null)
                return null;

            var manager = new ContentManager();
            ContentValue objContentValue = null;

            var coll = manager.Read(so);
            GenericContentDetails model = null;
            if (coll != null && coll.Any())
            {
                objContentValue = coll[0];
                model = Adapters.ContentAdapter.AdaptDetails(objContentValue);
            }
            return model;
        }

        public GenericContentCollection GetArchive(ContentSearcher so, int LanguageId, int TemeId)
        {
            if (so == null)
                return null;

            var manager = new ContentManager();

            var coll = manager.Read(so);
            //GenericContentArchives modelValue = null;
            var modelColl = new GenericContentCollection();
            if (coll != null && coll.Any())
            {
                modelColl.Page = so.PageNumber;
                modelColl.PageSize = so.PageSize;
                modelColl.PagesCount = coll.PagerTotalCount;
                modelColl.TotalCount = coll.PagerTotalNumber;


                modelColl.Items = coll.Select(cv =>
                {
                    var model = Adapters.ContentAdapter.AdaptArchives(cv);
                    if (model != null)
                    {
                        model.ThemeId = TemeId;
                        model.LanguageId = LanguageId;
                    }
                    return model;
                }).ToArray();

                //foreach (ContentValue cv in coll)
                //{
                //    modelValue = Adapters.GenericContentAdapters.AdaptArchives(cv);

                //    if (modelValue != null)
                //    {
                //        //if (TemeId == 205) //literature watch
                //        //{
                //        //    modelValue.ReferenceAuthors = HelsinnCorporate.Corporate.Helper.GenericHelper.GetReferencesAuthors(cv.Key);
                //        //}

                //        modelValue.PagerTotalNumber = coll.PagerTotalNumber;
                //        modelValue.ThemeId = TemeId;
                //        modelValue.LanguageId = LanguageId;
                //        modelColl.Add(modelValue);
                //    }
                //    modelValue = null;
                //}
            }
            return modelColl;
        }
    }
}
