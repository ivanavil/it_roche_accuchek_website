﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Xml;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Inclub.Web.Portal.Keys;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Localization;
using Healthware.HP3.Core.Localization.ObjectValues;



namespace Inclub.Web.Portal.BL
{
    public class GenericContentManage
    {
        public ContentCollection GetArchive(ContentSearcher so)
        {
            if (null == so)
                return null;

            ContentManager manager = new ContentManager();
            var coll = manager.Read(so);

            manager = null;

            return coll;
        }


        public IEnumerable<Models.GenericContentBox> LastNews(ContentSearcher so)
        {

            ContentManager manager = new ContentManager();
            var coll = manager.Read(so);

            GenericContentBox modelValue = null;
            var modelColl = new List<Models.GenericContentBox>();
            if (coll != null && coll.Any())
            {
                foreach (ContentValue cv in coll)
                {
                    modelValue = Adapters.BoxAndRelatedAdapters.Adapter(cv);

                    if (modelValue != null)
                    {
                        modelColl.Add(modelValue);
                    }
                    modelValue = null;
                }
            }

            return modelColl;

        }


        public IContent GetDetailGenCont(ContentExtraSearcher so, bool hasReferences)
        {
            if (so == null)
                return null;

            var manager = new ContentExtraManager();
            ContentExtraValue objContentExtraValue = null;

            var coll = manager.Read(so);
            GenericContentDetails model = null;
            if (coll != null && coll.Any())
            {
                objContentExtraValue = coll.FirstOrDefault();
                model = Adapters.ContentAdapter.AdaptDetail(objContentExtraValue, hasReferences);
            }
            return model;
        }
    }
}
