﻿using System.Linq;
using System.Web.Routing;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Site;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;

namespace Inclub.Web.Portal.BL
{
    public class Shared
    {
        public static ThemeValue ReadTheme(RequestContext requestContext, ThemeIdentificator themeKey, int keyLang)
        {
            var searcher = SearcherBuilders.InitThemeSearcher(requestContext, SearcherBuilders.DataLoadingType.Full);
            searcher.Key = themeKey;
            searcher.KeyLanguage.Id = keyLang;
            searcher.Display = SelectOperation.All();
            searcher.LoadRoles = true;
            var manager = new ThemeManager();
            manager.Cache = true;
            var themes = manager.Read(searcher);
            return (null == themes || !themes.Any()) ? null : themes[0];
        }

        public static ThemeValue ReadTheme(RequestContext requestContext, ThemeIdentificator themeKey)
        {
            var searcher = SearcherBuilders.InitThemeSearcher(requestContext, SearcherBuilders.DataLoadingType.Full);
            searcher.Key = themeKey;
            searcher.Display = SelectOperation.All();
            var manager = new ThemeManager();
            manager.Cache = true;
            var themes = manager.Read(searcher);
            return (null == themes || !themes.Any()) ? null : themes[0];
        }


        public static PageInfo ReadPageInfo(RequestContext requestContext, ThemeIdentificator themeKey, LanguageIdentificator LanguageKey)
        {
            var theme = ReadTheme(requestContext, themeKey, LanguageKey.Id);
            return (null != theme) ? new PageInfo(theme) : null;
        }

        public static PageInfo ReadPageInfo(RequestContext requestContext, ThemeIdentificator themeKey)
        {
            var theme = ReadTheme(requestContext, themeKey);
            return (null != theme) ? new PageInfo(theme) : null;
        }
    }
}
