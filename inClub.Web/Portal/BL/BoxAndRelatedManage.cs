﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Box.ObjectValues;
using Healthware.HP3.Core.Content.ObjectValues;
using Inclub.Web.Portal.Models;

namespace Inclub.Web.Portal.BL
{
    public class BoxAndRelatedManage
    {
        public IEnumerable<Models.GenericContentBox> ReadContentBox(int BoxId, short LanguageId)
        {
            var coll = Helper.GenericHelper.ReadContentBox(new BoxIdentificator(BoxId));

            GenericContentBox modelValue = null;
            var modelColl = new List<Models.GenericContentBox>();
            if (coll != null && coll.Any())
            {
                foreach (ContentValue cv in coll)
                {
                    modelValue = Adapters.BoxAndRelatedAdapters.Adapter(cv);

                    if (modelValue != null)
                    {
                        modelColl.Add(modelValue);
                    }
                    modelValue = null;
                }
            }

            return modelColl;

        }


       


        public IEnumerable<Models.GenericContentBox> ReadRelatedContentBox(ContentSearcher so, Keys.EnumeratorKeys.ContentRelationType rel)
        {
            var coll = Helper.GenericHelper.ReadContentRelation(so, new ContentRelationTypeIdentificator((int)rel));

            GenericContentBox modelValue = null;
            var modelColl = new List<Models.GenericContentBox>();
            if (coll != null && coll.Any())
            {
                foreach (ContentValue cv in coll)
                {
                    modelValue = Adapters.BoxAndRelatedAdapters.Adapter(cv);

                    if (modelValue != null)
                    {
                        modelColl.Add(modelValue);
                    }
                    modelValue = null;
                }
            }

            return modelColl;

        }

        /// <summary>
        /// Only one value
        /// </summary>
        /// <param name="BoxId"></param>
        /// <returns></returns>
        public Models.GenericContentBox ReadContentBox(int BoxId)
        {
            var coll = ReadContentsBox(new BoxIdentificator(BoxId));

            GenericContentBox modelValue = null;

            if (coll != null && coll.Any())
            {
                modelValue = Adapters.BoxAndRelatedAdapters.Adapter(coll.First());
            }

            return modelValue;

        }

        public IEnumerable<Models.GenericContentBox> ReadRelatedContentsBox(int BoxId)
        {
            var coll = ReadContentsBox(new BoxIdentificator(BoxId));
            GenericContentBox modelValue = null;
            var modelColl = new List<Models.GenericContentBox>();

            if (coll != null && coll.Any())
            {
                foreach (ContentValue cv in coll)
                {
                    modelValue = Adapters.BoxAndRelatedAdapters.Adapter(cv);

                    if (modelValue != null)
                    {
                        modelColl.Add(modelValue);
                    }
                    modelValue = null;
                }
            }

            return modelColl;

        }




        private ContentCollection ReadContentsBox(Healthware.HP3.Core.Box.ObjectValues.BoxIdentificator boxIdentificator)
        {
            var manager = new Healthware.HP3.Core.Content.ContentManager();

            var coll = manager.ReadContentBox(boxIdentificator);

            manager = null;

            return coll;

        }

    }
}