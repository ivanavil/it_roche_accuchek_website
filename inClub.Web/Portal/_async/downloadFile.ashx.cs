﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using System.Web.Script.Serialization;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Base;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.Web;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Site;
using Inclub.Web.Portal;

namespace inClub.Web.Portal._async
{
    /// <summary>
    /// Summary description for downloadFile
    /// </summary>
    public class downloadFile : IHttpHandler
    {
        MasterPageManager objMasterPageManager = new MasterPageManager();
        ContentManager objContentManager = new ContentManager();
        UserManager objUserManager = new UserManager();
        AccessService objAccess = new AccessService();


        int intResourcePk = 0;
        int intLanguageId = 0;
        int intGroupId = 0;

        DownloadManager oDownloadManager = new DownloadManager();
        DownloadValue oDownloadVal;
        //Dim accessService As AccessService = Nothing
        TicketValue objTicket = null;

        public void ProcessRequest(HttpContext context)
        {
            int.TryParse(SimpleHash.UrlDecryptRijndaelAdvanced(context.Request["intPk"]), out intResourcePk);
            objAccess = new AccessService();
            objTicket = objAccess.GetTicket();
            string _strSecurityCheck = checkSecurity(context);

            if (string.IsNullOrWhiteSpace(_strSecurityCheck))
            {
                try
                {
                    dynamic filename = string.Format("{0}.{1}", oDownloadVal.Key.PrimaryKey, oDownloadVal.DownloadTypeCollection[0].DownloadType.Label);
                    dynamic filePath = context.Server.MapPath(string.Format("~/hp3download/{0}", filename));

                    //context.Response.Write(filePath)

                    if (System.IO.File.Exists(filePath))
                    {
                        //Trace-------------------------------------------------
                        Inclub.Web.Portal.Helper.TraceObject objTrace = new Inclub.Web.Portal.Helper.TraceObject();
                        var _with1 = objTrace;
                        _with1.Content = null;
                        _with1.ContentId = oDownloadVal.Key.Id;
                        _with1.Description = string.Empty;
                        _with1.EventId = 5;
                        _with1.LanguageId = oDownloadVal.Key.IdLanguage;
                        _with1.SiteId = objTicket.Travel.KeySite.Id;
                        _with1.ThemeId = Inclub.Web.Portal.Keys.ThemeKeys.Documenti.Id;
                        _with1.UserId = objTicket.User.Key.Id;
                        Inclub.Web.Portal.Helper.TraceHelper.TraceEvent(objTrace);
                        //------------------------------------------------------

                        //increment download counter----------------------------
                        Healthware.HP3.Core.Base.Persistent.SqlConnectionManager manager = new Healthware.HP3.Core.Base.Persistent.SqlConnectionManager();
                        dynamic command = new SqlCommand("Update pp_contents set contents_qta_magazzino = (IsNull(contents_qta_magazzino,0) + 1) where contents_key = " + intResourcePk, manager.GetConnection());
                        command.CommandType = CommandType.Text;
                        manager.OpenConnection();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        command = null;
                        manager.CloseConnection();


                        //------------------------------------------------------

                        context.Response.Clear();
                        context.Response.ContentType = "application/octet-stream";
                        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", RemoveCharacter(oDownloadVal.Title).Replace(" ", "_"), oDownloadVal.DownloadTypeCollection[0].DownloadType.Label));
                        context.Response.TransmitFile(filePath);
                    }
                    else
                    {
                        showerror(context, "Il file richiesto non è disponibile.1");
                    }

                }
                catch (Exception ex)
                {
                    showerror(context, ex.ToString());
                }
            }
            else
            {
                showerror(context, _strSecurityCheck);
            }
        }

        public void showerror(HttpContext context, string strerrorMessage)
        {
            //context.Response.Write(strerrorMessage)
            context.Response.Clear();
            context.Response.Write(strerrorMessage);
            context.Response.StatusCode = 404;
            context.Response.End();
        }

        //Check dsella sicurezza sul download dei files
        private string checkSecurity(HttpContext context)
        {
            if (intResourcePk == 0)
                return "Il file richiesto non è disponibile.";

            //Check sull'autenticazione
            if (!objAccess.IsAuthenticated())
                return "Il download è disponibile solo per gli utenti autenticati.";

            DownloadSearcher oDownloadSearcher = new DownloadSearcher();
            var _with2 = oDownloadSearcher;
            _with2.key = new ContentIdentificator(intResourcePk);
            if (objTicket.Roles.HasRole(Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id) | objTicket.Roles.HasRole(Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id))
                _with2.Active = SelectOperation.All();

            oDownloadManager.Cache = false;
            DownloadCollection oDownloadColl = oDownloadManager.Read(oDownloadSearcher);
            //(New ContentIdentificator(intResourcePk))
            if ((oDownloadColl != null) && (oDownloadColl.Count > 0))
            {
                oDownloadVal = new DownloadValue();
                oDownloadVal = oDownloadColl[0];


                if (oDownloadVal.ContentType.Key.Id == Inclub.Web.Portal.Keys.ContentTypeKeys.Guide.Id)
                {
                    return string.Empty;
                }
                else 
                { 
                    objContentManager.Cache = false;

                    //Check sull'accoppiamente risorsa-gruppo
                    ContentRelationSearcher _so = new ContentRelationSearcher();
                    var _with3 = _so;
                    _with3.KeyContentRelated.PrimaryKey = intResourcePk;
                    _with3.KeyRelationType.Id = Inclub.Web.Portal.Keys.RelationTypeKeys.Resources;
                    _with3.ActiveContentRelated = SelectOperation.All();
                    _with3.ActiveContent = SelectOperation.All();

                    ContentRelationCollection _oColl = null;
                    _oColl = objContentManager.ReadContentRelation(_so);

                    int _intGroupId = 0;
                    if ((_oColl != null) && (_oColl.Count > 0))
                    {
                        _intGroupId = _oColl[0].KeyContent.Id;
                    }
                    else
                    {
                        return "Il file richiesto non è disponibile.";
                    }

                    intGroupId = _intGroupId;

                    _oColl = null;
                    _so = null;


                    //Check sull'accoppiamente utente-gruppo
                    if (objTicket.Roles.HasRole(Inclub.Web.Portal.Keys.RoleKeys.SysAdmin.Id))
                        return string.Empty;

                    ContentUserSearcher _usSo = new ContentUserSearcher();
                    var _with4 = _usSo;
                    _with4.KeyContent.Id = _intGroupId;
                    _with4.KeyUser.Id = objTicket.User.Key.Id;
                    _with4.Order = 1;
                    objContentManager.Cache = false;
                    ContentUserCollection oColl = null;
                    oColl = objContentManager.ReadContentUser(_usSo);
                    if ((oColl != null) && (oColl.Count > 0))
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return "Il file richiesto non è disponibile.";
                    }
                }

            }
            else
            {
                return "Il file richiesto non è disponibile.";
            }

            return "Il file richiesto non è disponibile.";
        }





        public string RemoveCharacter(string stringToCleanUp)
        {
            string characterToRemove = "";
            characterToRemove = "#$%&'()*+,-./\\~";
            char[] firstThree = characterToRemove.ToCharArray();
            int index = 0;
            for (index = 1; index <= firstThree.Length - 1; index++)
            {
                stringToCleanUp = stringToCleanUp.ToString().Replace(firstThree[index].ToString(), "");
            }
            return stringToCleanUp;
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}