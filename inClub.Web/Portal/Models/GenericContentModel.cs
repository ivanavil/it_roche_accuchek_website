﻿using System;
using System.Collections.Generic;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Models
{

    public interface IContent
    {
        Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator Key { get; set; }
        string Title { get; set; }
        DateTime? DatePublication { get; set; }
    }

    public class GenericContentBaseModel : IContent
    {
        public Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator Key { get; set; }
        public string Title { get; set; }
        public DateTime? DatePublication { get; set; }
        public string PageTitle { get; set; }
        public string Launch { get; set; }
    }



    public class GenericContentBox : GenericContentBaseModel
    {
        public System.Web.HtmlString Launch { get; set; }
        public int LanguageId { get; set; }
        public SiteAreaIdentificator SiteAreaKey { get; set; }
        public ContentTypeIdentificator ContenteTypeKey { get; set; }
        public string LinkUrl { get; set; }
    }

    public class GenericContentBoxCollection
    {

        public SiteAreaIdentificator SiteAreaKey { get; set; }
        public ContentTypeIdentificator ContenteTypeKey { get; set; }
        public int pKey { get; set; }
        public IEnumerable<GenericContentBox> coll { get; set; }
    }

    public class GenericContentArchives : GenericContentBaseModel
    {
        public string IdContentCrypt { get; set; }
        public System.Web.HtmlString Launch { get; set; }
        public string Launchstr { get; set; }
        public System.Web.HtmlString BookReferences { get; set; }
        public SiteAreaIdentificator SiteAreaKey { get; set; }
        public ContentTypeIdentificator ContenteTypeKey { get; set; }
        public int PagerTotalNumber { get; set; }
        public int LanguageId { get; set; }
        public int ThemeId { get; set; }
        public string LinkUrl { get; set; }
        public string LinkLabel { get; set; }
        public string UrlDetail { get; set; }
        public string Coverstr { get; set; }
        public string Copyright { get; set; }
        public string Codecss { get; set; }
        public string BlockColor { get; set; }
        public ContextIncludedValue[] Contexts { get; set; }

    }

    public class GenericContentDetails : GenericContentBaseModel, MetaData
    {
        public System.Web.HtmlString InternalTitle { get; set; }
        public System.Web.HtmlString FullText { get; set; }
        public System.Web.HtmlString FullTextOriginal { get; set; }
        public System.Web.HtmlString BookReferences { get; set; }
        public System.Web.HtmlString Launch { get; set; }
        public System.Web.HtmlString Abstract { get; set; }
        public System.Web.HtmlString FullTextComment { get; set; }
        public System.Web.HtmlString ReferenceAuthors { get; set; }
        public System.Web.HtmlString TitleContentExtra { get; set; }
        public System.Web.HtmlString Metadescription { get; set; }

        public string Codecss { get; set; }
        public string MetaKeywords
        { get; set; }

        public string MetaDescription
        { get; set; }
    }

    public class GenericContentCollection : Healthware.HP3.MVC.PagedCollection<GenericContentArchives>
    {

    }
}
