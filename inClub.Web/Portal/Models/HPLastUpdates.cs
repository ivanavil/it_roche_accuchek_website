﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inClub.Web.Portal.Models
{
     [Serializable()]
    public class HPLastUpdates
    {
        public int GroupId { get; set; }
        public String GroupName { get; set; }        
        public List<Discussioni> LastDiscussionsList { get; set; }
        public List<Contributo> LastPostsList { get; set; }
    }
}