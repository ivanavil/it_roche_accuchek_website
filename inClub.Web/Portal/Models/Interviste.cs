﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace inClub.Web.Portal.Models
{
    public class Interviste
    {
        public ContentIdentificator IntervistaKey { get; set; }
        public ContentIdentificator GroupKey { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public int NumContributi { get; set; }
        public int NumCommenti { get; set; }
        public String CreationDate { get; set; }        
        public String Ext { get; set; }       
        public String SizeKb { get; set; }
        public String FileName { get; set; }
        public String Poster { get; set; }
        public int Width { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
    }

    
}