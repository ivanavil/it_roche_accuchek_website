﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Models
{
    public class GenericSearcherContentModel : BaseSearcher
    {
        public int ThemeId { get; set; }
        public int ContextId { get; set; }
        public string SearchString { get; set; }
        public string KeysToExclude { get; set; }
        public string codeVideo { get; set; }
        public int idCurrVideo { get; set; }
        public Healthware.HP3.Core.Content.ObjectValues.ContentGenericComparer.SortType SortType { get; set; }
        public Healthware.HP3.Core.Content.ObjectValues.ContentGenericComparer.SortOrder SortOrder { get; set; }
    }

    public class GenericSearcherEngineContentModel : BaseSearcher
    {
        public string KeysContexts { get; set; }
        public string SearchString { get; set; }
        public string KeysToExclude { get; set; }
        public Healthware.HP3.Core.Content.ObjectValues.ContentGenericComparer.SortType SortType { get; set; }
        public Healthware.HP3.Core.Content.ObjectValues.ContentGenericComparer.SortOrder SortOrder { get; set; }

    }
}
