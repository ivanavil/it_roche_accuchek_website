﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Inclub.Web.Portal.Models
{
    public class DictionaryModel 
    {
        public string Testo { get; set; }
        public string KeyDictionary { get; set; }    
        public int LanguageID { get; set; }

    }

    public class DictionaryCollection : List<DictionaryModel>
    {

    }


}