﻿using System;
using System.Web;
namespace Inclub.Web.Portal.Models
{
    public class OperationResult
    {

        public OperationResult()
        {
            this.Successful = false;
            this.Message = string.Empty;
            this.StatusCode = 0;
            this.Result = null;
        }


        #region Properties

        public Boolean Successful { get; set; }

        public string Message { get; set; }

        public int StatusCode { get; set; }

        public object Result { get; set; }

        #endregion Properties
    }
}