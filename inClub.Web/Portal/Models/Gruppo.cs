﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace inClub.Web.Portal.Models
{
    [Serializable()]
    public class Gruppo
    {
        public ContentIdentificator GroupKey { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public UserIdentificator Admin { get; set; }
        public String AdminName { get; set; }
        public String Role { get; set; }
        public int NumIscritti { get; set; }
        public int NumDiscussioni { get; set; }
        public int NumContributi { get; set; }
        public int NumCommenti { get; set; }
        public int GrpAperto { get; set; }
        public bool isBoard { get; set; }
        public String BlockColor { get; set; }
        public String Cover { get; set; } 
        public String CreationDate { get; set; }
        public String LastUpdateDate { get; set; }
        public List<Discussioni> LastDiscussionList { get; set; }
    }
}