﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC.Attributes;
using Inclub.Web.Portal.Keys;



namespace Inclub.Web.Portal.Models
{
    public class UserLogIn
    {
        [Required(ErrorMessage = DictionaryKeys.REQUIRED_FIELD)]
        [Email(ErrorMessage = DictionaryKeys.VALIDEMAIL_FIELD)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        public string Email { get; set; }


        [Required(ErrorMessage = DictionaryKeys.REQUIRED_FIELD)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        public string Password { get; set; }

        public string returnUrl { get; set; }
        
        public bool webinar { get; set; }
    }

    public class UserRecoveryPassword
    {
        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //[Email(ErrorMessage = DictionaryKeys.Shared_InvalidEmailError)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        //[Label(DictionaryKeys.RegForm_Email)]
        //public string EmailRecovery { get; set; }

        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Captcha { get; set; }

    }

    public class UserLogged : Healthware.HP3.Core.User.ObjectValues.UserBaseValue
    {
        public string Email { get; set; }
        public bool userPasswordForceChange { get; set; }
        public int typeUserByRoleId { get; set; }
        public string siteThrougt { get; set; }
        //public bool isMMG { get; set; }
        public int specID { get; set; }

    }

    //public class UserChangePassword : UserForgotPassword
    //{
    //    //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
    //    public string OldPassword { get; set; }


    //}

    public class UserForgotPassword
    {

        public int Id { get; set; }

        //[System.ComponentModel.DataAnnotations.RegularExpression(@"^.{6,}$", ErrorMessage = "Inserire almeno 6 caratteri")]
        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        //[Label(DictionaryKeys.RegForm_Password)]
        public string Password { get; set; }

        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //[DataAnnotationsExtensions.EqualTo("Password")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "Le password non coincidono")]
        public string ConfirmPassword { get; set; }
    }

    public class UserBase
    {
        public string Title { get; set; }

       [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.FirstNameMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_FirstName)]
        public string FirstName { get; set; }

         [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.LastNameMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_LastName)]
        public string LastName { get; set; }


        [Required(ErrorMessage = "Campo obbligatorio")]
        [Email(ErrorMessage = "Obbligatorio")]
        [StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_Email)]
        public string Email { get; set; }

        public int Id { get; set; }
    }

    public class UserChangePassword
    {
        [System.ComponentModel.DataAnnotations.RegularExpression(@"^.{6,}$", ErrorMessage = "Inserire almeno 6 caratteri")]
        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [DataAnnotationsExtensions.EqualTo("Password")]
        [Compare("Password", ErrorMessage = "La password non coincide")]
        [Label("Conferma Password")]
        public string ConfirmPassword { get; set; }



        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string OldPassword { get; set; }
        public int Id { get; set; }
    }

    public class UserAvatar
    {
        public bool Success { get; set; }
        public string Avatar { get; set; }
        public int Id { get; set; }
    }


    public class UserResetPasswordForm
    {
        [System.ComponentModel.DataAnnotations.RegularExpression(@"^.{6,}$", ErrorMessage = "Inserire almeno 6 caratteri")]
        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [DataAnnotationsExtensions.EqualTo("Password")]
        [Compare("Password", ErrorMessage = "La password non coincide")]
        [Label("Conferma Password")]
        public string ConfirmPassword { get; set; }
        public int Id { get; set; }

    }


    public class UserAccessCode
    {
        public int Id { get; set; }
        public int MaritalStatus { get; set; }       
         public string Email { get; set; }       
         public string Surname { get; set; }       
         public string Name { get; set; }   
       public string Username { get; set; }   
         public string Password { get; set; }





        [Required(ErrorMessage = "Campo obbligatorio")]  
        public string AccessCode { get; set; }       

    }


    public class UserResetPassword
    {
        [Required(ErrorMessage = "Campo obbligatorio")]
        [Email(ErrorMessage = "Campo obbligatorio")]
        [StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_Email)]
        public string Email { get; set; }

        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Captcha { get; set; }
    }

    public class UserRegistration : UserEdit
    {


        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.MaxChar_50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.MaxChar_50)]
        public string Surname { get; set; }



        [DataAnnotationsExtensions.EqualTo("Password")]
        [StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        [Compare("Password", ErrorMessage = "Le password non coincidono")]
        [Label(DictionaryKeys.RegForm_ConfPassw)]
        public string ConfirmPassword { get; set; }


        [System.ComponentModel.DataAnnotations.RegularExpression(@"^.{6,}$", ErrorMessage = "Inserire almeno 6 caratteri")]
        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(200, ErrorMessage = DictionaryKeys.PasswordMaxLengthErrorMessage)]
        [Label(DictionaryKeys.RegForm_Password)]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.RegularExpression(@"^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$", ErrorMessage = "Codice Fiscale non valido")]
        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(200, ErrorMessage = "Codice Fiscale non valido")]      
        public string CodiceFiscale { get; set; }



        public string SkypeLink { get; set; }
        public string TwitterLink { get; set; }
        public string LinkedinLink { get; set; }

        public bool AcceptNotify { get; set; }
        public string AccessCode { get; set; }
        public string City { get; set; }
        public string Structure { get; set; }


        [Required(ErrorMessage = "Campo obbligatorio")]
        public string Captcha { get; set; }

         [System.ComponentModel.DataAnnotations.Range(typeof(bool), "true", "true", ErrorMessage = "Campo obbligatorio")]	      
        public bool TermsAndConditions { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(2, ErrorMessage = DictionaryKeys.FormRegAccept)]
         public string TermsMedical { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(2, ErrorMessage = DictionaryKeys.FormRegAccept)]
        public string TermsMedical1 { get; set; }

       [Required(ErrorMessage = "Campo obbligatorio")]
        public string TermsMedical2 { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
       public string TermsMedical3 { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        public string TermsMedical4 { get; set; }


        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(2, ErrorMessage = DictionaryKeys.FormRegAccept)]
        public string TermsResponsabilita { get; set; }

        public int MaritalStatus { get; set; }
        public int ProjectID { get; set; }
        public DateTime RegistrationDate { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.MaxChar_50)]
        public string NumeroIscrizioneAlbo { get; set; }

        [Required(ErrorMessage = "Campo obbligatorio")]
        [StringLength(50, ErrorMessage = DictionaryKeys.MaxChar_50)]
        public string ProvinciaAlbo { get; set; }


       

    }

    public class UserEdit : UserBase
    {
    
        [DataAnnotationsExtensions.EqualTo("Email")]
        [Compare("Email", ErrorMessage = "Le email non coincidono")]
        [StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        [Label("Conferma Email")]
        public string ConfirmEmail { get; set; }

        [Label(DictionaryKeys.RegForm_Telephone)]
        public string Telephone { get; set; }

        [Label(DictionaryKeys.RegForm_CellPhone)]
        public string CellPhone { get; set; }

       

        //medical specialty
       [Required(ErrorMessage = "Campo obbligatorio")]
       public int SelectedSpecId { get; set; }
       public SelectList Specializzazioni { get; set; }

        [Compare("AcceptInfoCompared", ErrorMessage = "Campo obbligatorio")]
        [Label(DictionaryKeys.RegForm_Privacy)]
        public bool AcceptInfo { get; set; }


    }

    public class inviaAmico : Healthware.HP3.Core.User.ObjectValues.UserBaseValue
    {

        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //[Email(ErrorMessage = DictionaryKeys.Shared_InvalidEmailError)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        //public string Email { get; set; }

        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //public string NomeAmico { get; set; }


        //public string CognomeAmico { get; set; }

        //[Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        //[Email(ErrorMessage = DictionaryKeys.Shared_InvalidEmailError)]
        //[StringLength(200, ErrorMessage = DictionaryKeys.EmailMaxLengthErrorMessage)]
        //public string EmailAmico { get; set; }

        //public string PageToShare { get; set; }
        //public int IdThema { get; set; }
        //public int IdContent { get; set; }


    }



    public class UserGeoLocalization
    {
        #region Properties

        public string IpAddress { get; set; }

        public string ContinentCode { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public string Latitude { get; set; }
        public string ZipCode { get; set; }

        public string Longitude { get; set; }
        #endregion Properties
    } 
}