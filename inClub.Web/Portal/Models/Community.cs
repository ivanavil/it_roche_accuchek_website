﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC.Attributes;
using Inclub.Web.Portal.Keys;


namespace Inclub.Web.Portal.Models
{


    public class ProponiGruppo
    {

        [Required(ErrorMessage = "Compilare il Titolo")]
        public string Title { get; set; }

        public string Description { get; set; }
        public bool Success { get; set; }

    
    }

}