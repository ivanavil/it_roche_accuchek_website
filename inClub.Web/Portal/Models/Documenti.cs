﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using System.Web.Mvc;

namespace inClub.Web.Portal.Models
{
    public class Documenti
    {
        public ContentIdentificator ResourceKey { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Ext { get; set; }
        public String Type { get; set; }
        public String TypeDoc { get; set; }
        public String Folder { get; set; }
        public int FolderID { get; set; }
        public int CommentID { get; set; }
        public UserIdentificator OwnerID { get; set; }
        public String OwnerName { get; set; }
        public String SizeKb { get; set; }
        public String CodeGetFile { get; set; }
        public ContentIdentificator GroupKey { get; set; }
        public int NumDownload { get; set; }
        public String CreationDate { get; set; }
        public string linkFile { get; set; }
    }

    public class AddDocumento
    {
        public int ID { get; set; }
        public string TitleNewDiscussion { get; set; }
        public string DescriptionNewDiscussion { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public short langID { get; set; }
        public int groupID { get; set; }
        public int groupIsOpen { get; set; }         
        public string role { get; set; }
        public string tempFile { get; set; }
        public bool Success { get; set; }
        public HttpPostedFileBase FileUpl { get; set; }
        public string SelectedDiscId { get; set; }
        public SelectList Discussion { get; set; }
        public int SelectedTypeDoc { get; set; }
        public SelectList TypeDoc { get; set; }        
    }

    public class ModificaDocumento
    {
        public int ID { get; set; }
        public string TitleNewDiscussion { get; set; }
        public string DescriptionNewDiscussion { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public short langID { get; set; }
        public int groupID { get; set; }
        public string folderID { get; set; }        
        public bool Success { get; set; }        
        public string SelectedDiscId { get; set; }
        public SelectList Discussion { get; set; }       
    }

    public class ShowVideo
    {
        public string codeFile { get; set; }
        public String fileName { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Ext { get; set; }
    }

}