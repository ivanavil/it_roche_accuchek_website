﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace inClub.Web.Portal.Models
{
   
    public class News
    {
        public ContentIdentificator NewsKey { get; set; }
        [AllowHtml]
        public String Title { get; set; }
        [AllowHtml]
        public String Description { get; set; }
        [AllowHtml]
        public String FullText { get; set; }
        public String CoverArchive { get; set; }
        public int Importance { get; set; }
        public String PublishDate { get; set; }
    
     
    }

    public class NewsCollection : List<News>
    {

    }
}