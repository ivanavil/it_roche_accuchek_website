﻿using System;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;

namespace Inclub.Web.Portal.Models
{
    public class ThemeModel
    {
        public interface ITheme
        {
            ThemeIdentificator Key { get; set; }
            string Description { get; set; }
            string IdCrypt { get; set; }
            Boolean ShowContent { get; set; }
            ThemeCollection ChildColl { get; set; }
            int ContentId { get; set; }
        }

        public class ThemeBaseModel : ITheme
        {
            public ThemeIdentificator Key { get; set; }
            public ThemeIdentificator KeyFather { get; set; }
            public string Description { get; set; }
            public string IdCrypt { get; set; }
            public Boolean ShowContent { get; set; }
            public ThemeCollection ChildColl { get; set; }
            public int ContentId { get; set; }
            public SiteAreaIdentificator SiteAreaKey { get; set; }
            public ContentTypeIdentificator ContenteTypeKey { get; set; }
            public LanguageIdentificator LanguageKey { get; set; }
        }

        public class ThemeContentModel : ThemeBaseModel
        {
            public GenericContentBaseModel GenericContentBaseModel { get; set; }
        }

    }
}
