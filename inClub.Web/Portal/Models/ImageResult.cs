﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Inclub.Web.Portal.Models
{
    public class ImageResult : System.Web.Mvc.FileStreamResult
    {
        public ImageResult(Image input, ImageFormat ctype) : this(input, input.Width, input.Height, ctype) { }
        public ImageResult(Image input, int width, int height, ImageFormat ctype) :
            base(
              GetMemoryStream(input, width, height, ctype),
             ctype.ToString())
        { }

        static MemoryStream GetMemoryStream(Image input, int width, int height, ImageFormat fmt)
        {
            // maintain aspect ratio
            if (input.Width > input.Height)
                height = input.Height * width / input.Width;
            else
                width = input.Width * height / input.Height;

            var bmp = new Bitmap(input, width, height);
            var ms = new MemoryStream();
            bmp.Save(ms, fmt);
            ms.Position = 0;
            return ms;

        }
    }
}