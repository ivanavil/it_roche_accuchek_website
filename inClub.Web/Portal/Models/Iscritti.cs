﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace inClub.Web.Portal.Models
{
    public class Iscritti
    {
        public UserIdentificator UserID { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Specialty { get; set; }
        public String Hospital { get; set; }
        public String Department { get; set; }
        public String City { get; set; }
        public String Photo { get; set; }
        public String Email { get; set; }
        public String NumContributi { get; set; }
        public String NumCommenti { get; set; }
        public String Role { get; set; }
        public String SkypeLink { get; set; }
        public String TwitterLink { get; set; }
        public String LinkedinLink { get; set; }
    }
}