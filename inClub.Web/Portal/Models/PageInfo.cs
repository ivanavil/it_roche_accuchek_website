﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace Inclub.Web.Portal.Models
{
    public class PageInfo
    {
        public PageInfo() { }

        public PageInfo(ThemeValue theme)
        {
            this.Key = theme.Key;
            this.Title = string.Empty;
            this.AreaTitle = theme.Description;
            this.Launch = new HtmlString(theme.Launch);
            this.MetaKeywords = theme.MetaKeywords;
            this.MetaDescription = theme.MetaDescription;
            this.SeoName = theme.SeoName;
            this.DomainSiteLabel = Helper.SiteAreaHelper.DomainSiteLabel();
            this.KeyFather = theme.KeyFather;
            this.KeyContent = theme.KeyContent;
            this.Roles = theme.Roles;
        }

        #region Methods

        public void Populate(MetaData meta)
        {
            if (null == meta) throw new ArgumentNullException("meta");
            if (!string.IsNullOrWhiteSpace(meta.Title))
                this.Title = meta.Title;
            if (!string.IsNullOrWhiteSpace(meta.MetaDescription))
                this.MetaDescription = meta.MetaDescription;
            if (!string.IsNullOrWhiteSpace(meta.MetaKeywords))
                this.MetaKeywords = meta.MetaKeywords;
        }

        public bool HasRole(RoleIdentificator roleKey)
        {
            if (null != this.Roles && this.Roles.Any())
            {
                return this.Roles.HasRole(roleKey.Id);
            }

            return false;
        }

        #endregion Methods

        #region Properties

        public ThemeIdentificator Key { get; set; }

        public ContentIdentificator KeyContent { get; set; }

        public ThemeIdentificator KeyFather { get; set; }

        public string AreaTitle { get; set; }

        public string Title { get; set; }

        public HtmlString Launch { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }

        public string SeoName { get; set; }

        public RoleCollection Roles { get; set; }
        //public bool NeedLogin { get; set; }

        public string DomainSiteLabel { get; set; }

        #endregion Properties
    }
}
