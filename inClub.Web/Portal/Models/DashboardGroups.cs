﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Healthware.HP3.Core.Base.ObjectValues;
using Healthware.HP3.Core.Content;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.User;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.Core.Utility;
using Healthware.HP3.Core.Web;
using System.Linq;

namespace inClub.Web.Portal.Models
{
    public class DashboardGroupMember
    {
        private int _adminId = 0;
        private string _adminEncryptedId = string.Empty;
        private string _adminFullName = string.Empty;
        private string _adminEmail = string.Empty;

        private bool _adminRecNotif = true;
        public int AdminId
        {
            get { return _adminId; }

            set { _adminId = value; }
        }

        public string AdminEncryptedId
        {
            get { return _adminEncryptedId; }

            set { _adminEncryptedId = value; }
        }

        public string AdminFullName
        {
            get { return _adminFullName; }

            set { _adminFullName = value; }
        }

        public string AdminEmail
        {
            get { return _adminEmail; }

            set { _adminEmail = value; }
        }

        public bool AdminRecNotif
        {
            get { return _adminRecNotif; }

            set { _adminRecNotif = value; }
        }

    }

    public class DashboardGroupMemberCollection : List<DashboardGroupMember>
    {
    }

    public class DashboardGroupMembers
    {
        private DashboardGroupMemberCollection _groupMemberColl = null;

        private string _groupMemberString = string.Empty;
        public DashboardGroupMemberCollection GroupMemberColl
        {
            get { return _groupMemberColl; }

            set { _groupMemberColl = value; }
        }

        public string GroupMemberString
        {
            get { return _groupMemberString; }

            set { _groupMemberString = value; }
        }
    }

    public class DashboardGroup
    {
        private int _groupId = 0;
        private string _groupEncryptedId = string.Empty;
        private int _groupActive = 0;
        private string _groupName = string.Empty;
        private string _groupDescription = string.Empty;
        private string _groupOwner = string.Empty;
        private int _groupOwnerId = 0;
        private string _groupAdministrator = string.Empty;
        private string _groupCreationDate = string.Empty;
        private string _groupUpdateDate = string.Empty;
        private string _groupUpdateReason = string.Empty;
        private string _groupLink = string.Empty;
        private string _GroupCoverSquare = string.Empty;
        private string _GroupCoverHead = string.Empty;

        private DashboardGroupMembers _groupMembers = null;
        public int GroupId
        {
            get { return _groupId; }

            set { _groupId = value; }
        }

        public string GroupEncryptedId
        {
            get { return _groupEncryptedId; }

            set { _groupEncryptedId = value; }
        }

        public int GroupActive
        {
            get { return _groupActive; }

            set { _groupActive = value; }
        }

        public string GroupName
        {
            get { return _groupName; }

            set { _groupName = value; }
        }

        public string GroupDescription
        {
            get { return _groupDescription; }

            set { _groupDescription = value; }
        }

        public string GroupOwner
        {
            get { return _groupOwner; }

            set { _groupOwner = value; }
        }

        public int GroupOwnerId
        {
            get { return _groupOwnerId; }

            set { _groupOwnerId = value; }
        }

        public string GroupAdministrator
        {
            get { return _groupAdministrator; }

            set { _groupAdministrator = value; }
        }

        public string GroupCreationDate
        {
            get { return _groupCreationDate; }

            set { _groupCreationDate = value; }
        }

        public string GroupUpdateDate
        {
            get { return _groupUpdateDate; }

            set { _groupUpdateDate = value; }
        }

        public string GroupUpdateReason
        {
            get { return _groupUpdateReason; }

            set { _groupUpdateReason = value; }
        }

        public DashboardGroupMembers GroupMembers
        {
            get { return _groupMembers; }

            set { _groupMembers = value; }
        }

        public string GroupLink
        {
            get { return _groupLink; }

            set { _groupLink = value; }
        }

        public string GroupCoverSquare
        {
            get { return _GroupCoverSquare; }

            set { _GroupCoverSquare = value; }
        }

        public string GroupCoverHead
        {
            get { return _GroupCoverHead; }

            set { _GroupCoverHead = value; }
        }
    }



    public class DashboardGroupManager
    {
        public DashboardGroupManager()
        {
        }

        public DashboardGroupMembers getGroupUserCollection(int intGroupId, short intLangId, int intUserRelationType = 0, string strSearchString = "")
        {
            UserSearcher oUserSearcher = new UserSearcher();

            oUserSearcher.InclusiveOf.LoadContexts = true;
            oUserSearcher.Status = 1;
            oUserSearcher.SiteStatus = 1; 

            oUserSearcher.RelatedTo.Content.Key = new ContentIdentificator(intGroupId, intLangId);
            if (!string.IsNullOrWhiteSpace(strSearchString))
                oUserSearcher.SearchString = strSearchString;
            if (intUserRelationType > 0)
                oUserSearcher.RelatedTo.Content.KeyRelationType.Id = intUserRelationType;

            oUserSearcher.RelatedTo.Content.RelationSearch = RelationTypeSearch.AllTypes();
            oUserSearcher.RelatedTo.Content.Active = SelectOperation.Enabled();
            oUserSearcher.RelatedTo.Content.Delete = SelectOperation.Enabled();
            oUserSearcher.RelatedTo.Content.LoadRelationDetails = false;


            //oUserSearcher.RelatedTo.Content.Active = SelectOperation.All();
            //oUserSearcher.RelatedTo.Content.Delete = SelectOperation.Enabled();

            //.Properties.Add("Key.Id")
            //.Properties.Add("Name")
            //.Properties.Add("Surname")
            //.Properties.Add("Email")

            DashboardGroupMembers _out = null;
            DashboardGroupMemberCollection _oColl = null;

            UserManager oUserManager = new UserManager();
            oUserManager.Cache = false;
            UserCollection oUserColl = oUserManager.Read(oUserSearcher);
            if ((oUserColl != null) && oUserColl.Any())
            {
                _out = new DashboardGroupMembers();
                _oColl = new DashboardGroupMemberCollection();

                string groupMemberString1 = string.Empty;
                foreach (UserValue obj in oUserColl)
                {
                    DashboardGroupMember _groupAdminVal = new DashboardGroupMember();
                    _groupAdminVal.AdminFullName = obj.Title + " " + obj.Name.ToUpper() + " " + obj.Surname.ToUpper();
                    _groupAdminVal.AdminId = obj.Key.Id;
                    _groupAdminVal.AdminEncryptedId = SimpleHash.EncryptRijndaelAdvanced(obj.Key.Id.ToString());
                    _groupAdminVal.AdminEmail = obj.Email.ToString();
                    _groupAdminVal.AdminRecNotif = ((obj.CanReceiveNewsletter != null) ? obj.CanReceiveNewsletter.Value : true);
                    _oColl.Add(_groupAdminVal);

                    groupMemberString1 += (string.IsNullOrEmpty(obj.Title) ? string.Empty : obj.Title + " ") + obj.Name + " " + obj.Surname + ", ";
                }

                if (groupMemberString1.Trim().EndsWith(","))
                    groupMemberString1 = groupMemberString1.Trim().Substring(0, groupMemberString1.LastIndexOf(","));
                _out.GroupMemberColl = _oColl;
                _out.GroupMemberString = groupMemberString1;
            }

            return _out;
        }


        //Aggiunta del Group Admin dato l'id del GPAdm
        public bool AddGroupAdmin(int groupId, short intLangId, int intUserid)
        {
            if (intUserid == 0 || groupId == 0 || intLangId == 0)
            {
                return false;
            }

            try
            {
                ProfilingManager oProfilingManager = new ProfilingManager();
                bool _remove = removeuserFromGroup(groupId, intLangId, intUserid, Inclub.Web.Portal.Keys.RoleKeys.Member.Id);
                bool _removeRole = oProfilingManager.RemoveUserRole(new UserIdentificator(intUserid), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.Member.Id));

                bool _blAdd = addUserToGroup(groupId, intLangId, intUserid, Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id);
                bool _blAddRole = oProfilingManager.CreateUserRole(new UserIdentificator(intUserid), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id));

                if (_remove & _blAdd & _removeRole & _removeRole)
                {
                    return true;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        //Rimozione del Group Admin dato l'id del GPAdm
        public bool RemoveGroupAdmin(int groupId, short intLangId, int intUserid)
        {
            if (intUserid == 0 || groupId == 0 || intLangId == 0)
            {
                return false;
            }

            try
            {
                ProfilingManager oProfilingManager = new ProfilingManager();
                bool _remove = removeuserFromGroup(groupId, intLangId, intUserid, Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id);
                bool _removeRole = oProfilingManager.RemoveUserRole(new UserIdentificator(intUserid), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.GrpAdmin.Id));

                bool _blAdd = addUserToGroup(groupId, intLangId, intUserid, Inclub.Web.Portal.Keys.RoleKeys.Member.Id);
                bool _blAddRole = oProfilingManager.CreateUserRole(new UserIdentificator(intUserid), new RoleIdentificator(Inclub.Web.Portal.Keys.RoleKeys.Member.Id));

                if (_remove & _blAdd & _removeRole & _removeRole)
                {
                    return true;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }


        //Private function to add user to an existing group. Type of user is specified as input param
        private bool addUserToGroup(int groupId, short intLangId, int intUserid, int intRelationtypeId)
        {
            bool _out = false;

            ContentUserValue vo = new ContentUserValue();
            var _with2 = vo;
            _with2.KeyRelationType.Id = intRelationtypeId;
            _with2.KeyUser.Id = intUserid;
            _with2.KeyContent.Id = groupId;
            _with2.KeyContent.IdLanguage = intLangId;
            _with2.Order = 1;

            ContentManager o = new ContentManager();
            ContentUserValue voOut = default(ContentUserValue);
            voOut = o.CreateContentUser(vo);
            if ((voOut != null) && voOut.Key.Id > 0)
                return true;

            return _out;
        }

        //Private function to remove a user from an existing group Type of user is specified as input param
        private bool removeuserFromGroup(int groupId, short intLangId, int intUserid, int intRelationtypeId)
        {
            ContentUserSearcher so = new ContentUserSearcher();
            var _with3 = so;
            _with3.KeyRelationType.Id = intRelationtypeId;
            _with3.KeyUser.Id = intUserid;
            _with3.KeyContent.Id = groupId;
            _with3.KeyContent.IdLanguage = intLangId;

            ContentManager o = new ContentManager();
            return o.RemoveContentUser(so);
        }



    }
}
