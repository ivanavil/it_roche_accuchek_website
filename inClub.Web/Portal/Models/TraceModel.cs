﻿
namespace Inclub.Web.Portal.Models
{
    public class TraceModel
    {
        public int ThemeId { get; set; }
        public int TraceId { get; set; }
        public string TraceDescription { get; set; }
        public int TraceLangId { get; set; }
    }
}
