﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Models
{
    public interface MetaData
    {
        string Title { get; set; }

        string MetaKeywords { get; set; }

        string MetaDescription { get; set; }
    }
}
