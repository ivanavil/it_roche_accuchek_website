﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inClub.Web.Portal.Models
{
    public class Archive
    {

        public IEnumerable Items
        {
            get;
            set;
        }
        public long TotalPagesCount
        {
            get;
            set;
        }
        public long TotalItemsCount
        {
            get;
            set;
        }
        public long CurrentPage
        {
            get;
            set;
        }
        public string NoItemsLabelKey
        {
            get;
            set;
        }

        public int CurrentThemeForLink
        {
            get;
            set;
        }

        public int CurrentTherapeuticAreaId
        {
            get;
            set;
        }

    }

    public class Archive<TModel> : Archive
    {
        public new IEnumerable<TModel> Items
        {
            get
            {
                return base.Items as IEnumerable<TModel>;
            }
            set
            {
                base.Items = value;
            }
        }
    }
}