﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;
using Healthware.HP3.MVC.Attributes;

namespace inClub.Web.Portal.Models
{
    public class Contributo
    {
        public CommentIdentificator PostKey { get; set; }
        public ContentIdentificator DiscussionKey { get; set; }
        public String DiscussionTitle { get; set; }
        public ContentIdentificator GroupKey { get; set; }
        public String CreationDate { get; set; }
        public UserIdentificator UserCreate { get; set; }
        public String UserCreateName { get; set; }
        public String UserCover { get; set; }
        public String Description { get; set; }
        public int NumCommenti { get; set; }        
        public List<ContentBaseValue> ResourceList { get; set; }
        public int FatherID { get; set; }  
    }

    public class AddContributo
    {
        public int ID { get; set; }
        public string Description { get; set; }        
        public short langID { get; set; }
        public int groupID { get; set; }
        public int discussionID { get; set; }
        public int fatherID { get; set; }
        public int discFolderKey { get; set; }
        public string role { get; set; }
        public string type { get; set; }  
        public bool Success { get; set; }
        public HttpPostedFileBase FileUpl { get; set; }        
    }
}