﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace inClub.Web.Portal.Models
{
    public class Discussioni
    {
        public ContentIdentificator DiscussionKey { get; set; }
        public ContentIdentificator GroupKey { get; set; }
        public String FolderKey { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public int NumContributi { get; set; }
        public int NumCommenti { get; set; }
        public String CreationDate { get; set; }
        public String LastUpdateDate { get; set; }
        public UserIdentificator UserCreate { get; set; }
        public String UserCreateName { get; set; }
        public String Role { get; set; }
    }

    public class AddDiscussione
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public short langID { get; set; }
        public int groupID { get; set; }        
        public bool Success { get; set; }        
    }

}