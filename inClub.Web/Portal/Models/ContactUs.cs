﻿using Healthware.HP3.MVC.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inclub.Web.Portal.Keys;

namespace Inclub.Web.Portal.Models
{
    public class ContactBaseModel
    {
        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Subject { get; set; }

        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Surname { get; set; }


        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        [Email(ErrorMessage = DictionaryKeys.Shared_InvalidEmailError)]
        public string Email { get; set; }

        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string Captcha { get; set; }

    }


    public class ContactUs : ContactBaseModel
    {
        [Required(ErrorMessage = DictionaryKeys.Shared_ErrorFieldRequired)]
        public string MessageBody { get; set; }


    }



}