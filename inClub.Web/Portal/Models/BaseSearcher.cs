﻿using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;

namespace Inclub.Web.Portal.Models
{
    public class BaseSearcher
    {
        public BaseSearcher()
        {
            Key = new ContentIdentificator();
            LanguageKey = new LanguageIdentificator();
        }

        public ContentIdentificator Key { get; set; }
        public LanguageIdentificator LanguageKey { get; set; }
        public int? PageNumber { get; set; }
        public int PageSize { get; set; }
        public int LanguageId { get; set; }
        public int Id { get; set; }
        public string IdCrypt { get; set; }
        public int SiteAreaId { get; set; }
        public int ContenTypeId { get; set; }
        public int ThemeId { get; set; }
        public string ThemeIdCrypt { get; set; }
        public int BitMask { get; set; }
        public int Year { get; set; }
        public bool CalculatePagerTotalCount { get; set; }
        public string KeysContext { get; set; }
    }
}
