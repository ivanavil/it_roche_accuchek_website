﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inclub.Web.Portal.Models
{
    public class MaterialModel : GenericContentBaseModel, MetaData
    {
        public string MetaKeywords
        { get; set; }

        public string MetaDescription
        { get; set; }
    }
}