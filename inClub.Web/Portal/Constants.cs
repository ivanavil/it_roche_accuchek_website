﻿
namespace Inclub.Web.Portal
{

    public static class Constants
    {
        /// <summary>
        /// "HP3"
        /// </summary>
        public const string Hp3KeyDomain = "HP3";

        /// <summary>
        /// "DP"
        /// </summary>
        public const string DefaultKeyDomain = "RIC";

        /// <summary>
        /// "Dompe"
        /// </summary>
        public const string SiteFolder = "Portal";
    }

   
}
