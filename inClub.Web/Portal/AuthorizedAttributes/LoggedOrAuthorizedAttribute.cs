﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Models;
using Inclub.Web.Portal.Keys;
using System.Web.Routing;

namespace Inclub.Web.Portal.AuthorizedAttributes
{
    public class LoggedOrAuthorizedAttribute : AuthorizeAttribute
    {
        public LoggedOrAuthorizedAttribute()
        {
            View = "error404";
            Master = String.Empty;

        }

        public string Message { get; set; }
        public String View { get; set; }
        public String Master { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            CheckIfUserIsAuthenticated(filterContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (!Helper.LoginHelper.IsAuthenticated())
            {
                
                if (filterContext.Controller.TempData["LoginError"] != null)
                {
                    this.Message = filterContext.Controller.TempData["LoginError"].ToString();

                    if (filterContext.Controller.TempData["Unauthorized"] != null)
                    {
                        filterContext.Controller.TempData.Remove("Unauthorized");
                        filterContext.Controller.TempData.Add("Unauthorized", this.Message);
                    }
                }
            }

            base.HandleUnauthorizedRequest(filterContext);
        }

        private void CheckIfUserIsAuthenticated(AuthorizationContext filterContext)
        {
            // If Result is null
            if (filterContext.Result == null)
                return;

            // If user is authenticated
            if (Helper.LoginHelper.IsAuthenticated())
            {
                if (String.IsNullOrEmpty(View))
                    return;
                var result = new ViewResult { ViewName = View, MasterName = Master };
                filterContext.Result = result;
            }
            else
            {
               
                if (String.IsNullOrEmpty(View))
                    return;
                var result = new ViewResult { ViewName = "_LogIn" };
                result.TempData.Add("Unauthorized", this.Message);

                filterContext.Result = result;

            }
        }
    }
}