﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;


namespace Inclub.Web.Portal.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ContextValue> hcpTypes)
        {
            return
                hcpTypes.OrderBy(type => type.Order)
                      .Select(type =>
                          new SelectListItem
                          {
                              Text = type.Description,
                              Value = type.Key.Id.ToString()
                          });
        }
    }
}
