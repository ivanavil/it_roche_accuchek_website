﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Utility;

namespace Inclub.Web.Portal.Extensions
{
    public static class StringExtensions
    {
        public static string FormatLinkTitle(this string title)
        {
            return StringUtility.StringUrlNormalize(title);
        }

        public static string FileExtensionForContentType(this string fileName)
        {
            var pieces = fileName.Split('.');
            var extension = pieces.Length > 1 ? pieces[pieces.Length - 1]
                : string.Empty;
            return (extension.ToLower() == "jpg") ? "jpeg" : extension;
        }
    }
}
