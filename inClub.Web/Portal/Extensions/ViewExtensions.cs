﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Inclub.Web.Portal.Models;

namespace Inclub.Web.Portal.Extensions
{
    public static class ViewExtensions
    {
        public static PageInfo GetPageInfo(this WebViewPage view)
        {
            var pageInfo = view.ViewBag.PageInfo as PageInfo ?? new PageInfo();
            return pageInfo;
        }


        public static Models.UserLogged GetUserBaseValueLogged(this WebViewPage view)
        {
            var userLogged = view.TempData["UserLogged"] as UserLogged;
            if (null == userLogged)
                view.TempData["UserLogged"] = userLogged = Helper.UserHelper.readUserLogged();

            return userLogged;
        }
    }
}
