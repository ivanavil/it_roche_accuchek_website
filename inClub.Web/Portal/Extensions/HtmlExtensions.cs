﻿using System;
using System.Web;


namespace Inclub.Web.Portal.Extensions
{
    public static class HtmlExtensions
    {
        public static HtmlString FormatDateEventShort(this System.Web.Mvc.HtmlHelper htmlHelper, DateTime dateStart, DateTime dateEnd)
        {
            var dateText = Helper.DateHelper.FormatDateEventShort(dateStart, dateEnd);
            return new HtmlString(dateText);
        }
    }
}

