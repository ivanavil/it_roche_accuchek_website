﻿using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Localization.ObjectValues;
using Inclub.Web.Portal.Helper;

namespace Inclub.Web.Portal.Extensions
{
    public static class UrlHelperExtensions
    {
        #region ThemeLink

        public static string ThemeLink(this UrlHelper urlHelper, ThemeIdentificator key)
        {
            var theme = BL.Shared.ReadTheme(urlHelper.RequestContext, key);
            if (null == theme) return string.Empty;

            string x = urlHelper.RouteUrl(RouteConfigurations.Default.RouteName, new { controller = theme.SeoName.FormatLinkTitle(), action = string.Empty });
            return x;
        }

        public static string ThemeLink(this UrlHelper urlHelper, ThemeIdentificator key, LanguageIdentificator KeyLang)
        {
            if (null == KeyLang) return string.Empty;

            var theme = BL.Shared.ReadTheme(urlHelper.RequestContext, key, KeyLang.Id);
            if (null == theme) return string.Empty;

            var lang = Helper.LanguageHelper.GetLanguageCode(KeyLang.Id);

            string x = urlHelper.RouteUrl(RouteConfigurations.Default.RouteName, new { controller = theme.SeoName.FormatLinkTitle(), action = string.Empty });
            return x;
        }


        public static string ThemeLinkDetail(this UrlHelper urlHelper, ThemeIdentificator key, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, LanguageIdentificator KeyLang, string title, string action, string param = "", string folder = "")
        {
            var theme = BL.Shared.ReadTheme(urlHelper.RequestContext, key, KeyLang.Id);
            if (null == theme) return string.Empty;

            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetails.RouteName,
                 new
                 {
                     controller = theme.SeoName.FormatLinkTitle(),
                     action = action,
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id,
                     folderName = folder,
                     parameter = param
                 });

            return x;
        }


        public static string ThemeLinkIndex(this UrlHelper urlHelper, ThemeIdentificator key, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, string title)
        {
            var theme = BL.Shared.ReadTheme(urlHelper.RequestContext, key);
            if (null == theme) return string.Empty;

            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetailsImplicit.RouteName,
                 new
                 {
                     controller = theme.SeoName.FormatLinkTitle(),
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id
                 });

            return x;
        }

        public static string ThemeLinkIndex(this UrlHelper urlHelper, ThemeIdentificator key, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, LanguageIdentificator KeyLang, string title)
        {
            var theme = BL.Shared.ReadTheme(urlHelper.RequestContext, key, KeyLang.Id);
            if (null == theme) return string.Empty;

            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetailsImplicit.RouteName,
                 new
                 {
                     controller = theme.SeoName.FormatLinkTitle(),
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id
                 });

            return x;
        }

        public static string ThemeLinkIndex(this UrlHelper urlHelper, ThemeValue themeValue, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, string title)
        {

            if (null == themeValue) return string.Empty;

            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetailsImplicit.RouteName,
                 new
                 {
                     controller = themeValue.SeoName.FormatLinkTitle(),
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id
                 });

            return x;
        }

        public static string ThemeLinkIndex(this UrlHelper urlHelper, string SeoName, Healthware.HP3.Core.Content.ObjectValues.ContentIdentificator contentKey, string title)
        {
            if (string.IsNullOrEmpty(SeoName))
                return string.Empty;


            string x = urlHelper.RouteUrl(RouteConfigurations.ContentDetailsImplicit.RouteName,
                 new
                 {
                     controller = SeoName.FormatLinkTitle(),
                     title = title.FormatLinkTitle(),
                     id = contentKey.Id
                 });

            return x;
        }

        public static int UrlId(this UrlHelper urlHelper)
        {

            int id = 0;

            if (urlHelper.RequestContext.RouteData.Values["id"] != null)
            {
                int.TryParse(urlHelper.RequestContext.RouteData.Values["id"].ToString(), out id);
            }

            return id;

        }


        #endregion ThemeLink
    }
}
