﻿
$(document).ready(function () {
  
   
    $(".jsDictionary").click(function () {



        var _key = $(".jsDictionary").attr("data-dKey");
        var xh = 600;
        var xw = 750;

        var DataWidth = $(".jsDictionary").attr("data-wh");
        var DataHeight = $(".jsDictionary").attr("data-ht");

        if (DataWidth != '') { xw = DataWidth; }
        if (DataHeight != '') { xh = DataWidth; }

        var urlD = '/popupDictionary?key=' + _key + '&EsternalUrl=false';

      
        $(".jsDictionary").colorbox({
            href: urlD,
            iframe: true,
            width: xw,
            scrolling: false,
            height: xh,
            innerWidth: 500, innerHeight: 409

        });


    });


    $(".jsDictionary2").click(function () {



        var _key = $(this).attr("data-dKey");
        var xh = 600;
        var xw = 750;

        var DataWidth = $(this).attr("data-wh");
        var DataHeight = $(this).attr("data-ht");

     
        var urlD = '/popupDictionary?key=' + _key + '&EsternalUrl=false';

    
        $(".jsDictionary2").colorbox({
            href: urlD,
            iframe: true,
            width: DataWidth,
            scrolling: false,
            height: DataHeight,
            innerWidth: 500, innerHeight: 409

        });


    });

   

    $(".jsOpenProfilePanel").click(function () {    
        $(".panelProfile").toggle();
    });

    $(".JResetPassword").click(function (e) {        
        var urlR = $(".JResetPassword").data("href");       
        e.preventDefault();
        OpenColorbox('320', '490', urlR, false);
    });


    $(".JChangePassword").click(function (e) {
        var urlR = $(".JChangePassword").data("href");
        e.preventDefault();
        OpenColorbox('350', '440', urlR, false);
    });


    $(".JChangeAvatar").click(function (e) {
        var urlR = $(".JChangeAvatar").data("href");
        e.preventDefault();
        OpenColorbox('350', '480', urlR, false);
    });

    $(".JOpenColorbox").click(function (e) {
        var urlR = $(".JOpenColorbox").data("href");
        e.preventDefault();
        OpenColorbox('600', '500', urlR, true);
    });

    $(".JOpenVideo").click(function (e) {
        var urlR = $(".JOpenVideo").data("href");
        e.preventDefault();
        OpenColorbox('500', '400', urlR, true);
    });


    $(".addContr").click(function (e) {
        var urlR = $(".addContr").data("href");
        e.preventDefault();
        OpenColorbox('600', '480', urlR, true);
    });

    $(".addDoc").click(function (e) {
        var urlR = $(".addDoc").data("href");
        e.preventDefault();
        OpenColorbox('600', '610', urlR, true);
    });

    $(".JCloseColorbox").click(function (e) {   
        CloseColorbox();
    });



    $(".JSdoLogin").click(function (e) {   
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#Email").focus();
    });

})




function leaveSite(EsternalLink) {
  
    var urlD = '/popupDictionary?key=LeaveToSite&EsternalUrl=' + encodeURIComponent(EsternalLink);
    var xh = 380;
    var xw = 550;

    OpenColorbox(xw, xh, urlD, false);

}

function viewVideo(path, widthPop, heightPop) {

    var urlD = '/popupVideo?pathVideo=' + encodeURIComponent(path);
    var xh = heightPop;
    var xw = widthPop;

    OpenColorbox(650, 470, urlD, false);

}


function OpenColorbox(w,h,u,s)
{

    $(".JOpenVideo").colorbox({
        href: u,
        iframe: true,
        width: w,
        scrolling: s,
        height: h,
        innerWidth: 500, innerHeight: 409
    });
    

    $(".JOpenColorbox").colorbox({
        href: u,
        iframe: true,
        width: w,
        scrolling: s,
        height: h,
        innerWidth: 500, innerHeight: 409
    });

    $(".JChangePassword").colorbox({
        href: u,
        iframe: true,
        width: w,
        scrolling: s,
        height: h,
        innerWidth: 500, innerHeight: 409
    });


    $(".JResetPassword").colorbox({
        href: u,
        iframe: true,
        width: w,
        scrolling: s,
        height: h,
        innerWidth: 500, innerHeight: 409
    });

    $(".JChangeAvatar").colorbox({
        href: u,
        iframe: true,
        width: w,
        scrolling: s,
        height: h,
        innerWidth: 500, innerHeight: 430
    });
}

function CloseColorbox() {
    parent.$.fn.colorbox.close();
}