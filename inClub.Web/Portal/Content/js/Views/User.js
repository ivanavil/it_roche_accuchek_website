﻿function getExtension(filename) {
    var extension = filename.split('.').pop().toLowerCase();
    var Valido = false;
    $(".jsImgError").show();
    if (extension == 'jpg' || extension == 'png' || extension == 'gif' || extension == '') {
        Valido = true;
        $(".jsImgError").hide();
    }

    return Valido;
}


$(function () {

    $('#file').change(function () {
       
        if (getExtension($("#file").val())) {

            var file = document.getElementById('file').files[0];

            // Create a file reader
            var reader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function (e) {

                var tempImg = new Image();
                tempImg.src = reader.result;
                tempImg.onload = function () {

                    //SMALL
                    var MAX_WIDTH = 100;
                    var MAX_HEIGHT = 100;
                    var tempW = tempImg.width;
                    var tempH = tempImg.height;
                    if (tempW > tempH) {
                        if (tempW > MAX_WIDTH) {
                            tempH *= MAX_WIDTH / tempW;
                            tempW = MAX_WIDTH;
                        }
                    } else {
                        if (tempH > MAX_HEIGHT) {
                            tempW *= MAX_HEIGHT / tempH;
                            tempH = MAX_HEIGHT;
                        }
                    }

                    var canvas = document.createElement("canvas");
                    canvas.width = tempW;
                    canvas.height = tempH;
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(this, 0, 0, tempW, tempH);
                    var dataURL = canvas.toDataURL("image/jpeg");
                    $('#image_preview').attr("src", dataURL);

                    //BIG
                    var MAX_WIDTHBIG = 800;
                    var MAX_HEIGHTBIG = 600;
                    var tempWBIG = tempImg.width;
                    var tempHBIG = tempImg.height;
                    if (tempWBIG > tempHBIG) {
                        if (tempWBIG > MAX_WIDTHBIG) {
                            tempHBIG *= MAX_WIDTHBIG / tempWBIG;
                            tempWBIG = MAX_WIDTHBIG;
                        }
                    } else {
                        if (tempHBIG > MAX_HEIGHTBIG) {
                            tempWBIG *= MAX_HEIGHTBIG / tempHBIG;
                            tempHBIG = MAX_HEIGHTBIG;
                        }
                    }

                    var canvasBIG = document.createElement("canvas");
                    canvasBIG.width = tempWBIG;
                    canvasBIG.height = tempHBIG;
                    var ctxBIG = canvasBIG.getContext("2d");
                    ctxBIG.drawImage(this, 0, 0, tempWBIG, tempHBIG);
                    var dataURLBIG = canvasBIG.toDataURL("image/jpeg");

                    $('#userImgCanvas').val(dataURLBIG);

                }
            }
            reader.readAsDataURL(file);
        }
        else {

            $('#userImgCanvas').val('');
            $('#file').val('');
            $('#image_preview').attr("src", '');
        }
    });

});