﻿$(document).ready(function () {
    managePaginator();
    $("#ArchiveLoader").hide();
    $('#moreInfo').click(function (e) {
        ViewMore();
        e.preventDefault();
    });
});

function managePaginator() {
    if (jsPagerTotalNumber == nPage || jsPagerTotalNumber == 0) {
        $('#moreInfo').hide();
        $(window).unbind("scroll");
    }
    else {
        $('#moreInfo').show();
        $(window).scroll(function () {
            ViewMore();
            $(window).unbind("scroll");
        });
    }
};

function ViewMore() {
    ++nPage;
    var soView = prepareSearcher();
    LoadArchive(soView);
};

function LoadArchive(soView) {
    $('#ArchiveLoader').show();
    $('#moreInfo').hide();
    $.post("/News/JsonArchives", soView, Success, 'json');
};

Success = function (data) {
    if (data) {
        $(".jsArchive").append(data.data).fadeIn();
        managePaginator();
        $("#ArchiveLoader").hide();
    }
};