﻿var Portal = Portal || {};

Portal.ContactUs = function (options) {
    var _defaultOptions = {
        actions: {
            archiveUrl: '/contatti/SendContact',
        },
        FormClass: '.JsContactForm',
        ClearClass: '.JsClearFields'
    };
    options = $.extend(true, _defaultOptions, options || {});
    var $form = $(options.FormClass);
    var $btnSubmit = $form.find(".jsSubmit");

    $btnSubmit.click(function (e) {

        var isValid = true;

        $(".jsOggetto").hide();
        $(".jsName").hide();
        $(".jsSurname").hide();
        $(".jsEmail").hide();
        $(".jsEmailFormat").hide();
        $(".jsCaptchaError").hide();
        $(".jsMessaggio").hide();

        if ($("#Subject").val() == '') {
            $(".jsOggetto").show();
            isValid = false;
        }

        if ($("#Name").val() == '') {
            $(".jsName").show();
            isValid = false;
        }

        if ($("#Surname").val() == '') {
            $(".jsSurname").show();
            isValid = false;
        }

        if ($("#Email").val() == '') {
            $(".jsEmail").show();
            isValid = false;
        }
        else {
            var email_reg_exp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/;
            if (!email_reg_exp.test($("#Email").val())) {                
                $(".jsEmailFormat").show();
                isValid = false;
            }

        }

        if ($("#Captcha").val() == '') {
           
            $(".jsCaptchaError").show();
            isValid = false;
        }

        if ($("#MessageBody").val() == '') {
            $(".jsMessaggio").show();
            isValid = false;
        }

        if (isValid) {

            //$btnSubmit.hide();
            $.post(options.actions.url,
             $form.serialize(),
             function (data) {

                 console.log(data);
                 if (data) {



                     if (data.Successful) {

                         $('.jsresultContact').html(data.Message);

                         $form.find(options.ClearClass).val('');
                         $('.jsContainerForm').hide();
                         $("#divLaunch").hide();
                     }
                     else if (!data.Successful && data.StatusCode == 5) //(int)Keys.EnumeratorKeys.StatusCode.CaptchaError
                     {
                         $('.jsresultContactCaptcha').html(data.Message);
                       

                     }



                     else if (!data.Successful && data.StatusCode == 1) //(int)Keys.EnumeratorKeys.StatusCode.GenericError
                     {
                         $('.jsresultContact').html(data.Message);

                     }
                 }

             }, 'json'
            ).always(function () {
                $btnSubmit.show();
            });
        };
        e.preventDefault();
    });

};

