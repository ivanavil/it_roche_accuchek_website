$(document).ready(function () {
   // $(".loginBtn").prettyPhoto();
    
    $(".loginBtn").prettyPhoto();
    $(".goExtSite").prettyPhoto();
    $(".prodpack").prettyPhoto();
    
    $(".goExtSite").click(function () {
        $('#extSite .readMoreBtn a').attr('href', $(this).attr('data-url'));
    })

    $('.carousel').carousel({
        interval: 5000
    })

    var example = $('.menu').superfish({
        //add options here if required
    });


    var owl = $("#owl-blog");

    owl.owlCarousel({
        items: 3,
        itemsDesktop: [1000, 3],
        itemsDesktopSmall: [900, 3],
        itemsTablet: [600, 2],
        itemsMobile: [600, 1]
    });
    $(".welLink").click(function () {
        $(".priboxDat").toggle();
    })
    
    // Custom Navigation Events
    $(".next").click(function () {
        owl.trigger('owl.next');
    })
    $(".prev").click(function () {
        owl.trigger('owl.prev');
    })
    //$('.masonry-grid').masonry({
        //itemSelector: '.masonry-item',
        //percentPosition: true
    //});
    $(".topBlue .search").click(function () {
        if ($('.seaTextbox').hasClass('seaTextboxVisible')) {
            $('.seaTextbox').removeClass('seaTextboxVisible');
        } else {
            $('.seaTextbox').addClass('seaTextboxVisible');
        }
    })

});

$(".boxIntSpa").hover(
  function () {
      $(this).addClass('boxIntSpaHover');
  }, function () {
      $(this).removeClass('boxIntSpaHover');
  }
);

$(".chisiamoValori").hover(
  function () {
      $(this).addClass('hoverState');
  }, function () {
      $(this).removeClass('hoverState');
  }
);

$(".chisiamoMission").hover(
  function () {
      $(this).addClass('hoverState');
  }, function () {
      $(this).removeClass('hoverState');
  }
);

$(".mobMenuIco").click(function () {
    $(".mobMenu").toggle();
})
$(".mobMenuClose").click(function () {
    $(".mobMenu").toggle();
})
$(".videoBoxTxt .watchVideo").click(function () {
    $(".videoBoxTxt").css('display', 'none');
    $(".videoBox").css('display', 'block');

    var videoPlayer = document.getElementById('videoPlayer');
    // Auto play
    videoPlayer.play()
})

$(".areaDtlPage .accVal h3").click(function () {
    $('.accTxt').css('display', 'none');
    if ($(this).hasClass('openAcc')) {
        $('.accVal h3').removeClass('openAcc');
    } else {
        $('.accVal h3').removeClass('openAcc');
        $(this).addClass('openAcc');
        $(this).parent().children('.accTxt').toggle();
    }
})

$(window).scroll(function () {
    if ($(window).scrollTop() > 31) {
        $('.header').css('top', '0');
    } else {
        curHea = 31 - $(window).scrollTop();
        $('.header').css('top', curHea + 'px');
    }
});

