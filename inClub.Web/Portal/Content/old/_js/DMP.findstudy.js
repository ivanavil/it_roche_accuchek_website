﻿var MapObject = (function ($) {
    var rel = 0;
    var countrydef = '';
    var countries = [];
    var countriesStudy = "";
    var conditionsStudy = "";
    var countrybyIp = "";
    var filterGeo = '';
    var filterSiteArea = '';
    var filterCon = '';
    var filterStudy = '';
    var studies;

    var filterURLThArea = '';
    var filterURLCondit = '';

    var CenterStudyDict = {};
    var CenterStudyInvestDict = {};
    $(function () {
        
        setURLFilter();
        GetStudiesbyContext();
        function GetStudiesbyContext() {
            //load studies
            $.ajax({
                cache: false,
                type: "POST",
                url: "/ClinicalTrials/GetMore",
                data: { "KeysContext": '', getjson: 'true' },
                success: function (data) {
                    studies = data;
                    loadCenterbyS();
                    initService();
                    $('.mapLoading').hide();
                }
            });
        }

        function setURLFilter() {
            //select tharea from url
            var urlp = getUrlParam("th");
            if (urlp != undefined) { filterURLThArea = urlp; $('#jsresetFilter').show(); }
            var urlp2 = getUrlParam("cond");
            if (urlp2 != undefined) { filterURLCondit = urlp2; $('#jsresetFilter').show(); }
        }


        function initService() {
            // alert(countrybyIp);
            filterGeo = countrybyIp;
            loadAllCenters(countries);
            var lenarray = 0;
            if (countrybyIp == '') {
                lenarray = countries.length;
            } else if (countrydef != '') { lenarray = rel; $('.jsCountry').html(countrydef); }
            else { loadAllCenters(countries); lenarray = countries.length; }

            LoadMap(lenarray);
            LoadCountries();
            LoadStudies('');
            //BindTrialsClick();
        }
    });


    //function BindTrialsClick() {

    //}


    function getUrlParam(name) {
        if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
            return decodeURIComponent(name[1]);
    }
    function loadCenterbyS() {
        //load all center by filter
        countries = [];
        
        CenterStudyDict = {};
        CenterStudyInvestDict = {};
        for (i = 0; i < studies.length ; i++) {
            
            for (j = 0; j < studies[i].Centers.length; j++) {
                if ((filterStudy == '' || studies[i].Key.Id == filterStudy) &&
                (filterCon == '' || (studies[i].ConditionsShortLabelTxt != null && studies[i].ConditionsShortLabelTxt.indexOf(filterCon) >= 0))&&
                 (filterSiteArea == '' || (studies[i].SiteAreaKey.Id == filterSiteArea)) &&
                    (filterURLThArea == '' || (studies[i].ThAreasTxt.toLowerCase().indexOf(filterURLThArea.toLowerCase()) >= 0)) &&
                    (filterURLCondit == '' || (studies[i].ConditionsTxt.toLowerCase().indexOf(filterURLCondit.toLowerCase()) >= 0))) {
                    

                    if ((filterGeo == '' || studies[i].Centers[j].Description.toLowerCase().indexOf(filterGeo.toLowerCase()) >= 0)) {


                        countries.push(studies[i].Centers[j]);
                        //alert(studies[i].CountriesTxt.toLowerCase());
                        // console.log(CenterStudyDict[studies[i].Centers[j].Key.Id]);
                        
                    }
                    
                }
                if(studies[i].SiteAreaKey.Id == 48){
                    if ((CenterStudyDict[studies[i].Centers[j].Key.Id] != null))
                        CenterStudyDict[studies[i].Centers[j].Key.Id] += "<li><a title='" + studies[i].Title + "' href='" + studies[i].Linkstr + "'>" + studies[i].Title.substring(0, 100) + "...</a></li> ";
                    else
                        CenterStudyDict[studies[i].Centers[j].Key.Id] = "<li><a title='" + studies[i].Title + "' href='" + studies[i].Linkstr + "'>" + studies[i].Title.substring(0, 100) + "...</a></li> ";
                }
                else if(studies[i].SiteAreaKey.Id == 59){
                    if ((CenterStudyInvestDict[studies[i].Centers[j].Key.Id] != null))
                        CenterStudyInvestDict[studies[i].Centers[j].Key.Id] += "<li><a title='" + studies[i].Title + "' href='" + studies[i].Linkstr + "'>" + studies[i].Title.substring(0, 100) + "...</a></li> ";
                    else
                        CenterStudyInvestDict[studies[i].Centers[j].Key.Id] = "<li><a title='" + studies[i].Title + "' href='" + studies[i].Linkstr + "'>" + studies[i].Title.substring(0, 100) + "...</a></li> ";
                }
                // console.log(studies[i].Centers[j].Title + " --- " + CenterStudyDict[studies[i].Centers[j].Key.Id]);
            }
        } 
    }
    //dropdown func
    var _selectProfile = 0;
    $(".jsCountry").click(function () {
        $('.jsulCountry').toggle();
    });
    $(".jsStudies").click(function () {
        $('.jsulStudy').toggle();
    });
    $(".jsCondition").click(function () {
        $('.jsulCondition').toggle();
    });

    $(".jsTrial").click(function () {
        $('.jsulTrial').toggle();
    });




    $(".jschkClinical").click(function () {
        SelectClinicalType($(this).val());
    });


    $("#ulTrials a").click(function () {
        if ($(this).attr("data-id") != '' ) {
            $("#allL").show();
        } else {
            $("#allL").hide();
        }

       $('.jsTrial').html($(this).html());
       SelectClinicalType($(this).attr("data-id"));
   });



   function LoadCountries() {
       //load countries
       var strtmp = "";

      // countries.sort(function (a, b) { return parseFloat(a.Countriestxt) - parseFloat(b.Countriestxt) });
       countries.sort(function (a, b) { return a.Countriestxt.localeCompare(b.Countriestxt) });

       for (i = 0; i < countries.length ; i++) {
           if (strtmp.indexOf(countries[i].Countriestxt) < 0 || strtmp == '') {
               strtmp += countries[i].Countriestxt + ' ';
               $('.jsulCountry').append(
                    $('<li>').append(
                        $('<a>').attr('data-pos', i).attr('rel', countries[i].Lat + ',' + countries[i].Lng).attr('class', 'aPosizione').append(countries[i].Countriestxt)
                ));
              
           }
       }
       /*$('.jsulCountry').append(
           $('<li>').append(
               $('<a>').attr('data-pos', i + 1).attr('rel', '').attr('class', 'aPosizione').append("Select Country")
       ));*/
       BindCountryClick();
       //end load countries
   }

   function ResetFilter() {
       filterGeo = '';
       filterCon = '';
       filterURLThArea = '';
       filterURLCondit = '';
       //filterSiteArea = '';
       filterStudy = '';
       $('.jsulCountry').empty();
       LoadCountries();
       $('.jsCountry').html("Select Country");
       $('.jsStudies ').html("Select Study");
       $('.jsCondition ').html("Select Condition");
       //$('.jsTrial ').html("All Studies");
   }

   function BindCountryClick() {
       $(".jsulCountry li a").click(function () {
           var countrysel = $(this).html();
           var countryselval = $(this).attr("rel");

           //ResetFilter();

           if (countryselval != '') {
               filterGeo = countrysel; $('#jsresetFilter').show();
           } else { filterGeo = ''; $('#jsresetFilter').hide(); }

           loadCenterbyS();
           loadAllCenters(countries);
           if (rel > 0) LoadMap(rel);

           if (_selectProfile == 0)
               _selectProfile++;

           $('.jsCountry').html($(this).html());

           $("#contJobDescr").fadeIn('fast');

           var element = $(this).attr("data-pos");

           $("#contJDescr div").each(function (index) {
               $(this).hide();
           });

           $("div#" + element).fadeIn('fast');

           $('.jsulCountry').hide();
           //LoadStudies();
           return false;
       });
   }
   
   var idsStudy = '';
   function LoadStudies() {
       //load Studies
       $('.jsulStudy').html('');
       conditionsStudy = '';
        studies.sort(function (a, b) {
            if (a.ConditionsShortLabelTxt != null) {
                return a.ConditionsShortLabelTxt.localeCompare(b.ConditionsShortLabelTxt)
            }
        });

        for (i = 0; i < studies.length ; i++) {
            if ((conditionsStudy.indexOf(studies[i].ConditionsShortLabelTxt) < 0 && studies[i].ConditionsShortLabelTxt != null && (studies[i].ConditionsShortLabelTxt.indexOf(";") < 0)) &&
                (filterSiteArea == '' || (studies[i].SiteAreaKey.Id == filterSiteArea))) {
                $('.jsulCondition').append(
                $('<li>').append(
                    $('<a>').attr('data-pos', i).attr('data-id', studies[i].Key.Id).attr('class', 'aPosizione').append(studies[i].ConditionsShortLabelTxt)
            ));
                conditionsStudy += studies[i].ConditionsShortLabelTxt;
            }
           
        }

        studies.sort(function (a, b) {
            if (a.ThAreasTxt != null) {
                return a.ThAreasTxt.localeCompare(b.ThAreasTxt)
            }
        });


       for (i = 0; i < studies.length ; i++) {
           if (filterSiteArea == '' || (studies[i].SiteAreaKey.Id == filterSiteArea)){
           $('.jsulStudy').append(
                   $('<li>').append(
                       $('<a>').attr('data-pos', i).attr('data-id', studies[i].Key.Id).attr('class', 'aPosizione').append(studies[i].Acronym + ' - ' + studies[i].ThAreasTxt)
               ));
           }
       }
       /*$('.jsulStudy').append(
            $('<li>').append(
                $('<a>').attr('data-pos', 0).attr('data-id', 0).attr('class', 'aPosizione').append("Select Study")
        ));*/
       /*$('.jsulCondition').append(
            $('<li>').append(
                $('<a>').attr('data-pos', 0).attr('data-id', 0).attr('class', 'aPosizione').append("Select Condition")
        ));*/
       BindStudyClick();
       BindConditionClick()
       BindResetClick();
       //end load studies
   }
   function BindStudyClick() {
       $(".jsulStudy li a").bind("click", function () {
           ResetFilter();
           var studyselvalval = $(this).attr("data-id");
           if (studyselvalval == 0) {
               filterStudy = ''; $('#jsresetFilter').hide();
           } else { filterStudy = $(this).attr("data-id"); $('#jsresetFilter').show(); }
           var key = $(this).attr("data-id");
           loadCenterbyS();
           loadAllCenters(countries);
          // $('.jsulCountry').empty();
          // LoadCountries();
           if (rel>0) LoadMap(rel);
           
           $('.jsStudies ').html($(this).html());
           //load map

           //close it
           $('.jsulStudy').toggle();
       });
   }

   function BindResetClick() {
       $("#jsresetFilter a").bind("click", function () {
           ResetFilter();
           $('.jsulCondition').empty();
           
           $('input[name=rbClinicalStuiesT]').attr('checked', false);
           //$('input[id=chkClinicalAll]').attr('checked', true);
           $("#chkClinicalAll").prop("checked", true);
           filterSiteArea = '';
           LoadStudies();
           

           $('#jsresetFilter').hide();
           loadCenterbyS();
           loadAllCenters(countries);
           LoadMap(rel);

           $('#selTrial').html("All Studies");
           $('#allL').hide();
           SelectClinicalType('');
       });
   }

   function BindConditionClick() {
       $(".jsulCondition li a").bind("click", function () {
           ResetFilter();
           var condselvalval = $(this).attr("data-id");
           if (condselvalval == 0) {
               filterCon = ''; $('#jsresetFilter').hide();
           } else { filterCon = $(this).html();  $('#jsresetFilter').show(); }
           
           var key = $(this).attr("data-id");
           loadCenterbyS();
           loadAllCenters(countries);
           if (rel > 0) LoadMap(rel);

           $('.jsCondition ').html($(this).html());

           //close it
           $('.jsulCondition').toggle();
       });
   }



   function SelectClinicalType(val) {
        // ResetFilter();
       
       $('.jsulStudy').empty('');
       $('.jsulCondition').empty();
       ResetFilter();
       filterSiteArea = val;

       //alert(filterSiteArea);

        loadCenterbyS();
        loadAllCenters(countries);
        
        LoadStudies();
        $('.jsulCountry').empty();
        LoadCountries();
        if (rel > 0) LoadMap(rel);
   }


   function loadAllCenters(coll) {
       rel = 0;
       
       for (i = 0; i < coll.length ; i++) {
               LoadObject(coll[i].Title, coll[i].Description, coll[i].Lat,
                      coll[i].Lng, coll[i].LinkLabel, coll[i].LinkUrl, coll[i].Cover, CenterStudyDict[coll[i].Key.Id], CenterStudyInvestDict[coll[i].Key.Id]);
           //console.log(CenterStudyDict[coll[i].Key.Id]);
               
               rel += 1;
               countrydef = coll[i].coll;
       }
       if (rel < 1) { alert("The search you performed did not match any documents."); }
   }
}(jQuery));