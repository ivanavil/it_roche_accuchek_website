﻿/*Exsternal Links*/
function bindExternalLinks(curDomain, linkEsclude) {
    //alert("test");
    var domArray = linkEsclude.split(";");
    var domainSelector = '';

    var i;
    for (i = 0; i < (domArray.length) ; i++) {
        domainSelector += ":not([href^='" + domArray[i] + "'])";
    };

    //add Current Domain
    domainSelector += ":not([href^='" + curDomain + "'])";


    $("a" + domainSelector + "[href^='http://']").each(function () {

        $(this).unbind();
        $(this).on("click", function (e) {
            e.preventDefault();
            openExternal(this);
            
        });

        $(this).removeAttr("target");
    });

    $("a" + domainSelector + "[href^='https://']").each(function (index) {

        $(this).unbind();
        $(this).on("click", function (e) {
            e.preventDefault();
            openExternal(this);
        });

        $(this).removeAttr("target");
    });


};

$(document).ready(function () {

    var curDomain = curDomainExtLink;
    var linkEsclude = linkEscludeExtLink;

    //disclaimer External links
    bindExternalLinks(curDomain, linkEsclude);

    $(".jsdialogExternalLinks").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: false,
        width: 580,
        height: 326,
        position: "center",
        overlay: {
            opacity: 70,
            background: 'grey'
        },
        close: function () {
            $(".jsdialogExternalLinks h2").remove("h2");
            $(".jsdialogExternalLinks .jsPLink").remove("p");
            //$(".jsdialogExternalLinks .jsbtnLink").removeAttr("href");
        }
    });
   
});


function openExternal(evt) {

    var url = $(evt).attr("href");
    var title = $(evt).attr("title");

    
    $(".jsdialogExternalLinks").dialog({
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            },
            Continue: function () {
                //location.href = url;
                window.open(url, '_blank');
                $(this).dialog("close");
            }
        }
    });

    //$(".jsdialogExternalLinks").prepend("<p class='jsPLink'>" + url + "</p>");
    //$(".jsdialogExternalLinks").prepend("<h2>" + title + "</h2>");

    //$(".jsdialogExternalLinks .jsbtnLink").attr("href", url);



    //$(".jsdialogExternalLinks .jsbtnLink").unbind();
    //$(".jsdialogExternalLinks .jsbtnLink").on("click", function () {
        
        /*var objTrace = {
            ThemeId: ThemeIdExtLink,
            TraceId: TraceIdExtLink,
            TraceLangId: TraceLangIdExtLink,
            TraceDescription: url
        };

        $.ajax({
            url: '/Trace/Index',
            type: 'Post',
            data: objTrace,
            success: function (data) { },
            complete: function () {
                $(".jsdialogExternalLinks").dialog("close");
            }
        });*/

    //});

    var keyDict = 'HC_DiscExternalLinks';
    if (null != $(evt).attr("class")) {
        var classArr = $(evt).attr("class").split(' ');

        if (classArr.length > 1)
            keyDict = classArr[1];
    }

    var objDict = { key: keyDict, TraceLangIdExtLink: TraceLangIdExtLink };

    $.ajax({
        url: '/GenericContent/GetDictionary',
        type: 'Post',
        data: objDict,
        success: function (data) {
            $("#viewExtLink").html(data);
        },
        complete: function () {
            $(".jsdialogExternalLinks").dialog("open");
        }
    });

    //$(".jsdialogExternalLinks").dialog("open");

};