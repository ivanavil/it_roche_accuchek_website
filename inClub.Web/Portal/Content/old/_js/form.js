/*

CUSTOM FORM ELEMENTS

Created by Ryan Fait
www.ryanfait.com

The only things you may need to change in this file are the following
variables: checkboxHeight, radioHeight and selectWidth (lines 24, 25, 26)

The numbers you set for checkboxHeight and radioHeight should be one quarter
of the total height of the image want to use for checkboxes and radio
buttons. Both images should contain the four stages of both inputs stacked
on top of each other in this order: unchecked, unchecked-clicked,
checked, checked-clicked.

You may need to adjust your images a bit if there is a slight vertical
movement during the different stages of the button activation.

The value of selectWidth should be the width of your select list image.

Visit http://ryanfait.com/ for more information.

*/

var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "302";

document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: '+selectWidth+"px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>");var Custom={init:function(){var e,t,i,o=document.getElementsByTagName("input"),s=Array();for(a=0;a<o.length;a++)("checkbox"==o[a].type||"radio"==o[a].type)&&o[a].className.indexOf("styled")>-1&&(s[a]=document.createElement("span"),s[a].className=o[a].type,1==o[a].checked&&("checkbox"==o[a].type?(position="0 -"+2*checkboxHeight+"px",s[a].style.backgroundPosition=position):(position="0 -"+2*radioHeight+"px",s[a].style.backgroundPosition=position)),o[a].parentNode.insertBefore(s[a],o[a]),o[a].onchange=Custom.clear,o[a].getAttribute("disabled")?s[a].className=s[a].className+=" disabled":(s[a].onmousedown=Custom.pushed,s[a].onmouseup=Custom.check));for(o=document.getElementsByTagName("select"),a=0;a<o.length;a++)if(o[a].className.indexOf("styled")>-1){for(t=o[a].getElementsByTagName("option"),i=t[0].childNodes[0].nodeValue,e=document.createTextNode(i),b=0;b<t.length;b++)1==t[b].selected&&(e=document.createTextNode(t[b].childNodes[0].nodeValue));s[a]=document.createElement("span"),s[a].className="select",s[a].id="select"+o[a].name,s[a].appendChild(e),o[a].parentNode.insertBefore(s[a],o[a]),o[a].getAttribute("disabled")?o[a].previousSibling.className=o[a].previousSibling.className+=" disabled":o[a].onchange=Custom.choose}document.onmouseup=Custom.clear},pushed:function(){element=this.nextSibling,this.style.backgroundPosition=1==element.checked&&"checkbox"==element.type?"0 -"+3*checkboxHeight+"px":1==element.checked&&"radio"==element.type?"0 -"+3*radioHeight+"px":1!=element.checked&&"checkbox"==element.type?"0 -"+checkboxHeight+"px":"0 -"+radioHeight+"px"},check:function(){if(element=this.nextSibling,1==element.checked&&"checkbox"==element.type)this.style.backgroundPosition="0 0",element.checked=!1;else{if("checkbox"==element.type)this.style.backgroundPosition="0 -"+2*checkboxHeight+"px";else for(this.style.backgroundPosition="0 -"+2*radioHeight+"px",group=this.nextSibling.name,inputs=document.getElementsByTagName("input"),a=0;a<inputs.length;a++)inputs[a].name==group&&inputs[a]!=this.nextSibling&&(inputs[a].previousSibling.style.backgroundPosition="0 0");element.checked=!0}},clear:function(){inputs=document.getElementsByTagName("input");for(var a=0;a<inputs.length;a++)"checkbox"==inputs[a].type&&1==inputs[a].checked&&inputs[a].className.indexOf("styled")>-1?inputs[a].previousSibling.style.backgroundPosition="0 -"+2*checkboxHeight+"px":"checkbox"==inputs[a].type&&inputs[a].className.indexOf("styled")>-1?inputs[a].previousSibling.style.backgroundPosition="0 0":"radio"==inputs[a].type&&1==inputs[a].checked&&inputs[a].className.indexOf("styled")>-1?inputs[a].previousSibling.style.backgroundPosition="0 -"+2*radioHeight+"px":"radio"==inputs[a].type&&inputs[a].className.indexOf("styled")>-1&&(inputs[a].previousSibling.style.backgroundPosition="0 0")},choose:function(){for(option=this.getElementsByTagName("option"),d=0;d<option.length;d++)1==option[d].selected&&(document.getElementById("select"+this.name).childNodes[0].nodeValue=option[d].childNodes[0].nodeValue)}};window.onload=Custom.init;