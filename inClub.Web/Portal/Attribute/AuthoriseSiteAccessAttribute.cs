﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Attribute
{
    public class AuthoriseSiteAccessAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
           
            }

        private  Models.UserLogged GetUserBaseValueLogged( ControllerBase ctlBase)
        {
            var userLogged = ctlBase.TempData["UserLogged"] as Models.UserLogged;
            if (null == userLogged)
                ctlBase.TempData["UserLogged"] = userLogged = Helper.UserHelper.readUserLogged();

            return userLogged;
        }

    }

    public class SiteAccessDeniedResult : ViewResult
    {

        public SiteAccessDeniedResult(string viewName)
        {
            if(String.IsNullOrWhiteSpace(viewName))
                throw new ArgumentNullException("viewName");
            ViewName = viewName;
        }

        public SiteAccessDeniedResult() : this("_NotLogged")
        {            
        }
    }

}
