﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Healthware.HP3.MVC;
using System.Linq;

namespace Inclub.Web.Portal
{
    public class RouteConfigurations
    {
        #region Members

        private static RouteConfiguration[] _configs = new RouteConfiguration[] { 
            new RouteConfiguration()
            {
                RouteName = RouteNames.ContentDetails,
                Url = "{controller}/{action}/{title}-{id}",
                Defaults = new RouteValueDictionary(new {action = "Details", site = Constants.SiteFolder, keydomain = Constants.DefaultKeyDomain }),
                Constraints = RouteConfigurations.GetConstraints(),
                Namespaces = new string[] { typeof(Controllers.HomeController).Namespace }
            },
             new RouteConfiguration()
            {
                RouteName = RouteNames.ContentDetailsImplicit,
                Url = "{controller}/{title}-{id}",
                Defaults = new RouteValueDictionary(new {action = "Detail", site = Constants.SiteFolder, keydomain = Constants.DefaultKeyDomain }),
                Constraints = RouteConfigurations.GetConstraints(),
                Namespaces = new string[] { typeof(Controllers.HomeController).Namespace }
            },
            new RouteConfiguration()
            {
                RouteName = RouteNames.Default,
                Url = "{controller}/{action}",
                Defaults = new RouteValueDictionary(new {controller = "Home", action = string.Empty, site = Constants.SiteFolder, keydomain = Constants.DefaultKeyDomain }),
                Constraints = RouteConfigurations.GetConstraints(),
                Namespaces = new string[] { typeof(Controllers.HomeController).Namespace }
            }
            // new RouteConfiguration()
            //{
            //    RouteName = "Languages",
            //    Url = "{language}",
            //    Defaults = new RouteValueDictionary(new {language="en", controller = "Home", action = string.Empty, site = Constants.SiteFolder, keydomain = Constants.DefaultKeyDomain }),
            //    Constraints = RouteConfigurations.GetConstraints(),
            //    Namespaces = new string[] { typeof(Controllers.HomeController).Namespace }
            //}
        };

        #endregion Members

        #region Private Methods

        public static RouteValueDictionary GetConstraints()
        {
            var validDomainsConfig = System.Configuration.ConfigurationManager.AppSettings["RouteValueConstraints"];
            if (string.IsNullOrWhiteSpace(validDomainsConfig))
                throw new System.ArgumentNullException("RouteValueConstraints in Appsettings is Empty or missing");

            var validDomains = validDomainsConfig.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(v => new Uri(v)).ToArray();

            return new RouteValueDictionary(new { domain = new Healthware.HP3.MVC.DomainRouteConstraint(validDomains) });
        }
        #endregion Private Methods

        #region Properties

        public static RouteConfiguration Default
        {
            get { return _configs[2]; }
        }

        public static RouteConfiguration ContentDetailsImplicit
        {
            get { return _configs[1]; }
        }

        public static RouteConfiguration ContentDetails
        {
            get { return _configs[0]; }
        }

        public static IEnumerable<RouteConfiguration> Configurations { get { return _configs; } }

        #endregion Properties

        public static class RouteNames
        {
            public const string Default = "Default";

            public const string ContentDetails = "ContentDetails";

            public const string ContentDetailsImplicit = "ContentDetailsImplicit";
        }


    }
}
