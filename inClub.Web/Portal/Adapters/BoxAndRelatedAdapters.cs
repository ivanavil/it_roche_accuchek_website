﻿using System.Web;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Adapters
{
    public class BoxAndRelatedAdapters
    {

        public static Models.GenericContentBox Adapter(ContentValue data)
        {
            if (data == null)
                return null;

            var model = new GenericContentBox()
            {
                Key = data.Key,
                Launch = new HtmlString(data.Launch),
                Title = data.Title,
                LanguageId = data.Key.IdLanguage,
                ContenteTypeKey = data.ContentType.Key,
                SiteAreaKey = data.SiteArea.Key,
                LinkUrl = data.LinkUrl
            };

            return model;
        }


        
    }
}
