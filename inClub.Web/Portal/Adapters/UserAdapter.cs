﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.User.ObjectValues;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Models;

namespace Inclub.Web.Portal.Adapters
{
    public class UserAdapter
    {
        public static UserRegistration AdaptUserEdit(UserHCPValue user)
        {
            if (user == null)
                return null;


            var model = new UserRegistration()
            {
                Id = user.Key.Id,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email.ToString(),
                CodiceFiscale = user.CF,
                AccessCode = user.HCPRegistration,
                SelectedSpecId = (int)user.HCPProfessionalCodeGeo.Id,
                MaritalStatus = (int)user.MaritalStatus,
                SkypeLink = user.HCPTelephone.ToString(),
                TwitterLink = user.HCPOtherInfo.ToString(),
                LinkedinLink = user.HCPMoreInfo.ToString(),
                Structure=user.HCPStructure.ToString(),
                City = user.HCPCity.ToString(),
                NumeroIscrizioneAlbo = (string)user.HCPProfessionalCodes.ToString(),
                ProvinciaAlbo = (string)user.HCPFax.GetString,
                AcceptNotify = (bool)user.CanReceiveNewsletter,


            };

            return model;
        }


        public static UserAccessCode AdaptUserAccessCode(UserHCPValue user)
        {
            if (user == null)
                return null;


            var model = new UserAccessCode()
            {
                Id = user.Key.Id,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email.ToString(),
                Username = user.Username.ToString(),
                Password = user.Password.ToString(),
                MaritalStatus = (int)user.MaritalStatus,
                AccessCode = user.HCPRegistration,
              

           


            };

            return model;
        }


        public static UserBase AdaptUser(UserValue user)
        {
            if (user == null)
                return null;

            var model = new UserBase()
            {
                Id = user.Key.Id,
                FirstName = user.Name,
                LastName = user.Surname,
                Email = user.Email.ToString()
            };

            return model;
        }


        public static UserChangePassword AdaptChangePasswordUser(UserValue user)
        {
            if (user == null)
                return null;

            var model = new UserChangePassword()
            {
                Id = user.Key.Id,
                Password = user.Password
            };

            return model;
        }

        public static UserLogged AdaptLoggedUser(UserValue user)
        {
            if (user == null)
                return null;

            var model = new UserLogged()
            {
                Key = user.Key,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email.ToString(),
                userPasswordForceChange = user.PasswordForceChange,
                Title = user.Title
            };

            return model;
        }


        public static UserLogIn AdaptLogInUser(UserValue user, string _returnUrl)
        {
            if (user == null)
                return null;

            var model = new UserLogIn()
            {
                Email = user.Email[0],
                Password = user.Password,
                returnUrl = _returnUrl
            };

            return model;
        }





    }
}