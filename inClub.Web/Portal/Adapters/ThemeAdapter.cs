﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inclub.Web.Portal.Helper;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.Utility;

namespace Inclub.Web.Portal.Adapters
{
    public class ThemeAdapter
    {
        public static ThemeModel.ThemeBaseModel Convert(ThemeValue tval)
        {
            if (tval == null)
                return null;

            var model = new ThemeModel.ThemeBaseModel
            {
                Description = tval.Description,
                Key = tval.Key,
                KeyFather = tval.KeyFather,
                IdCrypt = SimpleHash.UrlEncryptRijndaelAdvanced(tval.Key.Id.ToString()),
                ContentId = tval.KeyContent.Id,
                SiteAreaKey = tval.KeySiteArea,
                ContenteTypeKey = tval.KeyContentType,
                LanguageKey = tval.KeyLanguage
            };
            //if (!string.IsNullOrEmpty(tval.Note)) model.ShowContent = true;
            //if (!model.ShowContent)
            //{
            //    model.ChildColl = DompeTrials.Dompe.Helper.ThemeHelper.getThemeMenuCollectionByFather(tval.Key);
            //}

            return model;
        }
    }
}
