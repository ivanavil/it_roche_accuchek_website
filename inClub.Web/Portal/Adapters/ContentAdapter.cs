﻿using System.Web;
using Inclub.Web.Portal.Models;
using Healthware.HP3.Core.Content.ObjectValues;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Adapters
{
    public class ContentAdapter
    {
        public static GenericContentDetails AdaptDetails(ContentExtraValue data)
        {
            if (data == null)
                return null;

            var model = new GenericContentDetails()
            {
                MetaDescription = data.MetaDescription,
                MetaKeywords = data.MetaKeywords,
                DatePublication = data.DatePublish.HasValue ? data.DatePublish : null,
                FullTextOriginal = new HtmlString(data.FullTextOriginal),
                BookReferences = new HtmlString(data.Abstract),
                Title = data.Title,
                InternalTitle=new HtmlString(data.TitleContentExtra),
                FullText = new HtmlString(data.FullText),
                Launch = new HtmlString(data.Launch),
                Codecss = data.Code,
                Key = data.Key,
                Abstract = new HtmlString(data.Abstract)
            };
            return model;
        }


        public static Models.GenericContentDetails AdaptDetail(ContentExtraValue data, bool hasReferences)
        {
            if (data == null)
                return null;

            var model = new Models.GenericContentDetails
            {
                Title = data.Title,

                Key = data.Key,
                DatePublication = data.DatePublish.HasValue ? data.DatePublish : null,
                FullText = new HtmlString(data.FullText),
                PageTitle = data.PageTitle,
                Metadescription = new HtmlString(data.MetaDescription),
                MetaDescription = data.MetaDescription,
                MetaKeywords = data.MetaKeywords
            };


            if (hasReferences)
            {

                model.FullText = new HtmlString(data.FullText);
                model.TitleContentExtra = new HtmlString(data.TitleContentExtra);
                model.FullTextOriginal = new HtmlString(data.FullTextOriginal);
            }
            else
            {
                model.Abstract = new HtmlString(data.Abstract);
                model.FullText = new HtmlString(data.FullText);
                model.TitleContentExtra = new HtmlString(data.SubTitle);
                model.FullTextOriginal = new HtmlString(data.FullTextOriginal);
            }


            return model;
        }

        public static GenericContentDetails AdaptDetails(ContentValue data)
        {
            if (data == null)
                return null;

            var model = new GenericContentDetails()
            {
                MetaDescription = data.MetaDescription,
                MetaKeywords = data.MetaKeywords,
                DatePublication = data.DatePublish.HasValue ? data.DatePublish : null,
                Title = data.Title,
                Launch = new HtmlString(data.Launch),
                Key = data.Key
            };
            return model;
        }

        public static GenericContentArchives AdaptArchives(ContentValue data)
        {
            if (data == null)
                return null;

            var model = new GenericContentArchives()
            {
                DatePublication = data.DatePublish.HasValue ? data.DatePublish : null,
                Title = data.Title,
                Copyright = data.Copyright,
                Launch = new HtmlString(data.Launch),
                Key = data.Key,
                LinkUrl = data.LinkUrl,
                LinkLabel = data.LinkLabel,
                IdContentCrypt = Helper.CryptHelper.urlEncrypt(data.Key.Id.ToString()),
                Codecss = data.Code,
                ContenteTypeKey = data.ContentType.Key,
                SiteAreaKey = data.SiteArea.Key
            };
            return model;
        }

        public static GenericContentArchives AdaptArchivesSearchEngine(ContentValue data)
        {
            if (data == null)
                return null;

            var model = new GenericContentArchives()
            {
                DatePublication = data.DatePublish.HasValue ? data.DatePublish : null,
                Title = data.Title,
                Copyright = data.Copyright,
                Launch = new HtmlString(data.Launch),
                Key = data.Key,
                LinkUrl = data.LinkUrl,
                LinkLabel=data.LinkLabel,
                ContenteTypeKey = data.ContentType.Key,
                SiteAreaKey = data.SiteArea.Key
            };
            if (null != data.InclusiveOf.Contexts)
            {
                model.Contexts = data.InclusiveOf.Contexts;
            }
            return model;
        }

        public static ThemeModel.ThemeContentModel AdaptThemeContent(ContentValue dataContent, ThemeValue objThemeValue)
        {
            if (dataContent == null)
                return null;

            ThemeModel.ThemeContentModel model = new ThemeModel.ThemeContentModel();
            model.Key = objThemeValue.Key;
            model.Description = objThemeValue.SeoName;
            model.LanguageKey = objThemeValue.KeyLanguage;

            GenericContentBaseModel objGenericContentBaseModel = new GenericContentBaseModel();

            objGenericContentBaseModel.Key = dataContent.Key;
            objGenericContentBaseModel.Title = dataContent.Title;
            objGenericContentBaseModel.PageTitle = dataContent.PageTitle;
            objGenericContentBaseModel.Launch = dataContent.Launch;
            model.GenericContentBaseModel = objGenericContentBaseModel;

            return model;
        }
    }
}
