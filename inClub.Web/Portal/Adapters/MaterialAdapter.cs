﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Content.ObjectValues;

namespace Inclub.Web.Portal.Adapters
{
    public class MaterialsAdapter
    {
        public static Models.MaterialModel AdaptArchive(ContentValue data)
        {
            if (data == null)
                return null;

            var model = new Models.MaterialModel
            {
                Title = data.Title,
                Key = data.Key,
                Launch =data.Launch
            };

           
            return model;
        }

    }
}