﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Keys
{
    public class EnumeratorKeys
    {
        public enum coverPosition
        {
            first = 0,
            second = 1,
            third = 2
        }

        public enum coverDimension
        {
            WidthDetail = 800,
            HeightDetail = 455,
            WidthGroup = 665,
            HeightGroup = 378,
            WidthBox = 397,
            HeightBox = 229,
            NewsWidthArchive = 810,
            NewsHeightArchive = 287,
            NewsWidthDetail = 1080,
            NewsHeightDetail = 383,
         
        }


        public enum UserContextRelationType
        {
            Specialization = 1, //specializzazione
            MainActivity = 2, //Attività prevalente
            Title = 3 //Titolo Medico
        }

        public enum DataLoadingType
        {
            Full,
            Lite,
            List,
            SecondLevelTheme,
        }

        public enum ContentRelationType
        {
            DefaultRelation = 1,
            LinkRelation = 4,
            ImageRelation = 5,
            LandingRelation = 6,
            ContentRelation = 7,
            DownloadRelation = 8,
            Conditions=9,
            ConditionsShortLabel=18,
            Type=10,
            Interventions=11,
            Phases=12,
            ThAreas=13,
            StatusStudy = 14,
            StudyCenter = 15,
            StatusStudyGrp2 = 16
        }

        public enum ExtraFieldsType
        {
            AgeFrom = 1,
            AgeTo= 2,
            Gender= 3,
            Volunteers = 4,
            SecondDate= 5
        }

        public enum DownloadType
        {
            Download = 1, // for section type download
            DownloadConferencedReport = 2,
            DownloadRelated = 3 //downlaod correlati ad un contenuto in dettaglio
        }

        //Enumerato per gestire le diverse tipologie di contenuti per il generare i link nel motore di ricerca
        public enum ActionSearchEngineType
        {
            OnlyDetails = 1,
            Link = 2,
            Generic = 3,
            Download = 4
        }

        public enum StatusCode
        {
            Error = 1,
            Succeded = 2,
            Failed = 3,
            ExistsConflict = 4,
            CaptchaError = 5
        }

        public enum OperationMessage
        {
            MessageNotSent = 1,
            MessageSent = 2,
        }


        public enum UserStatus
        {
            NotFound = -1,
            Active = 1,
            Pending = 2,
            NoActive = 3,
            Disable = 4,
            Deleted = 5
        }
    }
}
