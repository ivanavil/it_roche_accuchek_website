﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Keys
{
    public class LabelKeys
    {

        public const int UserCoverWidth = 100;
        public const string LINK_PROTOCOL = "http://{0}";
        public const string TEMPLATE_BODY_PLACEHOLDER = "[TEMPLATEBODY]";

        public const string COVER_PATH = "~/HP3Image/cover/{0}";

        public const string COVER_HIDDEN = "~/{0}/Content/_slice/noCover1px.gif";

        public const string TEMPLATE_ERROR_LOG = "~/App_Data/Templates/ErrorNotify.txt";

        public const string DEFAULT_UNAVAILABLE_TEXT = "n.d.";

        public const string NAME_FILE = "{0}.{1}";

        public const string DOWNLOAD_PATH = "~/hp3download/";
        public const string TEMP_DOWNLOAD_PATH = "~/hp3download/tmpDownloadPath/";

        public const string RETURN_URL = "ReturnUrl";
        public const string UserIdSL_Generic = "usrsl"; //silentlogin Generic

        public const string QsAccessCode = "ac"; //silentlogin Generic

        public const string FolderFILE_TEMP = "~/UplFiles/tmp/";
        public const string FolderFILE = "~/UplFiles/";

        public const int LANGUAGE_PORTAL = 1;
        #region Regular expressions

        public const string TrimAllWhiteSpaces = @"\s+";
        public const string REG_EX_EMAIL = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        public const string REG_EX_CORRECT_URL = @"(?:http|https|ftp)://[a-zA-Z0-9\.\-]+(?:\:\d{1,5})?(?:[A-Za-z0-9\.\;\:\@\&\=\+\$\,\?/]|%u[0-9A-Fa-f]{4}|%[0-9A-Fa-f]{2})*";

        #endregion


        #region Email Replace
        public const string em_TITLE = "[TITLE]";
        public const string em_DESCRIPTION = "[DESCRIPTION]";
        public const string em_DATE = "[DATE]";
        public const string em_DATEHOUR = "[DATEHOUR]";
        public const string em_NAME = "[NAME]";
        public const string em_USERNAME = "[USER_NAME]";
        public const string em_COMPLETENAME = "[COMPLETENAME]";
        public const string em_NAMECENTER = "[NAMECENTER]";
        public const string em_SUBJECT = "[SUBJECT]";  
        public const string em_SURNAME = "[SURNAME]";
        public const string em_EMAIL = "[EMAIL]";     
        public const string em_MESSAGE = "[MESSAGE]";   
        public const string em_LINK = "[LINK]";
        public const string em_DATACOPYRIGHT = "[DATACOPYRIGHT]";
        public const string em_TELEPHONE = "[TELEPHONE]";
        public const string em_CONTENT_TITLE = "[CONTENT_TITLE]";
        public const string em_CONTENT_DESCRIPTION = "[CONTENT_DESCRIPTION]"; 
     
        public const string em_RESERVATION = "[RESERVATION]";
        public const string em_IDUSER = "[IDUSER]";

        #endregion


    }
}
