﻿using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class ContextGroupKeys 
    {
        private static ContextGroupIdentificator _conditions = null;
        private static ContextGroupIdentificator _conditionsI = null;
        private static ContextGroupIdentificator _conditionsShortLabel = null;
        private static ContextGroupIdentificator _conditionsShortLabelI = null;
        private static ContextGroupIdentificator _interventions = null;
        private static ContextGroupIdentificator _phases = null;
        private static ContextGroupIdentificator _thArea = null;
        private static ContextGroupIdentificator _thAreaI = null;
        private static ContextGroupIdentificator _studytype = null;
        private static ContextGroupIdentificator _statusStudy = null;
        private static ContextGroupIdentificator _statusStudyI = null;
        private static ContextGroupIdentificator _mainContent = null;
        public static ContextGroupIdentificator SpecializzazioniRegistrationForm = new ContextGroupIdentificator(59);
        public static ContextIdentificator _ctxDefaultGroup = null;
        public static ContextIdentificator _ctxWebinar = null;
        public static ContextIdentificator _ctxInterviste = null;


        public static ContextIdentificator CtxDefaultGroup
        {
            get
            {
                if (_ctxDefaultGroup == null)
                    _ctxDefaultGroup = new ContextIdentificator(96, Constants.DefaultKeyDomain, "grpDefault");

                return _ctxDefaultGroup;
            }
        }


        public static ContextIdentificator CtxWebinar
        {
            get
            {
                if (_ctxWebinar == null)
                    _ctxWebinar = new ContextIdentificator(97, Constants.DefaultKeyDomain, "webinar");

                return _ctxWebinar;
            }
        }


        public static ContextIdentificator CtxInterviste
        {
            get
            {
                if (_ctxInterviste == null)
                    _ctxInterviste = new ContextIdentificator(98, Constants.DefaultKeyDomain, "interviste");

                return _ctxInterviste;
            }
        }

        public static ContextGroupIdentificator Conditions
        {
            get
            {
                if (_conditions == null)
                    _conditions = new ContextGroupIdentificator(57, Constants.DefaultKeyDomain, "conditions");

                return _conditions;
            }
        }
        public static ContextGroupIdentificator ConditionsI
        {
            get
            {
                if (_conditionsI == null)
                    _conditionsI = new ContextGroupIdentificator(65, Constants.DefaultKeyDomain, "conditions");

                return _conditionsI;
            }
        }

        public static ContextGroupIdentificator ConditionsShortLabel
        {
            get
            {
                if (_conditionsShortLabel == null)
                    _conditionsShortLabel = new ContextGroupIdentificator(64, Constants.DefaultKeyDomain, "condShort");

                return _conditionsShortLabel;
            }
        }
        public static ContextGroupIdentificator ConditionsShortLabelI
        {
            get
            {
                if (_conditionsShortLabelI == null)
                    _conditionsShortLabelI = new ContextGroupIdentificator(68, Constants.DefaultKeyDomain, "condShort");

                return _conditionsShortLabelI;
            }
        }



        public static ContextGroupIdentificator Interventions
        {
            get
            {
                if (_interventions == null)
                    _interventions = new ContextGroupIdentificator(58, Constants.DefaultKeyDomain, "intervent");

                return _interventions;
            }
        }

        public static ContextGroupIdentificator Phases
        {
            get
            {
                if (_phases == null)
                    _phases = new ContextGroupIdentificator(59, Constants.DefaultKeyDomain, "phases");

                return _phases;
            }
        }

        public static ContextGroupIdentificator ThArea
        {
            get
            {
                if (_thArea == null)
                    _thArea = new ContextGroupIdentificator(60, Constants.DefaultKeyDomain, "tharea");

                return _thArea;
            }
        }

        public static ContextGroupIdentificator ThAreaI
        {
            get
            {
                if (_thAreaI == null)
                    _thAreaI = new ContextGroupIdentificator(66, Constants.DefaultKeyDomain, "tharea");

                return _thAreaI;
            }
        }

        public static ContextGroupIdentificator Studytype
        {
            get
            {
                if (_studytype == null)
                    _studytype = new ContextGroupIdentificator(61, Constants.DefaultKeyDomain, "studytype");

                return _studytype;
            }
        }

        public static ContextGroupIdentificator StatusStudy
        {
            get
            {
                if (_statusStudy == null)
                    _statusStudy = new ContextGroupIdentificator(62, Constants.DefaultKeyDomain, "statusStud");

                return _statusStudy;
            }
        }
        public static ContextGroupIdentificator StatusStudyI
        {
            get
            {
                if (_statusStudyI == null)
                    _statusStudyI = new ContextGroupIdentificator(67, Constants.DefaultKeyDomain, "statusStud");

                return _statusStudyI;
            }
        }

        public static ContextGroupIdentificator _MainContent
        {
            get
            {
                if (_mainContent == null)
                    _mainContent = new ContextGroupIdentificator(63, Constants.DefaultKeyDomain, "MainC");

                return _mainContent;
            }
        }
    }
}
