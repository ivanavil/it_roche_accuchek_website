﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inclub.Web.Portal.Keys
{
    public class CacheKeys
    {
        public const string getThemeCollectionByFather = "getThemeCollectionByFather|{0}|{1}";
        public const string getThemeTraceValue = "getThemeTraceValue|{0}";
        public const string getStudyDetailsByIdLang = "getStudyDetailsByIdLang|{0}|{1}";
        public const string getCenterRelByStudyIdLang = "getCenterRelByStudyIdLang|{0}|{1}";
        public const string BoxResources = "BoxResources|{0}|{1}";
        public const string CACHE_GROUP_USERCONTEXT = "cache_group_UserContext";
        public const string CACHE_GROUP_USERCONTENT = "cache_group_UserContent";
        public const string BoxRelatedContent = "BoxRelatedContent|{0}|{1}|{2}|{3}";
        public const string BoxLastNews = "BoxLastNews|{0}|{1}|{2}";
        public const string BoxDownloadableCard = "BoxDownloadableCard|{0}|{1}|{2}|{3}";
        public const string BoxRelatedPresentation = "BoxRelatedPresentation|{0}|{1}|{2}|{3}";
        public const string BoxRelatedVideoConferences = "BoxRelatedVideoConference|{0}|{1}|{2}|{3}";
        public const string SliderBigHomePage = "SliderBigHomePage|{0}|{1}";
        public const string SliderHighlightsHomePage = "SliderHighlightsHomePage|{0}|{1}";
        public const string BoxCancerCachexiaHP = "BoxCancerCachexiaHP|{0}|{1}";
        public const string BoxVideoHP = "BoxVideoHP|{0}|{1}";
        public const string calculateRate = "calculateRate|{0}|{1}";
        public const string GroupCacheUser = "GroupCacheUser";
        public const string GroupCacheInstantpollByUser = "GroupCacheInstantpollByUser_{0}";
        public const string GroupCacheInstantpollRate = "GroupCacheInstantpollRate";

    }
}
