﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class RoleKeys
    {

        private static RoleIdentificator _sysAdmin = null;
        private static RoleIdentificator _userAccess = null;
        private static RoleIdentificator _grpAdmin = null;
        private static RoleIdentificator _member = null;
        private static RoleIdentificator _accWebinar = null;
        private static RoleIdentificator _accWebinar2 = null;

        public static RoleIdentificator SysAdmin
        {
            get
            {
                if (_sysAdmin == null)
                    _sysAdmin = new RoleIdentificator(66, Constants.DefaultKeyDomain, "SysAdmin");

                return _sysAdmin;
            }
        }

        public static RoleIdentificator GrpAdmin
        {
            get
            {
                if (_grpAdmin == null)
                    _grpAdmin = new RoleIdentificator(66, Constants.DefaultKeyDomain, "GrpAdmin");

                return _grpAdmin;
            }
        }

        public static RoleIdentificator Member
        {
            get
            {
                if (_member == null)
                    _member = new RoleIdentificator(75, Constants.DefaultKeyDomain, "Member");

                return _member;
            }
        }

        public static RoleIdentificator userAccess
        {
            get
            {
                if (_userAccess == null)
                    _userAccess = new RoleIdentificator(73, Constants.DefaultKeyDomain, "rAccess");

                return _userAccess;
            }
        }

        public static RoleIdentificator accWebinar
        {
            get
            {
                if (_accWebinar == null)
                    _accWebinar = new RoleIdentificator(76, Constants.DefaultKeyDomain, "accWebinar");

                return _accWebinar;
            }
        }

        public static RoleIdentificator accWebinar2
        {
            get
            {
                if (_accWebinar2 == null)
                    _accWebinar2 = new RoleIdentificator(79, Constants.DefaultKeyDomain, "accWbnr2");

                return _accWebinar2;
            }
        }

    }
}
