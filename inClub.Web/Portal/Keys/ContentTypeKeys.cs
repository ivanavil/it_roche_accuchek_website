﻿using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class ContentTypeKeys
    {
        private static ContentTypeIdentificator _genContExt = null;
        private static ContentTypeIdentificator _genCont = null;
        private static ContentTypeIdentificator _gruppi = null;
        private static ContentTypeIdentificator _news = null;
        private static ContentTypeIdentificator _discussioni = null;
        private static ContentTypeIdentificator _documenti = null;
        private static ContentTypeIdentificator _folder = null;
        private static ContentTypeIdentificator _discpost = null;
        private static ContentTypeIdentificator _interviste = null;
        private static ContentTypeIdentificator _guide = null;

        public static ContentTypeIdentificator GenContExt
        {
            get
            {
                if (_genContExt == null)
                    _genContExt = new ContentTypeIdentificator(139, Constants.DefaultKeyDomain, "GenContExt");

                return _genContExt;
            }
        }

        public static ContentTypeIdentificator GenCont
        {
            get
            {
                if (_genCont == null)
                    _genCont = new ContentTypeIdentificator(129, Constants.DefaultKeyDomain, "GenContent");

                return _genCont;
            }
        }


        public static ContentTypeIdentificator Gruppi
        {
            get
            {
                if (_gruppi == null)
                    _gruppi = new ContentTypeIdentificator(130, Constants.DefaultKeyDomain, "Gruppi");

                return _gruppi;
            }
        }


        public static ContentTypeIdentificator Discussioni
        {
            get
            {
                if (_discussioni == null)
                    _discussioni = new ContentTypeIdentificator(132, Constants.DefaultKeyDomain, "Discussioni");

                return _discussioni;
            }
        }

        public static ContentTypeIdentificator Interviste
        {
            get
            {
                if (_interviste == null)
                    _interviste = new ContentTypeIdentificator(151, Constants.DefaultKeyDomain, "Interviste");

                return _interviste;
            }
        }

        public static ContentTypeIdentificator News
        {
            get
            {
                if (_news == null)
                    _news = new ContentTypeIdentificator(134, Constants.DefaultKeyDomain, "News");

                return _news;
            }
        }

        public static ContentTypeIdentificator Documenti
        {
            get
            {
                if (_documenti == null)
                    _documenti = new ContentTypeIdentificator(133, Constants.DefaultKeyDomain, "Documenti");

                return _documenti;
            }
        }

        public static ContentTypeIdentificator DocFolder
        {
            get
            {
                if (_folder == null)
                    _folder = new ContentTypeIdentificator(147, Constants.DefaultKeyDomain, "Folder");

                return _folder;
            }
        }

        public static ContentTypeIdentificator DiscPost
        {
            get
            {
                if (_discpost == null)
                    _discpost = new ContentTypeIdentificator(150, Constants.DefaultKeyDomain, "DiscPost");

                return _discpost;
            }
        }

        public static ContentTypeIdentificator Guide
        {
            get
            {
                if (_guide == null)
                    _guide = new ContentTypeIdentificator(152, Constants.DefaultKeyDomain, "Guide");

                return _guide;
            }
        }
    }
}
