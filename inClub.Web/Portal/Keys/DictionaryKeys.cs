﻿
namespace Inclub.Web.Portal.Keys
{
    public class DictionaryKeys
    {
        
        public const string AreaRiservata = "Area Riservata HCPs";
        public const string AreaPubblica = "AreaPub";
        public const string RelatedContents = "RelatedContents";
        public const string LastNews = "LastNews";
        public const string GenericError = "GenericError";
        public const string LeaveToSite = "LeaveToSite";
        #region Form
        public const string PrivacyPolicy = "PrivacyPolicy";
        public const string LabelEmail = "LabelEmail";
        public const string LabelPassword = "LabelPassword";

        public const string MaxChar_8 = "MaxChar_8";
        public const string MaxChar_20 = "MaxChar_20";
        public const string MaxChar_50 = "MaxChar_50";
        public const string MaxChar_100 = "MaxChar_100";
        public const string MaxChar_200 = "MaxChar200";
        public const string MaxChar_4000 = "MaxChar4000";

        public const string Shared_ErrorFieldRequired = "Shared_ErrorFieldRequired";
        public const string Shared_InvalidEmailError = "Shared_InvalidEmailError";
        public const string EmailMaxLengthErrorMessage = "EmailMaxLengthErrorMessage";
        public const string PasswordMaxLengthErrorMessage = "PasswordMaxLengthErrorMessage";
        public const string RegForm_FirstName = "RegForm_FirstName";
        public const string RegForm_LastName = "RegForm_LastName"; 

        public const string RegForm_ConfEmail = "RegForm_ConfEmail";
        public const string Activation_Succeded = "Activation_Succeded";

        public const string FirstNameMaxLengthErrorMessage = "FirstNameMaxLengthErrorMessage";
        public const string LastNameMaxLengthErrorMessage = "LastNameMaxLengthErrorMessage";

        public const string RegForm_Email = "RegForm_Email";
        public const string RegForm_Password = "RegForm_Password";
        public const string RegForm_ConfPassw = "RegForm_ConfPassw";
        public const string RegForm_OldPassw = "RegForm_OldPassw";
        public const string CaptchaError = "CaptchaError";

        public const string RegForm_CellPhone = "RegForm_CellPhone";
        public const string RegForm_Telephone = "RegForm_Telephone";
        public const string RegForm_Privacy = "RegForm_Privacy";

        public const string regFormMsg = "regFormMsg_{0}";
        public const string regFormMsgOk = "regFormMsgOK";

        public const string htmlMailTemplate = "htmlMailTemplate";

        public const string MsgConfirmRegistration = "MsgConfirmRegistration";
        public const string MsgDoubleOptinRegistration = "MsgDoubleOptinRegistration";
        public const string MsgRegistrationDone = "MsgRegistrationDone";


        public const string recoveryPasswordMsg = "recoveryPasswordMsg_{0}";


        public const string sendNewGroupOk = "sendNewGroupOk";
        public const string sendNewGroupKo = "sendNewGroupKo";


        public const string UpdateOk = "updateOk";
        public const string UpdateKO = "updateKO";
        public const string ResetPWD_KO = "ResetPWD_KO";
        public const string ResetPWD_OK = "ResetPWD_OK";
        public const string AccessCode_KO = "AccessCode_KO";

        public const string FormRegAccept = "FormRegAccept";


        #endregion 

        #region News 
        public const string ViewMoreButton = "ViewMoreButton";
            public const string NoItemsfnd = "NoItemsfnd"; //News
       #endregion

            #region Login

            public const string ErrorLoginCode = "ErrorLogin_{0}";
            public const string ResetPasswordLaunch = "ResetPasswordLaunch";
            #endregion

            #region HOME

            public const string HomeTitle = "HomeTitle";
            public const string HomeLaunch = "HomeLaunch";
            #endregion

            #region FOOTER


            public const string FooterLaunch = "FooterLaunch";
            #endregion

            #region GRUPPI

            public const string GruppiTitle = "GruppiTitle";
            public const string GruppiLaunch = "GruppiLaunch";
            public const string GruppiTitleProponi = "GruppiTitleProponi";
            public const string GruppiTextProponi = "GruppiTextProponi";
            public const string MsgNoGruppi = "MsgNoGruppi";
            #endregion
        
        public const string REQUIRED_FIELD = "HC_RequiredField";
        public const string PRIVACY_LABEL = "HC_PrivacyLabel";
        public const string PRIVACY_WARNING = "HC_PrivacyWarning";
        public const string SUBJECT_LABEL = "HC_SubjectLabel";
        public const string HTML_MAIL_TEMPLATE = "htmlMailTemplate";
        public const string VALIDEMAIL_FIELD = "HC_ValidateEmail";


  #region ContactUS          
        public const string ContactPage = "ContactPage";
        public const string SendContactUsSuccessfully = "SendContactUsSuccessfully";
        #endregion
    

    }
}