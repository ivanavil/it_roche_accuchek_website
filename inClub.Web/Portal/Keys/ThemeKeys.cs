﻿using Healthware.HP3.Common.KeyProviders;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public static class ThemeKeys
    {
        #region Members

        private readonly static ThemeKeyProvider _provider;

        private static ThemeIdentificator _root = null;
        private static ThemeIdentificator _home = null;
        private static ThemeIdentificator _RecuperaPassword = null;
        private static ThemeIdentificator _Registrazione = null;
        private static ThemeIdentificator _AccessCode = null;
        private static ThemeIdentificator _footer = new ThemeIdentificator(209);
        private static ThemeIdentificator _gruppi = null;
        private static ThemeIdentificator _hpgruppo = null;
        private static ThemeIdentificator _discussioni = null;
        private static ThemeIdentificator _video = null;
        private static ThemeIdentificator _webinar = null;
        


        
        public static ThemeIdentificator _community = new ThemeIdentificator(190);
        public static ThemeIdentificator _privacyPolicy = new ThemeIdentificator(211);
        public static ThemeIdentificator _DigitalNews = new ThemeIdentificator(197);
        public static ThemeIdentificator _contatti = new ThemeIdentificator(212);
        public static ThemeIdentificator _cookiePolicy = new ThemeIdentificator(213);
        public static ThemeIdentificator _aspettiLegali = new ThemeIdentificator(216);
        public static ThemeIdentificator _condizGen = new ThemeIdentificator(217);
        public static ThemeIdentificator _avvertenzaRischio = new ThemeIdentificator(220);
        public static ThemeIdentificator _infoTrattamento = new ThemeIdentificator(221);
        
        
         public static ThemeIdentificator _PopupDictionary = new ThemeIdentificator(215);
        private static ThemeIdentificator _documenti = null;

        public static ThemeIdentificator ModificaDati = new ThemeIdentificator(202);
        public static ThemeIdentificator ConfermaRegistrazione = new ThemeIdentificator(201);
        #endregion Members

        #region SeoName
        public static class SeoNames
        {
            public static string HomeEN = "home";
            public static string HomeIT = "home it";
        }


        #endregion SeoName

        static ThemeKeys()
        {
            _provider = new ThemeKeyProvider();
        }

        #region Properties




        public static ThemeIdentificator Root
        {
            get
            {
                _root = _root ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Root"));
                return _root;
            }
        }

        public static ThemeIdentificator Community
        {
            get
            {
                _community = _community ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Community"));
                return _community;
            }
        }

        public static ThemeIdentificator Home
        {
            get
            {
                _home = _home ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Home"));
                return _home;
            }
        }
        public static ThemeIdentificator RecuperaPassword
        {
            get
            {
                _RecuperaPassword = _RecuperaPassword ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "RecupePass"));
                return _RecuperaPassword;
            }
        }

        public static ThemeIdentificator AccessCode
        {
            get
            {
                _AccessCode = _AccessCode ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "AccessCode"));
                return _AccessCode;
            }
        }
        public static ThemeIdentificator Registrazione
        {
            get
            {
                _Registrazione = _Registrazione ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Registraz"));
                return _Registrazione;
            }
        }
       

        public static ThemeIdentificator Contatti
        {
            get
            {
                _contatti = _contatti ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Contatti"));
                return _contatti;
            }
        }

        public static ThemeIdentificator DigitalhealthNews
        {
            get
            {
                _DigitalNews = _DigitalNews ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "DigitalNews"));
                return _DigitalNews;
            }
        }
        public static ThemeIdentificator CookiePolicy
                {
                    get
                    {
                        _cookiePolicy = _cookiePolicy ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "CkPolicy"));
                        return _cookiePolicy;
                    }
                }

        public static ThemeIdentificator AspettiLegali
        {
            get
            {
                _aspettiLegali = _aspettiLegali ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "AspttLeg"));
                return _aspettiLegali;
            }
        }

        public static ThemeIdentificator CondizGen
        {
            get
            {
                _condizGen = _condizGen ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "CondGen"));
                return _condizGen;
            }
        }

        public static ThemeIdentificator Footer
        {
            get
            {
                _footer = _footer ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Footer"));
                return _footer;
            }
        }
        public static ThemeIdentificator PrivacyPolicy
        {
            get
            {
                _privacyPolicy = _privacyPolicy ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "PPolicy"));
                return _privacyPolicy;
            }
        }

        public static ThemeIdentificator AvvertenzaRischio
        {
            get
            {
                _avvertenzaRischio = _avvertenzaRischio ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "AvvRisc"));
                return _avvertenzaRischio;
            }
        }


        public static ThemeIdentificator InfoTrattamento
        {
            get
            {
                _infoTrattamento = _infoTrattamento ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "InfoTratt"));
                return _infoTrattamento;
            }
        }
        

        public static ThemeIdentificator PopupDictionary
        {
            get
            {
                _PopupDictionary = _PopupDictionary ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "PopupDict"));
                return _PopupDictionary;
            }
        }

        public static ThemeIdentificator Gruppi
        {
            get
            {
                _gruppi = _gruppi ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "Gruppi"));
                return _gruppi;
            }
        }

        public static ThemeIdentificator HPGruppo
        {
            get
            {
                _hpgruppo = _hpgruppo ?? _provider.PopulateKey(new ThemeIdentificator(Constants.DefaultKeyDomain, "HPGruppo"));
                return _hpgruppo;
            }
        }

        public static ThemeIdentificator Discussioni
        {
            get
            {
                _discussioni = _discussioni ?? _provider.PopulateKey(new ThemeIdentificator(195, Constants.DefaultKeyDomain, "Discussioni"));
                return _discussioni;
            }
        }

        public static ThemeIdentificator Documenti
        {
            get
            {
                _documenti = _documenti ?? _provider.PopulateKey(new ThemeIdentificator(196, Constants.DefaultKeyDomain, "Documenti"));
                return _documenti;
            }
        }


        public static ThemeIdentificator Video
        {
            get
            {
                _video = _video ?? _provider.PopulateKey(new ThemeIdentificator(218, Constants.DefaultKeyDomain, "VideoIntrv"));
                return _video;
            }
        }


        public static ThemeIdentificator Webinar
        {
            get
            {
                _webinar = _webinar ?? _provider.PopulateKey(new ThemeIdentificator(222, Constants.DefaultKeyDomain, "Webinar"));
                return _webinar;
            }
        }

        #endregion Properties
    }
}