﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class MailTemplateKeys
    {
        public static MailTemplateIdentificator ContactUs = new MailTemplateIdentificator("ContactUs");
        public static MailTemplateIdentificator RegistrationDoubleOptin = new MailTemplateIdentificator("RegistrationDoubleOptin");
        public static MailTemplateIdentificator RegistrationConfirm = new MailTemplateIdentificator("RegistrationConfirm");
        public static MailTemplateIdentificator ForgotPasswordMail = new MailTemplateIdentificator("ForgotPasswordMail");
        public static MailTemplateIdentificator PropostaGruppo = new MailTemplateIdentificator("PropostaGruppo");
        public static MailTemplateIdentificator RegistrationDone = new MailTemplateIdentificator("RegistrationDone");
        public static MailTemplateIdentificator NotificaBoardNewDoc = new MailTemplateIdentificator("NotificaBoardNewDoc");
        public static MailTemplateIdentificator NotificaDocApproved = new MailTemplateIdentificator("NotificaDocApproved");
    }
}