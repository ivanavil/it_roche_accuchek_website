﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;
using Healthware.HP3.Core.User.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class RelationTypeKeys
    {

        private static int _grpAdmin = 0;
        private static int _grpMember = 0;
        private static int _sysAdmin = 0;
        private static int _discussioni = 0;
        private static int _interviste = 0;
        private static int _discUser = 0;
        private static int _res = 0;
        private static int _resFolder = 0;
        private static int _mapDiscAtt = 0;


        public static int SysAdmin
        {
            get
            {
                if (_sysAdmin == 0)
                    _sysAdmin = 10;

                return _sysAdmin;
            }
        }

        public static int GrpAdmin
        {
            get
            {
                if (_grpAdmin == 0)
                    _grpAdmin = 11;

                return _grpAdmin;
            }
        }

        public static int GrpMember
        {
            get
            {
                if (_grpMember == 0)
                    _grpMember = 12;

                return _grpMember;
            }
        }

        public static int Discussioni
        {
            get
            {
                if (_discussioni == 0)
                    _discussioni = 16;

                return _discussioni;
            }
        }



        public static int Interviste
        {
            get
            {
                if (_interviste == 0)
                    _interviste = 23;

                return _interviste;
            }
        }

        public static int RelDisUser
        {
            get
            {
                if (_discUser == 0)
                    _discUser = 20;

                return _discUser;
            }
        }

        public static int Resources
        {
            get
            {
                if (_res == 0)
                    _res = 14;

                return _res;
            }
        }

        public static int ResourceFolder
        {
            get
            {
                if (_resFolder == 0)
                    _resFolder = 21;

                return _resFolder;
            }
        }

        public static int MapDiscAtt
        {
            get
            {
                if (_mapDiscAtt == 0)
                    _mapDiscAtt = 22;

                return _mapDiscAtt;
            }
        }
        
    }
}
