﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Healthware.HP3.Core.Site.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class SiteAreaKeys
    {
        private static SiteIdentificator _portalSite = null;
        private static SiteAreaIdentificator _home = null;
        private static SiteAreaIdentificator _BoxHome = null;
        private static SiteAreaIdentificator _gruppi = null;
        private static SiteAreaIdentificator _news = null;
        private static SiteAreaIdentificator _discussioni = null;
        private static SiteAreaIdentificator _documenti = null;
        private static SiteAreaIdentificator _discpost = null;
        private static SiteAreaIdentificator _interviste = null;
        private static SiteAreaIdentificator _guide = null;
      
        public static SiteIdentificator PortalSite
        {
            get
            {
                if (_portalSite == null)
                    _portalSite = new SiteIdentificator(46, Constants.DefaultKeyDomain, "Root");

                return _portalSite;
            }
        }

        public static SiteAreaIdentificator Home
        {
            get
            {
                if (_home == null)
                    _home = new SiteAreaIdentificator(47, Constants.DefaultKeyDomain, "Home");

                return _home;
            }
        }
        public static SiteAreaIdentificator BoxHome
        {
            get
            {
                if (_BoxHome == null)
                    _BoxHome = new SiteAreaIdentificator(71, Constants.DefaultKeyDomain, "BoxHome");

                return _BoxHome;
            }
        }
        public static SiteAreaIdentificator Gruppi
        {
            get
            {
                if (_gruppi == null)
                    _gruppi = new SiteAreaIdentificator(50, Constants.DefaultKeyDomain, "Gruppi");

                return _gruppi;
            }
        }

        public static SiteAreaIdentificator Discussioni
        {
            get
            {
                if (_discussioni == null)
                    _discussioni = new SiteAreaIdentificator(51, Constants.DefaultKeyDomain, "Discussion");

                return _discussioni;
            }
        }

        public static SiteAreaIdentificator Interviste
        {
            get
            {
                if (_interviste == null)
                    _interviste = new SiteAreaIdentificator(52, Constants.DefaultKeyDomain, "Interviste");

                return _interviste;
            }
        }

        public static SiteAreaIdentificator News
        {
            get
            {
                if (_news == null)
                    _news = new SiteAreaIdentificator(54, Constants.DefaultKeyDomain, "News");

                return _news;
            }
        }

        public static SiteAreaIdentificator Documenti
        {
            get
            {
                if (_documenti == null)
                    _documenti = new SiteAreaIdentificator(53, Constants.DefaultKeyDomain, "Documenti");

                return _documenti;
            }
        }


        public static SiteAreaIdentificator DiscPost
        {
            get
            {
                if (_discpost == null)
                    _discpost = new SiteAreaIdentificator(75, Constants.DefaultKeyDomain, "DiscPost");

                return _discpost;
            }
        }

        public static SiteAreaIdentificator GuidePol
        {
            get
            {
                if (_guide == null)
                    _guide = new SiteAreaIdentificator(76, Constants.DefaultKeyDomain, "GuidePol");

                return _guide;
            }
        }

    }
}
