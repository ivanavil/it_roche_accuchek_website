﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Healthware.HP3.Core.Web.ObjectValues;

namespace Inclub.Web.Portal.Keys
{
    public class TraceEventKeys
    {
        public static TraceEventIdentificator Download = new TraceEventIdentificator(5);
        public static TraceEventIdentificator Index = new TraceEventIdentificator(52);
        public static TraceEventIdentificator Detail = new TraceEventIdentificator(54);
        public static TraceEventIdentificator Details = new TraceEventIdentificator(56);
        public static TraceEventIdentificator SubmitRegForm = new TraceEventIdentificator(60);
        public static TraceEventIdentificator UpdateRegForm = new TraceEventIdentificator(61);
        public static TraceEventIdentificator ChangePassword = new TraceEventIdentificator(62);
        public static TraceEventIdentificator AccessByCode = new TraceEventIdentificator(71);
        public static TraceEventIdentificator ResetPassword = new TraceEventIdentificator(64);
        public static TraceEventIdentificator LogIn = new TraceEventIdentificator(56);
        public static TraceEventIdentificator LoginFailed = new TraceEventIdentificator(57);
        public static TraceEventIdentificator LoginOut = new TraceEventIdentificator(58);
        public static TraceEventIdentificator SendContactUs = new TraceEventIdentificator(72);
        public static TraceEventIdentificator AddContributo = new TraceEventIdentificator(73);
        public static TraceEventIdentificator AddCommento = new TraceEventIdentificator(74);
        public static TraceEventIdentificator AddDocumento = new TraceEventIdentificator(75);
        public static TraceEventIdentificator AddDiscussione = new TraceEventIdentificator(76);
        public static TraceEventIdentificator ViewVideo = new TraceEventIdentificator(59);
        public static TraceEventIdentificator ModDocumento = new TraceEventIdentificator(77);
        public static TraceEventIdentificator AprvDocumento = new TraceEventIdentificator(78);
        public static TraceEventIdentificator PubbVideo = new TraceEventIdentificator(79);
        public static TraceEventIdentificator IscrizioneWebinar = new TraceEventIdentificator(80);
    }
}
